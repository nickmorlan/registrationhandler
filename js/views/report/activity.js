$(function () {
  
  var demoButtons = []
  demoButtons['main'] = []
  demoButtons['custom'] = []
  $.each(rh.data.form_definition, function(idx, section) {
    if(typeof section.fields !== 'undefined') {
      if(idx == 'main') {
        if( typeof section.fields.birth_date !== 'undefined') $('#demo-buttons').append('<div class="col-md-4"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="age" rel="age">Age</a></div>')
        if( typeof section.fields.gender !== 'undefined') $('#demo-buttons').append('<div class="col-md-4"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="gender" title="gender">Gender</a></div>')
        if( typeof section.fields.shirt_size !== 'undefined') $('#demo-buttons').append('<div class="col-md-4"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="shirt_size" title="shirt_size">Shirt Size</a></div>')
      }
      if(idx == 'school') {
        if( typeof section.fields.grade_level !== 'undefined') $('#demo-buttons').append('<div class="col-md-4"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="grade_level">Grade Level</a></div>')
        if( typeof section.fields.school !== 'undefined') $('#demo-buttons').append('<div class="col-md-4"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="school">School</a></div>')
      }
      $.each(section.fields, function(idx2, field) {
          if(field.custom == 'true') $('#demo-buttons').append('<div class="col-md-4"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="custom" title="' + field.name + '">' + field.label + '</a></div>')
      })
    }
  })

  // donut graph
  var chartOptions = {    
    series: {
      pie: {
        show: true,  
        innerRadius: .5, 
        stroke: {
          width: 4
        }
      }
    }, 
    legend: {
      position: 'ne'
    }, 
    tooltip: true,
    tooltipOpts: {
      content: '%s: %y'
    },
    grid: {
      hoverable: true
    },
    colors: mvpready_core.layoutColors
  }


  var holder = $('#donut-chart')

  if (holder.length) {
    $.plot(holder, rh.data.donut, chartOptions )
  }


  // line graph
  var chartOptions = {
    xaxis: {
      min: new Date(rh.data.start_date).getTime(), //(new Date(d - 100 - 12096e5)).getTime(),
      max: new Date(rh.data.end_date).getTime(), //d.getTime(),
      mode: "time",
      tickSize: [5, "day"],
      monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
      timeFormat: "%b %e"
    },

    yaxis: {

    },

    series: {
      lines: {
        show: true, 
        fill: true,
        lineWidth: 2,
        fillColor: {
          colors: [{
            opacity: 0.2
          }, {
            opacity: 0.01
          }]
        } 
      },

      points: {
        show: true,
        radius: 2,
        fill: true,
        fillColor: "#ffffff",
        lineWidth: 2
      }
    },

    grid: {
      hoverable: true, 
      clickable: false, 
      borderWidth: 0,
      tickColor: 'rgba(255,255,255,0.22)'
    },

    legend: {
      show: false
    },

    tooltip: true,
    tooltipOpts: {
      content: '%s: %y'
    },

    colors: ['#ffffff']
  };

    
  var holder = $('#reports-line-chart');

  if (holder.length) {
    $.plot(holder, rh.data.line, chartOptions );
  }


  // horizontal graph

  var chartOptions = {
    xaxis: {

    },
    yaxis: {
      ticks: rh.data.horizontal.ticks
    },
    grid: {
      hoverable: true,
      clickable: false,
      borderWidth: 0
    },
    series: {
      stack: true
    },
    bars: {
      horizontal: true,
      show: true,
      barWidth: .25,
      fill: true,
      lineWidth: 0,
      fillColor: { colors: [ { opacity: 1 }, { opacity: 1 } ] }
    },
    tooltip: true,
    tooltipOpts: {
      content: '%s: $%x'
    },
    colors: mvpready_core.layoutColors
  }

  var holder = $('#stacked-horizontal-chart')

  if (holder.length) {
    $.plot(holder, rh.data.horizontal.data, chartOptions )
  }


  if(rh.data.ass != null) {
    var d = new Date();
    d.setHours(0,0,0,0);
    var chartOptions = {
      xaxis: {
        min: new Date(rh.start_date), //(new Date(d - 100 - 12096e5)).getTime(),
        max: new Date(rh.end_date), //d.getTime(),
        mode: "time",
        //timezone: "browser",
        tickSize: [2, "day"],
        monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        tickLength: 2
      },
      yaxis: {
        tickDecimals: 0
      },
      series: {
        lines: {
          show: true, 
          fill: true,
          lineWidth: 1
        },
        points: {
          show: true,
          radius: 3,
          fill: true,
          fillColor: "#ffffff",
          lineWidth: 1
        }
      },
      grid: { 
        hoverable: true, 
        clickable: false, 
        borderWidth: 0 
      },
      legend: {
        show: true,
        margin: [5, -25]
      },
      tooltip: true,
      tooltipOpts: {
        content: '%s: %y registered'
      },
      colors: mvpready_core.layoutColors
    }

    var holder = $('#area-chart')

    if (holder.length) {
      delete rh.data.start_date;
      delete rh.data.end_date;
      $.plot(holder, rh.data, chartOptions)
    }
  }


$('.demo-breakdown').on('click', function() {
  $.post('/report/_ajax_demo_breakdown', {activity_id: rh.data.activity_id, member_id: rh.data.member_id, source: $(this).attr('rel'), field: $(this).attr('title'), heading: $(this).text()}, function(response) {
    if(response.status == 'ok') {
      $('#demo-table').html('');
      $('#demo-table').append('<thead><tr><th>' + response.heading + '</th><th class="text-right">Total</th><th class="text-right">Perentage</th></tr></thead>');
      $('#demo-table').removeClass('hide');
      $.each(response.data, function(key, data) {
        $('table#demo-table').append('<tr><td>' + data.value + '</td><td class="text-right">' + data.total + '</td><td class="text-right">' + data.percentage + '%</td></tr>');
      })
    }

  

  })
})


})