function calculate_total() {
	var costs = rh.data.total + rh.data.fee

	if(rh.data.accept_donation == 'y') {
		var donation = $('#donation_amount').val()
	} else {
		var donation = 0
	}
	if(isNaN(donation)) donation = 0;
	$('#activity_cost').text('$' + (parseFloat(costs) + parseFloat(donation)).toFixed(2) );
	$('input[name=cost]').val((parseFloat(costs) + parseFloat(donation)).toFixed(2));
	
}

$(document).ready(function () {


//$('input[name=division]').on('change', function() {calculate_total()});
  var handler = StripeCheckout.configure({
    key: rh.data.stripe_api_key,
    token: function(token){
			var $input = $('<input type=hidden name=stripeToken />').val(token.id);
			var $email = $('<input type=hidden name=stripeEmail />').val(token.email);
			$('form').append($input).append($email).submit();
		}
  });

    $('#customButton').click(function(){
		if(rh.data.accept_donation == 'y') {
			if($('#activate_donation').prop('checked')) {
				var r = /^[0-9]+(.[0-9][0-9]?)?$/
				if(!r.test($.trim($('#donation_amount').val()))){
					$('#donation_error').show();
					return false;
				} else {
					$('#donation_error').hide();
				}
			}
		}

		var cost = (+($('input[name=cost]').val()) + +($('input[name=fee]').val())).toFixed(2);
		

		handler.open({
			image:       rh.data.checkout_image,
			email:       rh.data.checkout_email,
			address:     true,
			amount:      cost * 100,
			currency:    'usd',
			name:        rh.data.activity_name,
			description: 'Registration Fee ($' + cost + ')',
			panelLabel:  'Pay'
		});

      return false;
    });

    if(rh.data.accept_donation == 'y') {
		$('#activate_donation').on('change', function(){
			if($('#activate_donation').prop('checked') === true) {
				$('#donation_amount').prop('disabled', false).val('0.00').focus().select();
			} else {
				$('#donation_amount').prop('disabled', true).val('0.00');
				$('#donation_error').hide();
			}
				calculate_total();
		});
		$('#donation_amount').on('change', function(){calculate_total();});
	}
});
