function applyUser(data) {
	$('#first_name').val(data.first_name);
	$('#last_name').val(data.last_name);
	$('.register_form').each(function() {
		var el = $(this).attr('name');
		if(el.substr(0, 14) == 'custom_fields[') {
			// access custom field info
			var p0 = 'custom_fields';
			var test = el.substr(14);
			var n = test.indexOf(']');
			var p1 = test.substr(0, n);
			test = test.substr(n+2);
			var p2 = test.substr(0, test.indexOf(']'));
			$(this).val(data[p0][p1][p2]);
		} else {
			$(this).val(data[el]);
		}
	});
	$('.register_form_checkbox').each(function() {
		var el = $(this).attr('name');
		//console.log(this);
		if(el.substr(0, 14) == 'custom_fields[') {
			// access custom field info
			var p0 = 'custom_fields';
			var test = el.substr(14);
			var n = test.indexOf(']');
			var p1 = test.substr(0, n);
			test = test.substr(n+2);
			var p2 = test.substr(0, test.indexOf(']'));
			$(this).val(data[p0][p1][p2]);
		} else {
			$(this).val(data[el]);
		}
	
	});
	$('#userlogindiv').html('<p><strong>Your saved information has been applied to this form.</strong></p>');
	$('#usersavediv').hide();
	$('.rhinsert').hide();

}


$(function() {
	$('.datetimepicker').datetimepicker({
		pickTime: false,
    	format: 'MM/DD/YYYY'
	})


 	// custom validations
	window.ParsleyValidator
    	.addValidator('zipcode', function (value, requirement) {
			if($('#country').val() == 'US') return (/^\d{5}$|^\d{5}(\+|-)?\d{4}$/.test(value));
			if($('#country').val() == 'AU') return (/^\d{4}$/.test(value));
			if($('#country').val() == 'CA') return (/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i.test(value));
			return true;
	       
  		}, 32)
 		.addMessage('en', 'zipcode', 'Please specify a valid zip code.')	

	window.ParsleyValidator
    	.addValidator('phoneUS', function (value, requirement) {
			value = value.replace(/\s+/g, ""); 
			return value.length > 9; // && value.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);			
			//return true;
	       
  		}, 32)
 		.addMessage('en', 'phoneUS', 'Please specify a valid phone number.')	

 	if($('input[name=division]').length == 1) $('#division1').prop('checked', true);
	$('input[name=division]').on('click', function() {$('#amount_paid').val($(this).attr('rel'));});


		//console.log(rh.data);

	if(rh.data.activity.address1){
	  var geocoder;
	  var map;
	  function initialize() {
	   var latlng = new google.maps.LatLng(-34.397, 150.644);
		var mapOptions = {
		  zoom: 12,
		  //center: latlng,
		  mapTypeId: google.maps.MapTypeId.ROADMAP
		}
		
		geocoder = new google.maps.Geocoder();
		//var activity = 
		var address = rh.data.activity.address1 + ' ' + rh.data.activity.city + ' ' + rh.data.activity.state + ' ' + rh.data.activity.zip
		geocoder.geocode( { 'address': address}, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK) {
			map.setCenter(results[0].geometry.location);
			var marker = new google.maps.Marker({
				map: map,
				position: results[0].geometry.location
			});
		  } else {
			//alert("Geocode was not successful for the following reason: " + status);
		  }
		});
	
		map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
	  }
	
		  google.maps.event.addDomListener(window, 'load', initialize);
	}
})


		// // check for user registration info
		// $.post('/register/_ajax_get_user_choices', function (data) {
		// 		//console.log(data);
		// 		data = $.parseJSON(data);
		// 		//console.log(data);
		// 		if(data.status == 'success') {
		// 			//var text = '<p><strong>Please select the saved information you would like to use.</strong></p>';
		// 			var text = '';
		// 			$.each(data.info, function(index, value) {
		// 				var test = [<?PHP echo implode(', ', $_SESSION['user_selected']); ?>];
		// 				if($.inArray(index, test) == -1) {
		// 					text = text + '<p><textarea class="hide" id="ta' + index + '">' + JSON.stringify(value) + '</textarea><input type="button" class="freshbutton-blue userselect" id="us' + index + '" value="' + value.first_name + ' ' + value.last_name + '" /><br />(' + value.address_1 + ')</p>';
		// 				}
		// 			});
		// 			$('#userlogindiv').hide();
		// 			$('#userlogindiv').html(text);
		// 			if(text != '') $('#userlogindiv').show();
		// 		}
		// 	}
		// );
			







	// $('input[name=save]').on('change', function() {
	// 	//console.log($(this).val());
	// 	//console.log($('#saveaccount').is(':visible'));
	// 	if($(this).val() == 'y') {
	// 		if(!$('#saveaccount').is(':visible')) $('#saveaccount').slideToggle('fast');
	// 	} else {
	// 		if($('#saveaccount').is(':visible')) $('#saveaccount').slideToggle('fast');
	// 	}
	// });

	// $('#userlogin').on('click', function() {
	// 	//$('#userlogin').hide();
	// 	$('#userloading').show();
	// 	$('#usererrortext').hide();
	// 	$.post('/register/_ajax_user_login', {email: $('#loginformemail').val(), password: $('#loginformpassword').val()}, function (data) {
	// 			//console.log(data);
	// 			data = $.parseJSON(data);
	// 			//console.log(data);
	// 			if(data.status == 'error') {
	// 				$('#userlogin').show();
	// 				$('#userloading').hide();
	// 				$('#usererrortext').html('<strong>Invalid Login</strong>');
	// 				$('#usererrortext').show();
	// 				return;
	// 			}
	// 			if(data.info.length == 1) {	
	// 				$('input[name=user_select]').val('0')
	// 				applyUser(data.info[0]);
	// 			} else {
	// 				var text = '<p><strong>Please select the saved information you would like to use.</strong></p>';
	// 				$.each(data.info, function(index, value) {
	// 					text = text + '<p><textarea class="hide" id="ta' + index + '">' + JSON.stringify(value) + '</textarea><input type="button" class="freshbutton-blue userselect" id="us' + index + '" value="' + value.first_name + ' ' + value.last_name + '" /><br />(' + value.address_1 + ')</p>';
	// 				});
	// 				$('#userlogindiv').html(text);
	// 			}
	// 			$('input[name=user]').val(data.user_id);
	// 		}
	// 	);
			
	// });

	// $('#userlogindiv').on('click', 'input.userselect', function() {
	// 	var id = $(this).attr('id').substring(2);
	// 	//console.log(id);
	// 	$('input[name=user_select]').val(id);
	// 	var data = $.parseJSON($('#ta' + id).val());
	// 	applyUser(data);
	// });

	// $('#email').on('change', function() {
	// 	$.post('/register/_ajax_check_user_email', {email: $(this).val()}, function(data) {
	// 		$('.rhinsert').remove();
	// 		//console.log(data);
	// 		var data = $.parseJSON(data);
	// 		console.log(data);
	// 		if(data.status == 'success') {
	// 			var html = '<div class="info_box_green rhinsert" style="width:304px;"><p>It appears you have already saved informtion asoociated with this email. Try logging in to the right and quickly complete this signup form.</p></div>';
	// 			$(html).insertAfter($('#email'));
	// 			$('#loginformemail').val($('#email').val());
	// 		}
	// 	});
	// });

