function applyUser(data) {
    $('#first_name').val(data.first_name);
    $('#last_name').val(data.last_name);
    $('.register_form').each(function() {
        var el = $(this).attr('name');
        if(el.substr(0, 14) == 'custom_fields[') {
            // access custom field info
            var p0 = 'custom_fields';
            var test = el.substr(14);
            var n = test.indexOf(']');
            var p1 = test.substr(0, n);
            test = test.substr(n+2);
            var p2 = test.substr(0, test.indexOf(']'));
            $(this).val(data[p0][p1][p2]);
        } else {
            $(this).val(data[el]);
        }
    });
    $('.register_form_checkbox').each(function() {
        var el = $(this).attr('name');
        //console.log(this);
        if(el.substr(0, 14) == 'custom_fields[') {
            // access custom field info
            var p0 = 'custom_fields';
            var test = el.substr(14);
            var n = test.indexOf(']');
            var p1 = test.substr(0, n);
            test = test.substr(n+2);
            var p2 = test.substr(0, test.indexOf(']'));
            $(this).val(data[p0][p1][p2]);
        } else {
            $(this).val(data[el]);
        }
    
    });
    $('#userlogindiv').html('<p><strong>Your saved information has been applied to this form.</strong></p>');
    $('#usersavediv').hide();
    $('.rhinsert').hide();

}


$(function() {
    $('.datetimepicker').datetimepicker({
        pickTime: false,
        format: 'MM/DD/YYYY'
    })


    // custom validations
    window.ParsleyValidator
        .addValidator('zipcode', function (value, requirement) {
            if($('#country').val() == 'US') return (/^\d{5}$|^\d{5}(\+|-)?\d{4}$/.test(value));
            if($('#country').val() == 'AU') return (/^\d{4}$/.test(value));
            if($('#country').val() == 'CA') return (/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i.test(value));
            return true;
           
        }, 32)
        .addMessage('en', 'zipcode', 'Please specify a valid zip code.')    

    window.ParsleyValidator
        .addValidator('phoneUS', function (value, requirement) {
            value = value.replace(/\s+/g, ""); 
            return value.length > 9; // && value.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);           
            //return true;
           
        }, 32)
        .addMessage('en', 'phoneUS', 'Please specify a valid phone number.')    

//alert(rh.data.division);
    if(rh.data.division){
        $("input[name=division][value='" + rh.data.division + "']").prop('checked', true);
    }
    $('input[name=division]').on('click', function() {$('#amount_paid').val($(this).attr('rel'));});


        //console.log(rh.data);

    if(rh.data.activity.address1){
      var geocoder;
      var map;
      function initialize() {
       var latlng = new google.maps.LatLng(-34.397, 150.644);
        var mapOptions = {
          zoom: 12,
          //center: latlng,
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
        
        geocoder = new google.maps.Geocoder();
        //var activity = 
        var address = rh.data.activity.address1 + ' ' + rh.data.activity.city + ' ' + rh.data.activity.state + ' ' + rh.data.activity.zip
        geocoder.geocode( { 'address': address}, function(results, status) {
          if (status == google.maps.GeocoderStatus.OK) {
            map.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: map,
                position: results[0].geometry.location
            });
          } else {
            //alert("Geocode was not successful for the following reason: " + status);
          }
        });
    
        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
      }
    
          google.maps.event.addDomListener(window, 'load', initialize);
    }
})