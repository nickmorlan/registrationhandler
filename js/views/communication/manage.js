$(function() {
	// $('.main').on('change', function() {
	// //alert($(this).attr('id'));
	// 	if($(this).prop('checked')) {
	// 		$('.' + $(this).attr('id')).show();
	// 		$('div.' + $(this).attr('id') + ' input[type=checkbox]').prop('checked', true);
	// 	} else {
	// 		$('.' + $(this).attr('id')).hide();
	// 		$('div.' + $(this).attr('id') + ' input[type=checkbox]').prop('checked', false);
	// 	}
	// });

	// $('.list').on('change', function() {
	// 	//if(!$(this).prop('checked')) {
	// 		var group = $(this).attr('rel');
	// 		var what = true
	// 		$('input[rel=' + group + ']').each(function() {if(what) {if($(this).prop('checked')) what = false}});
			
	// 		if(what) {
	// 			$('.' + group).hide();
	// 			$('input#' + group).prop('checked', false);
	// 			//alert('yes');
	// 		}
	// 	//}
	// });

 	// change the form action when removing
	$('#del').on('click', function() { $('#update').attr('action', '/communication/remove_from_list').submit(); });
	$('#add').on('click', function() { 
			$('form#update').submit()
	});


 	// custom validations
	window.ParsleyValidator
    	.addValidator('emaillist', function (value, requirement) {
	            var theMails = value.match(/[^\n,]+/g),
	                allValid = true;
	            $.each(theMails, function(i, v){
	                if(!/(^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+(\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*|^"([\001-\010\013\014\016-\037!#-\[\]-\177]|\\[\001-\011\013\014\016-\177])*")@((?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+(?:[A-Z]{2,6}\.?|[A-Z0-9-]{2,}\.?)$)|\[(25[0-5]|2[0-4]\d|[0-1]?\d?\d)(\.(25[0-5]|2[0-4]\d|[0-1]?\d?\d)){3}\]$/i.test($.trim(v))){
	                    allValid = false;
	                    return false; //break each loop, we only need one invalid to be an error
	                }
	            });
	            return allValid;
  		}, 32)
 		.addMessage('en', 'emaillist', 'An email address in this list is invalid.')	
     
 



})