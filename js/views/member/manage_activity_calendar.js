$(document).ready(function() {
// 	var tooltips = $("[title]").tooltip();
// 	$(".date-picker").datepicker({
// 		showOn: 'button',
// 		buttonImage: "/images/site/calendarbuttonicon.png",
// 		showAnim: 'blind',
// 		dateFormat: 'mm/dd/yy',
// 		defaultDate: $(this).val()
// 	});
// 
// $('.valform').validate({
// 	rules: {
// 		title: {required: true},
// 		calendar_date: {required: true, date: true}
// 	}
// });  

    $('.datetimepicker').datetimepicker({
        pickTime: false,
        format: 'MM/DD/YYYY'
    })

	$('#show_form').on('click', function() { 
		$('#activity_form').removeClass('hide');
	});

	$('.edit').on('click', function() {
		var form = '#edit_form' + $(this).attr('rel');
		var del  = '#delete' + $(this).attr('rel');
		if($(form).hasClass('hide')) {
			$(this).text('Cancel');
			$(del).addClass('hide');
			$(form).removeClass('hide');
		} else {
			$(this).text('Edit');
			$(del).removeClass('hide');
			$(form).addClass('hide');
		}
	});
});