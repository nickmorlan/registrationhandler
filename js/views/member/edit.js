Stripe.setPublishableKey(rh.data.stripe_publishable_key);

function stripeResponseHandler(status, response) {
    //console.log(status);
    //console.log(response);
    if(status != 200) {
        $('button[type=submit]').prop('disabled', false);
        $('#save_dd').removeClass('hide');
        $('#loading_dd').addClass('hide');
        alert(response.error.message);
    } else  {
        $.post('/member/_ajax_save_stripe_bank_token', {token: response, member_id: rh.data.member_id, routing_number: $('#routing_number').val(), account_number: $('#account_number').val()}, function(data) {
            var res = jQuery.parseJSON(data);
            if(res.status == 'success') document.location.href = '/member/verify_bank_account/' + rh.data.member_id;
            else if(res.status == 'error') {
                $('button[type=submit]').prop('disabled', false);
                $('#save_dd').removeClass('hide');
                $('#loading_dd').addClass('hide');
                alert(res.message);
            } 
        });
    }
}


$('#routing_number').on('change', function() {
    if(!Stripe.bankAccount.validateRoutingNumber($('#routing_number').val(), 'US')) $('#routing_number').addClass("error");
    else $('#routing_number').removeClass("error");
});
$('#account_number').on('change', function() {
    if(!Stripe.bankAccount.validateAccountNumber($('#account_number').val(), 'US')) $('#account_number').addClass("error");
    else $('#account_number').removeClass("error");
});

$('#dd').on('submit', function(el) {
    if(!$('#routing_number').hasClass('parsley-error') || !$('#account_number').hasClass('parsley-error')) {
        $('button[type=submit]').prop('disabled', true);
        $('#save_dd').addClass('hide');
        $('#loading_dd').removeClass('hide');
        el.preventDefault();
        var nm  = $('#name').val();
        var rn  = $('#routing_number').val();
        var an  = $('#account_number').val();
        var tid = $('#tax_id').val();

        if(!Stripe.bankAccount.validateRoutingNumber(rn, 'US')) $('#routing_number').addClass("error");
        if(!Stripe.bankAccount.validateAccountNumber(an, 'US')) $('#account_number').addClass("error")

        Stripe.bankAccount.createToken({
            country: 'US', /*$('.country').val(),*/
            routingNumber: rn,
            accountNumber: an
        }, stripeResponseHandler);
}
});

function show_div() {
    if($('input[name=pay_via]:checked').val() == 'check') {
        $('#via_check').removeClass('hide');
        $('#via_paypal').addClass('hide');
        $('#via_stripe').addClass('hide');
        $('#via_direct_deposit').addClass('hide');
    } else if($('input[name=pay_via]:checked').val() == 'paypal') {
        $('#check').addClass('hide');
        $('#paypal').removeClass('hide');
        $('#via_stripe').addClass('hide');
        $('#via_direct_deposit').addClass('hide');
    } else if($('input[name=pay_via]:checked').val() == 'stripe') {
        $('#via_check').addClass('hide');
        $('#via_paypal').addClass('hide');
        $('#via_stripe').removeClass('hide');
        $('#via_direct_deposit').addClass('hide');
        $('#pay_via').prop('disabled', true);
    }else if($('input[name=pay_via]:checked').val() == 'direct_deposit') {
        $('#via_check').addClass('hide');
        $('#via_paypal').addClass('hide');
        $('#via_stripe').addClass('hide');
        $('#via_direct_deposit').removeClass('hide');   
    }   
}

$(function() {
    if(rh.data.payment.pay_via == 'stripe') {
        $('#pay_stripe').prop('checked', true);
    } else if(rh.data.payment.pay_via == 'direct_deposit') {
        $('#pay_direct_deposit').prop('checked', true);
        $('#via_direct_deposit').removeClass('hide');
    } else if(rh.data.payment.pay_via == 'check') {
        $('#pay_check').prop('checked', true);
        $('#via_check').removeClass('hide');
    }
        

    if(rh.data.payment.pay_via == undefined) {
        $('#routing_number').prop('disabled', true);
        $('#account_number').prop('disabled', true);
    }


    // custom validations
    window.ParsleyValidator
        .addValidator('ein', function (value, requirement) {
           return (/\/(^\d{3}-\d{2}-\d{4}$)|(^[1-9]\d?-\d{7}$)/.test($.trim(value)))
        }, 32)
        .addMessage('en', 'ein', 'Please specify a valid SSN or EIN.')  

    
    $('input[name=pay_via]').on('change', function() {show_div();});
    $('#payit').on('click', function(){show_div();})
});







