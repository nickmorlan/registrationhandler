$('.selectall').on('focus', function() { 
var $this = $(this);

    $this.select();

    window.setTimeout(function() {
        $this.select();
    }, 1);

    // Work around WebKit's little problem
    function mouseUpHandler() {
        // Prevent further mouseup intervention
        $this.off("mouseup", mouseUpHandler);
        return false;
    }

    $this.mouseup(mouseUpHandler);
});

$('#update').on('click', function(){
	var txt = $('.edittext').text();
	var newtxt = txt.replace(/>.+<\/a>/g, '>' + $('#val').val() + '</a>');
	$('.selectall').val(newtxt);
	$('.rhl').text($('#val').val());
});	




$(function () {
  if(rh.data != null) {
    var d = new Date();
    d.setHours(0,0,0,0);
    var chartOptions = {
      xaxis: {
        min: (new Date(d - 100 - 12096e5)).getTime(),
        max: d.getTime(),
        mode: "time",
        //timezone: "browser",
        tickSize: [2, "day"],
        monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        tickLength: 2
      },
      yaxis: {
        tickDecimals: 0
      },
      series: {
        lines: {
          show: true, 
          fill: true,
          lineWidth: 1
        },
        points: {
          show: true,
          radius: 3,
          fill: true,
          fillColor: "#ffffff",
          lineWidth: 1
        }
      },
      grid: { 
        hoverable: true, 
        clickable: false, 
        borderWidth: 0 
      },
      legend: {
        show: true,
        margin: [5, -25]
      },
      tooltip: true,
      tooltipOpts: {
        content: '%s: %y registered'
      },
      colors: mvpready_core.layoutColors
    }

    var holder = $('#area-chart')

    if (holder.length) {
      $.plot(holder, rh.data, chartOptions)
    }
  }
})