Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};

$(function() {
    $('#confirmation_text').wysihtml5()
    $('#description').wysihtml5()
 
    $('.datetimepicker').datetimepicker({
        pickTime: false,
        format: 'MM/DD/YYYY'
    })
   $('.datetimetimepicker').datetimepicker({
        pickDate: false
    })

    console.log(rh.data);

    // build initial form state
    if(rh.data.registration_start_date = '')
        $("#registration_start_date").data("DateTimePicker").setDate(new Date());
    
    if(rh.data.registration_end_date = '')
        $("#registration_end_date").data("DateTimePicker").setDate(new Date());
    
    if(rh.data.consent != ''){
        $('#consent_form').prop('checked', true);
        $('#consent_div').removeClass('hide'); 
    }
    
    if(rh.data.accept_donation == 'y')
        $('#accept_donation').prop('checked', true);
   
    if(rh.data.cost_definition != '') {
        var costs = JSON.parse(rh.data.cost_definition);
        var count = Object.size(costs.registration.division);
        //console.log(costs);
        //console.log(count);
        if(count > 1) {
            $('#multiple_costs').prop('checked', true);
            $('#multiple_costs_div').removeClass('hide');
            $('#single_costs_div').addClass('hide');
            $('#cost').removeAttr('required data-parsley-type');
        }
        $('#division1').val(costs.registration.division[1]);
        $('#cost1').val(costs.registration.cost[1]);
        $('#limit1').val(costs.registration.limit[1]);
        $('#cost').val(costs.registration.cost[1]);
        $('#limit').val(costs.registration.limit[1]);
        for(i = 1; i < count; i++) {
            var newNum = i + 1;
            var newElem = $('#input1').clone().attr('id', 'input' + newNum);
            newElem.children(':nth-child(1)').children(':nth-child(2)').children(':nth-child(1)').attr('id', 'division'+newNum).attr('name', 'division'+newNum).val(costs.registration.division[newNum]);
            newElem.children(':nth-child(2)').children(':nth-child(2)').children(':nth-child(1)').attr('id', 'cost'+newNum).attr('name', 'cost'+newNum).val(costs.registration.cost[newNum]);
            newElem.children(':nth-child(3)').children(':nth-child(2)').children(':nth-child(1)').attr('id', 'limit'+newNum).attr('name', 'limit'+newNum).val(costs.registration.limit[newNum]);
           
            //newElem.children(':nth-child(4)').children(':nth-child(1)').children(':nth-child(1)').attr('id', 'remove'+newNum);
            $('#input' + i).after(newElem);
            $('#division' + newNum).attr('required', 'true');
            $('#cost' + newNum).attr('required', 'true').attr('data-parsley-type', 'number');
        }
    }

    if(rh.data.activity_type == 'League') {
        $('.activity_city').removeClass('hide');
        $('#city').attr('required', 'true');
        $('.activity_state').removeClass('hide');
        $('#state').attr('required', 'true');
        $('.activity_zip').removeClass('hide');    
        $('#zip').attr('required', 'true').attr('data-parsley-zipcode', 'true');
    } else {
        $('.activity_location').removeClass('hide');
        $('#location').attr('required', 'true');
        $('.activity_address').removeClass('hide');
        $('#address1').attr('required', 'true');
        $('.activity_city').removeClass('hide');
        $('#city').attr('required', 'true');
        $('.activity_state').removeClass('hide');
        $('#state').attr('required', 'true');
        $('.activity_zip').removeClass('hide');  
        $('#zip').attr('required', 'true').attr('data-parsley-zipcode', 'true');
        $('.activity_date').removeClass('hide');
        $('.activity_time').removeClass('hide');
    }

    // page actions
    $('.deleteit').on('click', function() {
        if(confirm("Doing this will remove all associated registrations and the action is undoable.\nYou can first export your registration information for backup on the view registrations page.\n\nDo you really want to delete '" + $(this).attr('title') + "' and all its registrations at this time?")) {
            $('<form action="/member/destroy_activity/' + $(this).attr('id') + '" method="post"><input type="hidden" name="go" value="go" /></form>').appendTo('body').submit();
        }
    });

    $('#consent_form').on('click', function() {
        if($('#consent_form').prop('checked') === true) {
            $('#consent_div').removeClass('hide');
            $('#consent').attr('required', 'true');
        } else {
            $('#consent_div').addClass('hide');
            $('#consent').removeAttr('required');
        }
    });

    $('#multiple_costs').on('click', function() {
        if($('#multiple_costs').prop('checked') === true) {
            $('#multiple_costs_div').removeClass('hide');
            $('#division1').attr('required', 'true');
            $('#cost1').attr('required', 'true').attr('data-parsley-type', 'number');
            $('#single_costs_div').addClass('hide');
            $('#cost').removeAttr('required data-parsley-type');
        } else {
            $('#multiple_costs_div').addClass('hide');
            $('#division1').removeAttr('required data-parsley-type');
            $('#cost1').removeAttr('required data-parsley-type');
            $('#single_costs_div').removeClass('hide');
            $('#cost').attr('required', 'true').attr('data-parsley-type', 'number');
        }
    });
    
    $('#multiple_costs_div').on('click', 'a.remove_category', function() {
        var num = $(this).attr('id').substring(6);
        //$('#input' + num).after('<p class="red"><strong><em>You must click the save button to confirm these changes.</em></strong>');
        $('#input'+num).remove();
    });

    // add new input field
    $('#btn_add').click(function() {
        // check that something was entered into the last input before adding another
        var go = true;
        var i = 1;
        while($('#division'+i).length != 0) {
            if($('#division'+i).val() == "" && $('#division'+i).val() == '') go = false;
            i++;
        }

        // duplicate the input if we are good
        if(go) {
            var num     = $('.clone').length;
            var newNum  = new Number(num + 1);

            var newElem = $('#input' + num).clone().attr('id', 'input' + newNum);
            newElem.children(':nth-child(1)').children(':nth-child(2)').children(':nth-child(1)').attr('id', 'division'+newNum).attr('name', 'division'+newNum).val(costs.registration.division[newNum]);
            newElem.children(':nth-child(2)').children(':nth-child(2)').children(':nth-child(1)').attr('id', 'cost'+newNum).attr('name', 'cost'+newNum).val(costs.registration.cost[newNum]);
            newElem.children(':nth-child(3)').children(':nth-child(2)').children(':nth-child(1)').attr('id', 'limit'+newNum).attr('name', 'limit'+newNum).val(costs.registration.limit[newNum]);

            newElem.children(':nth-child(4)').removeClass('hide');
            newElem.children(':nth-child(4)').children(':nth-child(1)').children(':nth-child(1)').attr('id', 'remove'+newNum);
        
            $('#input' + num).after(newElem);
            $('#division' + newNum).attr('required', 'true');
            $('#cost' + newNum).attr('required', 'true').attr('data-parsley-type', 'number');
        }
    });

   // custom validations
    window.ParsleyValidator
        .addValidator('zipcode', function (value, requirement) {
            if($('#country').val() == 'US') return (/^\d{5}$|^\d{5}(\+|-)?\d{4}$/.test(value));
            if($('#country').val() == 'AU') return (/^\d{4}$/.test(value));
            if($('#country').val() == 'CA') return (/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i.test(value));
            return true;
           
        }, 32)
        .addMessage('en', 'zipcode', 'Please specify a valid zip code.')    

    window.ParsleyValidator
        .addValidator('phoneUS', function (value, requirement) {
            value = value.replace(/\s+/g, ""); 
            return value.length > 9; // && value.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);           
            //return true;
           
        }, 32)
        .addMessage('en', 'phoneUS', 'Please specify a valid phone number.')  

    // workaround..
    $('input.bootstrap-wysihtml5-insert-link-target').attr('name', 'bootstrap-wysihtml5-insert-link-target');
}); 


