$(document).on('ready', function () {		
		$(".date-picker").datepicker({
			showAnim: 'blind',
			dateFormat: 'mm/dd/yy',
			defaultDate: $(this).val()
		});
});

$('#save_form_def').on('click', function() {$('input#activity_form_submit').trigger('click');});


$('#activity_form').on('click', '.delete', function() {
	//alert($(this).attr('id'));
	var el = $(this).attr('id').replace('remove_button_', '');
	$('#child1_' + el + ' input').prop('disabled', true);
	$('#child1_' + el).addClass('deactivate');
	$('#child2_' + el).addClass('deactivate');
	$('#activate_' + el).removeClass('hide');
	$(this).addClass('hide');
	$('#remove_' + el).val(el);
});

$('#activity_form').on('click', '.delete_section', function() {
	//alert($(this).attr('id'));
	var el = $(this).attr('id').replace('remove_button_', '');
	$('#' + el + '_section input').prop('disabled', true);
	$('#' + el + '_section .reset_button').prop('disabled', false);
	//$('#' + el + '_section').addClass('deactivate');
	$('#' + el + '_section').addClass('deactivate');
	$('#activate_' + el).removeClass('hide');
	$(this).addClass('hide');
	$('#remove_' + el).val('all');
	$('#remove_' + el).prop('disabled', false);
});

$('#activity_form').on('click', '.activate', function() {
	//alert($(this).attr('id'));
	var el = $(this).attr('id').replace('activate_', '');
	$('#child1_' + el + ' input').prop('disabled', false);
	$('#child1_' + el).removeClass('deactivate');
	$('#child2_' + el).removeClass('deactivate');
	$('#remove_button_' + el).removeClass('hide');
	$(this).addClass('hide');
	$('#remove_' + el).val('');
});

$('#activity_form').on("click", '.reset_button', function() {
	var el = $(this).attr('id').replace('reset_', '');
	$('#' + el + '_section').css('opacity', '.3');
	$('#' + el + '_section .loader').removeClass('hide');
	//$('#remove_' + el).val('');
	var params = 'section=' + el;
	$.post('/member/_ajax_reset_form_section', params, function(data) {
		$('#' + el + '_section').replaceWith(data);
		
		$(".date-picker").datepicker({
			showAnim: 'blind',
			dateFormat: 'mm/dd/yy'
			
		});
	});

});

$('#toggle_new').on('click', function(){
	$('#add_new_field_div').removeClass('hide');
	$('#toggle_new').addClass('hide');
});

$('input[name=new_field_type]').on('change', function() {
	if($(this).attr('id') == 'new_field_type_checkbox' || $(this).attr('id') == 'new_field_type_select') {
		$('#new_values_div').removeClass('hide');
	} else {
		$('#new_values_div').addClass('hide');
	}
	if($(this).attr('id') == 'new_field_type_checkbox' || $(this).attr('id') == 'new_field_type_select' || $(this).attr('id') == 'new_field_type_textarea') {
		$('#new_field_validation_date').addClass('hide');
		$('label[for=new_field_validation_date]').addClass('hide');
		$('#new_field_validation_phoneUS').addClass('hide');
		$('label[for=new_field_validation_phoneUS]').addClass('hide');
		$('#new_field_validation_email').addClass('hide');
		$('label[for=new_field_validation_email]').addClass('hide');
	} else {
		$('#new_field_validation_date').removeClass('hide');
		$('label[for=new_field_validation_date]').removeClass('hide');
		$('#new_field_validation_phoneUS').removeClass('hide');
		$('label[for=new_field_validation_phoneUS]').removeClass('hide');
		$('#new_field_validation_email').removeClass('hide');
		$('label[for=new_field_validation_email]').removeClass('hide');
	}
});