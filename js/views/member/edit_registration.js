$(function() {
	$('#edit_form_div').hide();
	$('.datetimepicker').datetimepicker({
		pickTime: false,
    	format: 'MM/DD/YYYY'
	})

	$('.toggle_edit_button').on('click', function(){
		$('#edit_form_div').toggle()
		$('#details').toggle()
		$('html, body').animate({scrollTop:0}, '500', 'swing')
		
	})
	
	$('.deleteit').on('click', function() {
		if(confirm("Do you really want to delete registration for "+rh.data.first_name+' '+rh.data.last_name+"\n\nThis action cannot be undone an will not issue a refund.")) {
			$('<form action="/member/destroy_registration/'+rh.data.id+'/'+rh.data.activity_id+'" method="post"><input type="hidden" name="go" value="go" /></form>').appendTo('body').submit();
		}		
	})

	$('#setup_refund').on('click', function() {
		$('#setup_p').hide();
		$('#apply_refund_div').show();
	});

	$('#refund').on('click', function() {
		$('#refund').hide();
		$('#loading').show();
		$('<form action="/member/apply_refund/'+rh.data.id+'/'+rh.data.activity_id+'" method="post"><input type="hidden" name="go" value="go" /><input type="hidden" name="refund_amount" value="' + $('#refund_amount').val()  + '" /></form>').appendTo('body').submit();
	});

 	// custom validations
	window.ParsleyValidator
    	.addValidator('zipcode', function (value, requirement) {
			if($('#country').val() == 'US') return (/^\d{5}$|^\d{5}(\+|-)?\d{4}$/.test(value));
			if($('#country').val() == 'AU') return (/^\d{4}$/.test(value));
			if($('#country').val() == 'CA') return (/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i.test(value));
			return true;
	       
  		}, 32)
 		.addMessage('en', 'zipcode', 'Please specify a valid zip code.')	

	window.ParsleyValidator
    	.addValidator('phoneUS', function (value, requirement) {
			value = value.replace(/\s+/g, ""); 
			return value.length > 9; // && value.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);			
			//return true;
	       
  		}, 32)
 		.addMessage('en', 'phoneUS', 'Please specify a valid phone number.')	

})