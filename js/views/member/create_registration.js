$(function() {
	$('.datetimepicker').datetimepicker({
		pickTime: false,
    	format: 'MM/DD/YYYY'
	})


 	// custom validations
	window.ParsleyValidator
    	.addValidator('zipcode', function (value, requirement) {
			if($('#country').val() == 'US') return (/^\d{5}$|^\d{5}(\+|-)?\d{4}$/.test(value));
			if($('#country').val() == 'AU') return (/^\d{4}$/.test(value));
			if($('#country').val() == 'CA') return (/^[ABCEGHJKLMNPRSTVXY]{1}\d{1}[A-Z]{1} *\d{1}[A-Z]{1}\d{1}$/i.test(value));
			return true;
	       
  		}, 32)
 		.addMessage('en', 'zipcode', 'Please specify a valid zip code.')	

	window.ParsleyValidator
    	.addValidator('phoneUS', function (value, requirement) {
			value = value.replace(/\s+/g, ""); 
			return value.length > 9; // && value.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);			
			//return true;
	       
  		}, 32)
 		.addMessage('en', 'phoneUS', 'Please specify a valid phone number.')	


	$('input[name=division]').on('click', function() {$('#amount_paid').val($(this).attr('rel'));});

})