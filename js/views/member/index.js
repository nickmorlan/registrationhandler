$(function () {
  if(rh.data != null) {
    var d = new Date();
    d.setHours(0,0,0,0);
    var chartOptions = {
      xaxis: {
        min: (new Date(d - 100 - 12096e5)).getTime(), // two weeks ago
        max: d.getTime(),
        mode: "time",
        //timezone: "browser",
        tickSize: [2, "day"],
        monthNames: ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        tickLength: 2
      },
      yaxis: {
        tickDecimals: 0
      },
      series: {
        lines: {
          show: true, 
          fill: true,
          lineWidth: 1
        },
        points: {
          show: true,
          radius: 3,
          fill: true,
          fillColor: "#ffffff",
          lineWidth: 1
        }
      },
      grid: { 
        hoverable: true, 
        clickable: false, 
        borderWidth: 0 
      },
      legend: {
        show: true,
        margin: [5, -25]
      },
      tooltip: true,
      tooltipOpts: {
        content: '%s: %y registered'
      },
      colors: mvpready_core.layoutColors
    }

    var holder = $('#area-chart')

    if (holder.length) {
      $.plot(holder, rh.data, chartOptions)
    }
  }
})