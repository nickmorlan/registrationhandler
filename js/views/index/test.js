/* ========================================================
*
* MVP Ready - Lightweight & Responsive Admin Template
*
* ========================================================
*
* File: mvpready-landing.js
* Theme Version: 1.1.0
* Bootstrap Version: 3.1.1
* Author: Jumpstart Themes
* Website: http://mvpready.com
*
* ======================================================== */

var mvpready_landing = function () {
  
  "use strict"

  var initTweetable = function () {
    if (!$.fn.tweetable) { return false }

    $('#tweets').tweetable ({
      limit: 2,
      time: true,
      retweets: false,
      replies: false,
      html5: true
    })
  } 

  var initMastheadCarousel = function () {
    if (!$.fn.carousel) { return false }

    $('.masthead-carousel').carousel ({ interval: 10000 })
  }

  
  return {
    init: function () {
      mvpready_core.navHoverInit ({ delay: { show: 250, hide: 350 } })  

      initMastheadCarousel ()
      initTweetable ()

      // Components
      mvpready_core.initAccordions ()
      mvpready_core.initTooltips ()
      mvpready_core.initBackToTop ()
    }
  }

} ()

$(function () {
  mvpready_landing.init ()
})