function pop(url) {
	newwindow=window.open('http://youshouldsign.com/'+url,'name','height=450,width=550');
	if (window.focus) {newwindow.focus()}
	return false;
}
function setCCValidations() {
	if($('input[name=payment]:checked').val() == 'cc') {
			$('#account_num').rules('add', {
				  required: true,
				  creditcard: true, 
			}); 
			$('#exp_month').rules('add', {min: 0}); 
			$('#exp_year').rules('add', {
				min: function(e) {
					var d = new Date();
					var testYear  = '20' + $("#exp_year").val();
					var testMonth = $("#exp_month").val();
					if(parseInt(testMonth, 10) <= parseInt(d.getMonth(), 10)) {
						if(testYear <= d.getFullYear()) return d.getFullYear() + 1;	    						
					}
					 else if(testYear < d.getFullYear()) {
						return d.getFullYear();
					} 
					
					return 0;
				}
			});
			$('#name').rules('add', {required: true}); 
			$('#cvc').rules('add', {required: true}); 
			$('#billing_address1').rules('add', {required: true}); 
			//$('#bill_to_city').rules('add', {required: true}); 
			//$('#bill_to_state').rules('add', {required: true}); 
			$('#billing_zip').rules('add', {required: true}); 
			/*if($('#bill_to_country').val() == 'US') { 
				$('#bill_to_state').rules('add', {required: true}); 
				$('#bill_to_foreign_state').rules('remove'); 
			} else {
				$('#bill_to_foreign_state').rules('add', {required: true}); 
				$('#bill_to_state').rules('remove'); 
			}*/
		
	} else { 
		$('#account_num').rules('remove'); 
		$('#exp_month').rules('remove'); 
		$('#exp_year').rules('remove'); 
		$('#name').rules('remove'); 
		$('#cvc').rules('remove'); 
		$('#billing_address1').rules('remove'); 
		//$('#bill_to_city').rules('remove'); 
		//$('#bill_to_state').rules('remove'); 
		//$('#bill_to_foreign_state').rules('remove'); 
		$('#billing_zip').rules('remove'); 
	}

}

$('#payment_form').validate({
	/*submitHandler: function(form) {
	 	// check to go to paypal..
	 	if($('input[name=payment]:checked').val() == 'paypal') $('#payment_form').attr('action', '/register/pay_via_paypal');
		form.submit();
	},*/
 	invalidHandler: function(form, validator) {
 		console.log(validator);
 		$('#errmsg').show('fast');
 		$('#edit_shipping_info').show('fast');
    },
	errorContainer: '#error_info',
	
	errorPlacement: function(error, element){
                var errorClone = error.clone();
                        $('#error_info').append(errorClone);
                                error.insertAfter(element)                              
    },
    messages: {    		   			   
			   name:            {required: "A Name on Credit Card is required."},
			   account_num:     {required: "A Credit Card Number is required."},
			   exp_month:       {min: "The Expiration Date has passed or is invalid."},
	           exp_year:        {min: "The Expiration Date has passed or is invalid."},
	           cvc:             {min: "A CVC value is required."},
	           billing_address1:{required: "A Billing Address is required."},
	           billing_zip:     {required: "A Billing Zip Code is required."}
	           
	},
	rules: {
		name:        {required: true},
		account_num: {creditcard: true, number: true},
		billing_zip: {required: true},
		billing_address1: {required: true},
		cvc:         {number: true, minlength: 3, maxlength: 4},
	    exp_month:   {min: -1},
	    exp_year:    {min: function(e) {
	     						var d = new Date();
		   						var testYear  = '20' + $("#exp_year").val();
	    						var testMonth = $("#exp_month").val();
	    						if(parseInt(testMonth, 10) <= parseInt(d.getMonth(), 10)) {
	   								if(testYear <= d.getFullYear()) return d.getFullYear() + 1;	    						
	    						}
	    						 else if(testYear < d.getFullYear()) {
	   								return d.getFullYear();
	   							} 
	   							
	   							return 0;
	    					}
	    			}
		}

});


$('input[name=payment]').change(function() { 
	// show/hide payment options
	$('#payment_credit_card_billing').slideToggle();
	setCCValidations(); 
});

