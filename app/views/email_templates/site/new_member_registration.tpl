<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body{color:#505050;}
h3 {font-size:1.2em;margin-bottom:10px;}
p {	color: #505050;
	font-family:Arial;
	font-size:14px;
	line-height:150%;
	text-align:left; line-height:17px; margin-bottom:10px;}
</style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<div style="
	background-color: #eeeeee;
	width:100%;
	-webkit-text-size-adjust:none !important;
	margin:0;
	padding: 70px 0 70px 0;
">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="600" style="
	-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;
	box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;
	-webkit-border-radius:6px !important;
	border-radius:6px !important;
	background-color: #fdfdfd;
	border: 1px solid #d6d6d6;
	-webkit-border-radius:6px !important;
	border-radius:6px !important;
">
<tr>
<td align="center" valign="top">
                                    <!-- Header -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style="
	background-color: #274f7f;
	color: #ffffff;
	-webkit-border-top-left-radius:6px !important;
	-webkit-border-top-right-radius:6px !important;
	border-top-left-radius:6px !important;
	border-top-right-radius:6px !important;
	border-bottom: 0;
	font-family:Arial;
	font-weight:bold;
	line-height:100%;
	vertical-align:middle;
" bgcolor="#557da1"><tr>
<td>
                                            	<h1 style="
	color: #ffffff;
	margin:0;
	padding: 28px 24px;
	text-shadow: 0 1px 0 #7797b4;
	display:block;
	font-family:Arial;
	font-size:30px;
	font-weight:bold;
	text-align:left;
	line-height: 150%;
">Welcome</h1>

                                            </td>
                                        </tr></table>
<!-- End Header -->
</td>
                            </tr>
				<tr><td align="center" valign="top">
                                    <!-- Body -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="550"><tr>
<td valign="top" style="
	background-color: #fdfdfd;
	-webkit-border-radius:6px !important;
	border-radius:6px !important;
">
<div style="color: #505050;
	font-family:Arial;
	font-size:14px;
	line-height:150%;
	text-align:left; line-height:17px; margin-bottom:10px;">

		<h3>Welcome to RegistrationHandler.com, <?=$member->contact_name?>.</h3>
		<p>We strive to provide the best possible service to help you collect payments and gather registration information.</p>
		<p>As a new signup you will need to do a few things..</p>
		<p><strong>Set up your first activity</strong><br />
			Select the type of activity or event you are collecting registrations for and fill out the important details.</p>
		<p><strong>Set up payment information</strong><br />
			Select how you want to receive the registration money. We can direct deposit, send a check or you can integrate directly with stripe.com to quickly receive payments.  If you collect over $5000 during the year you will have to enter tax information before you recieve payments over that amount.  This is required by the IRS.</p>
		<p><strong>Promote!</strong><br />
			You can choose different buttons and link to promote your activity through Facebook, Twitter and any other outlet you can!</p>
		<p>All of this can be found on the member dashboard (<a href="https://www.registrationhandler.com/member">https://www.registrationhandler.com/member</a>).</p>
		<p>Don't hesitate to contact us if you have any questions.</p>
		<p>All the best,<br />Nick<br />RegistrationHandler Founder</p>
</div>
				</td></tr>
			</table>
				</td></tr>
				<tr><td align="center" valign="top">
                                    <!-- footer -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="550"><tr>
<td valign="top" style="
	background-color: #fdfdfd;
	-webkit-border-radius:6px !important;
	border-radius:6px !important;
">
<div style="color: #99b1c7;
	font-family:Arial;
	font-size:11px;
	line-height:150%;
	text-align:center; line-height:17px; margin-bottom:10px;">
		<p>RegistrationHandler.com &bull; 3465 S Arlington Rd #E179 &bull; Akron, OH 44312</p>
</div>
				</td></tr>
			</table>
				</td></tr>
			</table>
		</td>
	</tr>
</table>
</div>
</body>
</html><html>
	<body>
		<h3>Welcome to RegistrationHandler.com, <?=$member->contact_name?>.</h3>
		<p>We strive to provide the best possible service to help you collect payments and gather registration information.</p>
		<p>As a new signup you will need to do a few things..</p>
		<p><strong>Set up your first activity</strong><br />
			Select the type of activity or event you are collecting registrations for and fill out the important details.</p>
		<p><strong>Set up payment information</strong><br />
			Select how you want to receive the registration money. We can direct deposit, send a check or initiate a paypal transfer.  If you collect over $600 during the year you will have to enter tax information before you recieve payments over that amount.  This is required by the IRS.</p>
		<p><strong>Promote!</strong><br />
			You can choose different buttons and link to promote your activity through Facebook, Twitter and any other outlet you can!</p>
		<p>All of this can be found on the member dashboard(<a href="https://www.registrationhandler.com/member">https://www.registrationhandler.com/member</a>).</p>
		<p>Don't hesitate to contact us if you have any questions.</p>
		<p>All the best,<br />Nick<br />RegistrationHandler Founder</p>
	</body>
</html>