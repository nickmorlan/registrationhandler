<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
body{color:#505050;}
h3 {font-size:1.2em;margin-bottom:10px;}
p {	color: #505050;
	font-family:Arial;
	font-size:14px;
	line-height:150%;
	text-align:left; line-height:17px; margin-bottom:10px;}
</style>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
<div style="
	background-color: #eeeeee;
	width:100%;
	-webkit-text-size-adjust:none !important;
	margin:0;
	padding: 70px 0 70px 0;
">
<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
	<tr>
		<td align="center" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="600" style="
	-webkit-box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;
	box-shadow:0 0 0 3px rgba(0,0,0,0.025) !important;
	-webkit-border-radius:6px !important;
	border-radius:6px !important;
	background-color: #fdfdfd;
	border: 1px solid #d6d6d6;
	-webkit-border-radius:6px !important;
	border-radius:6px !important;
">
<tr>
<td align="center" valign="top">
                                    <!-- Header -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_header" style="
	background-color: #274f7f;
	color: #ffffff;
	-webkit-border-top-left-radius:6px !important;
	-webkit-border-top-right-radius:6px !important;
	border-top-left-radius:6px !important;
	border-top-right-radius:6px !important;
	border-bottom: 0;
	font-family:Arial;
	font-weight:bold;
	line-height:100%;
	vertical-align:middle;
" bgcolor="#557da1"><tr>
<td>
                                            	<h1 style="
	color: #ffffff;
	margin:0;
	padding: 28px 24px;
	text-shadow: 0 1px 0 #7797b4;
	display:block;
	font-family:Arial;
	font-size:30px;
	font-weight:bold;
	text-align:left;
	line-height: 150%;
"><?=$activity->name?></h1>

                                            </td>
                                        </tr></table>
<!-- End Header -->
</td>
                            </tr>
				<tr><td align="center" valign="top">
                                    <!-- Body -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="550"><tr>
<td valign="top" style="
	background-color: #fdfdfd;
	-webkit-border-radius:6px !important;
	border-radius:6px !important;
">
<div style="color: #505050;
	font-family:Arial;
	font-size:14px;
	line-height:150%;
	text-align:left; line-height:17px; margin-bottom:10px;">
				<p><?=$registration->first_name?> <?=$registration->last_name?> has invited you to join them for the <?=$activity->name?>.<p>
				<p>Register online at https://www.registrationhandler.com/register/<?=$activity->url_link()?> and sign up for team: <span style="background:#ffffaa;"><?PHP $fields = json_decode($registration->custom_fields, true);echo $fields['main']['groupteam_name'];?></span>.</p>
				<?PHP IF($message): ?><p><?=$registration->first_name?> says: <?=$message?></p><?PHP ENDIF; ?>
				<?=$activity->display_registration_page_event_info()?>
</div>
				</td></tr>
			</table>
				</td></tr>
				<tr><td align="center" valign="top">
                                    <!-- footer -->
                                	<table border="0" cellpadding="0" cellspacing="0" width="550"><tr>
<td valign="top" style="
	background-color: #fdfdfd;
	-webkit-border-radius:6px !important;
	border-radius:6px !important;
">
<div style="color: #99b1c7;
	font-family:Arial;
	font-size:11px;
	line-height:150%;
	text-align:center; line-height:17px; margin-bottom:10px;">
		<p><?=!empty($member->name) ? $member->name: $member->contact_name;?> &bull; <?=$member->display_mailing_address(true);?></p>
		<p style="font-size:10px;text-align:center;">Managed with RegistrationHandler.com</p>
</div>
				</td></tr>
			</table>
				</td></tr>
			</table>
		</td>
	</tr>
</table>
</div>
</body>
</html>