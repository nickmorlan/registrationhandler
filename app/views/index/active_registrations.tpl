<div class="grid_12">
	<?=Member::display_member_header_for((array)$this->registry['owner'])?>
</div>
<?PHP IF(empty($this->registry['activities']) && empty($this->registry['upcoming_activities']) && empty($this->registry['closed_activities'])): ?>
	<h3 style="margin-top:105px;text-align:center;">There are no activites currently open for registration.</h3>
<?PHP ENDIF;?>
<?PHP FOREACH($this->registry['activities'] as $activity): ?>
	<div class="col-md-offset-2 col-md-8">
		<div class="portlet-boxed"><div class="portlet-body">
			<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $activity->id . '-125.png')) : ?>
				<div class="pull-left" style="margin:0 10px 0;"><a href="/register/<?=$activity->url_link()?>"><img src="/images/activities/<?=$activity->id?>-125.png" /></a></div>
			<?PHP ENDIF;?>
			<div class="pull-left">
				<h5 style="margin-bottom:10px;"><?=$activity->name?></h5>
				<p><?=$activity->description?></p>
				<p>Registration Cost: <?=$activity->display_cost()?></p>
				<?PHP IF($activity->registration_end_date == date('Y-m-d', strtotime('- 3 days'))): ?>
					<p class="red"><strong>Only 3 days left to register!</strong></p>
				<?PHP ELSEIF($activity->registration_end_date == date('Y-m-d', strtotime('- 2 days'))): ?>
					<p class="red"><strong>Only 2 days left to register!</strong></p>
				<?PHP ELSEIF($activity->registration_end_date == date('Y-m-d', strtotime('- 1 days'))): ?>
					<p class="red"><strong>Only 1 day left to register!</strong></p>
				<?PHP ELSEIF($activity->registration_end_date == date('Y-m-d')): ?>
					<p class="red"><strong>Last day to register!</strong></p>
				<?PHP ENDIF; ?>
			</div>
			<div class="clearfix"></div>
			<hr />
			<p align="center"><a href="/register/<?=$activity->url_link()?>" class="btn btn-jumbo btn-success">Register Now!</a></p>
		</div></div>
	</div>
	<div class="clear"></div>
<?PHP ENDFOREACH; ?>
<div style="height:25px;"></div>
<?PHP FOREACH($this->registry['upcoming_activities'] as $activity): ?>
	<div class="grid_8 info_box" style="width:598px !important;margin-left:160px;">
		<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $activity->id . '-125.png')) : ?>
			<div class="left margin_bottom" style="margin:0 10px 0;"><a href="/register/<?=$activity->url_link()?>"><img src="/images/activities/<?=$activity->id?>-125.png" /></a></div>
		<?PHP ENDIF;?>
		<div class="right" style="width:450px;">
			<h5 style="margin-bottom:10px;"><?=$activity->name?></h5>
			<p><strong>Registration opens on <?=date('M d', strtotime($activity->registration_start_date))?>.</strong></p>
			<p><?=$activity->description?></p>
			<p>Registration Cost: <?=$activity->display_cost()?></p>
		</div>
	</div>
	<div class="clear"></div>
<?PHP ENDFOREACH; ?>
<?PHP FOREACH($this->registry['closed_activities'] as $activity): ?>
	<div class="grid_8 info_box" style="width:598px !important;margin-left:160px;background:#fbe3e4;">
		<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $activity->id . '-125.png')) : ?>
			<div class="left margin_bottom" style="margin:0 10px 0;"><a href="/register/<?=$activity->url_link()?>"><img src="/images/activities/<?=$activity->id?>-125.png" /></a></div>
		<?PHP ENDIF;?>
		<div class="right" style="width:450px;">
			<h5 style="margin-bottom:10px;"><?=$activity->name?></h5>
			<p><strong>Registration closed on <?=date('M d', strtotime($activity->registration_end_date))?>.</strong></p>
			<p><?=$activity->description?></p>
			<p>Registration Cost: <?=$activity->display_cost()?></p>
		</div>
	</div>
	<div class="clear"></div>
<?PHP ENDFOREACH; ?>
