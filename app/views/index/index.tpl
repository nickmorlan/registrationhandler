
        <h4 class="content-title">
          <span>Why Use Registration Handler?</span>
        </h4>

        <br>

        <div class="row">

          <div class="mini-feature">
            <i class="fa fa-globe mini-feature-icon text-secondary"></i>

            <div class="mini-feature-text">
              <h5 class="mini-feature-title">Wide Device Support</h5>            
              <p>Our site is built for mobile first, but we play well with all modern devices including desktops, laptops, tablets and phones. </p>
            </div>
          </div> <!-- /.mini-feature -->

          <div class="mini-feature">
            <i class="fa fa-money mini-feature-icon text-secondary"></i>

            <div class="mini-feature-text">
              <h5 class="mini-feature-title">Payment Options</h5>

              <p>Have your signup money quick. We can direct deposit the registration money into your bank account. We also have integration for stripe users.. And of course we call still mail the check.</p>
            </div>
          </div> <!-- /.mini-feature -->

          <div class="mini-feature">
            <i class="fa fa-envelope mini-feature-icon text-secondary"></i>

            <div class="mini-feature-text">
              <h5 class="mini-feature-title">Mailing Lists</h5>

              <p>Send emails to your registered signups to communicate important information.</p>
            </div>
          </div> <!-- /.mini-feature -->

        </div> <!-- /.row -->

        <div class="row">

          <div class="mini-feature">
            <i class="fa fa-credit-card mini-feature-icon text-secondary"></i>

            <div class="mini-feature-text">
              <h5 class="mini-feature-title">Accept Credit Cards</h5>            
              <p>Accept credit card payments without the hassle of a merchant account. We also integrate with Stripe and Authorize.net if your organization already has processing set up.</p>
            </div>
          </div> <!-- /.mini-feature -->

          <div class="mini-feature">
            <i class="fa fa-dashboard mini-feature-icon text-secondary"></i>

            <div class="mini-feature-text">
              <h5 class="mini-feature-title">Administration</h5>

              <p>Easily monitor your activities and registrations. You have full control over your collected information. </p>
            </div>
          </div> <!-- /.mini-feature -->

          <div class="mini-feature">
            <i class="fa fa-beer mini-feature-icon text-secondary"></i>

            <div class="mini-feature-text">
              <h5 class="mini-feature-title">Lowest Fees</h5>

              <p>Your only pay a low credit card processing fee out of each registration collected.  You pay it because we are charged this fee for every transaction we run. Depending on your payment setup this fee is as low as 2.9%</p>
            </div>
          </div> <!-- /.mini-feature -->

        </div> <!-- /.row -->


        <div class="row-divider"></div> <!-- /.row-divider -->


        <div class="row">

          <div class="col-sm-6 col-sm-push-6">
            
            <h4 class="content-title">
              <span>About Registration Handler</span>
            </h4>

            <p>Registration Handler is the quickest, easiest and most cost effective way to collect online registration information for you or your organization.</p>

            <br>

            <div class="row">
              <div class="col-sm-6">
                <ul class="icons-list">
                  <li>
                    <i class="icon-li fa fa-check text-primary"></i>
                    Built For the Mobile Web
                  </li>
                  <li>
                    <i class="icon-li fa fa-check text-primary"></i>
                    Custom Sign Up Forms
                  </li>
                  <li>
                    <i class="icon-li fa fa-check text-primary"></i>
                    Create Unlimited Activities
                  </li>
                </ul>
              </div>

              <div class="col-sm-6">
                <ul class="icons-list">
                  <li>
                    <i class="icon-li fa fa-check text-primary"></i>
                    Lowest Fees Around
                  </li>
                  <li>
                    <i class="icon-li fa fa-check text-primary"></i>
                    Email Your Signups
                  </li>
                  <li>
                    <i class="icon-li fa fa-check text-primary"></i>
                    Fast Payments
                  </li>
                </ul>
              </div>
            </div>

            <br>

            <a href="/member/register" class="btn btn-default">Start Today!</a>

            <br><br><br>

          </div> <!-- /.col -->


          
          <div class="col-sm-6 col-sm-pull-6 text-center">

            <br><br>

            <img src="/images/site/macbook.png" class="img-responsive" alt="Macbook">       
            
          </div> <!-- /.col -->

        </div> <!-- /.row -->


        
        <div class="row-divider"></div> <!-- /.row-divider -->



        <div class="row">
          
          <div class="col-sm-6">

            <h4 class="content-title">
              <span>Upcoming Events</span>
            </h4>

            <div class="recent-news">
            
              <div class="recent-news-item clearfix"><div class="recent-news-item-date">1<span>Nov</span></div>
                <div class="recent-news-item-description">
                  <h5 class="recent-news-item-title"><a href="https://www.registrationhandler.com/register/54/Green-Youth-Wrestling" title="This is a Standard Post">Green Youth Wrestling</a></h5>
                  <span class="recent-news-item-comments">0 Registered</span>
                  <div class="recent-news-item-excerpt">The Green Youth Wrestling Club is dedicated to the youth wrestling participants in the City of Green by providing an effectively managed system that promotes fundamental wrestling skill development, positive life skills and a deep sense of community pride. <a href="/register/54/Green-Youth-Wrestling" class="recent-news-item-more">Read More →</a></div>
                </div>
              </div> <!-- /.recent-news-item -->
              <div class="recent-news-item clearfix"><div class="recent-news-item-date">20<span>Aug</span></div>
                <div class="recent-news-item-description">
                  <h5 class="recent-news-item-title"><a href="https://www.registrationhandler.com/register/46/2014-GBYF-Tackle-Football" title="This is a Standard Post">GBYF Tacke Football</a></h5>
                  <span class="recent-news-item-comments">177 Registered</span>
                  <div class="recent-news-item-excerpt">Tackle football registration is for kids entering into grades 4 - 7 for the upcoming 2014/2015 school year.  GBYF is in the newly formed Northeast Ohio Youth Football Conference... <a href="/register/46/2014-GBYF-Tackle-Football" class="recent-news-item-more">Read More →</a></div>
                </div>
              </div> <!-- /.recent-news-item -->

              <div class="recent-news-item clearfix"><div class="recent-news-item-date">20<span>Aug</span></div>
                <div class="recent-news-item-description">
                  <h5 class="recent-news-item-title"><a href="https://www.registrationhandler.com/register/47/2014-GBYF-Tackle-Cheer" title="This is a Standard Post">GBYF Cheerleading</a></h5>
                  <span class="recent-news-item-comments">98 Registered</span>
                  <div class="recent-news-item-excerpt">Tackle cheer registration is for kids entering into grades 4 - 8 for the upcoming 2014/2015 school year.  GBYF is in the newly formed Northeast Ohio Youth Football Conference... <a href="/register/47/2014-GBYF-Tackle-Cheer" class="recent-news-item-more">Read More →</a></div>
                </div>
              </div> <!-- /.recent-news-item -->

            </div> <!-- /.recent-news -->

            <br class="visible-xs">

          </div> <!-- /.col -->



          <div class="col-sm-6">
              
            <h4 class="content-title">
              <span>Testimonials</span>

              <a class="btn btn-default btn-sm carousel-prev" href="#clients-carousel" data-slide="prev">&lsaquo;</a>
              <a class="btn btn-default btn-sm carousel-next" href="#clients-carousel" data-slide="next">&rsaquo;</a>
            </h4>

            <div id="clients-carousel" class="carousel slide" data-interval="0">
              <div class="carousel-inner">

                <div class="item active">

                  <div class="testimonial">

                    <p class="testimonial-text">
                    Hands down the easiest way to manage my registrations for our youth league. Saves me countless hours and simplifies our registration process tremendously.
                    </p>

                    <div class="testimonials-arrow"></div>

                    <div class="testimonial-author">
                      <div class="testimonial-image"><i class="fa fa-icon fa-user text-secondary fa-4x"></i></div>

                      <div class="testimonial-author-info">
                        <h5><a href="#">Nick M</a></h5> GBYF
                      </div>
                    </div>
                  </div> <!-- /.testimonial -->

                </div> <!-- /.item -->

                <div class="item">

                  <div class="testimonial">

                    <p class="testimonial-text">
                    Hands down the easiest way to manage my registrations for our youth league. Saves me countless hours and simplifies our registration process tremendously.
                    </p>

                    <div class="testimonials-arrow"></div>

                    <div class="testimonial-author">
                      <div class="testimonial-image"><i class="fa fa-icon fa-user text-secondary fa-4x"></i></div>

                      <div class="testimonial-author-info">
                        <h5><a href="#">Nick M</a></h5> GBYF
                      </div>
                    </div>
                  </div> <!-- /.testimonial -->

                </div> <!-- /.item -->

              </div> <!-- /.carousel-inner -->

            </div> <!-- /.carousel -->

          </div> <!-- /.col -->

        </div> <!-- /.row -->


        
        <div class="row-divider"></div> <!-- /.row-divider -->


