  <div class="col-sm-offset-2 col-sm-8">
    <h4 class="content-title">
      <span>Get in Touch</span>
    </h4>

    <p>Have any questions or concerns? Please don't hesitate to contact us and we will respond as soon as possible.</p>

    <p>We are usually in the office during EST working hours.</p>

    <br>
    <br>

    <h4 class="content-title">
      <span>Send an Email</span>
    </h4>

    <form class="form form-horizontal" method="post" action="/contact">
      <div class="form-group">
        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <label>Name: <span class="required">*</span></label>
          <input class="form-control" id="name" name="name" type="text" value="" required="">
        </div>

        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
          <label>Email: <span class="required">*</span></label>
          <input class="form-control" type="email" id="email" name="email" value="" required="">
        </div>
      </div>

      <div class="form-group">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <label>Subject: <span class="required">*</span></label>
          <input class="form-control" id="subject" name="subject" type="text" value="" required="">
        </div>
      </div>

      <div class="form-group">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <label>Message: <span class="required">*</span></label>
          <textarea class="form-control" id="text" name="message" rows="6" cols="40" required=""></textarea>
        </div>
      </div>

      <div class="form-group">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <button class="btn btn-primary" type="submit">Submit Message</button>
        </div>
      </div>

     
    </form>
  </div> <!-- /.col -->
  
</div> <!-- /.row -->