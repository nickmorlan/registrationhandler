<div class="grid_8 prefix_2">
	<h1>Effective Jun 1st, 2013.</h1>
	<p>In this Privacy Policy (“policy”), we, RegistrationHandler.com and r5kMedia LLC, provide information about how we collect, use, and transfer personally identifiable information from you, an individual person or website visitor, through RegistrationHandler.com.</p>
	
	<h2 style="margin-top:20px;">INFORMATION YOU PROVIDE</h2>
	<p>When you use our services, we ask for information such as your name, address, e-mail and phone number. This information is used for registration sign-up information by the event provider that you are signing up with. If you register as a provider with us we will also ask for either a payment address, paypal email or bank account and routing numbers to send money for your completed registrations.</p>
	
	<h2 style="margin-top:20px;">PROTECTION OF INFORMATION</h2>
	<p>r5k Media maintains reasonable security measures to protect your information from loss, destruction, misuse, unauthorized access or disclosure. When you enter personal information, we encrypt the transmission of that information using secure socket layer technology (SSL). We also use firewalls, check for vulnerabilities, review our web applications, and the configurations on the supporting infrastructure to help ensure security. However, no data transmission over the Internet or information storage technology can ever be guaranteed to be 100% secure.</p>
	
	<p>Any sensitive payment information is stored salted and encrypted on our server so that there is no way for anyone to see sensitive information even in the event of a data breach.</p>
	
	<p>R5k Media will share information with governmental authorities only when legally required to do so, and may cooperate with such authorities investigating claims of illegal activity such as (but not limited to) illegal transfer of copyrighted material, or user website postings or email messages that contain threats of violence.</p>
	
	<p>With the exception of legal responsibilities, your information is never shared or sold to any third parties. Ever.</p>
	
	<h2 style="margin-top:20px;">COOKIES</h2>
	<p>When you visit the Site or use the Service, we use session “cookies” – a piece of information stored on your computer – to allow the Site or Service to uniquely identify your browser while you are logged in and to enable RegistrationHandler.com to process your online transactions. We do not link the information we store in cookies to any personally identifiable information you submit while on our site. Session cookies also help us confirm your identity and are required in order to use the Service. Users who disable their web browsers’ ability to accept cookies will be able to browse our Site, but will not be able to access or take advantage of the Service.</p>

	<h2 style="margin-top:20px;">LOG FILES</h2>
	<p>As is true of most Web sites, we and our third party utility-tracking partners gather certain information automatically and store it in log files. This information includes internet protocol (IP) addresses, browser type, internet service provider (ISP), referring/exit pages, operating system, date/time stamp and clickstream data.</p>

	<p>We use this information, which does not identify individual users, to analyze trends, to administer the site, to track users’ movements around the site and to gather demographic information about our user base as a whole.</p>
	
	<h2 style="margin-top:20px;">LINKS TO OTHER SITES</h2>
	<p>This Web site contains links to other sites that are not owned or controlled by r5k Media. Please be aware that we, are not responsible for the privacy practices of such other sites. We encourage you to be aware when you leave our site and to read the privacy statements of each and every Web site that collects personally identifiable information. This privacy statement applies only to information collected by this Web site and our Services.</p>

	<h2 style="margin-top:20px;">CHANGES TO PRIVACY POLICY</h2>
	<p>If we change our Privacy Policy we will post the changes here.</p>

	<p>This policy is reviewed at least annually. Your continued use of the Site or Service constitutes your agreement to be bound by such changes to the privacy policy. Your only remedy, if you do not accept the terms of this privacy policy, is to discontinue use of the Site and Service.</p>
</div>
