<div class="grid_12 margin_bottom">
	<p class="breadcrumb"><a href="/" title="CPAP Wholesale, Homepage">CPAP Wholesale</a> > Sitemap</p>
</div>
<div class="grid_3">
	<div class="section_header" style="margin-top:-17px;"><img src="/images/site/bullet.png" align="left" /><h2>Manufacturers</h2></div>
	<div class="clear"></div>
	<div id="manufacturer_list" style="padding:3px;">
		<?PHP $count = 1; ?>
		<?PHP FOREACH($this->registry['manufacturers'] as $manufacturer) :?>
			<?PHP IF($count == 8): ?><div id="manufacturer_list_hidden" style="display:none;"><?PHP ENDIF; ?>
			<p><a href="/<?=$manufacturer['name']?>" title="<?=$manufacturer['name']?> Flashlights"><?=$manufacturer['name']?></a></p>
			<?PHP $count ++; ?>
		<?PHP ENDFOREACH; ?>
		</div>
	</div>
	<p><span id="expand" class="clickable">+ show more</span></p>
	<div class="section_header"><img src="/images/site/bullet.png" align="left" /><h2>Find a Flashlight</h2></div>
	<?PHP ProductAttribute::build_attribute_list(); ?>
</div>

<div class="grid_9" id="manufacturer_and_product_list">
	<h3>Manufacturers</h3>
	<?PHP IF (empty($this->registry['manufacturers'])) : ?>
		<p>No manufacturers found.</p>
	<?PHP ELSE : ?>
		<?PHP FOREACH($this->registry['manufacturers'] as $manufacturer) : ?>
			<p><a href="/<?=$manufacturer['name']?>" title="<?=$manufacturer['name']?> Flashlights"><?=$manufacturer['name']?></a></p>
		<?PHP ENDFOREACH; ?>
	<?PHP ENDIF ?>
<hr />
	<h3>Products</h3>
	<?PHP IF (empty($this->registry['products'])) : ?>
		<p>No products found.</p>
	<?PHP ELSE : ?>
		<?PHP $old_manufacturer = ''; ?>
		<?PHP FOREACH($this->registry['products'] as $product) : ?>
			<?PHP IF ($product['manufacturer'] != $old_manufacturer) : ?>
				<h4><?=$product['manufacturer']?></h4>
				<?PHP $old_manufacturer = $product['manufacturer']; ?>
			<?PHP ENDIF; ?>
			<p><a href="/<?=str_replace(' ', '+', $product['manufacturer'])?>/<?=str_replace(' ', '+', $product['product_name'])?>"><?=$product['product_name']?></a></p>
		<?PHP ENDFOREACH; ?>
	<?PHP ENDIF ?>
</div>
<script>
$('#expand').click(function() {
	$('#manufacturer_list_hidden').slideToggle('slow', function() {
		// Animation complete.
		var txt = $("#manufacturer_list_hidden").is(':visible') ? '<p>- show less</p>' : '<p>+ show more</p>';
		$("#expand").html(txt);
    });
});
</script>