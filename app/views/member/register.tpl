
      <h3>Get Started with a Free Account.</h3>

      <h5>Sign up in 30 seconds. No credit card required.</h5>
      <?=Helpers::display_errors($this->registry['member'])?>
    <form action="/member/register" id="register_form" name="register_form" class="form account-form parsley-form" method="post" accept-charset="utf-8" data-parsley-validate>
        <div class="form-group">
            <label for="email" class="placeholder-hidden">Email Address</label>
            <input type="email" class="form-control" id="email" name="email" value="<?=$this->registry['member']->email?>" placeholder="Your Email" tabindex="1" required>
        </div>
        <div class="form-group">
            <label for="password" class="placeholder-hidden">Password</label>
            <input type="password" class="form-control" id="password" name="password" value="<?=$this->registry['member']->password?>" placeholder="Password" tabindex="2" required data-parsley-minlength="5">
        </div>
        <div class="form-group">
            <label for="confirm_password" class="placeholder-hidden">Confirm Password</label>
            <input type="password" class="form-control" id="confirm_password" name="confirm_password" value="<?=$this->registry['member']->confirm_password?>" placeholder="Confirm Password" tabindex="3" data-parsley-equalto="#password">
        </div>
        <div class="form-group">
            <label for="name" class="placeholder-hidden">Organization Name (optional)</label>
            <input type="text" class="form-control" id="name" name="name" value="<?=$this->registry['member']->name?>" placeholder="Organization Name (optional)" tabindex="4">
        </div>
        <div class="form-group">
            <label for="contact_name" class="placeholder-hidden">Contact Name</label>
            <input type="text" class="form-control" id="contact_name" name="contact_name" value="<?=$this->registry['member']->contact_name?>" placeholder="Contact Name" tabindex="5" required>
        </div>
        <div class="form-group">
            <label for="contact_phone" class="placeholder-hidden">Contact Phone Number</label>
            <input type="text" class="form-control" id="contact_phone" name="contact_phone" value="<?=$this->registry['member']->contact_phone?>" placeholder="Contact Phone Number" tabindex="6" required>
        </div>
        <div class="form-group">
            <label for="address" class="placeholder-hidden">Address</label>
            <input type="text" class="form-control" id="address_1" name="address_1" value="<?=$this->registry['member']->address_1?>" placeholder="Address" tabindex="7" required>
            <input type="text" style="margin-top:-10px;" class="form-control" id="address_2" name="address_2" value="<?=$this->registry['member']->address_2?>" tabindex="8">
        </div>
        <div class="form-group">
            <label for="city" class="placeholder-hidden">City</label>
            <input type="text" class="form-control" id="city" name="city" value="<?=$this->registry['member']->city?>" placeholder="City" tabindex="9" required>
        </div>
        <div class="form-group">
            <label for="confirm_password" class="placeholder-hidden">State</label>
            <?=HTML::input_state_select($this->registry['member'], 'state', 'class="form-control" tabindex="10" required')?>
        </div>
        <div class="form-group">
            <label for="zip" class="placeholder-hidden">Zip Code</label>
            <input type="text" class="form-control" id="zip" name="zip" value="<?=$this->registry['member']->zip?>" placeholder="Zip Code" tabindex="11" required>
        </div>
       <div class="form-group">
          <label class="checkbox-inline">
          <input type="checkbox" class="" value="" tabindex="12" required data-parsley-mincheck="1" /> I agree to the <a href="/terms" target="_blank">Terms of Service</a> &amp; <a href="/privacy" target="_blank">Privacy Policy</a>
          </label>
        </div> <!-- /.form-group -->

        <div class="form-group">
          <button type="submit" class="btn btn-secondary btn-block btn-lg" tabindex="13">
          Create My Account &nbsp; <i class="fa fa-play-circle"></i>
          </button>
        </div> <!-- /.form-group -->
    </form>
    <div class="account-footer">
      <p>
      Already have an account? &nbsp;
      <a href="/member/login" class="">Login to your Account!</a>
      </p>
    </div> <!-- /.account-footer -->
