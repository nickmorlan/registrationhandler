<div class="layout layout-stack-sm layout-main-left">
   <div class="col-md-8 layout-main">
    <h3 class="content-title"><?=$this->registry['activity']->name?>
                <?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
                    <div class="pull-right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
                <?PHP ENDIF;?>
            </h3>


        <div class="portlet">
            <div class="portlet-body">

              <?PHP IF($this->registry['js_data'] != 'null'): ?>
                <div id="area-chart" class="chart-holder-250"></div>
              <?PHP ELSE: ?>
                <div class="col-md-offest-3"><p><strong>There is no recent registration activity to show.</strong></p></div>
              <?PHP ENDIF; ?>
            </div> <!-- /.portlet-body -->  

          </div> <!-- /.portlet -->
            
            <div class="portlet">
                <div class="row">
                    <?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-125.png') && $this->registry['activity']->accept_donation == 'n') : ?>
                        <div class="col-sm-3 center">
                            <img src="/images/activities/<?=$this->registry['activity']->id?>-125.png" />
                        </div>
                    <?PHP ENDIF; ?>
                    <div class="col-sm-3">
                        <div class="row-stat">
                            <p class="row-stat-label">Registrations</p>
                            <h3 class="row-stat-value"><?=$this->registry['completed_registrations']?></h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="row-stat">
                            <p class="row-stat-label">Collected</p>
                            <h3 class="row-stat-value">$<?=number_format($this->registry['registration_amount'], 2)?></h3>
                        </div>
                    </div>
                    <?PHP IF($this->registry['activity']->accept_donation == 'y') : ?>
                        <div class="col-sm-3">
                            <div class="row-stat">
                                <p class="row-stat-label">Donations</p>
                                <h3 class="row-stat-value">$<?=number_format($this->registry['donation_amount'], 2)?></h3>
                            </div>
                        </div>
                    <?PHP ENDIF; ?>
                    <div class="col-sm-3">
                        <div class="row-stat">
                            <p class="row-stat-label">Open Until</p>
                            <h3 class="row-stat-value"><?=(strtotime('today') > strtotime($this->registry['activity']->registration_end_date)) ? "Closed" : date('M j, Y', strtotime($this->registry['activity']->registration_end_date))?></h3>
                        </div>
                    </div>
                </div>
                
            </div>
        <div class="portlet">
        <hr />
        <h3>Registration Links</h2>
        <p>Copy and paste the text below the buttons to place them on your website. Or you could link directly to <?=$this->registry['activity']->full_url_link()?></p>
        <p style="margin:20px;"><label>Button Text</label><br /><input type="text" name="val" id="val" value="Register Now!" style="width:100px;font-size:11px;padding:5px 10px;"  /> <input type="button" class="btn btn-secondary" value="Update Text" id="update" /></p>
        <style>.rh-registration-link-blue {-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;border-radius:3px;text-align:center;padding:5px 16px;font-size:13px;font-weight:600;cursor:pointer;overflow:visible;text-decoration:none !important;color:#fff !important;border:1px solid #1c74b3;border-top-color:#2c8ed1;border-bottom-color:#0d5b97;background:#2181cf;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#37a3eb", endColorstr="#2181cf");background:-webkit-gradient(linear, left top, left bottom, from(#37a3eb), to(#2181cf));background:-moz-linear-gradient(top, #37a3eb, #2181cf);-moz-box-shadow:0 1px 0 #ddd,inset 0 1px 0 rgba(255,255,255,0.2);-webkit-box-shadow:0 1px 0 #ddd,inset 0 1px 0 rgba(255,255,255,0.2);box-shadow:0 1px 0 #ddd,inset 0 1px 0 rgba(255,255,255,0.2);text-shadow:rgba(0,0,0,0.2) 0 1px 0;-webkit-text-shadow:rgba(0,0,0,0.2) 0 1px 0;-moz-text-shadow:rgba(0,0,0,0.2) 0 1px 0;}
        .rh-registration-link-blue:hover {text-decoration:none !important;border:1px solid #1c74b3;border-top-color:#2c8ed1;border-bottom-color:#0d5b97;background:#2389dc;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#3baaf4", endColorstr="#2389dc");background:-webkit-gradient(linear, left top, left bottom, from(#3baaf4), to(#2389dc));background:-moz-linear-gradient(top, #3baaf4, #2389dc);}
        .rh-registration-link-blue:active {border:1px solid #1c74b3;border-bottom-color:#0d5b97;background:#2181cf;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#37a3eb", endColorstr="#2181cf");background:-webkit-gradient(linear, left top, left bottom, from(#37a3eb), to(#2181cf));background:-moz-linear-gradient(top, #37a3eb, #2181cf);-moz-box-shadow:0 1px 0 #fff,inset 0 1px 3px rgba(101,101,101,0.3);-webkit-box-shadow:0 1px 0 #fff,inset 0 1px 3px rgba(101,101,101,0.3);box-shadow:0 1px 0 #fff,inset 0 1px 3px rgba(101,101,101,0.3)}
        .rh-registration-link-red {-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;border-radius:3px;text-align:center;padding:5px 16px;font-size:13px;font-weight:600;cursor:pointer;overflow:visible;text-decoration:none !important;color:#fff !important;border-top:1px #A12B36 solid;border-right:1px #92222C solid;border-bottom:1px #821721 solid;border-left:1px #8F212B solid;background:#c32f39;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#db4457", endColorstr="#c32f39");background:-webkit-gradient(linear, left top, left bottom, from(#db4457), to(#c32f39));background:-moz-linear-gradient(top, #db4457, #c32f39);text-shadow:#355782 0 1px 2px;-webkit-text-shadow:#355782 0 1px 2px;-moz-text-shadow:#355782 0 1px 2px;-moz-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #e98a96;-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #e98a96;box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #e98a96;}
        .rh-registration-link-red:hover {text-decoration:none !important;border-top:1px #BD0E1B solid;border-right:1px #A60C17 solid;border-bottom:1px #9A0B16 solid;border-left:1px #AC0D19 solid;-moz-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0 0 3px #f08ea5;-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0 0 3px #f08ea5;box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0 0 3px #f08ea5;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#E34D60", endColorstr="#D4333E");background:-webkit-gradient(linear, left top, left bottom, from(#E34D60), to(#D4333E));background:-moz-linear-gradient(top, #E34D60, #D4333E);}
        .rh-registration-link-red:active {border-top:1px #A12B36 solid;border-right:1px #8F212B solid;border-bottom:1px #821721 solid;border-left:1px #982631 solid;background:#cb3a4f;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#a7242d", endColorstr="#cb3a4f");background:-webkit-gradient(linear, left top, left bottom, from(#a7242d), to(#cb3a4f));background:-moz-linear-gradient(top, #a7242d, #cb3a4f);-moz-box-shadow:0 0 0 #000,inset 0 2px 2px #9c212a;-webkit-box-shadow:0 0 0 #000,inset 0 2px 2px #9c212a;box-shadow:0 0 0 #000,inset 0 2px 2px #9c212a;}
        .rh-registration-link-green {-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;border-radius:3px;text-align:center;padding:5px 16px;font-size:13px;font-weight:600;cursor:pointer;overflow:visible;text-decoration:none !important;color:#fff !important;border-top:1px #028F05 solid;border-right:1px #056F08 solid;border-bottom:1px #056F08 solid;border-left:1px #056F08 solid;background:#069709;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#06b00a", endColorstr="#069709");background:-webkit-gradient(linear, left top, left bottom, from(#06b00a), to(#069709));background:-moz-linear-gradient(top, #06b00a, #069709);text-shadow:#056f08 0 1px 2px;-webkit-text-shadow:#056f08 0 1px 2px;-moz-text-shadow:#056f08 0 1px 2px;-moz-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d}
        .rh-registration-link-green:hover {text-decoration:none !important;border-top:1px #06b00a solid;border-right:1px #056F08 solid;border-bottom:1px #056F08 solid;border-left:1px #056F08 solid;background:#069709;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#13BF17", endColorstr="#0CAB0F");background:-webkit-gradient(linear, left top, left bottom, from(#13BF17), to(#0CAB0F));background:-moz-linear-gradient(top, #13BF17, #0CAB0F);text-shadow:#056f08 0 1px 2px;-webkit-text-shadow:#056f08 0 1px 1px;-moz-text-shadow:#056f08 0 1px 1px;-moz-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;}
        .rh-registration-link-green:active {background:#06b00a;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#069709", endColorstr="#06b00a");background:-webkit-gradient(linear, left top, left bottom, from(#069709), to(#06b00a));background:-moz-linear-gradient(top, #069709, #06b00a);-moz-box-shadow:0 0 0 #000,inset 0 2px 2px #057707;-webkit-box-shadow:0 0 0 #000,inset 0 2px 2px #057707;box-shadow:0 0 0 #000,inset 0 2px 2px #057707;}
        </style>
        <div class="col-sm-4">
            <p><a href="<?=$this->registry['activity']->full_url_link()?>" class="rh-registration-link-blue rhl">Register Now!</a></p>
            <p><textarea class="selectall edittext form-control" readonly><style>.rh-registration-link-blue {-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;border-radius:3px;text-align:center;padding:5px 16px;font-size:13px;font-weight:600;cursor:pointer;overflow:visible;text-decoration:none !important;color:#fff !important;border:1px solid #1c74b3;border-top-color:#2c8ed1;border-bottom-color:#0d5b97;background:#2181cf;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#37a3eb", endColorstr="#2181cf");background:-webkit-gradient(linear, left top, left bottom, from(#37a3eb), to(#2181cf));background:-moz-linear-gradient(top, #37a3eb, #2181cf);-moz-box-shadow:0 1px 0 #ddd,inset 0 1px 0 rgba(255,255,255,0.2);-webkit-box-shadow:0 1px 0 #ddd,inset 0 1px 0 rgba(255,255,255,0.2);box-shadow:0 1px 0 #ddd,inset 0 1px 0 rgba(255,255,255,0.2);text-shadow:rgba(0,0,0,0.2) 0 1px 0;-webkit-text-shadow:rgba(0,0,0,0.2) 0 1px 0;-moz-text-shadow:rgba(0,0,0,0.2) 0 1px 0;}.rh-registration-link-blue:hover {text-decoration:none !important;border:1px solid #1c74b3;border-top-color:#2c8ed1;border-bottom-color:#0d5b97;background:#2389dc;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#3baaf4", endColorstr="#2389dc");background:-webkit-gradient(linear, left top, left bottom, from(#3baaf4), to(#2389dc));background:-moz-linear-gradient(top, #3baaf4, #2389dc);}.rh-registration-link-blue:active {border:1px solid #1c74b3;border-bottom-color:#0d5b97;background:#2181cf;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#37a3eb", endColorstr="#2181cf");background:-webkit-gradient(linear, left top, left bottom, from(#37a3eb), to(#2181cf));background:-moz-linear-gradient(top, #37a3eb, #2181cf);-moz-box-shadow:0 1px 0 #fff,inset 0 1px 3px rgba(101,101,101,0.3);-webkit-box-shadow:0 1px 0 #fff,inset 0 1px 3px rgba(101,101,101,0.3);box-shadow:0 1px 0 #fff,inset 0 1px 3px rgba(101,101,101,0.3)}</style>
            <a href="<?=$this->registry['activity']->full_url_link()?>" class="rh-registration-link-blue">Register Now!</a></textarea></p>
        </div>  
        <div class="col-sm-4">
            <p><a href="<?=$this->registry['activity']->full_url_link()?>" class="rh-registration-link-red rhl">Register Now!</a></p>
            <p><textarea class="selectall form-control" readonly><style>.rh-registration-link-red {-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;border-radius:3px;text-align:center;padding:5px 16px;font-size:13px;font-weight:600;cursor:pointer;overflow:visible;text-decoration:none !important;color:#fff !important;border-top:1px #A12B36 solid;border-right:1px #92222C solid;border-bottom:1px #821721 solid;border-left:1px #8F212B solid;background:#c32f39;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#db4457", endColorstr="#c32f39");background:-webkit-gradient(linear, left top, left bottom, from(#db4457), to(#c32f39));background:-moz-linear-gradient(top, #db4457, #c32f39);text-shadow:#355782 0 1px 2px;-webkit-text-shadow:#355782 0 1px 2px;-moz-text-shadow:#355782 0 1px 2px;-moz-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #e98a96;-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #e98a96;box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #e98a96;}.rh-registration-link-red:hover {text-decoration:none !important;border-top:1px #BD0E1B solid;border-right:1px #A60C17 solid;border-bottom:1px #9A0B16 solid;border-left:1px #AC0D19 solid;-moz-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0 0 3px #f08ea5;-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0 0 3px #f08ea5;box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0 0 3px #f08ea5;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#E34D60", endColorstr="#D4333E");background:-webkit-gradient(linear, left top, left bottom, from(#E34D60), to(#D4333E));background:-moz-linear-gradient(top, #E34D60, #D4333E);}.rh-registration-link-red:active {border-top:1px #A12B36 solid;border-right:1px #8F212B solid;border-bottom:1px #821721 solid;border-left:1px #982631 solid;background:#cb3a4f;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#a7242d", endColorstr="#cb3a4f");background:-webkit-gradient(linear, left top, left bottom, from(#a7242d), to(#cb3a4f));background:-moz-linear-gradient(top, #a7242d, #cb3a4f);-moz-box-shadow:0 0 0 #000,inset 0 2px 2px #9c212a;-webkit-box-shadow:0 0 0 #000,inset 0 2px 2px #9c212a;box-shadow:0 0 0 #000,inset 0 2px 2px #9c212a;}</style>
            <a href="<?=$this->registry['activity']->full_url_link()?>" class="rh-registration-link-red">Register Now!</a></textarea></p>
        </div>  
        <div class="col-sm-4">
            <p><a href="<?=$this->registry['activity']->full_url_link()?>" class="rh-registration-link-green rhl">Register Now!</a></p>
            <textarea class="selectall form-control" readonly><style>.rh-registration-link-green {-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;-o-border-radius:3px;border-radius:3px;text-align:center;padding:5px 16px;font-size:13px;font-weight:600;cursor:pointer;overflow:visible;text-decoration:none !important;color:#fff !important;border-top:1px #028F05 solid;border-right:1px #056F08 solid;border-bottom:1px #056F08 solid;border-left:1px #056F08 solid;background:#069709;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#06b00a", endColorstr="#069709");background:-webkit-gradient(linear, left top, left bottom, from(#06b00a), to(#069709));background:-moz-linear-gradient(top, #06b00a, #069709);text-shadow:#056f08 0 1px 2px;-webkit-text-shadow:#056f08 0 1px 2px;-moz-text-shadow:#056f08 0 1px 2px;-moz-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d}.rh-registration-link-green:hover {text-decoration:none !important;border-top:1px #06b00a solid;border-right:1px #056F08 solid;border-bottom:1px #056F08 solid;border-left:1px #056F08 solid;background:#069709;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#13BF17", endColorstr="#0CAB0F");background:-webkit-gradient(linear, left top, left bottom, from(#13BF17), to(#0CAB0F));background:-moz-linear-gradient(top, #13BF17, #0CAB0F);text-shadow:#056f08 0 1px 2px;-webkit-text-shadow:#056f08 0 1px 1px;-moz-text-shadow:#056f08 0 1px 1px;-moz-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;-webkit-box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;box-shadow:0 1px 1px rgba(0,0,0,0.3),inset 0px 1px 0px #19ca1d;}.rh-registration-link-green:active {background:#06b00a;filter:progid:DXImageTransform.Microsoft.gradient(startColorstr="#069709", endColorstr="#06b00a");background:-webkit-gradient(linear, left top, left bottom, from(#069709), to(#06b00a));background:-moz-linear-gradient(top, #069709, #06b00a);-moz-box-shadow:0 0 0 #000,inset 0 2px 2px #057707;-webkit-box-shadow:0 0 0 #000,inset 0 2px 2px #057707;box-shadow:0 0 0 #000,inset 0 2px 2px #057707;}</style>
            <p><a href="<?=$this->registry['activity']->full_url_link()?>" class="rh-registration-link-green">Register Now!</a></textarea></p>
        </div>  
        <div class="clearfix"></div>
    </div>
    </div> <!-- /.main column -->

    <div class="col-md-4 layout-sidebar">

      <div class="portlet">
        <a href="/member/registrations/<?=$this->registry['activity']->id?>" class="btn btn-success btn-block ">View Registrations</a><br />
        <a href="/member/edit_activity/<?=$this->registry['activity']->id?>" class="btn btn-secondary btn-block">Edit Activity Details</a><br />
        <a href="/member/edit_activity_form/<?=$this->registry['activity']->id?>" class="btn btn-secondary btn-block">Edit Signup Form</a><br />
        <a href="/member/manage_activity_calendar/<?=$this->registry['activity']->id?>" class="btn btn-secondary btn-block">Manage Activity Calendar</a>
    </div> <!-- /.portlet -->


      <h4>Recent Activity</h4>

      <div class="well">
        
        <ul class="icons-list text-md">
            <?PHP FOREACH($this->registry['logs'] as $log): ?>
                <?=$log->display_as_list_item()?>
            <?PHP ENDFOREACH;?>
        </ul>
      </div> <!-- /.well -->

    </div> <!-- /.layout-sidebar -->
</div>


