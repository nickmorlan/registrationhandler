<h3>Forget Your Password?</h3>
<h5>Enter your email for intructions on how to reset it.</h5>

<form action="/member/forgot_password" id="password_form" name="password_form" method="post" class="form account-form"
accept-charset="utf-8">
    <div class="form-group">
        <label for="login-username" class="placeholder-hidden">Email</label>
        <input type="email" class="form-control" id="login-username" name="email" placeholder="Email Address" tabindex="1">
      </div> <!-- /.form-group -->
      <div class="form-group">
        <button type="submit" class="btn btn-secondary btn-block btn-lg" tabindex="2">
          Reset Password &nbsp; <i class="fa fa-refresh"></i>
        </button>
      </div> <!-- /.form-group -->

      <div class="form-group">
        <a href="/member/login"><i class="fa fa-angle-double-left"></i> &nbsp;Back to Login</a>
      </div> <!-- /.form-group -->
    </form>
</form>

<hr />
<p>If you have trouble accessing the website please <a href="/contact">contact us</a> and we can assist you with any troubles.</p>
