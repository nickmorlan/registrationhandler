
<div class="col-md-12">	
	<p><?=$this->registry['registration']->first_name?> <?=$this->registry['registration']->last_name?><br />
		<?PHP IF(!empty($this->registry['form_elements']['main']['address_1'])): ?>
			<?=$this->registry['registration']->address_1?><br />
			<?PHP IF(!empty($this->registry['registration']->address_2)): ?><?=$this->registry['registration']->address_2?><br /><?PHP ENDIF; ?>
			<?=$this->registry['registration']->city?>, <?=$this->registry['registration']->state?> <?=$this->registry['registration']->zip?>
		<?PHP ENDIF; ?>
	</p>
	<?PHP IF(!empty($this->registry['form_elements']['main']['email'])): ?><p><strong>Email </strong><?=$this->registry['registration']->email?></p><?PHP ENDIF; ?>
	<?PHP IF(!empty($this->registry['form_elements']['main']['phone'])): ?><p><strong>Phone </strong><?=$this->registry['registration']->phone?></p><?PHP ENDIF; ?>
	<?PHP IF(!empty($this->registry['form_elements']['main']['birth_date'])): ?><p><strong>Birthday </strong><?=date('M d, Y', strtotime($this->registry['registration']->birth_date))?></p><?PHP ENDIF; ?>
	<?PHP IF(!empty($this->registry['form_elements']['school'])): ?>
		<p><strong>Grade </strong><?=$this->registry['registration']->grade_level?></p>
		<p><strong>School </strong><?=$this->registry['registration']->school?></p>
	<?PHP ENDIF; ?>
	<?PHP FOREACH($this->registry['registration']->custom_fields_values['main'] as $field_name => $field_value) : ?>
		<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
		<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><?=nl2br($field_value)?></p>
	<?PHP ENDFOREACH ?>
	<?PHP IF(!empty($this->registry['form_elements']['comments'])): ?><p><strong>Comments From Registration Form</strong><br /><?=nl2br($this->registry['registration']->comments)?></p><?PHP ENDIF; ?>
	<?PHP IF(!empty($this->registry['registration']->custom_fields_values['comments'])) : ?>
		<?PHP FOREACH($this->registry['registration']->custom_fields_values['comments'] as $field_name => $field_value) : ?>
			<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
			<div class="col-md-6"><p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><?=nl2br($field_value)?></p></div>
		<?PHP ENDFOREACH ?>
	<?PHP ENDIF; ?>
</div>

<?PHP IF(!empty($this->registry['form_elements']['parent'])): ?>
	<div class="clear"></div>
	<h4 class="content-title margin_top_20">Parent Information</h4>
	<div class="col-md-6">
		<p><?=$this->registry['registration']->parent1_name?><br />
			<?PHP IF(!empty($this->registry['registration']->parent1_email)): ?>
				<?=$this->registry['registration']->parent1_email?><br />
			<?PHP ENDIF; ?>
			<?PHP IF(!empty($this->registry['registration']->parent1_phone)): ?>
				<?=$this->registry['registration']->parent1_phone?><br />
			<?PHP ENDIF; ?>
			<?PHP IF(!empty($this->registry['registration']->parent1_volunteer) && !empty($this->registry['registration']->parent1_name)): ?>
				<strong>Volunteer </strong><?=$this->registry['registration']->parent1_volunteer?><br />
			<?PHP ENDIF; ?>
	</div>
	<div class="col-md-6">
		<p><?=$this->registry['registration']->parent2_name?><br />
			<?PHP IF(!empty($this->registry['registration']->parent2_email)): ?>
				<?=$this->registry['registration']->parent2_email?><br />
			<?PHP ENDIF; ?>
			<?PHP IF(!empty($this->registry['registration']->parent2_phone)): ?>
				<?=$this->registry['registration']->parent2_phone?><br />
			<?PHP ENDIF; ?>
			<?PHP IF(!empty($this->registry['registration']->parent2_volunteer) && !empty($this->registry['registration']->parent2_name)): ?>
				<strong>Volunteer </strong><?=$this->registry['registration']->parent2_volunteer?><br />
			<?PHP ENDIF; ?>
	</div>
	<?PHP IF(!empty($this->registry['registration']->custom_fields_values['parent'])) : ?>
		<div class="col-md-6">
			<?PHP FOREACH($this->registry['registration']->custom_fields_values['parent'] as $field_name => $field_value) : ?>
				<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
				<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><?=nl2br($field_value)?></p>
			<?PHP ENDFOREACH ?>
		</div>
	<?PHP ENDIF; ?>
	<div class="clear"></div>
<?PHP ENDIF; ?>




<div class="clear"></div>
<?PHP IF(!empty($this->registry['form_elements']['medical']) || !empty($this->registry['form_elements']['emergency'])): ?>
	<div class="clear"></div>
	<h4 class="content-title margin_top_20">Medical Information</h4>
	<div class="col-md-12">
		<?PHP IF(!empty($this->registry['form_elements']['medical']['hospital_name'])): ?><p><strong>Prefered Hospital </strong><?=$this->registry['registration']->hospital_name?></p><?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['form_elements']['emergency']['emergency_contact'])): ?>
			<p><strong>Emergency Contact </strong><?=$this->registry['registration']->emergency_contact?></p>
			<p><strong>Relationship </strong><?=$this->registry['registration']->emergency_contact_relationship_?></p>
			<p><strong>Contact Phone </strong><?=$this->registry['registration']->emergency_contact_phone?></p>
			<?PHP IF(!empty($this->registry['registration']->custom_fields_values['emergency'])) : ?>
				<?PHP FOREACH($this->registry['registration']->custom_fields_values['emergency'] as $field_name => $field_value) : ?>
					<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
					<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><?=nl2br($field_value)?></p>
				<?PHP ENDFOREACH ?>
			<?PHP ENDIF; ?>
		<?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['form_elements']['medical']['physician_name'])): ?>
			<p><strong>Physician </strong><?=$this->registry['registration']->physician_name?></p>
			<p><strong>Physician Phone </strong><?=$this->registry['registration']->physician_phone?></p>
		<?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['form_elements']['medical']['dentist_name'])): ?>
			<p><strong>Dentist </strong><?=$this->registry['registration']->dentist_name?></p>
			<p><strong>Dentist Phone </strong><?=$this->registry['registration']->dentist_phone?></p>
		<?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['registration']->custom_fields_values['medical'])) : ?>
			<?PHP FOREACH($this->registry['registration']->custom_fields_values['medical'] as $field_name => $field_value) : ?>
				<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
				<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><?=nl2br($field_value)?></p>
			<?PHP ENDFOREACH ?>
		<?PHP ENDIF; ?>

		<?PHP IF(!empty($this->registry['form_elements']['medical']['medical_history'])): ?><p><strong>Medical History</strong><br /><?=nl2br($this->registry['registration']->medical_history)?></p><?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['form_elements']['medical']['allergies'])): ?><p><strong>Allergies</strong><br /><?=nl2br($this->registry['registration']->allergies)?></p><?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['registration']->custom_fields_values['medical'])) : ?>
			<?PHP FOREACH($this->registry['registration']->custom_fields_values['medical'] as $field_name => $field_value) : ?>
				<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
				<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><?=nl2br($field_value)?></p>
			<?PHP ENDFOREACH ?>
		<?PHP ENDIF; ?>
	</div>
	<hr />
<?PHP ENDIF; ?>
<div class="clear"></div>
