<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-7 col-md-8 layout-main">
		<h3 class="content-title">Activity Calendar for <?=$this->registry['activity']->name?></h3>

		<form id="activity_form" data-parsley-validate class="hide valform form-horizontal parsley-form" name="activity_form" method="post" action="/member/manage_activity_calendar/<?=$this->registry['activity']->id?>" autocomplete="off">
			<input type="hidden" name="action" value="add">

			<div class="form-group">
				<label class="col-md-3">Date <span style="color:#900;font-weight:normal">*</span></label> 
				<div class="col-md-8">
					<div id="datetimepicker" class="input-group date datetimepicker">
						<input id="calendar_date" class="form-control" type="text" value="<?=$this->registry['calendar_date']->calendar_date?>" name="calendar_date" required />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->
			<div class="form-group">
				<label class="col-md-3">Title <span style="color:#900;font-weight:normal">*</span></label>
				<div class="col-md-8">
					<input type="text" name="title" id="title" value="<?=$this->registry['calendar_date']->title?>" class="form-control demo-element" required />
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->
			<div class="form-group">
				<label class="col-md-3">Notes</label>
				<div class="col-md-8">
				<textarea name="notes" title="Any other important information about this registration" class="form-control" rows="6"><?=$this->registry['calendar_date']->notes?></textarea>
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->
			<p><input type="submit" name="submit" value="Save" title="Save Calendar Event" class="btn btn-success" /></p>
			<hr />
		</form>
		<?PHP IF(empty($this->registry['calendar_dates'])): ?>
			<p>There are no calendar items set up.</p>
		<?PHP ENDIF; ?>
		<?PHP FOREACH($this->registry['calendar_dates'] as $d): ?>
			<div class="well">
				<p><strong><?=date('M d Y', strtotime($d->calendar_date))?>:</strong> <?=$d->title?><br /><?=nl2br($d->notes)?></p>
				<div class="left"><p><span class="btn btn-small btn-secondary edit" rel="<?=$d->id?>">Edit</span></p></div>
				<div class="right"><p id="delete<?=$d->id?>"><a href="/member/destroy_activity_calendar/<?=$d->id?>" class="btn btn-small btn-primary" onclick="return confirm('Are you sure you want to delete \'<?=$d->title?>\'?');">Delete</a></p></div>
				<div class="clear"></div>
				<form id="edit_form<?=$d->id?>" data-parsley-validate class="hide valform form-horizontal parsley-form" name="activity_form" method="post" action="/member/manage_activity_calendar/<?=$this->registry['activity']->id?>" autocomplete="off">
					<input type="hidden" name="action" value="edit">
					<input type="hidden" name="id" value="<?=$d->id?>">

					<div class="form-group">
						<label class="col-md-3">Date <span style="color:#900;font-weight:normal">*</span></label> 
						<div class="col-md-8">
							<div id="datetimepicker" class="input-group date datetimepicker">
								<input id="calendar_date" class="form-control" type="text" value="<?=date('m/d/Y', strtotime($d->calendar_date))?>" name="calendar_date" required />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar"></span>
								</span>
							</div>
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
					<div class="form-group">
						<label class="col-md-3">Title <span style="color:#900;font-weight:normal">*</span></label>
						<div class="col-md-8">
							<input type="text" name="title" id="title" value="<?=$d->title?>" class="form-control demo-element" required />
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
					<div class="form-group">
						<label class="col-md-3">Notes</label>
						<div class="col-md-8">
						<textarea name="notes" title="Any other important information about this registration" class="form-control" rows="6"><?=$d->notes?></textarea>
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
					<p><input type="submit" name="submit" value="Save" title="Save Calendar Event" class="btn btn-success" /></p>
				</form>
			</div>
		<?PHP ENDFOREACH; ?>

	</div> <!-- /.main column -->

	<div class="col-sm-5 col-md-4 layout-sidebar">
		<a href="javascript:;" class="btn btn-block btn-jumbo btn-success" id="show_form"><i class="fa fa-calendar"></i> Add New Date</a>
		<?PHP IF(!empty($this->registry['calendar_dates'])): ?>
			<a href="/communication/compose_calendar_email/<?=$this->registry['activity']->id?>" class="btn btn-block btn-secondary"<img src="/images/site/16pxfileicons/ics.png" style="margin-right:3px;vertical-align:top;"/><i class="fa fa-envelope-o"></i> Email Calendar</a>
		<?PHP ENDIF; ?>
	</div>
</div>