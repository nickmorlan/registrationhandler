<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-12 col-md-12 layout-main"><h3 class="content-title">Create New Activity</h3></div>
	<?=$this->registry['activity']->display_errors()?>
	<form id="activity_form" name="activity_form" method="post" action="/member/create_activity" data-parsley-validate class="form form-horizontal parsley-form" enctype="multipart/form-data" data-parsley-excluded=".bootstrap-wysihtml5-insert-link-target">
		<?=$this->render_partial('member', 'create_new_activity_questions')?>
		<hr />
		<div class="clear"></div>
		<?=$this->render_partial('member', 'activity_form')?>
		<div class="invalid-form-error-message"></div>
		<div class="col-md-4 margin_bottom"><input type="submit" name="submit" value="Save" class="btn btn-success btn-block" /></div>
		<div class="col-md-4"><a href="/member/activity_detail/<?=$this->registry['activity']->id?>" class="btn btn-default btn-block toggle_edit_button">Cancel</a></div>
	</form>

</div>
