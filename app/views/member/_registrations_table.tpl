<div class="margin_top margin_bottom">
	<table width="100%">
		<tr>
			<th>Name</th>
			<th>Email</th>
			<th>Address</th>
			<th>Registered</th>
			<th><?PHP $method = ($this->controller_method == 'bailed_registrations') ? 'bailed_to_excel' : 'to_excel'; echo Registration::build_excel_export_link($method, array($this->registry['activity']->id))?></th>
	<?PHP FOREACH($this->registry['registrations'] as $c) :?>
		<?=HTML::alternate_tr_colors('dde6ec');?>
			<td><p><?=$c->first_name?> <?=$c->last_name?></p></td>
			<td><p><?=(!empty($c->email)) ? $c->email: $c->parent1_email?></p></td>
			<td><p><?=$c->address_1?></p></td>
			<td><p><?=date('M d, Y', strtotime($c->created_at . ' +0500'))?></p></td>
			<td><p><a href="/member/edit_registration/<?=$c->id?>/<?=$this->registry['activity']->id?>" title="Edit information for <?=$c->first_name?> <?=$c->last_name?>">details</a></p></td>
		</tr>
	<?PHP ENDFOREACH; ?>
		<tr class="last"><td colspan="6"></td></tr>
	</table>
</div>
