<?PHP $xml = simplexml_load_string($this->registry['activity']->form_definition);?>
<div class="grid_4">
	<p><span class="editable" id="first_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->first_name?></span> <span class="editable" id="last_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->last_name?></span><br />
		<span class="editable" id="address_1_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->address_1?></span><br />
		<?PHP IF(!empty($this->registry['registration']->address_2)): ?>
			<?=$this->registry['registration']->address_2?><br />
		<?PHP ENDIF; ?>
		<span class="editable" id="city_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->city?></span>, <span class="editable" id="state_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->state?></span> <span class="editable" id="zip_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->zip?></span>
	</p>
	<p><strong>Email </strong><span class="editable" id="email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->email?></span></p>
	<p><strong>Phone </strong><span class="editable" id="phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->phone?></span></p>
	<?PHP IF($xml->partials->main->attributes()->birth_date) : ?><p><strong>Birthday </strong><span class="editable" id="birth_date_<?=$this->registry['registration']->id?>"><?=date('M d, Y', strtotime($this->registry['registration']->birth_date))?></span></p><?PHP ENDIF; ?>
	<?PHP IF($xml->partials->school_info) : ?>
		<p><strong>Grade </strong><span class="editable" id="grade_level_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->grade_level?></span></p>
		<p><strong>School </strong><span class="editable" id="school_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->school?></span></p>
	<?PHP ENDIF; ?>
	<p><strong>Division / Team </strong><span class="editable" id="division_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->division?></span></p>
	<p><strong>Date Paid </strong><span class="editable" id="date_paid_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->date_paid?></span></p>
	<p><strong>Amount Paid </strong><span class="editable" id="amount_paid_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->amount_paid?></span></p>
	<p><strong>Transaction Reference </strong><span class="editable" id="transaction_ref_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->transaction_ref?></span></p>
</div>
<div class="left">
	<?PHP IF($xml->partials->parent_info) : ?>
		<div class="grid_8">
			<p><strong>Parent Informaton</strong></p>
		</div>
		<div class="clear"></div>
		<div class="grid_4">
			<p><span class="editable" id="parent1_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent1_name?></span><br />
				<?PHP IF(!empty($this->registry['registration']->parent1_email)): ?>
					<span class="editable" id="parent1_email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent1_email?></span><br />
				<?PHP ENDIF; ?>
				<?PHP IF(!empty($this->registry['registration']->parent1_phone)): ?>
					<span class="editable" id="parent1_email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent1_phone?></span><br />
				<?PHP ENDIF; ?>
		</div>
		<div class="grid_4">
			<p><span class="editable" id="parent2_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent2_name?></span><br />
				<?PHP IF(!empty($this->registry['registration']->parent2_email)): ?>
					<span class="editable" id="parent2_email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent2_email?></span><br />
				<?PHP ENDIF; ?>
				<?PHP IF(!empty($this->registry['registration']->parent2_phone)): ?>
					<span class="editable" id="parent2_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent2_phone?></span><br />
				<?PHP ENDIF; ?>
		</div>
		<div class="clear"></div>
	<?PHP ENDIF; ?>
	<div class="grid_8">
		<?PHP IF(!empty($this->registry['registration']->comments)): ?><p><strong>Comments From Registration Form</strong><br /><span class="editable_textarea" id="notes_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->comments)?></span></p><?PHP ENDIF; ?>
		<p><strong>Notes</strong><br /><span class="editable_textarea" id="notes_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->notes)?></span></p>
	</div>
</div>
<div class="clear"></div>
<?PHP IF($xml->partials->medical_info) : ?>
	<div class="grid_12">
		<hr style="margin-top:25px;" />
	</div>
	<div class="grid_12">
		<h4>Medical Information</h4>
	</div>
	<div class="grid_4">
		<?PHP IF($xml->partials->medical_info->attributes()->preferred_hospital) : ?><p><strong>Prefered Hospital </strong><span class="editable" id="hospital_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->hospital_name?></span></p><?PHP ENDIF; ?>
		<?PHP IF($xml->partials->medical_info->attributes()->emergency_contact) : ?>
			<p><strong>Emergency Contact </strong><span class="editable" id="emergency_contact_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->emergency_contact?></span></p>
			<p><strong>Contact Phone </strong><span class="editable" id="emergency_contact_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->emergency_contact_phone?></span></p>
		<?PHP ENDIF; ?>
		<?PHP IF($xml->partials->medical_info->attributes()->preferred_physician) : ?>
			<p><strong>Physician </strong><span class="editable" id="physician_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->physician_name?></span></p>
			<p><strong>Physician Phone </strong><span class="editable" id="physician_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->physician_phone?></span></p>
		<?PHP ENDIF; ?>
		<?PHP IF($xml->partials->medical_info->attributes()->preferred_dentist) : ?>
			<p><strong>Dentist </strong><span class="editable" id="dentist_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->dentist_name?></span></p>
			<p><strong>Dentist Phone </strong><span class="editable" id="dentist_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->dentist_phone?></span></p>
		<?PHP ENDIF; ?>
	</div>
	<div class="grid_8">
		<?PHP IF($xml->partials->medical_info->attributes()->medical_history) : ?><p><strong>Medical History</strong><br /><span class="editable_textarea" id="medical_history_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->medical_history)?></span></p><?PHP ENDIF; ?>
		<?PHP IF($xml->partials->medical_info->attributes()->allergies) : ?><p><strong>Allergies</strong><br /><span class="editable_textarea" id="allergies_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->allergies)?></span></p><?PHP ENDIF; ?>
	</div>
	<div class="clear"></div>
	<hr />
<?PHP ENDIF; ?>
<div class="clear"></div>

<script>
$(document).ready(function () {
	   if($('#country').val() == 'US') {
			   $('#state').show();
			   $('#foreign_state').hide();
	   } else {
			   $('#state').hide();
			   $('#foreign_state').show();
	   }
		$("#birth_date").datepicker({
			showAnim: 'blind',
			altFormat: 'mm/dd/yyyy'
		});
		$("#date_paid").datepicker({
			showAnim: 'blind',
			altFormat: 'mm/dd/yyyy'
		});

		$( "input:submit" ).button();
		$( "input:button" ).button();
		
});



// toggle foreign state
$('#country').change(function () {
	   if($('#country').val() == 'US') {
			   $('#state').val('')
			   $('#state').show();
			   $('#foreign_state').hide();
			   $('#foreign_state').attr('class', '');
			   $('#state').attr('class', 'required');
	   } else {
			   $('#state').hide();
			   $('#foreign_state').val('');
			   $('#foreign_state').show();
			   $('#foreign_state').attr('class', 'required');
			   $('#state').attr('class', '');
	   }
});


$('.editable').editable('/member/_ajax_update_registration/<?=$this->registry['activity_id']?>', {
	//indicator : 'Saving...',
	indicator : '<img src="/images/site/loading.gif" />',
	tooltip   : 'Click to edit...then press enter to save changes.',
	style     : 'inherit',
	onblur    : 'submit',
	cssclass  : 'editable_form',
	placeholder : '[Click to Edit]',
	callback  : function() {
					if($(this).attr('id') == 'first_name_<?=$this->registry['registration']->id?>' || $(this).attr('id') == 'last_name_<?=$this->registry['registration']->id?>') {
						var name = $('#first_name_<?=$this->registry['registration']->id?>').text() + ' ' + $('#last_name_<?=$this->registry['registration']->id?>').text();
						$('#name_header').text(name);
					}
			    }
});
$('.editable_textarea').editable('/member/_ajax_update_registration/<?=$this->registry['activity_id']?>', {
	//indicator : 'Saving...',
	indicator : '<img src="/images/site/loading.gif" />',
	tooltip   : 'Click to edit...then press enter to save changes.',
	style     : 'inherit',
	onblur    : 'submit',
	width     : '575px',
	height    : '175px',
	cssclass  : 'editable_form',
	type      : 'textarea',
	placeholder : '[Click to Edit]',
	data      : function(value, settings) {
      /* Convert <br> to newline. */
      var retval = value.replace(/<br[\s\/]?>/gi, '');
      return retval;
    }
});
</script>