<h3>Reset Your Password?</h3>
<h5>Enter the new password for <?=$this->registry['member']->contact_name?>.</h5>
<div class="grid_8">
    <form action="/member/reset_password/<?=$this->registry['key']?>/<?=$this->registry['id']?>" id="password_form" name="password_form" method="post" class="form account-form"
accept-charset="utf-8">
        <div class="form-group">
            <label for="password" class="placeholder-hidden">New Password</label>
            <input type="password" class="form-control" id="password" name="password" value="<?=$this->registry['member']->password?>" placeholder="Password" tabindex="2" required data-parsley-minlength="5">
        </div>
        <div class="form-group">
            <label for="confirm_password" class="placeholder-hidden">Confirm Password</label>
            <input type="password" class="form-control" id="confirm_password" name="confirm_password" value="<?=$this->registry['member']->confirm_password?>" placeholder="Confirm Password" tabindex="3" data-parsley-equalto="#password">
        </div>
        <button type="submit" class="btn btn-primary btn-block btn-lg" tabindex="2">
          Update Password &nbsp; <i class="fa fa-refresh"></i>
        </button>
    </form>
</div>
