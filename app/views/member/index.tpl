<div class="layout layout-stack-sm layout-main-left">
   <div class="col-md-8 layout-main">

        <?PHP IF(empty($this->registry['payment_info'])): ?>
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                <p><i class="fa fa-warning fa-2x"></i> <strong>You have not completed your payment details.</strong>
                <p>This must be completed before you can receive payments from RegistrationHandler.com.</p>
            </div>
        <?PHP ENDIF;?>
        <?PHP IF(false): //(empty($this->registry['member']->display_member_thumb())): ?>
            <div class="alert alert-warning">
                <p><strong>Upload an image header for your organization.</strong>
                <p>It will display on your main registration pages.</p>
                <p><a href="/member/edit" class="btn btn-secondary">Set Image</a></p>
            </div>
        <?PHP ENDIF;?>
        <?PHP IF(empty($this->registry['activities'])): ?>
            <div class="alert alert-warning">
                <p>You have not created any activities.</p>
            </div>
        <?PHP ENDIF;?>
        <div class="portlet">

        <h4 class="portlet-title">
          <u>Recent Registration Activity</u>
        </h4>
          
        <div class="portlet-body">

          <?PHP IF($this->registry['js_data'] != 'null'): ?>
            <div id="area-chart" class="chart-holder-250"></div>
          <?PHP ELSE: ?>
            <div class="col-md-offest-3"><p><strong>There is no recent registration activity to show.</strong></p></div>
          <?PHP ENDIF; ?>
        </div> <!-- /.portlet-body -->  

      </div> <!-- /.portlet -->

        <?PHP FOREACH($this->registry['activities'] as $activity): ?>
            <div class="portlet">
                <h4 class="portlet-title"><u><?=$activity->name?></u></h4>
                <div class="row">

                    <div class="col-sm-3 center">
                        <?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $activity->id . '-125.png')) : ?>
                            <img src="/images/activities/<?=$activity->id?>-125.png" />
                        <?PHP ENDIF; ?>
                    </div>
                    <div class="col-sm-3">
                        <div class="row-stat">
                            <p class="row-stat-label">Registrations</p>
                            <h3 class="row-stat-value"><?=$activity->registered?></h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <div class="row-stat">
                            <p class="row-stat-label">Open Until</p>
                            <h3 class="row-stat-value"><?=(strtotime('today') > strtotime($activity->registration_end_date)) ? "Closed" : date('M j, Y', strtotime($activity->registration_end_date))?></h3>
                        </div>
                    </div>
                    <div class="col-sm-3">
                        <a href="/member/registrations/<?=$activity->id?>" class="btn btn-info btn-sm btn-block pull-right margin_bottom">View Registrations</a>
                        <a href="/member/activity_detail/<?=$activity->id?>" class="btn btn-warning btn-sm btn-block pull-right">Activity Details</a>
                    </div>
                </div>
                
            </div>
        <?PHP ENDFOREACH; ?>

    </div> <!-- /.main column -->

    <div class="col-md-4 layout-sidebar">

      <div class="portlet">
        <?PHP $member_name = (!empty($this->registry['member']->name)) ? $this->registry['member']->name : $this->registry['member']->contact_name; ?>
        <h5><?=$member_name?></h5>
        <a href="/member/create_activity" class="btn btn-success btn-jumbo btn-block ">Create New Activity</a>
        <br>
        <?PHP IF(empty($this->registry['payment_info'])): ?>
            <a href="/member/payments" class="btn btn-secondary btn-block">Set Up Payment Info</a>
        <?PHP ENDIF;?>  </div> <!-- /.portlet -->


      <h4>Recent Site Activity</h4>

      <div class="well">
        
        <ul class="icons-list text-md">
            <?PHP FOREACH($this->registry['logs'] as $log): ?>
                <?=$log->display_as_list_item()?>
            <?PHP ENDFOREACH;?>
        </ul>
      </div> <!-- /.well -->

    </div> <!-- /.layout-sidebar -->
</div>
