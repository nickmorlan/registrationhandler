<div class="grid_12">
	<?PHP IF(empty($this->registry['activities'])) : ?>
		<p><em>No activities selected.</em> <a href="/member/create_activity">Create New Activity.</a></p>
	<?PHP ELSE: ?>
		<div class="tabs margin_bottom">
			<ul>Member Table</ul>
			<table width="100%"> 
			<?PHP FOREACH($this->registry['activities'] as $a) :?>
				<?=HTML::alternate_tr_colors('ccddea');?>
					<td><p><?=$a->name?></p></td>
					<td><p><?=$a->registration_start_date?></p></td>
					<td><p><?=$a->registration_end_date?></p></td>
					<td><p><a href="/member/edit_activity/<?PHP echo $a->id;?>">edit</a></p></td>
					<td><p><a href="/member/destroy_activity/<?PHP echo $a->id;?>" style='color:#aa0000;' onclick="return confirm('Do you really want to delete member <?PHP echo $a->id;?>?\n\nThis action cannot be undone.')">Delete</a></p></td>
				</tr>
			<?PHP ENDFOREACH; ?>
			</table>
		</div>
	<?PHP ENDIF;?>
</div>

<script>
$(document).ready(function () {
		$( "input:submit" ).button();
		$( "input:button" ).button();
				
});
</script>