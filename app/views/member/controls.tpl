<div class="grid_7">
	<form id="member_controls" action="/member/control" method="post">
		<p>
			<label for="form_background_color">Background Color</label><br />
			<?=HTML::text_field($this->registry['member_control'], 'form_background_color', 'text', 'color-picker')?>
		</p>
		<p>
			<label for="form_header_color">Header Color</label><br />
			<?=HTML::text_field($this->registry['member_control'], 'form_header_color', 'text', 'color-picker')?>
		</p>
		<p>
			<input type="submit" name="submit" value="Save" />
		</p>
	</form>
</div>

<script>
$(document).ready(function () {
		$( "input:submit" ).button();
		$( "input:button" ).button();
		
		$(".color-picker").miniColors({
			letterCase: 'uppercase',
			change: function(hex, rgb) {
				logData('change', hex, rgb);
			},
			open: function(hex, rgb) {
				logData('open', hex, rgb);
			},
			close: function(hex, rgb) {
				logData('close', hex, rgb);
			}
		});
		
});



</script>