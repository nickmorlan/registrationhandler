<div class="layout layout-stack-sm layout-main-left">
	<div class="col-sm-12 col-md-12 layout-main">
		<h3 class="content-title">Customize Signup Form
			<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
				<div class="pull-right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
			<?PHP ENDIF;?>
		</h3>


		<div class="col-sm-7 col-md-8">
			<p>Below you can add, remove or change the names of the form inputs for your registration page.</p>
			<p>You can set validations for certian fields so they only accept a certian input, like an email address for example.</p>
			<p>The 'Main Information Section' is required and cannot be removed.</p>
			<form id="new_activity_field" action="/member/edit_activity_form/<?=$this->registry['activity']->id?>" method="post">
				<div class="col-md-6"><a href="javascript:;" class="btn btn-block btn-success" id="toggle_new">Add New Field to Form</a></div>
				<br />
				<div id="add_new_field_div" class="hide portlet portlet-boxed" style="padding:2px;">
					<div>
						<div class="pull-left"><h4>Add a New Field</h4></div> <div class="pull-right"><input style="font-size:.75em;" type="submit" value="Add This Field" class="btn btn-sm btn-success"></div>
						<div class="pull-right right hide loader"><img src="/images/site/loading.gif" style="margin-top:5px;"></div>
						<div class="clear"></div>
					</div>
					<div style="padding:5px;margin-bottom:10px;">
						<div style="float:left;width:300px;">
							<p><label for="new_field_title">Field Title</label><br>
								<input style="width:200px;" type="text" name="new_field_title" class="form-control" value="Field Title">
							</p>
							<p>Will be added to section <br /><?=$this->registry['new_field_section']?></p>
							<p>And display this <em>optional</em> description<br />
							<textarea name="new_field_notes" class="form-control" style="width:225px;height:75px;font-size:10px;padding:3px;"></textarea></p>
						</div>
						<div class="form-group" style="float:left;width:150px;">
							<p><strong>This field is a...</strong><br> 
								<label><input type="radio" name="new_field_type" id="new_field_type_text" value="text" checked> text field</label><br />
								<label><input type="radio" name="new_field_type" id="new_field_type_textarea" value="textarea"> larger text area</label><br />
								<label><input type="radio" name="new_field_type" id="new_field_type_checkbox" value="checkbox"> checkbox[es]</label><br />
								<label><input type="radio" name="new_field_type" id="new_field_type_select" value="select"> dropdown select</label>
							</p>
						</div>
						<div class="form-group" style="float:left;width:150px;">
							<p><strong>And is...</strong><br> 
								<label for="new_field_validation_required"><input type="checkbox" name="new_field_validation[]" id="new_field_validation_required" value="required"> required</label><br />
								<label for="new_field_validation_date"><input type="checkbox" name="new_field_validation[]" id="new_field_validation_date" value="date"> date</label><br />
								<label for="new_field_validation_phoneUS"><input type="checkbox" name="new_field_validation[]" id="new_field_validation_phoneUS" value="phoneUS"> phone number</label><br />
								<label for="new_field_validation_email"><input type="checkbox" name="new_field_validation[]" id="new_field_validation_email" value="email"> email address</label>
							</p>
						</div>
						<div style="float:left;width:250px;margin-left:300px;" id="new_values_div" class="hide">
							<p><strong>With these values...</strong><span style="font-size:11px;">(one per line)</span><br> 
								<textarea name="new_field_values" class="form-control" style="width:225px;height:75px;font-size:10px;padding:3px;"></textarea>
						</div>
						<div class="clear"></div>
					</div>
				</div>
			</form>
			<br />
			<br />
			<form id="activity_form" name="activity_form" method="post" action="/member/edit_activity_form/<?=$this->registry['activity']->id?>" autocomplete="off">
				<?PHP echo $this->registry['activity']->display_definition_for_edit_page(); ?>
			 	<div class="col-md-6 margin-bottom"><input type="submit" id="activity_form_submit" name="submit" value="Save" class="btn btn-block btn-success" /></div>
				<div class="col-md-6"><a href="/member/activity_detail/<?=$this->registry['activity']->id?>" class="btn btn-default btn-block toggle_edit_button">Cancel</a></div>
			</form>		
		<div class="clear"></div>
	</div>
</div>
