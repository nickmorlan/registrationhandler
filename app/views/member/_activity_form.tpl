 <link rel="stylesheet" href="/css/bootstrap-wysihtml5.css">
 <link rel="stylesheet" href="/js/plugins/fileupload/bootstrap-fileupload.css">
<?=$this->registry['activity']->display_errors()?>
<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-7 col-md-8">
		<div class="form-group">
		    <label class="col-md-4">Activity Name <span style="color:#900;font-weight:normal">*</span></label>
		    <div class="col-md-8">
		      <input type="text" name="name" id="name" value="<?=$this->registry['activity']->name?>" class="form-control demo-element ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="This is the name of your activity and will show up on your registration page." title="Activity Name" required />
		    </div> <!-- /.col -->
		</div> <!-- /.form-group -->

		<div class="form-group">
		    <label class="col-md-4">Website</label>
		    <div class="col-md-8">
		      <input type="text" name="website" id="website" value="<?=$this->registry['activity']->website?>" class="form-control demo-element ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Optional, the website for this activity, it will show up on the info and confirmation pages." title="Activity Website" />
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->

		<div class="form-group">
			<div class="col-md-4">	
				<label>Optional Image</label>
			</div>
			<div class="col-md-8">
				<div class="fileupload fileupload-new" data-provides="fileupload">
					<div class="fileupload-new thumbnail" xstyle="width: 200px; height: 200px;">
						<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '.png')): ?>
							<p><img src="/images/activities/<?=$this->registry['activity']->id?>.png" /><br />
						<?PHP ENDIF; ?>
					</div>
					<a href="/member/delete_activity_image/<?=$this->registry['activity']->id?>"  style='color:#aa0000;' onclick="return confirm('Do you really want to delete the image? This action cannot be undone.');">delete image</a></p>

				<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"></div>
					<div>
						<span class="btn btn-default btn-file">
						<span class="fileupload-new">Select image</span>
						<span class="fileupload-exists">Change</span>
						<input type="file" name="image" />
					</span>

					<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
				</div> <!-- /div -->

				</div> <!-- /.fileupload -->
				<p>Upload an image to appear on your registration form.<br />It will be resized to a maximum 200 pixels tall or 200 pixels wide.</p>
			</div> <!-- /.col -->

		</div> <!-- /.form-group -->

		<div class="form-group addrs hide activity_location">
		    <label class="col-md-4">Location</label>
		    <div class="col-md-8">
		      <input type="text" name="location" id="location" value="<?=$this->registry['activity']->location?>" class="form-control demo-element ui-popover createaddr" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Location, building, field of the activity or event is being held at." title="Activity Location" />
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->

		<div class="form-group addrs hide activity_address">
		    <label class="col-md-4">Address</label>
		    <div class="col-md-8">
			    <input type="text" name="address1" id="address1" value="<?=$this->registry['activity']->address1?>" class="form-control demo-element ui-popover createaddr" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Address of the activity or event is being held at." title="Activity Location" /><br />
				<input class="form-control createaddr" type="text" name="address2" id="address2" value="<?=$this->registry['activity']->address2?>" />
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
	

		<div class="form-group addrs hide activity_city">
		    <label class="col-md-4">City</label>
		    <div class="col-md-8">
		      <input type="text" name="city" id="city" value="<?=$this->registry['activity']->city?>" class="form-control createaddr" />
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
		<div class="form-group addrs hide activity_state">
		    <label class="col-md-4">State</label>
		    <div class="col-md-8">
				<?=HTML::input_state_select($this->registry['activity'], 'state', 'text', 'createaddr')?>	    
			</div> <!-- /.col -->
		</div><!-- /.form-group -->
		<div class="form-group addrs hide activity_zip">
		    <label class="col-md-4">Zip</label>
		    <div class="col-md-8">
		      <input type="text" name="zip" id="zip" value="<?=$this->registry['activity']->zip?>" class="form-control createaddr" />
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
		<div class="form-group addrs hide activity_date">
		    <label class="col-md-4">Activity Date</label>
		    <div class="col-md-4">
				<div class="input-group date datetimepicker">
					<input type="text" name="activity_date" id="activity_date" value="<?=$this->registry['activity']->activity_date?>" class="datetimepicker form-control demo-element ui-popover createaddr" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Starting date of activity or event" title="Activity Date" />
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
		<div class="form-group addrs hide activity_date">
		    <label class="col-md-4">Start Time</label>
		    <div class="col-md-4">
				<div class="input-group date datetimetimepicker">
		     		<input type="text" name="activity_time" id="activity_time" value="<?=$this->registry['activity']->activity_time?>" class="form-control demo-element ui-popover createaddr" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Starting time if applicalbe." title="Activity Time" />
					<span class="input-group-addon"><span class="glyphicon glyphicon-time"></span> </div>
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
	
		<div class="form-group">
			<label class="col-md-offset-4" style="background:#ff0;"><input type="checkbox" id="consent_form" name="consent_form" value="y" /> I have a consent form or waiver that participants must agree to.</label>
		</div>
		<div id="consent_div" class="form-group hide">
		    <label class="col-md-4">Consent/Waiver</label>
			<div class="col-md-8">
				<textarea name="consent" id="consent" class="form-control ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Enter the text of your consent form and new signups will have to check a box agreeing to the terms." title="Consent Form or Waiver" rows="6"><?=$this->registry['activity']->consent?></textarea>
			</div>
		</div>
		<hr />
	</div>


 	<div class="col-sm-5 col-md-4">
  		<div class="well"><p>Fill out the general information about your activity.</p></div>
  	</div>
		

   <div class="col-sm-7 col-md-8"><!-- pricing info -->

		<div class="form-group">
			<label class="col-md-offset-4" style="background:#ff0;"><input type="checkbox" name="multiple_costs" id="multiple_costs" value="y" /> This activity has different divisions, categories or different price levels.</label>
		</div>

		<div id="single_costs_div">
			<div class="form-group">
			    <label class="col-md-4">Registration Cost <span style="color:#900;font-weight:normal">*</span></label>
			    <div class="col-md-2">
			      <input type="text" name="cost" id="cost" value="<?=$this->registry['activity']->cost?>" class="form-control ui-popover " data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="The cost to participate in this activity. Credit card transaction fees starting at 2.9% + $.30 will be deducted from this amount." title="Registration Cost" required data-parsley-type="number" />
			    </div> <!-- /.col -->
			</div><!-- /.form-group -->
			<div class="form-group">
			    <label class="col-md-4">Registration Limit</label>
			    <div class="col-md-2">
			      <input type="text" name="limit" id="limit" value="<?=$this->registry['activity']->limit?>" class="form-control ui-popover " data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Limit the number of registrations accepted. Leave blank or set to 0 for no limit." title="Registration Limit" data-parsley-type="number" />
			    </div> <!-- /.col -->
			</div><!-- /.form-group -->
		</div><!-- /#single_costs_div -->

		<div id="multiple_costs_div" class="hide">
			<div class="clone" id="input1">
				<div class="form-group">
				    <label class="col-md-4">Division/Category Name <span style="color:#900;font-weight:normal">*</span></label>
				    <div class="col-md-8">
				      <input type="text" name="division1" id="division1" value="<?=$this->registry['activity']->division1?>" class="form-control ui-popover " data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="The name of the category or division.. ex. 'Boys U12' for a youth soccer league." title="Category or Division" />
				    </div> <!-- /.col -->
				</div><!-- /.form-group -->
				<div class="form-group">
				    <label class="col-md-4">Registration Cost <span style="color:#900;font-weight:normal">*</span></label>
				    <div class="col-md-2">
				      <input type="text" name="cost1" id="cost1" value="<?=$this->registry['activity']->cost1?>" class="form-control ui-popover " data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="The cost to participate in this activity. Credit card transaction fees starting at 2.9% + $.30 will be deducted from this amount." title="Registration Cost" />
				    </div> <!-- /.col -->
				</div><!-- /.form-group -->
				<div class="form-group">
				    <label class="col-md-4">Registration Limit</label>
				    <div class="col-md-2">
				      <input type="text" name="limit1" id="limit1" value="<?=$this->registry['activity']->limit1?>" class="form-control ui-popover " data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Limit the number of registrations accepted. Leave blank or set to 0 for no limit." title="Registration Limit" />
				    </div> <!-- /.col -->
				</div><!-- /.form-group -->
			</div> <!-- /.clone -->
			<div class="col-md-offset-4 col-md-8">
				<a href="javascript:;" id="btn_add" class="btn btn-primary margin_bottom"><i class="fa fa-money fa-lg"></i> Add Pricing Category</a>
			</div>
			<div class="clearfix"></div>
		</div><!-- /#multiple_costs_div -->

		<div class="form-group">
		    <label class="col-md-4">Multi-Registration Discount</label>
		    <div class="col-md-4">
		      <input type="text" name="multiple_registration_discount" id="multiple_registration_discount" value="<?=$this->registry['activity']->multiple_registration_discount?>" class="form-control ui-popover " data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Optional.. The discounted cost for subsequent registrations in this activity. (Example.. A mom is signing up two kids, the second or more signups will be discounted by this amount.)" title="Multi-Registration Discount" />
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
		
		<hr />
	</div>


 	<div class="col-sm-5 col-md-4">
  		<div class="well">
  			<p>Complete your pricing information.</p>
  			<p>Credit card transaction fees are deducted from the registration cost.</p>
  		</div>
  	</div>
   	
   	<div class="col-sm-7 col-md-8"><!-- page info -->
		<div class="form-group">
		    <label class="col-md-4">Start Date</label>
		    <div class="col-md-4">
				<div class="input-group date datetimepicker">
					<input type="text" name="registration_start_date" id="registration_start_date" value="<?=$this->registry['activity']->registration_start_date?>" class="datetimepicker form-control ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Starting date for accepting registrations." title="Start Date" required />
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
		<div class="form-group">
		    <label class="col-md-4">End Date</label>
		    <div class="col-md-4">
			   <div class="input-group date datetimepicker">
					<input type="text" name="registration_end_date" id="registration_end_date" value="<?=$this->registry['activity']->registration_end_date?>" class="datetimepicker form-control ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Closing date for accepting registrations." title="End Date" required />
					<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
					</span>
				</div>
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
		<div class="form-group">
		    <label class="col-md-4">Active</label>
		    <div class="col-md-4">
		      <?=HTML::input_select($this->registry['activity'], 'active', array('y' => 'Yes', 'n' => 'No'))?>
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
		<div class="form-group">
		    <label class="col-md-3">Signup Page Text</label>
		    <div class="col-md-9">
				<textarea name="description" id="description" class="form-control ui-popover " data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Give some information or details about this activity.  This text will appear on the signup page." title="Signup Page Text" rows="12" required ><?=$this->registry['activity']->description?></textarea>
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
		<div class="form-group">
		    <label class="col-md-3">Confirmation Page Text</label>
		    <div class="col-md-9">
				<textarea name="confirmation_text" id="confirmation_text" class="form-control ui-popover " data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Thank someone for signing up or let them know what to expect next.  This text will appear on the page after registration is completed." title="Confirmation Page Text" rows="12" required ><?=$this->registry['activity']->confirmation_text?></textarea>
		    </div> <!-- /.col -->
		</div><!-- /.form-group -->
	
		<hr />
	</div>
 	<div class="col-sm-5 col-md-4">
  		<div class="well">
  			<p>Enter your signup form information.</p>
  			<p>This controls how your registration form is displayed and sets cutoff times for signing up.</p>
  		</div>
  	</div>

</div>

</div>
<div class="clear"></div>