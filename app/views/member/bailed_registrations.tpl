<div class="layout layout-stack-sm layout-main-left">
   <div class="col-md-8 layout-main">

        <div class="alert alert-info">
            <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
            <p>These are the people who have signed up and did not complete payment. There can be many reasons for this. </p>
            <p>You have captured thier contact information so why not follow up with them?</p>
        </div>
        <input type="hidden" id="activity_id" value="<?=$this->registry['activity']->id?>" />
        <input type="hidden" id="member_id" value="<?=$this->registry['member']->id?>" />
        <div class="portlet">
        <?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-125.png')) : ?>
            <div class="pull-right clear"><img src="/images/activities/<?=$this->registry['activity']->id?>-125.png" /></div>
        <?PHP ENDIF;?>

        <h3 class="portlet-title clear">
          <u><?=$this->registry['activity']->name?></u>
        </h3>

        <div class="portlet-body">

          <table class="table table-striped table-bordered" id="table-1">
            <thead>
              <tr>
                <th style="width: 5%"></th>
                <th style="width: 20%">Last Name</th>
                <th style="width: 20%">First Name</th>
                <th style="width: 20%">Email</th>
                <th style="width: 23%">Address</th>
                <th style="width: 12%">Registered</th>
              </tr>
            </thead>

            <tfoot>
              <tr>
                <th></th>
                <th>Last Name</th>
                <th>First Name</th>
                <th>Email</th>
                <th>Address</th>
                <th>Registered</th>
              </tr>
            </tfoot>
          </table>

        </div> <!-- /.portlet-body -->

      </div> <!-- /.portlet -->
    </div>

    <div class="col-md-4 layout-sidebar">

      <div class="portlet">
        <h5><?=$this->registry['activity']->name?></h5>
        <a href="/member/registrations/<?=$this->registry['activity']->id?>" class="btn btn-success btn-jumbo btn-block "><i class="fa fa-arrow-circle-left"></i> Back to Registrations</a>
        <br>
        <a href="/export/bailed_to_excel/<?=$this->registry['activity']->id?>" class="btn btn-secondary btn-block "><i class="fa fa-file-excel-o"></i> Export Bailed to Excel</a>
        <br>
      </div> <!-- /.portlet -->


      <h4>Recent Activity</h4>

      <div class="well">
        
        <ul class="icons-list text-md">
            <?PHP FOREACH($this->registry['logs'] as $log): ?>
                <?=$log->display_as_list_item()?>
            <?PHP ENDFOREACH;?>
        </ul>
      </div> <!-- /.well -->

    </div> <!-- /.layout-sidebar -->
</div>



