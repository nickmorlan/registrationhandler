<?PHP IF(empty($this->registry['payment']->pay_via)): ?>
      <div class="alert alert-danger">
          <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
        <p><i class="fa fa-warning fa-2x"></i> <strong>You have not completed your payment details.</strong>
        <p>This must be completed before you can receive payments from RegistrationHandler.com.</p>
      </div>
<?PHP ENDIF;?>
<?PHP IF(!empty($this->registry['payment']->pay_via)): ?>
  <div class="alert alert-info">
    <p><em>You are currnetly set up to receive payments via <strong><?=ucwords(str_replace('_', ' ', $this->registry['payment']->pay_via))?></strong></em>.</p>
  </div>
<?PHP ENDIF; ?>

<?=$this->registry['error']?>
<!--<p>Account Balance: $<?=$this->registry['balance']?></p>
<p>Next Payment: $<?=$this->registry['next_payment']?></p>
<p>Next Payment Date: <?=$this->registry['next_payment_date']?></p>-->
  <div class="row">
    <div class="col-md-12">
      <p style="margin-top:10px;">Any member who is not integrated with stripe and processes more than $5000 will need to enter thier tax id information for tax reporting purposes and may receive a 1099-MISC tax document.</p>
      <p>Payments over the $5000 threshold will be held until tax information is filled out.</p>
      <p>Financial information is encrypted and secured.</p>
    </div>
    <div class="col-md-12">
      <form id="payment_form" method="post" action="/member/payments" data-parsley-validate class="form form-horizontal parsley-form" autocomplete="off">
          <div class="form-group">
            <label class="col-md-3">Payee Name</label>
            <div class="col-md-5">
              <input type="text" name="name" id="name" value="<?=$this->registry['payment']->name?>" class="form-control demo-element ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="This is your legal name or the name of your business or non-profit entity. This name will be used for payments and tax purposes." title="Payee Info" required />
            </div> <!-- /.col -->
          </div> <!-- /.form-group -->
         <div class="form-group">
            <label class="col-md-3">Tax ID (SSN/EIN)</label>
            <div class="col-md-5">
              <input type="text" name="tax_id" id="tax_id" value="<?=$this->registry['payment']->tax_id?>" class="form-control demo-element ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="Either your US Social Security number(for individuals) or Federal US EIN. Please include the dashes as well. Once successfully entered the tax information is not fully displayed." title="Tax ID Info" required data-parsley-ein />
            </div> <!-- /.col -->
          </div> <!-- /.form-group -->
          <div class="col-md-3"><a data-toggle="modal" href="#basicModal1">More Info</a></div><div class="col-md-3"><button type="submit" class="btn btn-block btn-success" name="Save">Update Tax Information</button></div>
      </form> 
    </div>
  </div>
  <civ class="row">
  <div class="col-md-12">
    <h4 class="content-title">Your Payment Method</h4>
    <div class="checkbox"><label for="pay_stripe"><input type="radio" name="pay_via" id="pay_stripe" value="stripe" data-parsley-required data-parsley-multiple="lists" data-parsley-errors-container="#ers" data-parsley-error-message="You must select a payment method."/> Pay me though Stripe</label></div>
    
    <div class="checkbox"><label for="pay_direct_deposit"><input type="radio" name="pay_via" id="pay_direct_deposit" value="direct_deposit" data-parsley-required data-parsley-multiple="lists" data-parsley-errors-container="#ers" data-parsley-error-message="You must select a payment method."/> Pay me by direct deposit (U.S. only)</label></div>

    <div class="checkbox"><label for="pay_check"><input type="radio" name="pay_via" id="pay_check" value="check" data-parsley-required data-parsley-multiple="lists" data-parsley-errors-container="#ers" data-parsley-error-message="You must select a payment method."/> Pay me with a mailed check</label></div>

    
    <div id="via_check" class="hide">
      <div class="well">
        <p>Checks are send out the first of every month when your account balance meets the minimun threshold of <strong>$100</strong>. If the account balance is not over the threshold funds will continue to accrue and will be payed out on the next sheduled payment after the threshold has been reached.</p>
        <p>Account balance is calculated on a NET-15 basis, meaning the funds will be available in your account 15 days after payment has been captured.</p>
        <p>The fee for registrations that get paid out by check is 5% + $.30 per credit card transaction.</p>
      </div>
          <p>Payments for the above named payee will be mailed to:</p>
        <form id="check_form" action="/member/payments" method="post" data-parsley-validate class="form form-horizontal parsley-form" autocomplete="off">
          <input type="hidden" name="pay_via" value="check" />
         <div class="form-group">
            <label class="col-md-3">Address</label>
            <div class="col-md-7">
              <input type="text" name="address_1" id="address_1" value="<?=$this->registry['payment']->address_1?>" class="form-control demo-element" required /><br />
              <input type="text" name="address_2" id="address_2" value="<?=$this->registry['payment']->address_2?>" class="form-control demo-element" /><br />
            </div> <!-- /.col -->
          </div> <!-- /.form-group -->
         <div class="form-group">
            <label class="col-md-3">City</label>
            <div class="col-md-7">
              <input type="text" name="city" id="city" value="<?=$this->registry['payment']->city?>" class="form-control demo-element" required />
            </div> <!-- /.col -->
          </div> <!-- /.form-group -->
         <div class="form-group">
            <label class="col-md-3">State</label>
            <div class="col-md-7">
              <?=HTML::input_state_select($this->registry['member'], 'state')?>
            </div> <!-- /.col -->
          </div> <!-- /.form-group -->
         <div class="form-group">
            <label class="col-md-3">Zip</label>
            <div class="col-md-7">
              <input type="text" name="zip" id="zip" value="<?=$this->registry['payment']->zip?>" class="form-control demo-element" required />
            </div> <!-- /.col -->
          </div> <!-- /.form-group -->

          <div class="col-md-3 col-md-offset-3"><button type="submit" class="btn btn-block btn-success" name="Save">Save Payment Address</button></div>
      </form>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      <div id="via_stripe" class="hide">
      <div class="col-md-4">
        <div class="left" style="padding-left:50px;margin-top:50px;"><p><a href="https://connect.stripe.com/oauth/authorize?response_type=code&scope=read_write&client_id=<?=STRIPE_LIVE_CLIENT_ID?>" class="stripe-connect"><span>Connect with Stripe</span></a></p></div>
      </div>
      <div class="col-md-8">
        <div class="well">
          <p><i class="fa fa-info-circle fa-lg"></i> <strong>Stripe allows for payments to be sent directly to your bank account 2 days after the registration is completed on the website.</strong></p>
          <p><i class="fa fa-info-circle fa-lg"></i> <strong>Stripe is the fastest way to receive registration money.</strong></p>
          <p>Click the connect button to set up a new Stripe account or to connect your current account with RegistrationHnadler.com. </p>
          <p>Connecting with Stripe also has the lowest fee structure of the payment methods.  You are charged 2.9% plus $.30 per credit card transaction.</p>
          <p>Explore the <a href="https://stripe.com" target="_blank">Stripe</a> website to learn more.</p>
        </div>
      </div>

    </div>

     <div id="via_direct_deposit" class="hide">
      <?PHP IF(false): //(empty($this->registry['payment']->tax_id)) :?>
        <div class="alert alert-warning">
          <p><i class="fa fa-warning fa-2x"></i> <strong>Tax Information Missing.</strong></p>
          <p>To receive account transfers you must first fill out your tax payer information above.</p>
        </div>
      <?PHP ENDIF; ?>
        <div class="col-md-6">
          <form id="dd" method="post" action="/member/payment" data-parsley-validate class="form form-horizontal parsley-form" autocomplete="off">
          <input type="hidden" name="pay_via" value="direct_deposit" />
             <div class="form-group">
                <label class="col-md-4">Routing Number</label>
                <div class="col-md-8">
                  <input type="text" name="routing_number" id="routing_number" value="<?=$this->registry['payment']->routing_number?>" class="form-control demo-element ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="The bank routing number is a 9 digit code found at the bottom of your check." title="Routing Number" required data-parsley-type="number" />
                </div> <!-- /.col -->
              </div> <!-- /.form-group -->

            <div class="form-group clear">
                <label class="col-md-4">Account Number</label>
                <div class="col-md-8">
                  <input type="text" name="account_number" id="account_number" value="<?=$this->registry['payment']->account_number?>" class="form-control demo-element ui-popover" data-toggle="tooltip" data-placement="bottom" data-trigger="focus" data-content="The account number can also be found at the bottom of your checks and is the longest of the numbers printed." title="Account Number" required data-parsley-type="number" />
                </div> <!-- /.col -->
              </div> <!-- /.form-group -->
              <div class="clear"></div>
              <div class="member_alert hide" id="loading_dd" style="width:285px !important";><p><i class="fa fa-cog fa-spin fa-lg"></i> Processing..</p></div>          
              <div class="col-md-4"><a data-toggle="modal" href="#basicModal">More Info</a></div><div class="col-md-6"><button type="submit" id ="save_dd" class="btn btn-block btn-success" name="Save">Save Bank Details</button></div>
          </form>
        </div>
        <div class="col-md-6">
          <div class="well">
            <p>Direct deposits are scheduled for the first of every month when your account balance meets the minimun threshold of <strong>$10</strong>. If the account balance is not over the threshold funds will continue to accrue and will be payed out on the next sheduled payment after the threshold has been reached.</p>
            <p>If you have successfully saved your direct deposit information then secure fields such as bank account number and routing code will not fully appear to protect your personal information. If you want to update your direct deposit payment information you will have to re-enter the data for these fields.</p>
            <p>You are charged 3.5% plus $.30 per credit card transaction if being paid by direct deposit.</p>
          </div>
      </div>
    </div>
  </div>
</div>
<div class="grid_12">
    <hr />
  </div>



      <div id="basicModal" class="modal fade">

        <div class="modal-dialog">

          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="modal-title">Account Numbers</h3>
            </div> <!-- /.modal-header -->

            <div class="modal-body">
              <p>You can find your bank routing number and account number within the string of numbers located at the bottom of one of your checks. </p>
              <p>The numbers at the bottom of your check include a nine-digit bank routing number, the check number, and your account number.</p>
              <p><img src="/images/site/Bank_Check.gif" /></p>
            </div> <!-- /.modal-body -->

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> <!-- /.modal-footer -->

          </div> <!-- /.modal-content -->

        </div><!-- /.modal-dialog -->

      </div><!-- /.modal -->
      <div id="basicModal1" class="modal fade">

        <div class="modal-dialog">

          <div class="modal-content">

            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
              <h3 class="modal-title">Tax Information</h3>
            </div> <!-- /.modal-header -->

            <div class="modal-body">
            <p>U.S. law requires us to collect tax information from payees who are U.S. citizens, U.S. residents, or U.S. corporations.</p>
            <p>If you are an individual, your Tax ID Number is your Social Security number, and your Tax Name is your legal name.</p>
            <p>If you represent a non-individual, such as a corporation, partnership, or non-profit organization, the Tax ID Number is the organization's IRS employer identification number (EIN), and the Tax Name is the name of the organization.</p>
            <p>If you represent a sole proprietorship, your Tax ID Number is either your Social Security number or the sole proprietorship's IRS employer identification number (EIN). The Tax Name is your personal name, not the business name of the sole proprietorship.</p>
            </div> <!-- /.modal-body -->

            <div class="modal-footer">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div> <!-- /.modal-footer -->

          </div> <!-- /.modal-content -->

        </div><!-- /.modal-dialog -->

      </div><!-- /.modal -->
