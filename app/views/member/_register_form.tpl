<p>
	<label for="name">Organization Name</label><br />
	<?=HTML::text_field($this->registry['member'], 'name')?>
</p>
<p>
	<label for="contact_name">Contact Name</label> <span class="red">*</span><br />
	<?=HTML::text_field($this->registry['member'], 'contact_name')?>
</p>
<p>
	<label for="contact_phone">Contact Phone</label> <span class="red">*</span><br />
	<?=HTML::text_field($this->registry['member'], 'contact_phone')?>
</p>
<p>
	<label for="address_1">Address</label> <span class="red">*</span><br />
	<?=HTML::text_field($this->registry['member'], 'address_1')?><br />
	<?=HTML::text_field($this->registry['member'], 'address_2')?>
</p>
<p>
	<label for="city">City</label> <span class="red">*</span><br />
	<?=HTML::text_field($this->registry['member'], 'city')?>
</p>
<p>
	<label for="state">State</label> <span class="red">*</span><br />
	<?=HTML::input_state_select($this->registry['member'], 'state')?>
</p>
<p>
	<label for="zip">Zip</label> <span class="red">*</span><br />
	<?=HTML::text_field($this->registry['member'], 'zip')?>
</p>

