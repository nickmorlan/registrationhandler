<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-7 col-md-8 layout-main">

		<h3 class="content-title">New Registration for <?=$this->registry['activity']->name?>
			<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
				<div class="pull-right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
			<?PHP ENDIF;?>
		</h3>


		<form id="new_registration" name="new_registration" data-parsley-validate method="post" action="/member/create_registration/<?=$this->registry['activity']->id?>" class="form-horizontal parsley-form">
			<?PHP FOREACH($this->registry['registration']->errors as $err): ?>
				<p class="red">&bull; <?=$err?></p>
			<?PHP ENDFOREACH; ?>
			<?=$this->registry['activity']->build_form_html_from_json_definition($this->registry['registration'], 0, true)?>

			<hr />	
			
			<?=$this->registry['activity']->display_category_selection_for_member_create_registration_page()?>
			
			<div class="form-group">
				<label class="col-md-3">Amount Paid</label>
				<div class="col-md-8">
					<input type="text" name="amount_paid" id="amount_paid" value="<?=$this->registry['payment']->amount_paid?>" class="form-control demo-element" />
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->

			<div class="form-group">
				<label class="col-md-3">Date Paid</label>
				<div class="col-md-8">
					<div id="datetimepicker-date_paid" class="input-group date datetimepicker">
						<input id="date_paid" class="form-control" type="text" value="<?=$this->registry['payment']->date_paid?>" name="date_paid" />
						<span class="input-group-addon">
							<span class="glyphicon glyphicon-calendar"></span>
						</span>
					</div>
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->

			<div class="form-group">
				<label class="col-md-3">Payment Reference</label>
				<div class="col-md-8">
					<input type="text" name="transaction_ref" id="transaction_ref" value="<?=$this->registry['payment']->transaction_ref?>" class="form-control demo-element" />
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->

			<div class="form-group">
				<label class="col-md-3">Administrative Notes</label>
				<div class="col-md-8">
				<textarea name="notes" title="Any other important information about this registration" class="form-control" rows="6"><?=$this->registry['registration']->notes?></textarea>
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->

		
			<div class="col-md-4 col-md-offset-3 margin_bottom"><input type="submit" name="submit" value="Save" class="btn btn-success btn-block" /></div>
			<div class="col-md-4"><a href="/member/registrations/<?=$this->registry['activity']->id?>" class="btn btn-default btn-block toggle_edit_button">Cancel</a></div>
		</form>
		

	</div>	

</div>
