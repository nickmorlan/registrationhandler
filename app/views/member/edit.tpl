<link rel="stylesheet" href="/js/plugins/fileupload/bootstrap-fileupload.css">
<link rel="stylesheet" href="/css/stripeconnect.css">
<div class="layout layout-main-right layout-stack-sm">

    <div class="col-md-3 col-sm-4 layout-sidebar">

      <div class="nav-layout-sidebar-skip">
        <strong>Tab Navigation</strong> / <a href="#settings-content">Skip to Content</a>	
      </div>

      <ul id="myTab" class="nav nav-layout-sidebar nav-stacked">
          <li class="active">
          <a href="#profile-tab" data-toggle="tab">
          <i class="fa fa-user"></i> 
          &nbsp;&nbsp;Profile Settings
          </a>
        </li>

        <li>
          <a href="#password-tab" data-toggle="tab">
          <i class="fa fa-lock"></i> 
          &nbsp;&nbsp;Change Password
          </a>
        </li>

        <li>
          <a href="#messaging" data-toggle="tab">
          <i class="fa fa-bullhorn"></i> 
          &nbsp;&nbsp;Notifications
          </a>
        </li>

        <li>
          <a id="payit" href="#payments-tab" data-toggle="tab">
          <i class="fa fa-dollar"></i> 
          &nbsp;&nbsp;Payment Settings
          </a>
        </li>

       </ul>

    </div> <!-- /.col -->



	<div class="col-md-9 col-sm-8 layout-main">
		<div id="settings-content" class="tab-content stacked-content">
			<div class="tab-pane fade in active" id="profile-tab">
				<h3 class="content-title"><u>Edit Profile</u></h3>

				<p>This is your main account information.</p>
				<p>None of your information is ever sold, rented or shared with a third party.</p>

				<br><br>

				<form action="/member/edit" id="edit_form" name="edit_form" data-parsley-validate method="post" class="form form-horizontal parsley-form" accept-charset="utf-8" autocomplete="off" enctype="multipart/form-data">

					<div class="form-group">
						<label class="col-md-3">Contact Email</label>
						<div class="col-md-7">
							<?=$this->registry['member']->email?>
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
						
					<div class="form-group">
						<label class="col-md-3">Organization Name</label>
						<div class="col-md-7">
							<input type="text" name="name" id="name" value="<?=$this->registry['member']->name?>" class="form-control" />
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
						
					<div class="form-group">
						<label class="col-md-3">Contact Name <span class="red">*</span></label>
						<div class="col-md-7">
							<input type="text" name="contact_name" id="contact_name" value="<?=$this->registry['member']->contact_name?>" class="form-control" required />
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
						
					<div class="form-group">
						<label class="col-md-3">Contact Phone <span class="red">*</span></label>
						<div class="col-md-7">
							<input type="text" name="contact_phone" id="contact_phone" value="<?=$this->registry['member']->contact_phone?>" class="form-control" required data-parsley-minlength="10"/>
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
						
					<div class="form-group">
						<label class="col-md-3">Address <span class="red">*</span></label>
						<div class="col-md-7">
							<input type="text" name="address_1" id="address_1" value="<?=$this->registry['member']->address_1?>" class="form-control" required /><br />
							<input type="text" name="address_2" id="address_2" value="<?=$this->registry['member']->address_2?>" class="form-control" />
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
						
					<div class="form-group">
						<label class="col-md-3">City <span class="red">*</span></label>
						<div class="col-md-7">
							<input type="text" name="city" id="city" value="<?=$this->registry['member']->city?>" class="form-control" required />
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
						
					<div class="form-group">
						<label class="col-md-3">State <span class="red">*</span></label>
						<div class="col-md-7">
							<?=HTML::input_state_select($this->registry['member'], 'state')?>
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
						
					<div class="form-group">
						<label class="col-md-3">Zip <span class="red">*</span></label>
						<div class="col-md-7">
							<input type="text" name="zip" id="zip" value="<?=$this->registry['member']->zip?>" class="form-control" required />
						</div> <!-- /.col -->
					</div> <!-- /.form-group -->
						
					<div class="form-group">
						<div class="col-md-3">	
							<label>Profile Header</label><br />
							(appears your signup forms)
						</div>
						<div class="col-md-7">
							<div class="fileupload fileupload-new" data-provides="fileupload">
								<div class="fileupload-new thumbnail" xstyle="width: 180px; height: 180px;">
									<?=$this->registry['member']->display_member_thumb()?>
								</div>

							<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"></div>
								<div>
									<span class="btn btn-default btn-file">
									<span class="fileupload-new">Select image</span>
									<span class="fileupload-exists">Change</span>
									<input type="file" name="image" />
								</span>

								<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
							</div> <!-- /div -->

							</div> <!-- /.fileupload -->
							<p>You can add a main image for your account.  It will show up as a header at the top of your signup pages. Use an image 900px wide by 200px tall for the best looking results. Be sure to save the form after you select the image you want to use.</p>
						</div> <!-- /.col -->

					</div> <!-- /.form-group -->

					

					<div class="col-md-7 col-md-push-3"><button type="submit" class="btn btn-success"><i class="fa fa-floppy-o"></i> Save</button></div>
				</form>
			</div>

	        <div class="tab-pane fade" id="password-tab">

	          <h3 class="content-title"><u>Change Password</u></h3>

	          <p>Enter your current password folowed by your new desired password.</p>

	          <br><br>

	          <form action="/member/new_password" method="post" data-parsley-validate class="form form-horizontal parsley-form" accept-charset="utf-8" autocomplete="off">

	            <div class="form-group">
	              <label class="col-md-3">Current Password</label>
	              <div class="col-md-7">
	                <input type="password" name="old-password" class="form-control" required />
	              </div> <!-- /.col -->
	            </div> <!-- /.form-group -->


	            <hr>


	            <div class="form-group">
	              <label class="col-md-3">New Password</label>
	              <div class="col-md-7">
	                <input type="password" name="password" id="password" class="form-control" required data-parsley-minlength="6" />
	              </div> <!-- /.col -->
	            </div> <!-- /.form-group -->


	            <div class="form-group">
	              <label class="col-md-3">New Password Confirm</label>
	              <div class="col-md-7">
	                <input type="password" name="confirm_password" class="form-control" data-parsley-equalto="#password" />
	              </div> <!-- /.col -->
	            </div> <!-- /.form-group -->


	            <div class="form-group">
	              <div class="col-md-7 col-md-push-3">
	                <button type="submit" class="btn btn-primary">Save Changes</button>
	                &nbsp;
	                <button type="reset" class="btn btn-default">Cancel</button>
	              </div> <!-- /.col -->
	            </div> <!-- /.form-group -->

	          </form>
	        </div> <!-- /.tab-pane -->

	        <div class="tab-pane fade" id="payments-tab">

	          <h3 class="content-title"><u>Payment Settings</u></h3>
	          <?=$this->render_partial('member', 'payment_settings')?>

	        </div> <!-- /.tab-pane -->

	    </div>
	</div>
</div>
