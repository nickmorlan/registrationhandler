<div class="grid_4">
	<p><span id="first_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->first_name?></span> <span id="last_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->last_name?></span><br />
		<?PHP IF(!empty($this->registry['form_elements']['main']['address_1'])): ?>
			<span id="address_1_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->address_1?></span><br />
			<?PHP IF(!empty($this->registry['registration']->address_2)): ?><span id="address_2_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->address_2?></span><br /><?PHP ENDIF; ?>
			<span id="city_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->city?></span>, <span id="state_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->state?></span> <span id="zip_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->zip?></span>
		<?PHP ENDIF; ?>
	</p>
	<?PHP IF(!empty($this->registry['form_elements']['main']['email'])): ?><p><strong>Email </strong><span id="email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->email?></span></p><?PHP ENDIF; ?>
	<?PHP IF(!empty($this->registry['form_elements']['main']['phone'])): ?><p><strong>Phone </strong><span id="phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->phone?></span></p><?PHP ENDIF; ?>
	<?PHP IF(!empty($this->registry['form_elements']['main']['birth_date'])): ?><p><strong>Birthday </strong><span id="birth_date_<?=$this->registry['registration']->id?>"><?=date('M d, Y', strtotime($this->registry['registration']->birth_date))?></span></p><?PHP ENDIF; ?>
	<?PHP IF(!empty($this->registry['form_elements']['school'])): ?>
		<p><strong>Grade </strong><span id="grade_level_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->grade_level?></span></p>
		<p><strong>School </strong><span id="school_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->school?></span></p>
	<?PHP ENDIF; ?>
	<?PHP FOREACH($this->registry['registration']->custom_fields_values['main'] as $field_name => $field_value) : ?>
		<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
		<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><span id="main_<?=$field_name?>"><?=nl2br($field_value)?></span></p>
	<?PHP ENDFOREACH ?>
</div>
	<div style="border:1px dashed #ddd;float:left;width:628px;background:#fafbfc;margin-bottom:10px;">
	<div class="grid_4" style="width:290px;">
		<p><span id="division_<?=$this->registry['registration']->id?>"><strong><?=$this->registry['registration']->division?></strong></span></p>
		<p><strong>Date Paid </strong><span id="date_paid_<?=$this->registry['registration']->id?>"><?PHP IF(!empty($this->registry['registration']->date_paid)):?><?=date('M d, Y', strtotime($this->registry['registration']->date_paid))?><?PHP ENDIF; ?></span></p>
		<?PHP IF(empty($this->registry['registration']->r5k_payment_reference)) :?>
			<p id="setup_p"><input type="button" class="freshbutton-silver" id="setup_refund" value="Setup Refund" /></p>
			<div id="apply_refund_div" class="info_box_yellow" style="display:none;">
				<p><em>Enter a dollar amount to be refunded.</em><br /><input type="text" name="refund_amount" id="refund_amount" value="" style="width:70px;font-size:11px;padding:5px 7px;"/><input type="button" class="freshbutton-blue" id="refund" value="Apply Refund" />
				<img src="/images/site/loading.gif" id="loading" style="display:none;margin-left:10px;vertical-align:-5px;" /><p>
			</div>
		<?PHP ENDIF; ?>
	</div>
	<div class="grid_4" style="width:290px;">
		<p><strong>Amount Paid </strong><span id="registration_amount_<?=$this->registry['registration']->id?>">$<?=$this->registry['registration']->registration_amount?></span></p>
		<?PHP IF(!empty($this->registry['registration']->donation_amount)) : ?><p><strong>Donation Amount </strong>$<?=$this->registry['registration']->donation_amount?></p><?PHP ENDIF; ?>
		<p><strong>Transaction Reference </strong><span id="transaction_ref_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->transaction_ref?></span></p>
	</div>
	<div class="grid_8">
		<p><strong>Notes</strong><br /><span id="notes_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->notes)?></span></p>
	</div><div class="clear"></div>
	</div>
	<?PHP IF(!empty($this->registry['form_elements']['parent'])): ?>
		<div class="grid_12">
			<p><strong>Parent Informaton</strong></p>
		</div>
		<div class="grid_3">
			<p><span id="parent1_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent1_name?></span><br />
				<?PHP IF(!empty($this->registry['registration']->parent1_email)): ?>
					<span id="parent1_email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent1_email?></span><br />
				<?PHP ENDIF; ?>
				<?PHP IF(!empty($this->registry['registration']->parent1_phone)): ?>
					<span id="parent1_email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent1_phone?></span><br />
				<?PHP ENDIF; ?>
				<?PHP IF(!empty($this->registry['registration']->parent1_volunteer) && !empty($this->registry['registration']->parent1_name)): ?>
					<span id="parent1_volunteer_<?=$this->registry['registration']->id?>"><strong>Volunteer </strong><?=$this->registry['registration']->parent1_volunteer?></span><br />
				<?PHP ENDIF; ?>
		</div>
		<div class="grid_4">
			<p><span id="parent2_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent2_name?></span><br />
				<?PHP IF(!empty($this->registry['registration']->parent2_email)): ?>
					<span id="parent2_email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent2_email?></span><br />
				<?PHP ENDIF; ?>
				<?PHP IF(!empty($this->registry['registration']->parent2_phone)): ?>
					<span id="parent2_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->parent2_phone?></span><br />
				<?PHP ENDIF; ?>
				<?PHP IF(!empty($this->registry['registration']->parent2_volunteer) && !empty($this->registry['registration']->parent2_name)): ?>
					<span id="parent2_volunteer_<?=$this->registry['registration']->id?>"><strong>Volunteer </strong><?=$this->registry['registration']->parent2_volunteer?></span><br />
				<?PHP ENDIF; ?>
		</div>
		<?PHP IF(!empty($this->registry['registration']->custom_fields_values['parent'])) : ?>
			<div class="clear"></div>
			<div class="grid_8">
				<?PHP FOREACH($this->registry['registration']->custom_fields_values['parent'] as $field_name => $field_value) : ?>
					<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
					<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><span id="parent_<?=$field_name?>"><?=nl2br($field_value)?></span></p>
				<?PHP ENDFOREACH ?>
			</div>
		<?PHP ENDIF; ?>
		<div class="clear"></div>
	<?PHP ENDIF; ?>
	<div class="grid_8">
		<?PHP IF(!empty($this->registry['form_elements']['comments'])): ?><p><strong>Comments From Registration Form</strong><br /><span id="notes_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->comments)?></span></p><?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['registration']->custom_fields_values['comments'])) : ?>
			<?PHP FOREACH($this->registry['registration']->custom_fields_values['comments'] as $field_name => $field_value) : ?>
				<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
				<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><span id="comments_<?=$field_name?>"><?=nl2br($field_value)?></span></p>
			<?PHP ENDFOREACH ?>
		<?PHP ENDIF; ?>
	</div>
<div class="clear"></div>
<?PHP IF(!empty($this->registry['form_elements']['medical']) || !empty($this->registry['form_elements']['emergency'])): ?>
	<div class="grid_12">
		<hr style="margin-top:25px;" />
	</div>
	<div class="grid_12">
		<h4>Medical Information</h4>
	</div>
	<div class="grid_4">
		<?PHP IF(!empty($this->registry['form_elements']['medical']['hospital_name'])): ?><p><strong>Prefered Hospital </strong><span id="hospital_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->hospital_name?></span></p><?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['form_elements']['emergency']['emergency_contact'])): ?>
			<p><strong>Emergency Contact </strong><span id="emergency_contact_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->emergency_contact?></span></p>
			<p><strong>Relationship </strong><span id="emergency_contact_relationship_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->emergency_contact_relationship_?></span></p>
			<p><strong>Contact Phone </strong><span id="emergency_contact_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->emergency_contact_phone?></span></p>
			<?PHP IF(!empty($this->registry['registration']->custom_fields_values['emergency'])) : ?>
				<?PHP FOREACH($this->registry['registration']->custom_fields_values['emergency'] as $field_name => $field_value) : ?>
					<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
					<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><span id="emergency_<?=$field_name?>"><?=nl2br($field_value)?></span></p>
				<?PHP ENDFOREACH ?>
			<?PHP ENDIF; ?>
		<?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['form_elements']['medical']['physician_name'])): ?>
			<p><strong>Physician </strong><span id="physician_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->physician_name?></span></p>
			<p><strong>Physician Phone </strong><span id="physician_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->physician_phone?></span></p>
		<?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['form_elements']['medical']['dentist_name'])): ?>
			<p><strong>Dentist </strong><span id="dentist_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->dentist_name?></span></p>
			<p><strong>Dentist Phone </strong><span id="dentist_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->dentist_phone?></span></p>
		<?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['registration']->custom_fields_values['medical'])) : ?>
			<?PHP FOREACH($this->registry['registration']->custom_fields_values['medical'] as $field_name => $field_value) : ?>
				<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
				<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><span id="medical_<?=$field_name?>"><?=nl2br($field_value)?></span></p>
			<?PHP ENDFOREACH ?>
		<?PHP ENDIF; ?>
	</div>
	<div class="grid_8">
		<?PHP IF(!empty($this->registry['form_elements']['medical']['medical_history'])): ?><p><strong>Medical History</strong><br /><span id="medical_history_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->medical_history)?></span></p><?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['form_elements']['medical']['allergies'])): ?><p><strong>Allergies</strong><br /><span id="allergies_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->allergies)?></span></p><?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['registration']->custom_fields_values['medical'])) : ?>
			<?PHP FOREACH($this->registry['registration']->custom_fields_values['medical'] as $field_name => $field_value) : ?>
				<?PHP IF(is_array($field_value)){ $field_value = implode(' &amp; ', $field_value);} ?>
				<p><strong><?=ucwords(str_replace('_', ' ',$field_name))?> </strong><span id="medical_<?=$field_name?>"><?=nl2br($field_value)?></span></p>
			<?PHP ENDFOREACH ?>
		<?PHP ENDIF; ?>
	</div>
	<div class="clear"></div>
	<hr />
<?PHP ENDIF; ?>
<div class="clear"></div>

<script>
$(document).ready(function () {
		$("#birth_date").datepicker({
			showAnim: 'blind',
			altFormat: 'mm/dd/yyyy'
		});
		$("#date_paid").datepicker({
			showAnim: 'blind',
			altFormat: 'mm/dd/yyyy'
		});
		
});

$('#setup_refund').on('click', function() {
	$('#setup_p').hide();
	$('#apply_refund_div').show();
});

$('#refund').on('click', function() {
	$('#refund').hide();
	$('#loading').show();
	$('<form action="/member/apply_refund/<?=$this->registry['registration']->id?>/<?=$this->registry['activity']->id?>" method="post"><input type="hidden" name="go" value="go" /><input type="hidden" name="refund_amount" value="' + $('#refund_amount').val()  + '" /></form>').appendTo('body').submit();
});

</script>