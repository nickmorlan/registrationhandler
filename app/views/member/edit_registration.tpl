<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-7 col-md-8 layout-main">

		<h3 class="content-title"><?=$this->registry['registration']->first_name?> <?=$this->registry['registration']->last_name?>
			<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
				<div class="pull-right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
			<?PHP ENDIF;?>
		</h3>


		<div id="edit_form_div">
			<form id="edit_registration" name="edit_registration" method="post" action="/member/edit_registration/<?=$this->registry['registration']->id?>/<?=$this->registry['activity']->id?>" class="form-horizontal">
					
				<div class="form-group">
					<label class="col-md-3">Amount Paid</label>
					<div class="col-md-8">
						<input type="text" name="amount_paid" id="amount_paid" value="<?=$this->registry['registration']->amount_paid?>" class="form-control" />
					</div> <!-- /.col -->
				</div> <!-- /.form-group -->

				<div class="form-group">
					<label class="col-md-3">Date Paid</label>
					<div class="col-md-8">
			            <div class="input-group date datetimepicker" id="d">
			       			<input type="text" name="date_paid" id="date_paid" value="<?=$this->registry['registration']->date_paid?>" class="form-control" />
							<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
							</span>
						</div>
					</div> <!-- /.col -->
				</div> <!-- /.form-group -->

				<div class="form-group">
					<label class="col-md-3">Team/Division/Category</label>
					<div class="col-md-8">
						<input type="text" name="division" id="division" value="<?=$this->registry['registration']->division?>" class="form-control" />
					</div> <!-- /.col -->
				</div> <!-- /.form-group -->

				<div class="form-group">
					<label class="col-md-3">Transaction Reference</label>
					<div class="col-md-8">
						<input type="text" name="transaction_ref" id="transaction_ref" value="<?=$this->registry['registration']->transaction_ref?>" class="form-control" />
					</div> <!-- /.col -->
				</div> <!-- /.form-group -->

				<div class="form-group">
					<label class="col-md-3">Notes about <?=$this->registry['registration']->first_name?> <?=$this->registry['registration']->last_name?></label>
					<div class="col-md-8">
						<textarea name="notes" id="notes" rows="6" class="form-control"><?=$this->registry['registration']->notes?></textarea>
					</div> <!-- /.col -->
				</div> <!-- /.form-group -->
				<hr />
				<?=$this->registry['activity']->build_form_html_from_json_definition($this->registry['registration'], 0, true)?>

				<hr />
				<div class="col-md-5 margin_bottom"><input type="submit" name="submit" value="Save" class="btn btn-success btn-block" /></div>
				<div class="col-md-5"><input type="button" class="btn btn-default btn-block toggle_edit_button" value="Cancel" /></div>
			</form>
		</div>
		
		<div id="details">
			<?=$this->render_partial('member', 'edit_registration_form_json_bootstrap')?>
		</div>

	</div>


	<div class="col-sm-5 col-md-4 layout-sidebar">
		<div class="portlet">
			<a href="/member/registrations/<?=$this->registry['activity']->id?>" class="btn btn-secondary btn-block btn-sm toggle_edit_button"><i class="fa fa-arrow-circle-left"></i> <?=$this->registry['activity']->name?></a><br />
			<a href="javascript:;" class="btn btn-success btn-block btn-jumbo toggle_edit_button"><i class="fa fa-pencil-square-o"></i> Edit Info</a><br />
		</div>
		<div class="well">
			<?PHP /* IF(empty($this->registry['registration']->r5k_payment_reference)) :
				<p id="setup_p"><a href="javascript:;" class="btn btn-info btn-block btn-sm" id="setup_refund"><i class="fa fa-money"></i> Setup Refund</a></p>
				<div id="apply_refund_div" class="alert alert-warning" style="display:none;">
					<p><em>Enter a dollar amount to be refunded.</em><br /><input type="text" name="refund_amount" id="refund_amount" value="" style="width:70px;font-size:11px;padding:5px 7px;"/><input type="button" class="btn btn-success btn-sm" id="refund" value="Apply Refund" />
					<img src="/images/site/loading.gif" id="loading" style="display:none;margin-left:10px;vertical-align:-5px;" /></p>
				</div>
			 ENDIF; */ ?>
			<p><strong><?=$this->registry['registration']->division?></strong></p>
			<p><strong>Date Paid </strong><span id="date_paid_<?=$this->registry['registration']->id?>"><?PHP IF(!empty($this->registry['registration']->date_paid)):?><?=date('M d, Y', strtotime($this->registry['registration']->date_paid))?><?PHP ENDIF; ?></span></p>
		
			<p><strong>Amount Paid </strong>$<?=$this->registry['registration']->registration_amount?></p>
			<?PHP IF($this->registry['registration']->donation_amount > .01) : ?><p><strong>Donation Amount </strong>$<?=$this->registry['registration']->donation_amount?></p><?PHP ENDIF; ?>
			<p><strong>Transaction Reference </strong><?=$this->registry['registration']->transaction_ref?></p>
			<p><strong>Notes</strong><br /><?=nl2br($this->registry['registration']->notes)?></p>
		</div>
		<a href="javascript:;" class="btn btn-danger btn-sml btn-block deleteit" id="<?=$activity->id?>"><i class="fa fa-trash-o"></i> Delete</a>
	</div>

</div>