<div class="grid_10">
	<p class="home_link"><a href="/member" class="freshbutton-silver">Home</a> > <strong>Update Password</strong></p>
</div>
<div class="grid_2"><p align="right"><a href="/member/logout" class="freshbutton-silver">Logout</a></p></div>
<div class="grid_12"><hr /></div>
<div class="grid_12">
	<h3>Please enter your new password.</h3>
</div>
<div class="grid_8">
	<form action="/member/new_password" id="password_form" name="password_form" method="post" accept-charset="utf-8">
		<p>
			<label for="password">Current Password</label><br />
			<?=HTML::text_field($this->registry['member'], 'current_password', 'password')?>
		</p>
		<p>
			<label for="password">New Password</label><br />
			<?=HTML::text_field($this->registry['member'], 'password', 'password')?>
		</p>
		<p>
			<label for="password_confirmation">Confirm Password</label><br />
			<?=HTML::text_field($this->registry['member'], 'confirm_password', 'password')?>
		</p>
		<p><input type="submit" name="submit" value="Save Password" class="submit_button" /></p>
	</form>
</div>
<div class="grid_4">
	<div class="info_box_yellow">
		<p>Confirm you current password and enter the new password for <?=$this->registry['member']->contact_name?>.</p>
	</div>
</div>
<script>
$(document).on('ready', function () {
		$( "input:submit" ).button();
});

$('#password_form').validate({
	rules: {
		password:    {required: true, minlength: 5},
		confirm_password:    {required: true, equalTo: "#password"}

    	}
});
</script>