<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-7 col-md-8">
		<div class="form-group">
			<label class="col-md-4">What best describes this activity?</strong> <span style="color:#900;font-weight:normal">*</span></label>
			<div class="col-md-8">
				<div class="checkbox">
					<label><input type="radio" name="activity_type" id="type_sports_league" value="League" required data-parsley-errors-container="#erstype" data-parsley-error-message="You must select one of these."/> Sports League</label>
				</div>
				<div class="checkbox">
					<label><input type="radio" name="activity_type" id="type_camp" value="Camp" /> Skills Camp, Sports Clinic, Multi-Day Event</label>
				</div>
				<div class="checkbox">
					<label><input type="radio" name="activity_type" id="type_event" value="Event" /> Single Event or Fundraiser</label>
				</div>
				<div class="checkbox">
					<label><input type="radio" name="activity_type" id="type_run" value="Race" /> Race Event (5k, cycling, etc)</label>
				</div>
				
				<div id="erstype" class="form-group"></div>
			</div> <!-- /.col -->
		</div> <!-- /.form-group -->
		<div id="secondary_div" class="hide">
			<div class="form-group">
				<label class="col-md-4">Additional Registration Information Collected</strong></label>
				<div class="col-md-8">
					<div class="checkbox league camp event race">
						<label><input type="checkbox" class="form_def" name="info[]" id="birth_date" value="birth_date"/> I want the birthday/age of the participants.</label>
					</div>
					<div class="checkbox league camp event race">
						<label><input type="checkbox" class="form_def" name="info[]" id="shirt" value="shirt"/> I want shirt sizes of the particpants.</label>
					</div>
					<div id="additional_shirt" class="additional" style="padding-left:20px;" class="hide"><p><em>Enter any information about shirt sizing or distribution you would like to show on the registration form as a note for the user.</em></p><textarea name="shirt_notes" id="shirt_notes" style="height:100px;"></textarea></div>
					<div class="checkbox league camp event">
						<label><input type="checkbox" class="form_def" name="info[]" id="parent" value="parent"/> This is a youth activity and I want to collect parent information.</label>
					</div>
					<div id="additional_youth" class="additional" style="padding-left:20px;" class="hide">
						<div class="checkbox">
							<label><input type="checkbox" class="form_def" name="info[]" id="parent_volunteer" value="parent_volunteer"/> I would like parents to be able to volunteer.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="form_def" name="info[]" id="parent_coach" value="parent_coach"/> I would like parents to be able to volunteer as coaches.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="form_def" name="info[]" id="school" value="school"/> I want to collect school attended and grade level.</label>
						</div>
					</div>
					<div class="checkbox league camp event">
						<label><input type="checkbox" class="form_def" name="info[]" id="medical" value="medical"/> This is a physical activity and I want medical history and contacts.</label>
					</div>
					<div id="additional_med" class="additional" style="padding-left:20px;" class="hide">
						<div class="checkbox">
							<label><input type="checkbox" class="form_def" name="info[]" id="medical_comments" value="medical_comments"/> I want to know medical information (history, allergies, current prescriptions, etc.)</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="form_def" name="info[]" id="primary_doctor" value="primary_physician"/> Collect primary physician contact information.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="form_def" name="info[]" id="primary_dentist" value="primary_dentist"/> Collect preferred dentist contact information.</label>
						</div>
						<div class="checkbox">
							<label><input type="checkbox" class="form_def" name="info[]" id="preferred_hospital" value="preferred_hospital"/> I want to know the preferred hospital.</label>
						</div>
					</div>
					<div class="checkbox league camp event race">
						<label><input type="checkbox" class="form_def" name="info[]" id="emergency" value="emergency"/> I want info for an additional emergency contact.</label>
					</div>
					<div class="checkbox league camp event race">
						<label><input type="checkbox" class="form_def" name="info[]" id="comments" value="comments"/> I would like a field to leave comments during registration.</label>
					</div>
					<div class="checkbox league camp event">
						<label><input type="checkbox" class="form_def" name="info[]" id="donation" value="donation"/> I would like to be able to take donations during checkout.</label>
					</div>
				
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->

		</div>


	</div>


 	<div class="col-sm-5 col-md-4">
  		<div class="well"><p>Select the options that best describe your new activity and specify what information you would like to capture.  You can further customize the signup form after the activity has been created.</p></div>
  	</div>
</div>
