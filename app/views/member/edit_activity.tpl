<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-12 col-md-12 layout-main">
	<h3 class="content-title">Edit Activity Information
					<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
					<div class="pull-right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
				<?PHP ENDIF;?>
	</h3>

	</div>
	<form id="activity_form" name="activity_form" method="post" action="/member/edit_activity/<?=$this->registry['activity']->id?>" data-parsley-validate class="form form-horizontal parsley-form" enctype="multipart/form-data">
		
		<?=$this->render_partial('member', 'activity_form')?>
		<div class="col-md-4 margin_bottom"><input type="submit" name="submit" value="Save" class="btn btn-success btn-block" /></div>
		<div class="col-md-4"><a href="/member/activity_detail/<?=$this->registry['activity']->id?>" class="btn btn-default btn-block toggle_edit_button">Cancel</a></div>
	</form>

</div>
