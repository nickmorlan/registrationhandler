<div class="grid_12">
	<h1>Page Error</h1>
	<p>The requested view is not here.</p>
	<p><ul>
		<li>Method: <?=$this->registry['method']?></li>
		<li>View: <?=$this->registry['view']?></li>
		<li>View File: <?=$this->registry['view_file']?></li>
	</ul>
	</p>
</div>