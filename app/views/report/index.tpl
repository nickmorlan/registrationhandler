<div class="layout layout-stack-sm layout-main-left">
   <div class="col-md-8 layout-main">

        <?PHP IF(empty($this->registry['activities'])): ?>
            <div class="alert alert-warning">
                <p>You have not created any activities.</p>
            </div>
        <?PHP ENDIF;?>
        <div class="portlet">

        <h4 class="portlet-title">
          <u>Recent Registration Activity</u>
        </h4>
          
        <div class="portlet-body">

          <?PHP IF($this->registry['js_data'] != 'null'): ?>
            <div id="area-chart" class="chart-holder-250"></div>
          <?PHP ELSE: ?>
            <div class="col-md-offest-3"><p><strong>There is no recent registration activity to show.</strong></p></div>
          <?PHP ENDIF; ?>
        </div> <!-- /.portlet-body -->  

      </div> <!-- /.portlet -->

        <?PHP FOREACH($this->registry['activities'] as $activity): ?>
            <div class="portlet">
                <h4 class="portlet-title"><u><?=$activity->name?></u></h4>
                <div class="row">

                    <div class="col-sm-3 center">
                        <?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $activity->id . '-125.png')) : ?>
                            <img src="/images/activities/<?=$activity->id?>-125.png" />
                        <?PHP ENDIF; ?>
                    </div>
                    <div class="col-sm-6">
                     <table class="table keyvalue-table">
                      <tbody>
                        <tr>
                          <td class="kv-key"><i class="fa fa-group kv-icon kv-icon-secondary"></i> Total Registrations</td>
                          <td class="kv-value"><?=$activity->registered?></td>
                        </tr>
                        <tr>
                          <td class="kv-key"><i class="fa fa-dollar kv-icon kv-icon-primary"></i> Generated</td>
                          <td class="kv-value">$<?=number_format($activity->amount, 2)?></td>
                        </tr>
                       </tbody>
                    </table>
                    </div>
                    <div class="col-sm-3">
                        <a href="/report/activity/<?=$activity->id?>" class="btn btn-info btn-sm btn-block pull-right margin_bottom">View Breakdown</a>
                    </div>
                </div>
                
            </div>
        <?PHP ENDFOREACH; ?>

    </div> <!-- /.main column -->

    <div class="col-md-4 layout-sidebar">

      <div class="portlet">
        <?PHP $member_name = (!empty($this->registry['member']->name)) ? $this->registry['member']->name : $this->registry['member']->contact_name; ?>
        <h5><?=$member_name?></h5>
        <a href="/report/payment" class="btn btn-secondary btn-lg btn-block ">Payment Report</a>
        <br>
        <?PHP IF(empty($this->registry['payment_info'])): ?>
            <a href="/member/payments" class="btn btn-secondary btn-block">Set Up Payment Info</a>
        <?PHP ENDIF;?>  </div> <!-- /.portlet -->


    </div> <!-- /.layout-sidebar -->
</div>
