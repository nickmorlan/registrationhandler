<div class="clear"></div>
<div class="grid_12">
	<hr style="margin-top:25px;" />
</div>
<div class="grid_12">
	<h4>Medical Information</h4>
</div>
<div class="grid_4">
	<p><strong>Prefered Hospital </strong><span class="editable" id="hospital_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->hospital_name?></span></p>
	<p><strong>Emergency Contact </strong><span class="editable" id="emergency_contact_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->emergency_contact?></span></p>
	<p><strong>Contact Phone </strong><span class="editable" id="emergency_contact_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->emergency_contact_phone?></span></p>
	<p><strong>Physician </strong><span class="editable" id="physician_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->physician_name?></span></p>
	<p><strong>Physician Phone </strong><span class="editable" id="physician_phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->physician_phone?></span></p>
</div>
<div class="grid_8">
	<p><strong>Medical History</strong><br /><span class="editable_textarea" id="medical_history_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->medical_history)?></span></p>
	<p><strong>Allergies</strong><br /><span class="editable_textarea" id="allergies_<?=$this->registry['registration']->id?>"><?=nl2br($this->registry['registration']->allergies)?></span></p>
</div>
