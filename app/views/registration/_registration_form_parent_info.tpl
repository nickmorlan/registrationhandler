<div class="grid_12"><hr /></div>
<div class="grid_7">
	<h3>Parent Information</h3>
	<p>
		<label for="parent1_name">Parent/Guardian Name:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'parent1_name')?>
	</p>
	<p>
		<label for="parent1_email">Email:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'parent1_email')?>
	</p>
	<p>
		<label for="parent1_phone">Phone:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'parent1_phone')?>
	</p>
	<p>
		<label for="parent2_name">Parent/Guardian Name:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'parent2_name')?>
	</p>
	<p>
		<label for="parent2_email">Email:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'parent2_email')?>
	</p>
	<p>
		<label for="parent2_phone">Phone:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'parent2_phone')?>
	</p>
</div>
<div class="grid_5">
	<div class="info_box" style="background:#ffc">
		<p><strong>Parent Information</strong> Please let us know the school to be attended and grade level for the participant during this activity.</p>
	</div>
</div>
<div class="clear"></div>