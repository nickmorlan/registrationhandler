<div class="grid_12"><hr /></div>
<div class="grid_7">
	<h3>Registrant Information</h3>
	<p>
		<label for="registration">First Name:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'first_name')?>
	</p>
	<p>
		<label for="registration">Last Name:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'last_name')?>
	</p>
	<?PHP IF($var['phone']) : ?>
		<p>
			<label for="phone">Phone:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'phone')?>
		</p>
	<?PHP ENDIF; ?>
	<?PHP IF($var['email']) : ?>
		<p>
			<label for="email">Email:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'email')?>
		</p>
	<?PHP ENDIF; ?>
	<p>
		<label for="registration">Address:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'address_1')?><br />
		<?=HTML::text_field($this->registry['registration'], 'address_2')?>
	</p>
	<p>
		<label for="registration">City:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'city')?>
	</p>
	<p>
		<label for="state">State</label><br />
		<?=HTML::input_state_select($this->registry['registration'], 'state')?>
	</p>
	<p>
		<label for="zip">Zip</label><br />
		<?=HTML::text_field($this->registry['registration'], 'zip')?>
	</p>
	<?PHP IF($var['birth_date']) : ?>
		<p>
			<label for="birth_date">Birth Date:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'birth_date', 'text', 'date-picker')?>
		</p>
	<?PHP ENDIF; ?>
</div>
<div class="grid_5">
	<div class="info_box" style="background:#ffc">
		<p><strong>Registrant Information</strong> Please fill out information about the registrant.</p>
	</div>
</div>
<div class="clear"></div>