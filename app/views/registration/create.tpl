<div class="grid_11">
	<p class="home_link"><a href="/member">Home</a> > <a href="/member/registrations/<?=$this->registry['activity']->id?>"><?=$this->registry['activity']->name?></a> > New Registration</p>
</div>
<div class="grid_1"><p class="logout_link" align="right"><a href="/member/logout">Logout</a></p></div>
<div class="clear"></div>
<form id="new_registration" name="new_registration" method="post" action="/registration/create/<?=$this->registry['registration']->id?>">
	<?PHP
		$xml = simplexml_load_string($this->registry['activity']->form_definition);
		foreach($xml->partials as $v ) 
			foreach($v as $k => $t) 
				$this->render_partial('registration', 'registration_form_' . $k);			
	?>
	<p><input type="submit" name="submit" value="Continue" /></p>
</form>

<script type="text/javascript">
	$(document).ready(function() {
	// init submit buttons
		$( "input:submit" ).button();
		$( "input:button" ).button();
		
	});
</script>