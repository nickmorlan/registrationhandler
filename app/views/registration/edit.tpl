<div class="grid_10">
	<p class="home_link"><a href="/member">Home</a> > <a href="/member/registrations/<?=$this->registry['activity']->id?>"><?=$this->registry['activity']->name?></a> > <?=$this->registry['registration']->first_name?> <?=$this->registry['registration']->last_name?></p>
</div>
<div class="grid_1"><p class="help_link" align="right">help</p></div>
<div class="grid_1"><p class="logout_link" align="right"><a href="/member/logout">Logout</a></p></div>
<div class="clear"></div>
<form id="edit_registration" name="edit_registration" method="post" action="/registration/edit/<?=$this->registry['registration']->id?>">
	<div class="grid_12">
	<hr />
		<div class="left"><h3><span id="name_header"><?=$this->registry['registration']->first_name?> <?=$this->registry['registration']->last_name?></span></h3></div>	
		<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
			<div class="right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
		<?PHP ENDIF;?>
	<hr />
	</div>
	<?=$this->render_partial('registration', 'form')?>
</form>

<script type="text/javascript">
	$(document).ready(function() {
	// init submit buttons
		$( "input:submit" ).button();
		$( "input:button" ).button();
		
	});
</script>