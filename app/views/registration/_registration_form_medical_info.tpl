<div class="grid_12"><hr /></div>
<div class="grid_7">
	<h3>Medical Information</h3>
	<?PHP IF($var['emergency_contact']) : ?>
		<p>
			<label for="emergency_contact">Emergency Contact:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'emergency_contact')?>
		</p>
		<p>
			<label for="emergency_contact_phone">Emergency Contact Phone:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'emergency_contact_phone')?>
		</p>
	<?PHP ENDIF; ?>
	<?PHP IF($var['preferred_hospital']) : ?>
		<p>
			<label for="hospital_name">Preferred Hospital:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'hospital_name')?>
		</p>
	<?PHP ENDIF; ?>
	<?PHP IF($var['preferred_physician']) : ?>
		<p>
			<label for="physician_name">Prefered Physician:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'physician_name')?>
		</p>
		<p>
			<label for="physician_phone">Physician Phone:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'physician_phone')?>
		</p>
	<?PHP ENDIF; ?>
	<?PHP IF($var['preferred_dentist']) : ?>
		<p>
			<label for="dentist_name">Dentist Name:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'dentist_name')?>
		</p>
		<p>
			<label for="dentist_phone">Dentist Phone:</label><br />
			<?=HTML::text_field($this->registry['registration'], 'dentist_phone')?>
		</p>
	<?PHP ENDIF; ?>
</div>
<div class="grid_5">
	<div class="info_box" style="background:#ffc">
		<p><strong>Medical Information</strong> Please fill out your preferred medical contacts and history if applicable.</p>
	</div>
</div>
<div class="clear"></div>
<div class="grid_7">
	<?PHP IF($var['medical_history']) : ?>
		<p>
			<label for="medical_history">Medical History:</label><br />
			<?=HTML::text_area($this->registry['registration'], 'medical_history')?>
		</p>
	<?PHP ENDIF; ?>
	<?PHP IF($var['allergies']) : ?>
		<p>
			<label for="allergies">Allergies:</label><br />
			<?=HTML::text_area($this->registry['registration'], 'allergies')?>
		</p>
	<?PHP ENDIF; ?>
</div>
