<div class="grid_4">
	<p><span class="editable" id="first_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->first_name?></span> <span class="editable" id="last_name_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->last_name?></span><br />
		<span class="editable" id="address_1_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->address_1?></span><br />
		<?PHP IF(!empty($this->registry['registration']->address_2)): ?>
			<?=$this->registry['registration']->address_2?><br />
		<?PHP ENDIF; ?>
		<span class="editable" id="city_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->city?></span>, <span class="editable" id="state_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->state?></span> <span class="editable" id="zip_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->zip?></span>
	</p>
	<?PHP IF($var['phone']) : ?><p><strong>Phone </strong><span class="editable" id="phone_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->phone?></span></p><?PHP ENDIF; ?>
	<?PHP IF($var['email']) : ?><p><strong>Email </strong><span class="editable" id="email_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->email?></span></p><?PHP ENDIF; ?>
	<?PHP IF($var['birth_date']) : ?><p><strong>Birthday </strong><span class="editable" id="birth_date_<?=$this->registry['registration']->id?>"><?=date('M d, Y', strtotime($this->registry['registration']->birth_date))?></span></p><?PHP ENDIF; ?>
	<?PHP IF($var['school_info']) : ?>
		<p><strong>Grade </strong><span class="editable" id="grade_level_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->grade_level?></span></p>
		<p><strong>School </strong><span class="editable" id="school_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->school?></span></p>
	<?PHP ENDIF; ?>
	<p><strong>Division / Team </strong><span class="editable" id="division_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->division?></span></p>
	<p><strong>Date Paid </strong><span class="editable" id="date_paid_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->date_paid?></span></p>
	<p><strong>Amount Paid </strong><span class="editable" id="amount_paid_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->amount_paid?></span></p>
	<p><strong>Tranaction Reference </strong><span class="editable" id="transaction_ref_<?=$this->registry['registration']->id?>"><?=$this->registry['registration']->transaction_ref?></span></p>
</div>
