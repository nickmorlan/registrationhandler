<?=$this->registry['activity']->display_member_header()?>
<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-12 col-md-12 layout-main">
	<h3 class="content-title"><?=$this->registry['activity']->name?>
				<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
					<div class="pull-right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
				<?PHP ENDIF;?>
			</h3>



<div class="col-md-3">
	<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '.png')) : ?>
		<img src="/images/activities/<?=$this->registry['activity']->id?>.png" class="img-responsive margin_bottom" />
	<?PHP ENDIF;?>
	<?PHP IF(!empty($this->registry['closed_error'])): ?>
		<p><strong>Registration Cost:</strong> <?=$this->registry['activity']->display_cost()?></p>
	<?PHP ENDIF; ?>
	<?PHP IF(file_exists($this->registry['ics_file'])): ?>
		<div class="well margin_top">
			<p style="text-align:center;"><strong>Event Calendar</strong></p>
			<p><img src="/images/site/16pxfileicons/ics.png" style="margin-right:3px;vertical-align:top;"/><a href="/register/download_ics/<?=$this->registry['activity']->id?>" title="<?=$this->registry['activty']->name?> Important Dates"><?=$this->registry['activity']->name?></a></p>
		</div>
	<?PHP ENDIF; ?>
</div>
<div class="col-md-6">
	<?=$this->registry['activity']->display_registration_page_event_info($this->registry['registration'])?>
	<?PHP IF(!empty($this->registry['activity']->website)): ?>
		<p><a href="<?=$this->registry['activity']->website?>" title="<?=$this->registry['activity']->website?>" target="_blank"><?=$this->registry['activity']->website?></a></p>
	<?PHP ENDIF; ?>
	<p><strong>Event Details</strong><p>
	<p><?=$this->registry['activity']->description?></p>
</div>

 <div class="col-md-3">
   <?PHP IF(!empty($this->registry['activity']->address1)) :?>
    	<div class="portlet portlet-boxed">
    		<div id="map-canvas" style="height: 180px;"></div>
    		<?=$this->registry['activity']->display_address()?>
    	</div>
    <?PHP ENDIF; ?>
 </div>
</div>
<hr class="fancy" style="margin-top:20px;" />
   <div class="col-sm-offset-1 col-sm-10 col-md-offset-1 col-md-10 layout-main">
   	<?PHP IF(!empty($this->registry['closed_error'])): ?>
   		<div class="alert alert-danger"><i class="fa fa-icon fa-exclamation-triangle fa-lg"></i> <?=$this->registry['closed_error']?></div>
	<?PHP ELSE: ?>
		<form id="register_form" method="post" action="/register/<?=$this->registry['activity']->url_link()?>" data-parsley-validate  class="form-horizontal parsley-form">
			<?PHP FOREACH($this->registry['registration']->errors as $err): ?>
				<p class="red">&bull; <?=$err?></p>
			<?PHP ENDFOREACH; ?>
			<?=$this->registry['activity']->build_form_html_from_json_definition($this->registry['registration'], $this->registry['current_registration_count'])?>
			<?PHP IF(!empty($this->registry['activity']->consent)) : ?>
				<hr />
				<div class="form-group">
					<div class="col-md-offset-3 col-md-8">
						<p>To complete registration you must agree to the organizers waiver below.</p>
						<textarea id="consent" disabled="disabled" class="form-control" rows="6"><?=$this->registry['activity']->consent?></textarea>
						<label for="consent_box"><input type="checkbox" class="required" required value="y" id="consent_box" name="consent" data-parsley-errors-container="#ers-consent" data-parsley-error-message="You must check to agree to the terms above." /> I agree to the conditions above.</label>
						<br />
						<label>Please enter your initials to consent. <input type="text" required class="required form-control" name="consent_initials" id="consent_initials" value="" style="width:50px;font-size:.9em;display:inline;margin-left:10px;" data-parsley-minlength="2" maxlength="5" data-parsley-errors-container="#ers-initials" data-parsley-error-message="You must enter your initials for consent." /></label>
						<br />
						<div id="ers-consent" class="form-group"></div>
						<div id="ers-initials" class="form-group"></div>
					</div>
				</div>
			<?PHP ENDIF; ?>
			<hr />
			<div class="col-md-offset-3 col-md-8">
				<?=$this->registry['activity']->display_category_selection(); ?>	
			</div>
			<div class="col-md-offset-3 col-md-3 margin_bottom">
				<?PHP IF($this->registry['current_registration_count'] > 0) : ?>
						<a href="/register/confirm" class="btn btn-block btn-sm btn-primary margin_bottom" >Cancel this Registration</a>
				<?PHP ENDIF; ?>
				<input type="submit" name="submit" value="Register Another Person" class="btn btn-block btn-sm btn-secondary" />
			</div>
			<div class="col-md-4">
				<input type="submit" name="submit" value="Continue to Payment" class="tn btn-block btn-success btn-lg" />
			</div>
			<input type="hidden" name="user_select" value="" />
			<input type="hidden" name="user" value="" />
		</form>
	<?PHP ENDIF; ?>
</div>