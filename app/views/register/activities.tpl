<?=Member::display_member_header_for($this->registry['themember'])?>
<?PHP IF(empty($this->registry['activities'])) :?>
	<div style="padding-top:55px !important;text-align:center;"><h2>Registrations are closed.</h2></div>
<?PHP ENDIF; ?>
<?PHP FOREACH($this->registry['activities'] as $activity): ?>
	<div class="col-md-8">
		<div class="well">
			<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $activity->id . '-125.png')) : ?>
				<div class="left margin_bottom" style="margin:0 10px 0;"><a href="/register/<?=$activity->url_link()?>"><img src="/images/activities/<?=$activity->id?>-125.png" /></a></div>
			<?PHP ENDIF;?>
			<div class="left">
				<h5 style="margin-bottom:10px;"><?=$activity->name?></h5>
				<p><strong>Regsitration Ends:</strong> <?=date('D M j Y', strtotime($activity->registration_end_date))?></p>
				<p><?=$activity->description?></p>
				<p>Registration Cost: <?=$activity->display_cost()?></p>
				<p><a href="/register/<?=$activity->url_link()?>" class="freshbutton-lightblue">Click to Register for <?=$activity->name?></a></p>
			</div>
			<div class="clear"></div>
		</div>
	</div>
	<div class="clear"></div>
<?PHP ENDFOREACH; ?>