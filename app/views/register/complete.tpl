<?=$this->registry['activity']->display_member_header()?>
<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-12 col-md-12 layout-main">
	<h3 class="content-title"><?=$this->registry['activity']->name?>
				<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
					<div class="pull-right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
				<?PHP ENDIF;?>
			</h3>
	</div>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=1431926310369682";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

	<div class="col-md-5">
		<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '.png')) : ?>
			<div><img src="/images/activities/<?=$this->registry['activity']->id?>.png" /></div>
		<?PHP ENDIF; ?>
			<p><?=$this->registry['activity']->name?></p>
			<div class="fb-share-button left" data-href="https://www.registrationhandler.com/register/<?=$this->registry['activity']->url_link()?>" data-type="button_count"></div>
		    <div class="left" style="margin-left:40px;"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="en" data-url="https://www.registrationhandler.com/register/<?=$this->registry['activity']->url_link()?>" data-counturl="https://www.registrationhandler.com/register/<?=$this->registry['activity']->url_link()?>" data-text="I signed up for <?=$this->registry['activity']->name?>!">Tweet</a></div>
			<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>
	    	<div class="clear"></div>
	    	<p style="font-size:10px;">Help us promote by posting to facebook or tweeting about it!</p>
	    <hr />
		<?PHP IF(file_exists($this->registry['ics_file'])): ?>
			<div class="info_box">
				<p style="text-align:center;"><strong>Download Event Calendar</strong></p>
				<p><img src="/images/site/16pxfileicons/ics.png" style="margin-right:3px;vertical-align:top;"/><a href="/register/download_ics/<?=$this->registry['activity']->id?>" title="<?=$this->registry['activty']->name?> Important Dates"><?=$this->registry['activity']->name?></a></p>
			</div>
		<?PHP ENDIF; ?>
		<?PHP IF(!empty($this->registry['activity']->confirmation_text)) : ?>
			<?=$this->registry['activity']->confirmation_text?>
		<?PHP ELSE: ?>	
			<p><strong>Thank you for registering.</strong></p>
		<?PHP ENDIF; ?>
		<p style="margin-top:15px;"><a href="http://<?=$this->registry['activity']->website?>" title="<?=$this->registry['activity']->name?>"><?=$this->registry['activity']->website?></a></p>
		<hr />
		<div class="col-md-6"><a href="/activities/<?=$this->registry['member']['id']?>/<?=$this->registry['member']['name']?>" class="btn btn-block btn-secondary">Register Another</a></div>
	</div>
	<div class="col-md-7">
		<?PHP FOREACH($this->registry['registrations'] as $registration): ?>
			<div class="well">
				<div class="right"><?=$this->registry['activity']->display_tumbnail_image()?></div>
				<h3><?=$this->registry['activity']->name?></h3>
				<?PHP IF(!empty($registration->division)) :?>
					<p style="margin-top:-10px;"><?=$registration->division?></p>
				<?PHP ENDIF; ?>
				<p class="exclamation_point_green green"><strong><em>Paid $<?=$registration->amount_paid?> on <?=date('D M j Y', strtotime($registration->date_paid))?></em></strong></p>
				<?PHP IF($registration->donation_amount > 0 ) : ?>
					<p><strong>Thank you for your donation of $<?=$registration->donation_amount?>.</strong></p>
				<?PHP ENDIF; ?>
				<?=$this->registry['activity']->generate_confirmation_for_registration($registration)?>
			</div>
		<?PHP ENDFOREACH; ?>

	</div>
</div>
