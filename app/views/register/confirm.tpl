<div class="layout layout-stack-sm layout-main-left">
   <div class="col-sm-12 col-md-12 layout-main">
	<h3 class="content-title"><?=$this->registry['activity']->name?> Review
				<?PHP IF(file_exists(SITE_ROOT . 'images/activities/' . $this->registry['activity']->id . '-35.png')) : ?>
					<div class="pull-right"><img src="/images/activities/<?=$this->registry['activity']->id?>-35.png" style="margin-top:-5px;" /></div>
				<?PHP ENDIF;?>
			</h3>

<div class="col-sm-7 col-md-8">
	<div class="alert alert-info"><em>Please confirm the information below. You can correct any inaccuracies using the 'Edit Information' link at the bottom of the yellow box. Click the purchase button below to enter payment information over a secure server connection.</em></div>
	<?PHP FOREACH($this->registry['registrations'] as $registration): ?>
		<div class="well">
			<div class="right"><?=$this->registry['activity']->display_tumbnail_image()?></div>
			<h3><?=$this->registry['activity']->name?></h3>
			<p style="margin-top:-10px;">($<?=number_format($registration->cost, 2)?>) <?=$registration->division?></p>
			<?=$this->registry['activity']->generate_confirmation_for_registration($registration)?>
			<div class="clear"></div>
			<div class="left"><p><a href="/register/edit/<?=$registration->id?>/<?=$this->registry['activity']->url_link()?>" class="btn btn-sm btn-secondary" id="edit_<?=$registration->id?>" /><i class="fa fa-pencil-square-o"></i> Edit Information</a></p></div>
			<?PHP IF(count($this->registry['registrations']) > 1):?><div class="right"><p align="right"><a href="/register/destroy_registration/<?=$registration->id?>/<?=$this->registry['activity']->url_link()?>" class="btn btn-small btn-primary" id="remove_<?=$registration->id?>" onclick="return confirm('Are you sure you want to remove the registration for <?=$registration->first_name?> <?=$registration->last_name?>?');" />Remove</a></p></div><? ENDIF; ?>
			<div class="clear"></div>
		</div>
		<div class="clear"></div>
	<?PHP ENDFOREACH?>
	<p align="right" style="margin-right:10px;"><a href="/register/<?=$this->registry['activity']->url_link()?>" class="btn btn-sm btn-secondary">Register Another Person</a></p>

</div>


<div class="col-sm-5 col-md-4">
	<h3><?=$this->registry['activity']->name?> Payment</h3>
	<p><strong>Registration is not complete until payment information is entered after clicking 'Purchase' button below.</strong></p>	
		<form id="payment_form" action="/register/pay/stripe" method="post" autocomplete="off">
			<hr />
			<?PHP FOREACH($this->registry['lineitems'] as $division => $item): ?>
				<div class="col-xs-8 col-sm-8 col-md-8"><p align="right">(<?=$item['number']?>) <?=$division?></p></div>
				<div class="col-xs-4 col-md-4 col-md-4"><p align="right">$<?=number_format($item['cost'], 2)?></p></div>
			<?PHP ENDFOREACH; ?>
			<input type="hidden" name="registration_id" value="<?=$this->registry['registration']->id?>" />
			<input type="hidden" name="confirm_session" value="<?=$this->registry['confirm_session']?>" />
			<input type="hidden" name="cost" value="<?=number_format($this->registry['total'], 2)?>" />
			<input type="hidden" name="fee" value="<?=number_format($this->registry['fee'], 2)?>" />
			<div class="col-xs-8 col-sm-8 col-md-8"><p align="right">Processing Fee</p></div>
			<div class="col-xs-4 col-sm-4 col-md-4"><p align="right">$<?=number_format($this->registry['fee'], 2)?></div>
				<div class="clear"></div>
			<?PHP IF($this->registry['activity']->accept_donation == 'y') :?>
				<div class="col-xs-8 col-sm-8 col-md-8"><p align="right"><label> <input type="checkbox" id="activate_donation" />I would like to make a donation of</label></p></div>
				<div class="col-xs-4 col-sm-4 col-md-4"><p align="right"><strong>$</strong><input type="text" name="donation_amount" id="donation_amount" class="form-control" value="0.00" style="width:40px;padding:4px;font-size:.8em;display:inline;" disabled="disabled" /></p></div>
				<span style="color:#900;font-size:.75em" id="donation_error" class="hide"><br /><em>Please enter a valid dollar amount.</em></span>
				<hr />
			<?PHP ENDIF; ?>
			<div class="col-xs-7 col-sm-6 col-md-6"><p align="right">Total</p></div>
			<div class="col-xs-5 col-sm-6 col-md-6"><p align="right" style=""><span class="green" style="font-weight:bold;font-size:16px;" id="activity_cost">$<?=number_format($this->registry['total'] + $this->registry['fee'], 2)?></span></p></div>
			<br class="clear" />
		 	<a href="javascript:;" id="customButton" class="btn btn-block btn-jumbo btn-success"><i class="fa fa-thumbs-o-up"></i> Purchase</a>
		</form>
</div>
