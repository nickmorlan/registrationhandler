<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<meta HTTP-EQUIV="CACHE-CONTROL" CONTENT="NO-CACHE">
	<title>Registrations Administration</title>
	<link rel="stylesheet"  href="/css/reset.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/960.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/admin.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/redmond/jquery-ui-1.8.17.custom.css"  type="text/css" />
	<script type="text/javascript" src="/js/jquery-1.7.1.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-1.8.18.custom.min.js"></script>
	<script type="text/javascript" src="/js/jquery.validate.min.js"></script>

</head>
<body>
	<div class="container_12">
		<div class="grid_12" id="header"><h1>CPAP Wholesale Administration</h1></div>
		<div id="nav" class="grid_12">
			<p style="float:left;display:inline;">
				<a href="/admin/index">Home</a>
			</p>
			<p style="float:right;">
				<a href="/admin/index/login">Login</a> | <?=date('F d, Y')?>
			</p>
			<hr />
		</div>
	</div>
	<div class="clear"></div>
	<div class="container_12" id="main">
		<?PHP Application::flash(); ?>
		<?PHP $this->render(get_class($this), $this->controller_method); ?>
	</div>
	
	<div class="container_12">
		<div class="grid_12"><div id="footer_admin">&copy; <?=date('Y')?> CPAP Supplies.</div></div>
	</div>
    <div class="clear"></div>
</body>
</html>
<?PHP IF (DEBUG): ?>
	<pre style="margin-top:100px;"><?PHP print_r($this); ?></pre>
	<pre><?PHP print_r($_POST); ?></pre>
	<pre><?PHP print_r($_SESSION); ?></pre>
<?PHP ENDIF;?>