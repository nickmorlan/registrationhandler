<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <title><?=$this->registry['sub_title']?> &middot; Registration Handler</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Google Font: Open Sans -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/bootstrap.datetimepicker.css">

  <!-- App CSS -->
  <link rel="stylesheet" href="/css/mvpready-admin.css">
  <link rel="stylesheet" href="/css/mvpready-landing.css">
  <link rel="stylesheet" href="/css/mvpready-flat.css">
  <link rel="stylesheet" href="/css/animate.css">
  <link href="/css/rh.css" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>

<body class=" ">

<div id="wrapper">

  <header class="navbar navbar-inverse" role="banner">

    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-cog"></i>
        </button>

        <a href="/" class="navbar-brand navbar-brand-img">
          <img src="/images/site/logo.png" alt="Registration Handler">
        </a>
      </div> <!-- /.navbar-header -->
     <nav class="collapse navbar-collapse" role="navigation">

        <ul class="nav navbar-nav navbar-right mainnav-menu">

          <li class="" id="navlihome">
            <a href="/">Home</a>
          </li>    

        <!--  <li class="">
            <a href="./page-features.html">Features</a>
          </li>      

          <li class="">
            <a href="./page-faq.html">FAQ</a>
          </li> -->

          <li class="" id="navlipricing">
            <a href="/pricing">Pricing</a>
          </li>   

          <li class="" id="navlicontact">
            <a href="/contact">Contact</a>
          </li>     

          <!-- <li class="dropdown">

            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            Dropdown
            <i class="mainnav-caret"></i>
            </a>

            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="./page-notifications.html">
                <i class="fa fa-bell"></i> 
                &nbsp;&nbsp;Notifications
                </a>
              </li>     

              <li>
                <a href="./components-icons.html">
                <i class="fa fa-smile-o"></i> 
                &nbsp;&nbsp;Font Icons
                </a>
              </li> 

            </ul>

          </li> -->

          <li>
            <a href="/member/login">Login</a>
          </li>    

        </ul>

        <ul class="nav navbar-nav navbar-social navbar-left">   

 
        </ul>

      </nav>


    </div> <!-- /.container -->

  </header>



    <div class="masthead">
      
      <div class="container">

        <h1 class="masthead-subtitle">
          <?=$this->registry['sub_title']?>
        </h1>
        

      </div> <!-- /.container -->

    </div> <!-- /.masthead -->



    <div class="content">

      <div class="container" style="min-height:500px;">
      <?PHP Application::flash(); ?>
      <?PHP $this->render(get_class($this), $this->view); ?>
      </div> <!-- /.container -->     

    </div> <!-- /.content -->


</div> <!-- /#wrapper -->

<footer class="footer">

  <div class="container">
      
      <div class="row">

        <div class="col-sm-4">
          <h4 class="content-title">
            <span>Registration Handler</span>
          </h4>        

          <p>Registration Handler is the easiest way to collect signups for youth sports, fundraisers, races or any other activity. You set up your signup form we handle the heavy lifting.</p>
        </div> <!-- /.col -->


        <div class="col-sm-4">

          <h4 class="content-title">
            <span>Twitter Feed</span>
          </h4>

          <div id="tweets"></div>
        </div> <!-- /.col -->


       <!-- <div class="col-sm-3">

          <h4 class="content-title">
            <span>Stay Connected</span>
          </h4>

          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero, nam quae veniam optio sunt exercitationem.</p>

          <br>

          <ul class="footer-social-link">
            <li>
              <a href="javascript:;" class="ui-tooltip" title="Facebook" data-placement="bottom">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li>
              <a href="javascript:;" class="ui-tooltip" title="Twitter" data-placement="bottom">
                <i class="fa fa-twitter"></i>
              </a>
            </li>
            <li>
              <a href="javascript:;" class="ui-tooltip" title="Google+" data-placement="bottom">
                <i class="fa fa-google-plus"></i>
              </a>
            </li>
          </ul>
          
        </div>-> <!-- /.col -->

        
        <div class="col-sm-4">
            
        <h4 class="content-title">
          <span>Stay Updated</span>
        </h4>

        <p>Get emails about new theme launches &amp;  future updates.</p>

          
          <div class="form-group">
            <!-- <label>Email: <span class="required">*</span></label> -->
            <input class="form-control" id="newsletter_email" name="newsletter_email" type="text" value="" required="" placeholder="Email Address">
          </div> <!-- /.form-group -->

          <div class="form-group">
            <button class="btn btn-transparent">Subscribe Me</button>
          </div> <!-- /.form-group -->


      </div> <!-- /.col -->

      </div> <!-- /.row -->

  </div> <!-- /.container -->

</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Core JS <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->

<script src="/js/libs/jquery-1.10.2.min.js"></script>
 
<script src="/js/libs/bootstrap.min.js"></script>

<!--[if lt IE 9]>
<script src="./js/libs/excanvas.compiled.js"></script>
<![endif]-->

<!-- App JS -->
<script src="/js/mvpready-core.js"></script>
<script src="/js/mvpready-landing.js"></script>
<script src="/js/plugins/tweetable/tweetable.jquery.min.js"></script>
<script>
 var rh = {};
 <?PHP IF(!empty($this->registry['js_data'])) : ?>
  rh.data = <?=$this->registry['js_data']?>
 <?PHP ENDIF; ?>
 
 if($('#flash.success').length > 0) 
  $('#flash.success').delay(5000).slideToggle('fast');
 if($('#flash.status').length > 0) 
  $('#flash.status').delay(5000).slideToggle('fast');
 
 $('#navli<?=$this->registry['nav_marker']?>').addClass('active');
</script>

<!-- Plugin JS -->
<? $this->load_registry_js_files() ?>


<?PHP IF (DEBUG): ?>
  <pre style="margin-top:50px;"><?PHP print_r($this); ?></pre>
  <pre>POST <?PHP print_r($_POST); ?></pre>
  <pre>SESSION <?PHP echo session_id() . ' '; print_r($_SESSION);?></pre>
<pre>COOKIE <?PHP print_r($_COOKIE); ?></pre>
<?PHP ENDIF;?>
</body>
</html>