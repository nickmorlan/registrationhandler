<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <title>Accept Online Registrations with Registration Handler</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Google Font: Open Sans -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">

  <!-- App CSS -->
  <link rel="stylesheet" href="/css/mvpready-landing.css">
  <link rel="stylesheet" href="/css/mvpready-flat.css">
  <link rel="stylesheet" href="/css/animate.css">
  <link href="/css/rh.css" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="/favicon.ico">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>

<body class=" ">

<div id="wrapper">

  <header class="navbar navbar-inverse" role="banner">

    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-cog"></i>
        </button>

        <a href="/member" class="navbar-brand navbar-brand-img">
          <img src="/images/site/logo.png" alt="Registration Handler">
        </a>
      </div> <!-- /.navbar-header -->
     <nav class="collapse navbar-collapse" role="navigation">

        <ul class="nav navbar-nav navbar-right mainnav-menu">

          <li class="">
            <a href="/">Home</a>
          </li>    

          <!--<li class="">
            <a href="/features">Features</a>
          </li>      

          <li class="">
            <a href="/faq">FAQ</a>
          </li>-->

          <li class="">
            <a href="/pricing">Pricing</a>
          </li>

          <li class="">
            <a href="/contact">Contact</a>
          </li>     

          <!-- <li class="dropdown">

            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            Dropdown
            <i class="mainnav-caret"></i>
            </a>

            <ul class="dropdown-menu" role="menu">
              <li>
                <a href="./page-notifications.html">
                <i class="fa fa-bell"></i> 
                &nbsp;&nbsp;Notifications
                </a>
              </li>     

              <li>
                <a href="./components-icons.html">
                <i class="fa fa-smile-o"></i> 
                &nbsp;&nbsp;Font Icons
                </a>
              </li> 

            </ul>

          </li> -->

          <li>
            <a href="/member/login">Login</a>
          </li>    

        </ul>

        <ul class="nav navbar-nav navbar-social navbar-left">   

 
        </ul>

      </nav>


    </div> <!-- /.container -->

  </header>

   <div class="masthead">
      
      <div class="container">
            <!-- starts carousel -->

        <div id="masthead-carousel" class="carousel masthead-carousel slide">
          <div class="carousel-inner">

            <div class="item active">

              <div class="row">

                <div class="col-md-6 masthead-text animated fadeInDownBig">
                  <h1 class="masthead-title">Easy Online Registration</h1>

                  <p class="masthead-">
                  Quickly setup your event, sports league, fundraiser or any other activity that you need to collect registration information.  Collect payments securely online without having to worry about setting up a merchant account. Use pre-defined forms or customize them to collect all of your required information.
                  </p>

                  <br>

                  <div class="masthead-actions">
                    

                    <a href="/member/register" class="btn btn-primary btn-jumbo">
                    Create an Account
                    </a>
                  </div> <!-- /.masthead-actions -->
                </div> <!-- /.masthead-text -->

                <div class="col-md-6 masthead-img animated fadeInUpBig">
                  <form class="form masthead-form well" method="post" action="/member/register">

                    <h3 class="text-center">Sign Up, It's Free!</h3>

                    <p class="text-center">No contracts, no hassles, standard credit card processing fees apply.</p>

                    <br>

                    <div class="form-group">
                      <!-- <label class="" for="full_name">Full Name</label> -->
                      <input type="text" name="name" placeholder="Your Name" class="form-control">
                    </div> <!-- /.form-group -->

                    <div class="form-group">
                      <!-- <label class="" for="email_address">Email Address</label> -->
                      <input type="email" name="email" placeholder="Your Email" class="form-control">
                    </div> <!-- /.form-group -->

                    <div class="form-group">
                      <!-- <label class="" for="organization">Organization</label> -->
                      <input type="text" name="organization" placeholder="Organization Name" class="form-control">
                    </div> <!-- /.form-group -->

                    <div class="form-group">
                      <input type="hidden" name="go" value="go" />
                      <button type="submit" class="btn btn-secondary btn-jumbo btn-block">Get Started Today!</button>
                    </div> <!-- /.form-group -->

                  </form>     
               </div> <!-- /.masthead-img -->

              </div> <!-- /.row -->

            </div> <!-- /.item -->

            <div class="item">

              <div class="row">
                
                <div class="col-md-6 masthead-text animated fadeInLeftBig">
                  <h1 class="masthead-title">Built for Modern Devices</h1>

                  <p class="masthead-">
                  Built for mobile devices first. Let's face it, there is a big shift in the web usage paradigm. People are using thier phones and tablets more and more to get things done online. Don't worry though, we still play nice with PCs.
                  </p>

                  <br>

                  <div class="masthead-actions">


                    <a href="/member/register" class="btn btn-primary btn-jumbo">
                    Create an Account
                    </a>
                  </div> <!-- /.masthead-actions -->
                </div> <!-- /.masthead-text -->

                <div class="col-md-6 masthead-img animated fadeInRightBig">
                  <img src="/images/site/mobile.png" alt="slide2" class="img-responsive" />     
                 </div> <!-- /.masthead-img -->

              </div> <!-- /.row -->

            </div> <!-- /.item -->

 
          </div>  <!-- /.carousel-inner -->
          <!-- Carousel nav -->

          <div class="carousel-controls">
            <a class="carousel-control left" href="#masthead-carousel" data-slide="prev">&lsaquo;</a>
            <a class="carousel-control right" href="#masthead-carousel" data-slide="next">&rsaquo;</a>
          </div>
            
        </div> <!-- /.masthead-carousel -->

        </div>

    </div> <!-- /.masthead -->



  <div class="content">

    <div class="container">

    	<?PHP Application::flash(); ?>
		  <?PHP $this->render(get_class($this), $this->view); ?>

    </div> <!-- /.container -->

  </div> <!-- .content -->

</div> <!-- /#wrapper -->

<footer class="footer">

  <div class="container">
      
      <div class="row">

        <div class="col-sm-4">
          <h4 class="content-title">
            <span>Registration Handler</span>
          </h4>        

          Registration Handler is the easiest way to collect signups for youth sports, fundraisers, races or any other activity. You set up your signup form we handle the heavy lifting.
        </div> <!-- /.col -->


        <div class="col-sm-4">

          <h4 class="content-title">
            <span>Twitter Feed</span>
          </h4>

          <div id="tweets"></div>
        </div> <!-- /.col -->


       <!-- <div class="col-sm-3">

          <h4 class="content-title">
            <span>Stay Connected</span>
          </h4>

          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Libero, nam quae veniam optio sunt exercitationem.</p>

          <br>

          <ul class="footer-social-link">
            <li>
              <a href="javascript:;" class="ui-tooltip" title="Facebook" data-placement="bottom">
                <i class="fa fa-facebook"></i>
              </a>
            </li>
            <li>
              <a href="javascript:;" class="ui-tooltip" title="Twitter" data-placement="bottom">
                <i class="fa fa-twitter"></i>
              </a>
            </li>
            <li>
              <a href="javascript:;" class="ui-tooltip" title="Google+" data-placement="bottom">
                <i class="fa fa-google-plus"></i>
              </a>
            </li>
          </ul>
          
        </div>-> <!-- /.col -->

        
        <div class="col-sm-4">
            
        <h4 class="content-title">
          <span>Stay Updated</span>
        </h4>

        <p>Get emails about new launches &amp;  future updates.</p>

        <form action="/email_signup" class="form" method="post">
          
          <div class="form-group">
            <!-- <label>Email: <span class="required">*</span></label> -->
            <input class="form-control" id="newsletter_email" name="newsletter_email" type="email" value="" required placeholder="Email Address">
          </div> <!-- /.form-group -->

          <div class="form-group">
            <input type="submit" class="btn btn-transparent" value="Subscribe Me" />
          </div> <!-- /.form-group -->

        </form>

      </div> <!-- /.col -->

      </div> <!-- /.row -->

  </div> <!-- /.container -->

</footer>
<footer class="copyright">
  <div class="container">

    <div class="row">

      <div class="col-sm-12">
        <p>Copyright &copy; 2014 Registration Handler.</p>
      </div> <!-- /.col -->

    </div> <!-- /.row -->
    
  </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Core JS <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->

<script src="/js/libs/jquery-1.10.2.min.js"></script>
 
<script src="/js/libs/bootstrap.min.js"></script>

<!--[if lt IE 9]>
<script src="./js/libs/excanvas.compiled.js"></script>
<![endif]-->

<!-- App JS -->
<script src="/js/mvpready-core.js"></script>
<script src="/js/mvpready-landing.js"></script>
<script>
 var rh = {};
 <?PHP IF(!empty($this->registry['js_data'])) : ?>
 	rh.data = <?=$this->registry['js_data']?>
 <?PHP ENDIF; ?>
 
 if($('#flash.success').length > 0) 
 	$('#flash.success').delay(5000).slideToggle('fast');
 if($('#flash.status').length > 0) 
 	$('#flash.status').delay(5000).slideToggle('fast');
 
 $('#navli<?=$this->registry['nav_marker']?>').addClass('active');
</script>

<!-- Plugin JS -->
<? $this->load_registry_js_files() ?>


<?PHP IF (DEBUG): ?>
	<pre style="margin-top:50px;"><?PHP print_r($this); ?></pre>
	<pre>POST <?PHP print_r($_POST); ?></pre>
	<pre>SESSION <?PHP echo session_id() . ' '; print_r($_SESSION);?></pre>
  <pre>COOKIE <?PHP print_r($_COOKIE); ?></pre>
<?PHP ENDIF;?>
</body>
</html>