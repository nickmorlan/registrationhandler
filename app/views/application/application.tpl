﻿<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/Organization">
<head>
	<meta charset="utf-8" />
	<meta itemprop="name" content="adsfdsaf">
	<meta itemprop="keywords" content="<?=(empty($this->registry['meta_keywords'])) ? SITE_META_KEYWORDS: $this->registry['meta_keywords']?>" />
	<meta itemprop="description" content="<?=(empty($this->registry['meta_desc'])) ? SITE_META_DESCRIPTION: $this->registry['meta_desc']?>" />
	<title><?=(empty($this->registry['title_tag'])) ? "Registrations" : $this->registry['title_tag']?></title>
	<link rel="stylesheet"  href="/css/reset.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/960.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/app.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/freshbutton.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/redmond/jquery-ui-1.10.3.custom.min.css"  type="text/css" />
	<?=implode(' ', $this->registry['css_files'])?>
	<script type="text/javascript" src="/js/iefixes.js"></script>
	<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
	<?=implode(' ', $this->registry['js_files'])?>
	<?PHP IF(get_class($this) == 'MemberController'): ?><script type="text/javascript" src="https://js.stripe.com/v2/"></script><?PHP ENDIF; ?>
	<!--[if lt IE 8]>
	<style type="text/css" media="screen">
	html {filter:gray;}
	body {margin-top:0;}
	#browseSad {background:#000;border-bottom:10px solid #fff;left:0;top:0;width:100%;}
	#browseSad * {color:#fff;text-align:center;}
	#browseSad div {margin:24px 48px;}
	#browseSad h1 {font-size:21px;margin:0 0 5px;}
	#browseSad h2 {font-size:16px;margin:0 0 10px;}
	#browseSad p {font-size:14px;margin:0;}
	#browseSad a {font-weight:bold;text-decoration:underline;}
	</style>
	<div id="browseSad"><div>
	<h1>If you can see this message, you are using Internet Explorer 6! It is time for an update!</h1>
	<h2>We are sorry, but Internet Explorer 6 is so old that we cannot support it anymore.</h2>
	<p>For the best possible experience, please <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home" title="Upgrade to the latest version of IE" target="_blank">update your browser</a>, or why not switch to a more <a href="http://www.mozilla.com/en-US/firefox/fx/" title="Upgrade to the latest version of Firefox" target="_blank">web compliant browser.</a></p>
	</div></div>
	<![endif]-->
</head>
<body>
	<div id="header">
		<div class="container_12">
			<div class="grid_12">
				<a href="/" title="Online Registrations"><img src="/images/site/logo.png" alt="Online Registrations"  /></a>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="container_12" id="main">
		<?PHP Application::flash(); ?>
		<?PHP $this->render(get_class($this), $this->view); ?>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
	<div id="footer">
		<div class="container_12">
			<div class="grid_12">
				<?PHP IF(empty($this->registry['member'])): ?>
					<p align="center"><a href="/member/login" title="Member Login">Log In</a></p>
				<?PHP ENDIF; ?>
				<p align="center">&copy; 2012-<?=date('Y')?> r5kmedia • All Rights Reserved. <a href="/terms" title="RegistrationHandler Terms and Conditions">Terms and Conditions</a> | <a href="privacy" title="RegistrationHandler Pricay Policy">Privacy Policy</a></p>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	
	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-51803023-1', 'auto');
	  ga('send', 'pageview');

	</script>
	<noscript>
		<div id="no_script_warning" class="alert">
			<p><strong>WARNING:</strong> Your browser does not have JavaScript enabled. The site relies heavily on JavaScript.<p>
			<p>The Help portion of the toolbar on most browsers will tell you how to enable and disable JavaScript.
		</div>
	</noscript>
</body>
</html>
<?PHP IF (DEBUG): ?>
<pre style="margin-top:50px;"><?PHP print_r($this); ?></pre>
<pre>POST <?PHP print_r($_POST); ?></pre>
<pre>SESSION <?PHP echo session_id() . ' '; print_r($_SESSION);?></pre>
<pre>COOKIE <?PHP print_r($_COOKIE); ?></pre>
<?PHP ENDIF;?>
