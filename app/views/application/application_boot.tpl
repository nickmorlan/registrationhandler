<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html lang="en" class="no-js"> <!--<![endif]-->
<head>
  <title>Memeber Dashboard &middot; Registration Handler</title>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <!-- Google Font: Open Sans -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,400italic,600,600italic,800,800italic">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Oswald:400,300,700">

  <!-- Font Awesome CSS -->
  <link rel="stylesheet" href="/css/font-awesome.min.css">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/bootstrap.datetimepicker.css">

  <!-- App CSS -->
  <link rel="stylesheet" href="/css/mvpready-admin.css">
  <link rel="stylesheet" href="/css/mvpready-flat.css">
  <link href="/css/rh.css" rel="stylesheet">

  <!-- Favicon -->
  <link rel="shortcut icon" href="favicon.ico">

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
  <![endif]-->
</head>

<body class=" ">

<div id="wrapper">

  <header class="navbar navbar-inverse" role="banner">

    <div class="container">

      <div class="navbar-header">
        <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <i class="fa fa-cog"></i>
        </button>

        <a href="/member" class="navbar-brand navbar-brand-img">
          <img src="/images/site/logo.png" alt="Registration Handler">
        </a>
      </div> <!-- /.navbar-header -->


      <nav class="collapse navbar-collapse" role="navigation">

        <ul class="nav navbar-nav noticebar navbar-left">
          <?=$this->registry['member']->build_member_notification_menu_items()?>
          <?=$this->registry['member']->build_member_message_menu_items()?>
          <?=$this->registry['member']->build_member_alert_menu_items()?>
        </ul>


        <ul class="nav navbar-nav navbar-right">    


          <!--<li>
             <div class="pull-right margin_top_5"><?PHP //$this->registry['member']->display_member_thumb()?></div>
          </li>-->   

          <li class="dropdown navbar-profile">
            <a class="dropdown-toggle" data-toggle="dropdown" href="javascript:;">
              <i class="fa fa-user fa-2x"></i>
              <i class="fa fa-caret-down"></i>
            </a>

            <ul class="dropdown-menu" role="menu">

              <li>
                <a href="/member/edit">
                  <i class="fa fa-user"></i> 
                  &nbsp;&nbsp;My Account
                </a>
              </li>

              <li class="divider"></li>

              <li>
                <a href="/member/logout">
                  <i class="fa fa-sign-out"></i> 
                  &nbsp;&nbsp;Logout
                </a>
              </li>

            </ul>

          </li>

        </ul>

      </nav>

    </div> <!-- /.container -->

  </header>


  <div class="mainnav">

    <div class="container">

      <a class="mainnav-toggle" data-toggle="collapse" data-target=".mainnav-collapse">
        <span class="sr-only">Toggle navigation</span>
        <i class="fa fa-bars"></i>
      </a>

      <nav class="collapse mainnav-collapse" role="navigation">


        <ul class="mainnav-menu">

          <li class="dropdown active">
            <a href="/member">
            Dashboard
            </a>
          </li>

          <li class="dropdown">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown">
            Mailings
            <i class="mainnav-caret"></i>         
            </a>
           <ul class="dropdown-menu" role="menu">

              <li>
                <a href="/communication">
                <i class="fa fa-institution"></i> 
                &nbsp;&nbsp;Mailing Overview
                </a>
              </li>
             <li>
             <li>
                <a href="/communication/compose">
                <i class="fa fa-envelope"></i> 
                &nbsp;&nbsp;Compose Message
                </a>
              </li>
             <li>
                <a href="/communication/manage">
                <i class="fa fa-group"></i> 
                &nbsp;&nbsp;Manage Lists
                </a>
              </li>
            </ul>
          </li>
          <li class="dropdown">
            <a href="/report">
            Reports
            </a>
          </li>


          </ul>
      </nav>

    </div> <!-- /.container -->

  </div> <!-- /.mainnav -->

  <div class="content">

    <div class="container">
      <?PHP Application::flash(); ?>
      <?PHP $this->render(get_class($this), $this->view); ?>

    </div> <!-- /.container -->

  </div> <!-- .content -->

</div> <!-- /#wrapper -->

<footer class="footer">
  <div class="container">
    <p class="pull-left">Copyright &copy; 2013 - 2014 r5k Media.</p>
  </div>
</footer>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Core JS <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script> -->

<script src="/js/libs/jquery-1.10.2.min.js"></script>
 
<script src="/js/libs/bootstrap.min.js"></script>

<!--[if lt IE 9]>
<script src="./js/libs/excanvas.compiled.js"></script>
<![endif]-->

<!-- App JS -->
<script src="/js/mvpready-core.js"></script>
<script src="/js/mvpready-admin.js"></script>
<script>
 var rh = {};
 <?PHP IF(!empty($this->registry['js_data'])) : ?>
  rh.data = <?=$this->registry['js_data']?>
 <?PHP ENDIF; ?>
 
 if($('#flash.success').length > 0) 
  $('#flash.success').delay(5000).slideToggle('fast').delay(300).remove();
// if($('#flash.status').length > 0) 
 // $('#flash.status').delay(5000).slideToggle('fast');
 
</script>

<!-- Plugin JS -->
<? $this->load_registry_js_files() ?>


<?PHP IF (DEBUG): ?>
  <pre style="margin-top:50px;"><?PHP print_r($this); ?></pre>
  <pre>POST <?PHP print_r($_POST); ?></pre>
  <pre>SESSION <?PHP echo session_id() . ' '; print_r($_SESSION);?></pre>
<pre>COOKIE <?PHP print_r($_COOKIE); ?></pre>
<?PHP ENDIF;?>
</body>
</html>