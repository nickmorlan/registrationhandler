﻿<!DOCTYPE html>
<html lang="en" itemscope itemtype="http://schema.org/Organization">
<head>
	<meta charset="utf-8" />
	<meta itemprop="name" content="adsfdsaf">
	<meta itemprop="keywords" content="<?=(empty($this->registry['meta_keywords'])) ? SITE_META_KEYWORDS: $this->registry['meta_keywords']?>" />
	<meta itemprop="description" content="<?=(empty($this->registry['meta_desc'])) ? SITE_META_DESCRIPTION: $this->registry['meta_desc']?>" />
	<title><?=(empty($this->registry['title_tag'])) ? SITE_META_TITLE : $this->registry['title_tag']?></title>
	<link rel="stylesheet"  href="/css/reset.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/960.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/app.css"  type="text/css" />
	<link rel="stylesheet"  href="/css/redmond/jquery-ui-1.10.3.custom.min.css"  type="text/css" />
	<link rel="stylesheet" href="/css/nivo-slider/nivo-slider.css" type="text/css" />
    <link rel="stylesheet" href="/css/nivo-slider/themes/default/default.css" type="text/css" media="screen" />
	<link rel="stylesheet"  href="/css/freshbutton.css"  type="text/css" />
	<!--[if IE 6]>
		<style type="text/css" media="screen">
		html {filter:gray;}
		body {margin-top:0;}
		#browseSad {background:#000;border-bottom:10px solid #fff;left:0;top:0;width:100%;}
		#browseSad * {color:#fff;text-align:center;}
		#browseSad div {margin:24px 48px;}
		#browseSad h1 {font-size:21px;margin:0 0 5px;}
		#browseSad h2 {font-size:16px;margin:0 0 10px;}
		#browseSad p {font-size:14px;margin:0;}
		#browseSad a {font-weight:bold;text-decoration:underline;}
		</style>
		<div id="browseSad"><div>
		<h1>If you can see this message, you are using Internet Explorer 6! It is time for an update!</h1>
		<h2>We are sorry, but Internet Explorer 6 is so old that we cannot support it anymore.</h2>
		<p>For the best possible experience, please <a href="http://windows.microsoft.com/en-US/internet-explorer/products/ie/home" title="Upgrade to the latest version of IE" target="_blank">update your browser</a>, or why not switch to a more <a href="http://www.mozilla.com/en-US/firefox/fx/" title="Upgrade to the latest version of Firefox" target="_blank">web compliant browser.</a></p>
		</div></div>
	<![endif]-->
	<!--[if lt IE 9]>
		<style type="text/css" media="screen">
			form#email_capture input[name=email]{padding-top:11px;height:27px;}
		</style>
	<![endif]-->
	</head>
<body>
	<div id="header">
		<div class="container_12">
			<div class="grid_6">
				<img src="/images/site/logo.png" alt="" style="margin-left:-30px;" />
			</div>
			<div class="prefix_4 grid_2" style="margin-top:10px;text-align:right;">
				<p><a id="login" class="freshbutton-green" title="member_login">Log In</a></p>
			</div>
			
		</div>
	</div>
	<div id="splash">
		<div class="container_12">
			<div class="grid_7">
				<img src="/images/site/laptop1.png" alt="Easy Sports Registrations" style="margin-top:10px;margin-left:-30px;" />
			</div>
			<div class="grid_5">
				<img src="/images/site/splashtext.png" alt="Easy Sports Registrations" style="margin-top:0px;margin-left:-30px;" />
				<form id="email_capture" style="display:inline;" action="/member/register" method="post">
					<input type="hidden" name="from" value="index" />
					<input type="hidden" name="go" value="<?=session_id()?>" />
					<input type="input" id="email" name="email"  value="Enter Email" onfocus="if(this.value=='Enter Email'){this.value=''}" onblur="if(this.value==''){this.value='Enter E-mail'}" /><a href="" id="signup_link"></a>
				</form>
			</div>
		</div>
	</div>
	<div id="splash_main">
		<div class="container_12">
				<?PHP Application::flash(); ?>
				<?PHP $this->render(get_class($this), $this->view); ?>
				<div class="clear"></div>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div id="splash_stripe">
		<div class="container_12">
			<div class="grid_12">
				<blockquote>Hands down the easiest way to manage my registrations for our youth league.  Saves me countless hours and simplifies our registration process tremendously.<cite>Nick M.</cite></blockquote>
			</div>
		</div>
	</div>
	<div id="footer" style="margin-top:0px;">
		<div class="container_12">
			<div class="grid_1">
				<p><a href="/pricing" title="Online Registration Pricing">Pricing</a></p>
			</div>
			<div class="grid_1">
				<p><a href="/benefits" title="Online Registration Benefits">Benefits</a></p>
			</div>
			<div class="grid_1">
				<p><a href="/features" title="Online Registration Features">Features</a></p>
			</div>
			<div class="grid_2">
				<p><a href="/online_youth_football_registrations" title="Online Youth Football Registration">Online Football Registration</a></p>
			</div>
			<div class="prefix_6 grid_1">
				<p align="right"><a href="/member/login" title="Member Login">Log In</a></p>
			</div>
			<div class="grid_12">
				<p align="center">&copy; 2012-<?=date('Y')?> r5kmedia • All Rights Reserved. <a href="/terms" title="RegistrationHandler Terms and Conditions">Terms and Conditions</a> | <a href="privacy" title="RegistrationHandler Pricay Policy">Privacy Policy</a></p>
			</div>
		</div>
	</div>
	<div class="clear"></div>
	<div class="info_box" style="height:210px;z-index:9999;" id="dialog" title="Member Login">
		<form action="/member/login" id="login_form" name="login_form" method="post">
			<p style="padding:10px 10px 0 10px;">
				<label for="email">Email</label><br />
				<?=HTML::text_field($this->registry['member'], 'email')?>
			</p>
			<p style="padding:0 10px;">
				<label for="password">Password</label><br />
				<input type="password" name="password" value="" />
			</p>
			<p style="padding:0 10px;"><input type="submit" name="submit" value="Log In" class="freshbutton-lightblue" /></p>
		</form>
	</div>

	<noscript>
		<div id="no_script_warning" class="alert">
			<p><strong>WARNING:</strong> Your browser does not have JavaScript enabled. The CPAP Wholesale store relies heavily on JavaScript.<p>
			<p>The Help portion of the toolbar on most browsers will tell you how to enable and disable JavaScript.
			<p>Otherwise please call us Monday through Friday 9am - 5pm ET at 888-598-8515 and we will be glad to help you out over the phone.</p>
		</div>
	</noscript>
	<script type="text/javascript" src="/js/jquery-1.10.2.min.js"></script>
	<script type="text/javascript" src="/js/jquery-ui-1.10.3.custom.min.js"></script>
	<script type="text/javascript" src="/js/jquery.validate.min.js"></script>
    <script type="text/javascript">
		$('#signup_link').on('click', function(e) { e.preventDefault();$('#email_capture').submit(); });
		
		$( "#dialog" ).dialog({
			autoOpen: false,
			show: {
				effect: "blind",
				duration: 200
			},
			hide: {
				effect: "explode",
				duration: 100
			},
			width: 400
		});   
		$('#login').on('click', function() { $( "#dialog" ).dialog( "open" );});
		</script>
</body>
</html>
<?PHP IF (DEBUG): ?>
<pre style="margin-top:50px;"><?PHP print_r($this); ?></pre>
<pre>POST <?PHP print_r($_POST); ?></pre>
<pre>SESSION <?PHP echo session_id() . ' '; print_r($_SESSION);?></pre>
<pre>COOKIE <?PHP print_r($_COOKIE); ?></pre>
<?PHP ENDIF;?>
