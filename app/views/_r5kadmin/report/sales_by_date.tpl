<div class="grid_12">
	<center><h4>Sales Report By Date Range</h4></center>
	<hr />
</div>
<div class="grid_12">
	<div id="tabs">
		<form id="report_form" method="post" action="/admin/report/sales_by_date">
		<p><center>Enter a date or range, or select an option from the dropdown menu.</center></p>
		<p><center><label for="start_date">Start Date:&nbsp;</label><input type="text" name="start_date" id="start_date" class="required" value="<?=$this->registry['notice']->start_date?>" style="width:95px;" />&nbsp;&nbsp;<label for="end_date">End Date:&nbsp;</label><input type="text" name="end_date" id="end_date" class="required" value="<?=$this->registry['notice']->end_date?>" style="width:95px;" />&nbsp;&nbsp;
		<label for="date_select">View:&nbsp;</label>
		<select name='date_select' id='date_select'>
			<option value=''>&lt;no selection&gt;</option>
			<option value='last_seven_days'>last 7 days</option>
			<option value='this_month'>this month</option>
			<option value='last_month'>last month</option>
		</select></center></p>

		<p><center><?=$this->registry['site_select_menu']?></center></p>

		<p><center><input type="submit" name="submit" value="Generate Report" /></center></p>
		</form>
	</div>
</div>
<hr />
<div class="grid_12">
	<?PHP IF(empty($this->registry['orders'])) : ?>
		<?PHP IF(empty($this->registry['no_results_message'])) : ?>
			<p><em>No orders have been selected.</em></p>
		<?PHP ELSE: ?>
			<p><em><?=$this->registry['no_results_message'] ?></em></p>
		<?PHP ENDIF ?>
	<?PHP ELSE: ?>
		<h3><center>Sales Report for <?=$this->registry['range']?><?PHP IF (!empty($this->registry['site_name'])) : ?> - <?=$this->registry['site_name']?><?PHP ELSE : ?> - All Sites<?PHP ENDIF ?></center></h3>
		<form id="export_to_excel" method="post" action="/admin/report/excel_sales_by_date">
		<input type="hidden" name="start_date" id="start_date" value="<?=$this->registry['start_date'] ?>" />
		<input type="hidden" name="end_date" id="end_date" value="<?PHP IF (!empty($this->registry['end_date'])) : ?><?=$this->registry['end_date'] ?><?PHP ENDIF ?>" />
		<input type="hidden" name="site_name" id="site_name" value="<?PHP IF (!empty($this->registry['site_name'])) : ?><?=$this->registry['site_name'] ?><?PHP ENDIF ?>" />

		<p><center><input type="submit" name="submit" value="Export To Excel" /></center></p>
		</form>
		<p>(Note: The calculation for margin and net profit does not include shipping or sales tax.)</p>
		<div class="tabs margin_bottom">
			<table width="100%">
			<th><p>Order Number</p></th>
			<th><p>Date</p></th>
			<th><p>Subtotal</p></th>
			<th><p>Shipping</p></th>
			<th><p>Sales Tax (State)</p></th>
			<th><p>Total</p></th>
			<th><p>Cost</p></th>
			<th><p>Net Profit</p></th>
			<th><p>Margin</p></th>
			<th style="white-space:nowrap"></th>
			<?PHP FOREACH($this->registry['orders'] as $o) :?>
				<?=HTML::alternate_tr_colors('ccddea');?>
					<td><p><?=$o['id']?></p></td>
					<td><p><?=date('F j, Y', strtotime($o['date']))?></p></td>
					<td><p>$<?=number_format($this->registry['order_subtotals'][$o['id']], 2)?></p></td>
					<td><p>$<?=number_format($o['ship_cost'], 2)?></p></td>
					<td><p>$<?=number_format($o['sales_tax'], 2)?></p></td>
					<td><p>$<?=number_format($this->registry['order_totals'][$o['id']], 2)?></p></td>

					<?PHP IF ($this->registry['order_cost_totals'][$o['id']] > 0) : ?>

						<td><p>$<?=number_format($this->registry['order_cost_totals'][$o['id']], 2)?></p></td>
						<td><p>$<?=number_format($this->registry['order_subtotals'][$o['id']] -  $this->registry['order_cost_totals'][$o['id']], 2)?></p></td>
						<td><p><?=round(($this->registry['order_subtotals'][$o['id']] -  $this->registry['order_cost_totals'][$o['id']])/$this->registry['order_subtotals'][$o['id']], 2)?></p></td>

					<?PHP ELSE : ?>

						<td><p>(N/A)</p></td>
						<td><p>(N/A)</p></td>
						<td><p>(N/A)</p></td>

					<?PHP ENDIF; ?>


					<td><p><a href="/admin/report/view_order/<?PHP echo $o['id'];?>">View</a></p></td>
				</tr>
			<?PHP ENDFOREACH; ?>
			<td><p><strong>TOTALS</strong></p></td>
			<td></td>
			<td><p><strong>$<?=number_format($this->registry['subtotals'], 2)?></p></strong></td>
			<td><p><strong>$<?=number_format($this->registry['shipping'], 2)?></p></strong></td>
			<td><p><strong>$<?=number_format($this->registry['tax'], 2)?></p></strong></td>
			<td><p><strong>$<?=number_format($this->registry['sales'], 2)?></p></strong></td>

			<?PHP IF ($this->registry['total_costs'] > 0) : ?>

				<td><p><strong>$<?=number_format($this->registry['total_costs'], 2)?></strong></p></td>
				<td><p><strong>$<?=number_format($this->registry['subtotals'] -  $this->registry['total_costs'], 2)?></strong></p></td>
				<td><p><strong><?=round(($this->registry['subtotals'] -  $this->registry['total_costs'])/$this->registry['subtotals'], 2)?></strong></p></td>
			<?PHP ELSE : ?>

				<td><p><strong>(N/A)</strong></p></td>
				<td><p><strong>(N/A)</strong></p></td>
				<td><p><strong>(N/A)</strong></p></td>

			<?PHP ENDIF; ?>

			</table>
		</div>
	<?PHP ENDIF;?>
	<?PHP IF($this->controller_method == 'index') AdminHelper::build_admin_pagination_links($this->registry['paginator']); ?>
</div>


<script type="text/javascript">
	$(document).ready(function() {
		// add calendars to date fields
		$("#start_date").datepicker({
			showAnim: 'blind'
		});
		$("#end_date").datepicker({
			showAnim: 'blind'
		});
	});

	function format_date(d) {
		var d_date = d.getDate();
		var d_month = d.getMonth();
		d_month++;
		var d_year = d.getFullYear();
		if (d_date < 10) {
			d_date = '0' + d_date;
		}
		if (d_month < 10) {
			d_month = '0' + d_month;
		}
		return d_month + "/" + d_date + "/" + d_year;
	}

	$('#date_select').change(function() {
		if($('#date_select').val() == 'last_seven_days') {
			var d1 = new Date();
			with(d1) {
				setDate(getDate() - 6);
			}
			$('#start_date').val(format_date(d1));
			var d2 = new Date();
			$('#end_date').val(format_date(d2));
		}
		if($('#date_select').val() == 'this_month') {
			var d1 = new Date();
			with(d1) {
				setDate(1);
			}
			$('#start_date').val(format_date(d1));
			var d2 = new Date();
			$('#end_date').val(format_date(d2));
		}
		if($('#date_select').val() == 'last_month') {
			var d1 = new Date();
			with(d1) {
				setDate(1);
				setMonth(getMonth() - 1);
			}
			$('#start_date').val(format_date(d1));
			var d2 = new Date();
			with(d2) {
				setDate(0);
			}
			$('#end_date').val(format_date(d2));
		}
		if($('#date_select').val() == '') {
			$('#start_date').val('');
			$('#end_date').val('');
		}
	});
</script>