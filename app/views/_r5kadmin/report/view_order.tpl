<div class="grid_12">
	<h1><center>Order Number <?=$this->registry['order']->id?><br />Placed on <?=date('F j, Y', strtotime($this->registry['order']->date))?></center></h1>
</div>
<hr />
<div class="grid_5">
	<div class="section_header">
		<h2>Order Details</h2>
	</div>
	<div class="clear"></div>
	<hr />
	<h3>Billing Information</h3>
	<p><?=$this->registry['order']->display_billing_address()?></p>
	<?PHP IF ($this->registry['order']->po_number != "") : ?>
		<p><strong>PO Number:</strong> <?=$this->registry['order']->po_number?></p>
	<?PHP ENDIF; ?>
	<hr />
	<h3>Shipping Information</h3>
	<?PHP IF ($this->registry['order']->gift_order == "y") : ?>
		<p><strong><em>This order is a gift.</em></strong></p>
	<?PHP ENDIF ?>
	<p><?=$this->registry['order']->display_shipping_address()?><br />
		<strong>via:</strong> <?=$this->registry['order']->ship_via?>
	</p>
	<?PHP IF (!empty($this->registry['order']->gift_note)) : ?>
		<hr />
		<h3>Gift note:</h3><p><?=nl2br($this->registry['order']->gift_note)?></p>
	<?PHP ENDIF ?>
	<?PHP IF (!empty($this->registry['order']->special_shipping)) : ?>
		<hr />
		<h3>Special instructions:</h3><p><?=nl2br($this->registry['order']->special_shipping)?></p>
	<?PHP ENDIF ?>
</div>
<div class="grid_7">
	<div class="section_header">
		<h2>Order Contents</h2>
	</div>
	<table width="100%">
	<th><p>Item</p></th>
	<th><p>Price</p></th>
	<th><p>Quantity</p></th>
	<th><p>Total</p></th>
	<th style="white-space:nowrap"></th>
	<th style="white-space:nowrap"></th>
	<?PHP FOREACH ($this->registry['lineitems'] as $product): ?>
		<?=HTML::alternate_tr_colors('ccddea');?>
			<td><p><strong><?=$product['product_name']?></strong> <br />SKU: <?=$product['sku']?></p></td>
			<td><p>$<?=number_format($product['price'], 2)?></p></td>
			<td><p>x <?=$product['quantity']?></p></td>
			<td><p>$<?=number_format(floatval($product['price'])*intval($product['quantity']), 2)?></p></td>

		</tr>
	<?PHP ENDFOREACH; ?>
	</table>
	<hr />
	<p><strong>Subtotal:</strong> $<?=number_format($this->registry['order']->subtotal, 2)?></p>
	<hr />
	<?PHP IF($this->registry['order']->sales_tax > 0) : ?>
		<p>(<?=$this->registry['order']->ship_to_state ?>) <strong>Sales Tax:</strong> $<?=number_format($this->registry['order']->sales_tax, 2)?></p>
		<hr />
	<?PHP ENDIF; ?>
	<p>(<?=$this->registry['order']->ship_via?>) <strong>Shipping:</strong> $<?=number_format($this->registry['order']->ship_cost, 2)?></p>
	<hr />
	<?PHP IF($this->registry['order']->order_discount > 0) : ?>
		<p><strong>Discount:</strong> ($<?=number_format($this->registry['order']->order_discount, 2)?>)</p>
		<hr />
	<?PHP ENDIF; ?>
	<p><strong>Total:</strong> <strong class="price"><?=$this->registry['order']->order_total?></p></strong>
</div>


