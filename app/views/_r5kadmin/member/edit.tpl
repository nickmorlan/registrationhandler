<div class="grid_12">
	<div class="tabs">
		<ul>Edit Member</ul>
		<div style="padding:20px;">
		<form id="edit_member" name="edit_member" method="post" action="/admin/member/edit/<?=$this->registry['member']->id?>">
			<input type="hidden" name="id" value="<?=$this->registry['member']->id?>" />
			<?=$this->render_partial('member', 'form')?>
			<p align="right"><input type="submit" name="submit" value="Save Changes" /></p>
		</form>
		</div>
	</div>
</div>
<script type="text/javascript">
	$(document).ready(function() {
	// init submit buttons
		$( "input:submit" ).button();
		$( "input:button" ).button();
		
		$('.tabs').tabs();

	});
</script>