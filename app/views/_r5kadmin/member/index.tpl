<div class="grid_12">
	<h3>Member List</h3>

	<?PHP IF(empty($this->registry['members'])) : ?>
		<p><em>No members selected.</em> <a href="/admin/member">show all</a></p>
	<?PHP ELSE: ?>
		<p><?PHP AdminHelper::build_search_bar('members', '/admin/member/'); ?> <a href="/admin/member">show all</a><p>
		<div class="tabs margin_bottom">
			<ul>Member Table</ul>
			<table width="100%"> 
			<?PHP FOREACH($this->registry['members'] as $c) :?>
				<?=HTML::alternate_tr_colors('ccddea');?>
					<td><p><?=$c->bill_to_last_name?></p></td>
					<td><p><?=$c->bill_to_first_name?></p></td>
					<td><p><?=$c->bill_to_address?> <?=$c->bill_to_address2?></p></td>
					<td><p><?=$c->bill_to_city?></p></td>
					<td><p><?=$c->bill_to_state?></p></td>
					<td><p><?=$c->email?></p></td>
					<td><p><a href="/admin/member/edit/<?PHP echo $c->id;?>">edit</a></p></td>
					<td><p><a href="/admin/member/destroy/<?PHP echo $c->id;?>" style='color:#aa0000;' onclick="return confirm('Do you really want to delete member <?PHP echo $c->id;?>?\n\nThis action cannot be undone.')">Delete</a></p></td>
				</tr>
			<?PHP ENDFOREACH; ?>
			</table>
		</div>
	<?PHP ENDIF;?>
	<?PHP IF($this->controller_method == 'index') AdminHelper::build_admin_pagination_links($this->registry['paginator']); ?>
</div>
<script type="text/javascript">
	$(document).ready(function() {
	// init submit buttons
		$( "input:submit" ).button();
		$( "input:button" ).button();

		$('.tabs').tabs();
	});
		
</script>