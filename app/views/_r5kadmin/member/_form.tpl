<div class="left">

	<p><label for="product_discount">Product discount:</label><br />
		<input type="text" name="product_discount" value="<?=$this->registry['member']->product_discount?>" class="required" />
	</p>
	<p><label for="email">Email:</label><br />
		<input type="text" name="email" value="<?=$this->registry['member']->email?>" class="required" />
	</p>
	<p><label for="password">Password:</label><br />
		<input type="text" name="password" value="<?=$this->registry['member']->password?>" class="required" />
	</p>
	<p><label for="phone_number_1">Phone Number 1:</label><br />
		<input type="text" name="phone_number_1" value="<?=$this->registry['member']->phone_number_1?>" class="required" />
	</p>
	<p><label for="phone_number_2">Phone Number 2:</label><br />
		<input type="text" name="phone_number_2" value="<?=$this->registry['member']->phone_number_2?>" />
	</p>
	<p><label for="fax_number">Fax Number:</label><br />
		<input type="text" name="fax_number" value="<?=$this->registry['member']->fax_number?>" />
	</p>
	<p><label for="bill_to_title">(Billing) Title:</label><br />
		<input type="text" name="bill_to_title" value="<?=$this->registry['member']->bill_to_title?>" class="required" />
	</p>
	<p><label for="bill_to_first_name">(Billing) First Name:</label><br />
		<input type="text" name="bill_to_first_name" value="<?=$this->registry['member']->bill_to_first_name?>" class="required" />
	</p>
	<p><label for="bill_to_mi">(Billing) MI:</label><br />
		<input type="text" name="bill_to_mi" value="<?=$this->registry['member']->bill_to_mi?>" class="required" />
	</p>
	<p><label for="bill_to_last_name">(Billing) Last Name:</label><br />
		<input type="text" name="bill_to_last_name" value="<?=$this->registry['member']->bill_to_last_name?>" class="required" />
	</p>
	<p><label for="bill_to_organization">(Billing) Organization:</label><br />
		<input type="text" name="bill_to_organization" value="<?=$this->registry['member']->bill_to_organization?>" />
	</p>
	<p><label for="bill_to_address">(Billing) Address:</label><br />
		<input type="text" name="bill_to_address" value="<?=$this->registry['member']->bill_to_address?>" class="required" />
	</p>
	<p><label for="bill_to_address_2">(Billing) Address Line 2:</label><br />
		<input type="text" name="bill_to_address_2" value="<?=$this->registry['member']->bill_to_address_2?>" />
	</p>
	<p><label for="bill_to_address_3">(Billing) Address Line 3:</label><br />
		<input type="text" name="bill_to_address_3" value="<?=$this->registry['member']->bill_to_address_2?>" />
	</p>
	<p><label for="bill_to_city">(Billing) City:</label><br />
		<input type="text" name="bill_to_city" value="<?=$this->registry['member']->bill_to_city?>" class="required" />
	</p>
	<p><label for="bill_to_state">(Billing) State:</label><br />
		<input type="text" name="bill_to_state" value="<?=$this->registry['member']->bill_to_state?>"class="required" />
	</p>
	<p><label for="bill_to_zip">(Billing) Zip:</label><br />
		<input type="text" name="bill_to_zip" value="<?=$this->registry['member']->bill_to_zip?>" class="required" />
	</p>
	<p><label for="bill_to_country">(Billing) Country:</label><br />
		<input type="text" name="bill_to_country" value="<?=$this->registry['member']->bill_to_country?>" class="required" />
	</p>
</div>
<div class="right">
	<p><label for="ship_to_title">(Shipping) Title:</label><br />
		<input type="text" name="ship_to_title" value="<?=$this->registry['member']->ship_to_title?>" class="required" />
	</p>
	<p><label for="ship_to_first_name">(Shipping) First Name:</label><br />
		<input type="text" name="ship_to_first_name" value="<?=$this->registry['member']->ship_to_first_name?>" class="required" />
	</p>
	<p><label for="ship_to_mi">(Shipping) MI:</label><br />
		<input type="text" name="ship_to_mi" value="<?=$this->registry['member']->ship_to_mi?>" class="required" />
	</p>
	<p><label for="ship_to_last_name">(Shipping) Last Name:</label><br />
		<input type="text" name="ship_to_last_name" value="<?=$this->registry['member']->ship_to_last_name?>" class="required" />
	</p>
	<p><label for="ship_to_organization">(Shipping) Organization:</label><br />
		<input type="text" name="ship_to_organization" value="<?=$this->registry['member']->ship_to_organization?>" />
	</p>
	<p><label for="ship_to_address">(Shipping) Address:</label><br />
		<input type="text" name="ship_to_address" value="<?=$this->registry['member']->ship_to_address?>" class="required" />
	</p>
	<p><label for="ship_to_address_2">(Shipping) Address Line 2:</label><br />
		<input type="text" name="ship_to_address_2" value="<?=$this->registry['member']->ship_to_address_2?>" />
	</p>
	<p><label for="ship_to_address_3">(Shipping) Address Line 3:</label><br />
		<input type="text" name="ship_to_address_3" value="<?=$this->registry['member']->ship_to_address_3?>" />
	</p>
	<p><label for="ship_to_city">(Shipping) City:</label><br />
		<input type="text" name="ship_to_city" value="<?=$this->registry['member']->ship_to_city?>" class="required" />
	</p>
	<p><label for="ship_to_state">(Shipping) State:</label><br />
		<input type="text" name="ship_to_state" value="<?=$this->registry['member']->ship_to_state?>" class="required" />
	</p>
	<p><label for="ship_to_zip">(Shipping) Zip:</label><br />
		<input type="text" name="ship_to_zip" value="<?=$this->registry['member']->ship_to_zip?>" class="required" />
	</p>
	<p><label for="ship_to_country">(Shipping) Country:</label><br />
		<input type="text" name="ship_to_country" value="<?=$this->registry['member']->ship_to_country?>" class="required" />
	</p>


</div>
<div class="clear"></div>