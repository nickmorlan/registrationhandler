<div class="grid_7">
	<p>
		<label for="emergency_contact">Emergency Contact:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'emergency_contact')?>
	</p>
	<p>
		<label for="emergency_contact_phone">Emergency Contact Phone:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'emergency_contact_phone')?>
	</p>
	<p>
		<label for="hospital_name">Preferred Hospital:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'hospital_name')?>
	</p>
	<p>
		<label for="physician_name">Prefered Physician:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'physician_name')?>
	</p>
	<p>
		<label for="physician_phone">Physician Phone:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'physician_phone')?>
	</p>
	<p>
		<label for="dentist_name">Dentist Name:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'dentist_name')?>
	</p>
	<p>
		<label for="dentist_phone">Dentist Phone:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'dentist_phone')?>
	</p>
	<p>
		<label for="medical_history">Medical History:</label><br />
		<?=HTML::text_area($this->registry['registration'], 'medical_history')?>
	</p>
	<p>
		<label for="allergies">Allergies:</label><br />
		<?=HTML::text_area($this->registry['registration'], 'allergies')?>
	</p>
	<p>
		<label for="comments">Comments:</label><br />
		<?=HTML::text_area($this->registry['registration'], 'comments')?>
	</p>
</grid>
<div class="grid_5">
	<div class="info_box" style="background:#ffc">
		<p><strong>School Information</strong> Please let us know the school to be attended and grade level for the participant during this activity.</p>
	</div>
</div>
<div class="clear"></div>