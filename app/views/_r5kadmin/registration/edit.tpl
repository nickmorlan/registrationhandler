<div class="grid_12">
	<div class="tabs">
		<ul>Edit registration</ul>
		<form id="edit_registration" name="edit_registration" method="post" action="/admin/registration/edit/<?=$this->registry['registration']->id?>" enctype="multipart/form-data">
			<?=$this->render_partial('registration', 'form')?>
		</form>
	</div>
</div>

<script type="text/javascript">
	$(document).ready(function() {
	// init submit buttons
		$( "input:submit" ).button();
		$( "input:button" ).button();
		
		$('.tabs').tabs();

	});
</script>