<div class="grid_12">
	<div class="tabs">
		<ul>Create registration</ul>
		<form id="new_registration" name="new_registration" method="post" action="/admin/registration/create">
			<?=$this->render_partial('registration', 'form')?>
		</form>
	</div>
</div>


<script type="text/javascript">
	$(document).ready(function() {
	// init submit buttons
		$( "input:submit" ).button();
		$( "input:button" ).button();
		
		$('.tabs').tabs();

	});
</script>
