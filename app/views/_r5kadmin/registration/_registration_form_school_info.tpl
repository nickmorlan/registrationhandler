<div class="grid_7">
	<p>
		<label for="school">School:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'school')?>
	</p>
	<p>
		<label for="grade_level">Grade Level:</label><br />
		<?=HTML::text_field($this->registry['registration'], 'grade_level')?>
	</p>
</div>
<div class="grid_5">
	<div class="info_box" style="background:#ffc">
		<p><strong>School Information</strong> Please let us know the school to be attended and grade level for the participant during this activity.</p>
	</div>
</div>
<div class="clear"></div>