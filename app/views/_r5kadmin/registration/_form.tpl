<div class="left">
	<p>
		<label for="registration">First Name:</label><br />
		<input type="text" name="first_name" id="first_name" value="<?=$this->registry['registration']->first_name?>" class="required" />
	</p>
	<p>
		<label for="registration">Last Name:</label><br />
		<input type="text" name="last_name" id="last_name" value="<?=$this->registry['registration']->last_name?>" class="required" />
	</p>
	<p>
		<label for="registration">Address:</label><br />
		<input type="text" name="address_1" id="address_1" value="<?=$this->registry['registration']->address_1?>" class="required" /><br />
		<input type="text" name="address_2" id="address_2" value="<?=$this->registry['registration']->address_2?>"  />
	</p>
	<p>
		<label for="registration">City:</label><br />
		<input type="text" name="city" id="city" value="<?=$this->registry['registration']->city?>" class="required" />
	</p>
	<p>
		<label for="state">State</label><br />
		<?PHP IF($this->registry['registration']->state == 'US') : ?>
			<?=HTML::input_state_select($this->registry['registration'], 'state')?>
			<input type="text" id="foreign_state" name="foreign_state" value="" style="display:none;" />
		<?PHP ELSE : ?>
			<?=HTML::input_state_select($this->registry['registration'], 'state', 'style="display:none;"')?>
			<input type="text" id="foreign_state" name="foreign_state" value="<?=$this->registry['member']->state?>" />
		<?PHP ENDIF; ?>
	</p>
	<p>
		<label for="zip">Zip</label><br />
		<input type="text" name="zip" id="zip" value="<?=$this->registry['registration']->zip?>" class="required" / ><br />
	</p>
	<p>
		<label for="ship_to_country">Country</label><br />
		<?=HTML::input_country_select($this->registry['registration'], 'country')?>
	</p>
	<p>
		<label for="grade_level">Grade Level:</label><br />
		<input type="text" name="grade_level" id="grade_level" value="<?=$this->registry['registration']->grade_level?>" class="required" />
	</p>
	<p>
		<label for="school">School:</label><br />
		<input type="text" name="school" id="school" value="<?=$this->registry['registration']->school?>" class="required" />
	</p>
	<p>
		<label for="division">Division:</label><br />
		<input type="text" name="division" id="division" value="<?=$this->registry['registration']->division?>" class="required" />
	</p>
	<p>
		<label for="birth_date">Birth Date:</label><br />
		<input type="text" name="birth_date" id="birth_date" value="<?=date('m/d/Y', strtotime($this->registry['registration']->birth_date))?>" class="required" />
	</p>
	<p>
		<label for="parent1_name">Parent Name:</label><br />
		<input type="text" name="parent1_name" id="parent1_name" value="<?=$this->registry['registration']->parent1_name?>" class="required" />
	</p>
	<p>
		<label for="parent1_email">Parent Email:</label><br />
		<input type="text" name="parent1_email" id="parent1_email" value="<?=$this->registry['registration']->parent1_email?>" class="required" />
	</p>
	<p>
		<label for="parent1_phone">Parent Phone:</label><br />
		<input type="text" name="parent1_phone" id="parent1_phone" value="<?=$this->registry['registration']->parent1_phone?>" class="required" />
	</p>
	<p>
		<label for="parent2_name">Parent Name:</label><br />
		<input type="text" name="parent2_name" id="parent2_name" value="<?=$this->registry['registration']->parent2_name?>" class="required" />
	</p>
	<p>
		<label for="parent2_email">Parent Email:</label><br />
		<input type="text" name="parent2_email" id="parent2_email" value="<?=$this->registry['registration']->parent2_email?>" class="required" />
	</p>
	<p>
		<label for="parent2_phone">Parent Phone:</label><br />
		<input type="text" name="parent2_phone" id="parent2_phone" value="<?=$this->registry['registration']->parent2_phone?>" class="required" />
	</p>
	<p>
		<label for="date_paid">Date Paid:</label><br />
		<input type="text" name="date_paid" id="date_paid" value="<?=date('m/d/Y', strtotime($this->registry['registration']->date_paid))?>" class="required" />
	</p>
	<p>
		<label for="amount_paid">Amount Paid:</label><br />
		<input type="text" name="amount_paid" id="amount_paid" value="<?=$this->registry['registration']->amount_paid?>" class="required" />
	</p>
	<p>
		<label for="transaction_ref">Transaction Reference:</label><br />
		<input type="text" name="transaction_ref" id="transaction_ref" value="<?=$this->registry['registration']->transaction_ref?>" class="required" />
	</p>
	<p>
		<label for="hospital_name">Preferred Hospital:</label><br />
		<input type="text" name="hospital_name" id="hospital_name" value="<?=$this->registry['registration']->hospital_name?>" class="required" />
	</p>
	<p>
		<label for="emergency_contact">Emergency Contact:</label><br />
		<input type="text" name="emergency_contact" id="emergency_contact" value="<?=$this->registry['registration']->emergency_contact?>" class="required" />
	</p>
	<p>
		<label for="emergency_contact_phone">Emergency Contact Phone:</label><br />
		<input type="text" name="emergency_contact_phone" id="emergency_contact_phone" value="<?=$this->registry['registration']->emergency_contact_phone?>" class="required" />
	</p>
	<p>
		<label for="physician_name">Prefered Physician:</label><br />
		<input type="text" name="physician_name" id="physician_name" value="<?=$this->registry['registration']->physician_name?>" class="required" />
	</p>
	<p>
		<label for="physician_phone">Physician Phone:</label><br />
		<input type="text" name="physician_phone" id="physician_phone" value="<?=$this->registry['registration']->physician_phone?>" class="required" />
	</p>
	<p>
		<label for="dentist_name">Dentist Name:</label><br />
		<input type="text" name="dentist_name" id="dentist_name" value="<?=$this->registry['registration']->dentist_name?>" class="required" />
	</p>
	<p>
		<label for="dentist_phone">Dentist Phone:</label><br />
		<input type="text" name="dentist_phone" id="dentist_phone" value="<?=$this->registry['registration']->dentist_phone?>" class="required" />
	</p>
	<p>
		<label for="medical_history">Medical History:</label><br />
		<textarea name="medical_history" id="medical_history"><?=$this->registry['registration']->medical_history?></textarea>
	</p>
	<p>
		<label for="comments">Comments:</label><br />
		<textarea name="comments" id="comments"><?=$this->registry['registration']->comments?></textarea>
	</p>
	<p>
		<label for="notes">Notes:</label><br />
		<textarea name="notes" id="notes"><?=$this->registry['registration']->notes?></textarea>
	</p>
	<p>
		<input type="submit" name="submit" value="Save" />
	</p>
</div>
<div class="clear"></div>

<script>
$(document).ready(function () {
	   if($('#country').val() == 'US') {
			   $('#state').show();
			   $('#foreign_state').hide();
	   } else {
			   $('#state').hide();
			   $('#foreign_state').show();
	   }
		$("#birth_date").datepicker({
			showAnim: 'blind',
			altFormat: 'mm/dd/yyyy'
		});
		$("#date_paid").datepicker({
			showAnim: 'blind',
			altFormat: 'mm/dd/yyyy'
		});

		$( "input:submit" ).button();
		$( "input:button" ).button();
		
});



// toggle foreign state
$('#country').change(function () {
	   if($('#country').val() == 'US') {
			   $('#state').val('')
			   $('#state').show();
			   $('#foreign_state').hide();
			   $('#foreign_state').attr('class', '');
			   $('#state').attr('class', 'required');
	   } else {
			   $('#state').hide();
			   $('#foreign_state').val('');
			   $('#foreign_state').show();
			   $('#foreign_state').attr('class', 'required');
			   $('#state').attr('class', '');
	   }
});


</script>