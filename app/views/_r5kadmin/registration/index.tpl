<div class="grid_12">
	<h4>registration List</h4>
	<p align="right"><a href="/admin/registration/create">+ Create New</a></p>
	<?PHP IF(empty($this->registry['registrations'])) : ?>
		<p><em>None selected.</em>  <a href="/admin/registration">show all</a><p></p>
	<?PHP ELSE: ?>
		<p><?PHP AdminHelper::build_search_bar('registrations', '/admin/registration/'); ?> <a href="/admin/registration">show all</a><p><p>
		<div class="tabs margin_bottom">
			<ul>Registrations</ul>
			<table width="100%">
			<?PHP FOREACH($this->registry['registrations'] as $c) :?>
				<?=HTML::alternate_tr_colors('ccddea');?>
					<td><p><?=$c->first_name?> <?=$c->last_name?></p></td>
					<td><p><?=$c->birth_date?></p></td>
					<td><p><?=$c->address_1?></p></td>
					<td><p><?=$c->created_at?></p></td>
					<td><p><a href="/admin/registration/edit/<?PHP echo $c->id;?>">edit</a></p></td>
					<td><p><a href="/admin/registration/destroy/<?PHP echo $c->id;?>" style='color:#aa0000;' onclick="return confirm('Do you really want to delete registration <?PHP echo $c->registration;?>?\n\nThis action cannot be undone.')">Delete</a></p></td>
				</tr>
			<?PHP ENDFOREACH; ?>
			</table>
		</div>
	<?PHP ENDIF;?>
</div>
<?PHP IF($this->controller_method == 'index') AdminHelper::build_admin_pagination_links($this->registry['paginator']); ?>
<script type="text/javascript">
	$(document).ready(function() {
	// init submit buttons
		$( "input:submit" ).button();
		$( "input:button" ).button();
		
		$('.tabs').tabs();

		//hover states on the static widgets
		$('.a_link').hover(
			function() { $(this).addClass('ui-state-hover'); }, 
			function() { $(this).removeClass('ui-state-hover'); }
		);
	});
</script>