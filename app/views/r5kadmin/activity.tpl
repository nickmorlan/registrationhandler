      <div class="portlet">

        <h4 class="portlet-title">
          <u><?=$this->registry['activity']->name?></u>
        </h4>

        <div class="portlet-body">

          <div class="row">

            <div class="col-sm-6 col-md-3">
              <div class="row-stat">
                <p class="row-stat-label">Revenue Generated</p>
                <h3 class="row-stat-value">$<?=number_format($this->registry['amount'], 2)?></h3>
              </div> <!-- /.row-stat -->
            </div> <!-- /.col -->

            <div class="col-sm-6 col-md-3">
              <div class="row-stat">
                <p class="row-stat-label">Donations</p>
                <h3 class="row-stat-value">$<?=number_format($this->registry['donations'], 2)?></h3>
              </div> <!-- /.row-stat -->
            </div> <!-- /.col -->

            <div class="col-sm-6 col-md-3">
              <div class="row-stat">
                <p class="row-stat-label">Registrations</p>
                <h3 class="row-stat-value"><?=number_format($this->registry['completed_registrations'])?></h3>
              </div> <!-- /.row-stat -->
            </div> <!-- /.col -->

            <div class="col-sm-6 col-md-3">
              <div class="row-stat">
                <p class="row-stat-label">Bailed Registrations</p>
                <h3 class="row-stat-value"><?=number_format($this->registry['bailed_registrations'])?></h3>
              </div> <!-- /.row-stat -->
            </div> <!-- /.col -->
            
          </div> <!-- /.row -->

        </div> <!-- /.portlet-body -->

      </div> <!-- /.portlet -->



      

          <div class="row">

            <div class="col-md-8">

              <div class="portlet">

                <h4 class="portlet-title">
                  <u>Registration History</u>
                </h4>

                <div class="portlet-body">

                  <div class="chart-bg chart-bg-secondary">
                    <div id="reports-line-chart" class="chart-holder-250"></div>
                  </div> <!-- /.bg-chart -->

                  <br>

              </div> <!-- /.portlet-body -->

            </div> <!-- /.portlet -->
              
            </div> <!-- /.col -->

            <div class="col-md-4">         

              <div class="portlet">

                <h4 class="portlet-title">
                  <u>Category Breakdown</u>
                </h4>

                <div class="portlet-body">

                  <div id="donut-chart" class="chart-holder" style="width: 70%;"></div>
                  
                </div> <!-- /.portlet-body -->

              </div> <!-- /.portlet -->
              
            </div> <!-- /.col -->
            
          </div> <!-- /.row -->


      <div class="row">

        <div class="col-md-6">

          <div class="portlet">

            <h4 class="portlet-title">
              <u>Revenue Details</u>
            </h4>

            <div class="portlet-body">

              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Division</th>
                    <th class="text-right">Registered</th>
                    <th class="text-right">Revenue</th>
                  </tr>
                </thead>

                <tbody>
                  <?PHP FOREACH($this->registry['totals'] as $totals) : ?>
                  <?PHP FOREACH($totals['division'] as $k => $t) : ?>
                    <tr>
                      <td><?=$k?></td>
                      <td class="text-right"><?=$t['registrations']?></td>
                      <td class="text-right">$<?=number_format($t['registration_amount'], 2)?></td>
                    </tr>
                  <?PHP ENDFOREACH; ?>
                  <?PHP ENDFOREACH; ?>
                </tbody>
                  <?PHP IF($this->registry['activity']->accept_donation == 'y'): ?>
                   <thead>
                      <tr>
                        <th>Division</th>
                        <th class="text-right">Donations</th>
                        <th class="text-right">Amount</th>
                      </tr>
                    </thead>
                    <tbody>
                    <?PHP FOREACH($this->registry['totals'] as $totals) : ?>
                    <?PHP FOREACH($totals['division'] as $k => $t) : ?>
                      <tr>
                        <td><?=$k?></td>
                        <td class="text-right"><?=number_format($t['donations'])?></td>
                        <td class="text-right">$<?=number_format($t['donation_amount'], 2)?></td>
                      </tr>
                    <?PHP ENDFOREACH; ?>
                    <?PHP ENDFOREACH; ?>
                    </tbody>
                  <?PHP ENDIF; ?>
                </tbody>
              </table>

            </div> <!-- /.portlet-body -->

          </div> <!-- /.portlet -->
          
        </div> <!-- /.col -->



        <div class="col-md-6">

          <div class="portlet">

            <h4 class="portlet-title">
              <u>Revenue Breakdown</u>
            </h4>

            <div class="portlet-body">

              <div id="stacked-horizontal-chart" class="chart-holder-250"></div>
              
            </div> <!-- /.portlet-body -->

          </div> <!-- /.portlet -->
          
        </div> <!-- /.col -->
        

      </div> <!-- /.row -->



      <div class="row">
        <div class="col-md-12">
        <h4 class="portlet-title">
          <u>Registration Demographics Breakdown</u>
        </h4>
        </div>

        <div class="col-md-6">

          <div class="portlet">
            
            <div class="porlet-body" id="demo-buttons">

              <div class="col-md-4"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="area">Area</a></div>              
              <!--<div class="col-md-3"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="shirt_size">Shirt Size</a></div>              
              <div class="col-md-3"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="grade_level">Grade Level</a></div>              
              <div class="col-md-3"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="school">School</a></div>              
              <div class="col-md-3"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="age">Age</a></div>              
              <div class="col-md-3"><a href="javascript:;" class="btn btn-default btn-block demo-breakdown" rel="gender">Gender</a></div> -->             
            </div> <!-- /.portlet-body -->

          </div> <!-- /.portlet -->
          
        </div> <!-- /.col -->


        <div class="col-md-6">

          <div class="portlet">

            <div class="porlet-body">

              <table class="table table-bordered hide" id="demo-table">
 
              </table>
              
            </div> <!-- /.portlet-body -->

          </div> <!-- /.portlet -->
          
        </div> <!-- /.col -->
        
      </div> <!-- /.row -->

      <script>
      //console.log(<?=$this->registry['activity']->form_definition_json?>)
      </script>