<div class="layout layout-stack-sm layout-main-left">
   <div class="col-md-8 layout-main">
        <h3 class="content-title">Payment History</h3>

         <?PHP IF(empty($this->registry['payment_info'])): ?>
            <div class="alert alert-danger">
                <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
                <p><i class="fa fa-warning fa-2x"></i> <strong>You have not completed your payment details.</strong></p>
                <p>This must be completed before you can receive payments from RegistrationHandler.com.</p>
            </div>
        <?PHP ENDIF;?>
        
        <div class="portlet">
        <div class="portlet-body">
            <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 20%">Date Paid</th>
                <th style="width: 60%">Description</th>
                <th style="width: 20%">Amount</th>
              </tr>
            </thead>
            <tbody>
            <?PHP IF(empty($this->registry['history'])) : ?>
              <tr>
                <td colspan="3">There is no payment history.</td>
              </tr>
            <?PHP ELSE: ?>
              <?PHP FOREACH($this->registry['history'] as $h) : ?>
                  <tr>
                      <td><?=date('M j, Y', strtotime($h->date_paid))?></td>
                      <td><?=$h->description?></td>
                      <td>$<?=$h->amount?></td>
                  </tr>
              <?PHP ENDFOREACH; ?>
            <?PHP ENDIF; ?>
            </tbody>
          </table>
          </div>
        </div> <!-- /.portlet-body -->

    </div>
    <div class="col-md-4 layout-sidebar">
        <?PHP IF(!empty($this->registry['payment_info']->pay_via)): ?>
            <div class="alert alert-info">
                <p><em>You are currnetly set up to receive payments via <strong><?=ucwords(str_replace('_', ' ', $this->registry['payment_info']->pay_via))?></strong></em>.</p>
            </div>
        <?PHP ENDIF; ?>
        <?PHP IF($this->registry['payment_info']->pay_via != 'stripe'): ?>
            <div class="portlet">
                <table class="table keyvalue-table">
                  <tbody>
                    <tr>
                      <td class="kv-key"><i class="fa fa-group kv-icon kv-icon-secondary"></i> Unpaid Registrations</td>
                      <td class="kv-value"><?=$this->registry['registration_count']?></td>
                    </tr>
                    <tr>
                      <td class="kv-key"><i class="fa fa-dollar kv-icon kv-icon-primary"></i> Pending Amount</td>
                      <td class="kv-value">$<?=number_format($this->registry['amount_pending'], 2)?></td>
                    </tr>
                   </tbody>
                </table>
            </div>
        <?PHP ENDIF; ?>
    </div>
</div>
