 <div class="col-md-12">
	<h3 class="content-title">Please Review Your Message</h3>
</div>
<div class="col-md-12">
	<form id="activity_form" name="activity_form" method="post" action="/communication/confirm" enctype="multipart/form-data">
		<div class="col-md-7">
			<p><strong>From: </strong><?=$_POST['from_name']?> &lt;<?=$_POST['from_email']?>&gt;</p>
			<input type="hidden" name="from_name" id="from_name" value="<?=$_POST['from_name']?>" />
			<input type="hidden" name="from_email" id="from_email" value="<?=$_POST['from_email']?>" />
			<p><strong>Subject: </strong><?=$_POST['subject']?></p>
			<input type="hidden" name="subject" id="subject" value="<?=$_POST['subject']?>" />
			<div class="well"><?=$_POST['message']?>
				<textarea style="display:none;" name="message"><?=$_POST['message']?></textarea>
				<?PHP IF(!empty($this->registry['attachments'])): ?>
					<hr />
					<?PHP FOREACH($this->registry['attachments'] as $attachment) :?>
						<p><?PHP IF(file_exists(SITE_ROOT . 'images/site/16pxfileicons/' . $attachment['extension'] . '.png')): ?><img src="/images/site/16pxfileicons/<?=$attachment['extension']?>.png" style="margin-right:3px;vertical-align:top;"/><?PHP ENDIF;?><?=$attachment['original_name']?></p>
						<input type="hidden" name="attachments[]" value="<?=$attachment['file']?>" />
						<input type="hidden" name="attachments_name[]" value="<?=$attachment['original_name']?>" />
					<?PHP ENDFOREACH; ?>
				<?PHP ENDIF;?>
				<?PHP IF(!empty($_POST['ics'])): ?>
					<hr />
					<input type="hidden" name="ics" value="<?=$_POST['ics']?>" />
					<p><img src="/images/site/16pxfileicons/ics.png" style="margin-right:3px;vertical-align:top;"/><?=$_POST['ics']?></p>
				<?PHP ENDIF;?>
			</div>
			<div class="col-md-4" id="submit_p"><input type="submit" name="submit" class="btn btn-block btn-success" title="Send" value="Send Message" /></div>
			<p style="margin:40px 0;" class="loading hide"><i class="fa fa-spinner fa-spin fa-lg"></i> Queuing Emails..</p>
		</div>
		<div class="col-md-offset-1 col-md-4">
		<div class="alert alert-info">
			<?PHP IF(in_array('email_it', $_POST['actions']) && !empty($_POST['lists'])) :?>
				<?PHP FOREACH($this->registry['keys'] as $key => $name) : ?><input type="hidden" name="keys[<?=$key?>]" value="<?=$name?>" /><?PHP ENDFOREACH;?>
				<input type="hidden" name="actions[]" value="email_it" />
				<p><i class="fa fa-info-circle fa-lg"></i> <strong>This message will be emailed to:</strong></p>
				<?PHP $count = count($_POST['lists']);?>
				<?PHP FOR($i = 0; $i < $count; $i ++) : ?>
					<input type="hidden" name="lists[]" value="<?=$_POST['lists'][$i]?>" />
					<?PHP IF(strpos($_POST['lists'][$i], 'd') > 0):?>
						<input type="hidden" name="skip[]" value="<?=substr($_POST['lists'][$i], 0, strpos($_POST['lists'][$i], 'd'))?>" />
						<p>&mdash;<?=$_POST['keys'][$_POST['lists'][$i]]?></p>
					<?PHP ELSE: ?>
						<p><?=$_POST['keys'][$_POST['lists'][$i]]?></p>
					<?PHP ENDIF;?>
				<?PHP ENDFOR; ?>
			<?PHP ENDIF; ?>
			<?PHP IF(in_array('facebook_it', $_POST['actions'])) :?>
				<input type="hidden" name="actions[]" value="facebook_it" />
				<p>- This message will be posted on facebook.</p>
			<?PHP ENDIF; ?>
		</div>
		</div>
	</form>
</div>
