<div class="col-md-12">
	<h3 class="content-title">Add or Remove Emails From Your Communications</h3>
	<form id="update" action="/communication/add_to_list" data-parsley-validate method="post" class="form parsley-form">
		<div class="col-md-6">
			<div class="well">
				<p><strong id="select_lists">Apply to these Activities.</strong></p>
				<hr />
				<div class="form-group">
					<?PHP FOREACH($this->registry['keys'] as $key => $name) : ?><input type="hidden" name="keys[]" value="<?=$name?>" /><?PHP ENDFOREACH;?>
					<?PHP FOREACH($this->registry['list_selection'] as $name => $info) : ?>
						<input type="hidden" name="keys[a<?=$info['id']?>]" value="<?=$name?>" />
						<?PHP ECHO '<div class="checkbox"><label for="a' . $info['id'] . '"> <input type="checkbox" name="lists[]" value="a' . $info['id'] .'" id="a' . $info['id'] . '" data-parsley-required data-parsley-multiple="lists" data-parsley-errors-container="#ers" data-parsley-error-message="You must select at least one activity."/>' . $name . '</label></div>'; ?>
						<?PHP IF(count($info['divisions']) > 1): ?>
							<div class="divisions a<?=$info['id']?>" style="padding-left:20px;display:none;">
								<?PHP FOREACH($info['divisions'] as $k => $division) : ?>
									<input type="hidden" name="keys[a<?=$info['id']?>d<?=$k?>]" value="<?=$division?>" />
									<?PHP ECHO '<div class="checkbox"><label for="a' . $info['id'] . $k . '"> <input type="checkbox" name="lists[]" value="a' . $info['id'] . 'd' . $k . '" id="a' . $info['id'] . $k . '" class="list" rel="a' . $info['id'] . '" data-parsley-required data-parsley-multiple="lists" />' . $name . ' ' . $division . '</label></div>';?>
								<?PHP ENDFOREACH; ?>
							</div>
						<?PHP ENDIF; ?>
					<?PHP ENDFOREACH; ?>
				</div>
			</div>
			<div id="ers" class="form-group"></div>
			<div class="form-group">
				<label>Enter email addresses one per line in the field below to update.</label>
				<textarea name="emails" rows="6" class="form-control" required data-parsley-emaillist></textarea>
			</div>
			<img src="/images/site/loading.gif" id="loading" class="hide" style="margin:15px 150px;">
			<div class="row">
				<div class="col-md-6"><input type="submit" class="btn btn-success btn-block btn-sm" value="Add to Mailing List" /></div>
				<div class="col-md-6"><input type="submit" class="btn btn-danger btn-block btn-sm" id="del" value="Delete from List" /></div>
			</div>
		</div>
	</form>
</div>
