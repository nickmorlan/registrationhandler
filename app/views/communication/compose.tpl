 <link rel="stylesheet" href="/css/bootstrap-wysihtml5.css">
 <link rel="stylesheet" href="/js/plugins/fileupload/bootstrap-fileupload.css">
 <div class="col-md-12">
	<h3 class="content-title">Compose a New Message</h3>
</div>
	<form id="communication_form" name="communication_form" method="post" action="/communication/confirm" enctype="multipart/form-data" data-parsley-validate method="post" class="form form-horizontal parsley-form">
		<div class="col-md-8">
			<div class="form-group">
				<label class="col-md-3">From Name</label>
				<div class="col-md-8">
					<input type="text" name="from_name" id="from_name" value="<?=$this->registry['from_name']?>" class="form-control" required />
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->
			<div class="form-group">
				<label class="col-md-3">From Email</label>
				<div class="col-md-8">
					<input type="email" name="from_email" id="from_email" value="<?=$this->registry['from_email']?>" class="form-control" required />
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->
			<div class="form-group">
				<label class="col-md-3">Subject</label>
				<div class="col-md-8">
					<input type="text" name="subject" id="subject" value="<?=$this->registry['subject']?>" class="form-control" required />
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->
			<div class="form-group">
				<div class="col-md-11">
					<textarea name="message" id="message" class="form-control" rows="12" required ><?=$this->registry['message']?></textarea>
				</div> <!-- /.col -->
			</div> <!-- /.form-group -->


			
            <div class="form-group">

				<div class="col-md-3"><label>Attachments</label></div>

				<div class="col-md-7">
				<div class="fileupload fileupload-new" data-provides="fileupload">

					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"></div>

					<div>
						<span class="btn btn-default btn-file">
						<span class="fileupload-new">Select file</span>
						<span class="fileupload-exists">Change</span>
						<input type="file" name="attachments[]" />
						</span>

						<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
					</div> <!-- /div -->

				</div> <!-- /.fileupload -->

				<div class="fileupload fileupload-new clear" data-provides="fileupload">

					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"></div>

					<div>
						<span class="btn btn-default btn-file">
						<span class="fileupload-new">Select file</span>
						<span class="fileupload-exists">Change</span>
						<input type="file" name="attachments[]" />
						</span>

						<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
					</div> <!-- /div -->

				</div> <!-- /.fileupload -->

				<div class="fileupload fileupload-new clear" data-provides="fileupload">

					<div class="fileupload-preview fileupload-exists thumbnail" style="max-width: 200px; max-height: 200px; line-height: 20px;"></div>

					<div>
						<span class="btn btn-default btn-file">
						<span class="fileupload-new">Select file</span>
						<span class="fileupload-exists">Change</span>
						<input type="file" name="attachments[]" />
						</span>

						<a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
					</div> <!-- /div -->

				</div> <!-- /.fileupload -->

			 </div> <!-- /.form-group -->			

           </div>
		</div>
		<div class="col-md-4">
			<div class="well">
				<p><strong id="select_lists">Send an email to..</strong></p>
				<hr />
				<input type="hidden" name="actions[]" value="email_it" id="email_it" />
				<?PHP FOREACH($this->registry['keys'] as $key => $name) : ?><input type="hidden" name="keys[]" value="<?=$name?>" /><?PHP ENDFOREACH;?>
				<?PHP FOREACH($this->registry['list_selection'] as $name => $info) : ?>
					<input type="hidden" name="keys[a<?=$info['id']?>]" value="<?=$name?>" />
					<?PHP ECHO '<div class="checkbox"><label for="a' . $info['id'] . '"> <input type="checkbox" name="lists[]" class="main" value="a' . $info['id'] .'" id="a' . $info['id'] . '" data-parsley-required data-parsley-multiple="lists" data-parsley-errors-container="#ers" data-parsley-error-message="You must select at least one activity."/>' . $name . '</label></div>'; ?>
					<?PHP IF(count($info['divisions']) > 1): ?>
						<div class="divisions a<?=$info['id']?>" style="padding-left:20px;display:none;">
							<?PHP FOREACH($info['divisions'] as $k => $division) : ?>
								<input type="hidden" name="keys[a<?=$info['id']?>d<?=$k?>]" value="<?=$division?>" />
								<?PHP ECHO '<div class="checkbox"><label for="a' . $info['id'] . $k . '"> <input type="checkbox" name="lists[]" value="a' . $info['id'] . 'd' . $k . '" id="a' . $info['id'] . $k . '" class="list" rel="a' . $info['id'] . '" data-parsley-multiple="lists" />' . $name . ' ' . $division . '</label></div>';?>
							<?PHP ENDFOREACH; ?>
						</div>
					<?PHP ENDIF; ?>
				<?PHP ENDFOREACH; ?>
			</div>
			<div id="ers" class="form-group"></div>
			<?PHP IF(!empty($this->registry['member']->facebook_app_token)): ?>
				<div class="well">
					<p>
						<input type="checkbox" name="actions[]" value="facebook_it" id="facebook_it" /> <label for="facebook_it">Post this to FaceBook</label>
					</p>
				</div>
			<?PHP ENDIF; ?>
		</div>
		<div class="col-md-12">
			<div class="col-md-3 pull-right"><input type="submit" class="btn btn-jumbo btn-block btn-success" title="Send" value="Send" /></div>
		</div>
	</form>
</div>