<div class="layout layout-stack-sm layout-main-left">
   <div class="col-md-8 layout-main">
		<h3 class="content-title">Communicate With Your Registrations</h3>
	    <div class="portlet">

		<?PHP IF($this->registry['activity_count'] == 0): ?>
			<div class="alert alert-danger">
			    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
				<p><i class="fa fa-warning fa-2x"></i> <strong>You have not created any activities.</strong>
				<p>You must first create an activity that people can register for before you try to email them.</p>
	   		</div>
		<?PHP ELSEIF($this->registry['registration_count'] == 0): ?>
			<div class="alert alert-danger">
			    <a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a>
				<p><i class="fa fa-warning fa-2x"></i> <strong>There are no registrations.</strong>
				<p>There is no one to email right now.</p>
	   		</div>
		<?PHP ENDIF;?>
		
	      <table class="table table-striped table-bordered" id="table-1">
	        <thead>
	          <tr>
	            <th style="width: 5%"></th>
	            <th style="width: 75%">Subject</th>
	            <th style="width: 20%">Sent</th>
	          </tr>
	        </thead>

	        <tfoot>
	          <tr>
	            <th></th>
	            <th>Subject</th>
	            <th>Sent</th>
	          </tr>
	        </tfoot>
	      </table>

		   </div> <!-- /.portlet-body -->

	</div>
	<div class="col-md-4 layout-sidebar">
		<div class="portlet">
			<a href="/communication/compose" class="btn btn-success btn-block btn-jumbo"><i class="fa fa-envelope"></i> Compose <span class="hidden-xs hidden-sm hidden-md">New </span>Message</a><br />
			<a href="/communication/manage" class="btn btn-success btn-block btn-lg"><i class="fa fa-group"></i> Add or Remove Email<span class="hidden-lg">s</span><span class="hidden-xs hidden-sm hidden-md"> Addresses</span></a>
		</div>
		<?PHP IF(empty($this->registry['member']->facebook_app_token) || empty($this->registry['member']->facebook_uid)): ?>
			<!--<div class="well">
				<p>Connect with facebook and have the ability to post messages to your facebook wall.</p>
				<a href="/communication/fbconnect" class="btn btn-secondary btn-block"><i class="fa fa-facebook-square"></i> Connect with Facebook</a>-->
			</div>
		<?PHP ENDIF;?>
	</div>
</div>
