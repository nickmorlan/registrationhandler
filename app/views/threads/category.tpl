<?PHP FOREACH($this->registry['categories'] as $cat): ?>
	<div class="grid_3">
		<p><a href="/threads/product/<?=$cat['product_id']?>" title="<?=$cat['name']?>"><?=$cat['name']?><br /><img src="<?=str_replace(array('http:', '/catalog'), array('https:', '/labcatalog'), $cat['image'])?>" alt="<?=$cat['name']?>" style="max-height:100px;"/></a> $<?=number_format($this->registry['quotes'][$cat['product_id']]['garment_breakdown'][0]['price_per_shirt'], 2)?></p>
	</div>
<?PHP ENDFOREACH; ?>