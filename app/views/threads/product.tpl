<div class="grid_5">
	<?PHP FOREACH($this->registry['product']['colors'] as $k => $color): ?>
		<div id="productimg<?=$k?>" class="hide prodimg clickable">
			<img src="<?=str_replace('http://i1.ooshirts.com', 'https://8a52b406ac72b723b416-57220dc4dba4daa2628e7f66fcefe74e.ssl.cf2.rackcdn.com', $color['front_image'])?>" alt="<?=$this->registry['product']['name']?> <?=$color['name']?>" style="max-width:350px;"/>
		</div>
	<?PHP ENDFOREACH; ?>
	<?PHP FOREACH($this->registry['product']['pictures'] as $pic): ?>
		<?PHP IF(substr($pic, -11) != 'catalog.jpg') :?>
			<div class="left">
				<a href="<?=$pic?>" rel="lightbox"><img src="<?=$pic?>" style="max-width:50px;" alt="<?=$this->registry['product']['name']?>" /></a>
			</div>
		<?PHP ENDIF; ?>
	<?PHP ENDFOREACH; ?>

</div>
<div class="grid_7">
	<p><h3><?=$this->registry['product']['name']?></h3></p>
	<p><?=$this->registry['product']['comments']?></p>
	<p><?=$this->registry['product']['materials']?></p>
	<hr />
	<p><strong><span style="font-size:1.2em;">1)</span> Select a Color - </strong><span id="colordisplay"><?=$this->registry['product']['colors'][0]['name']?></span></p>
	<?PHP FOREACH($this->registry['product']['colors'] as $k => $color) :?>
		<div class="clickable asdf" style="float:left; width:50px;height:30px;" rel="<?=$k?>" title="<?=$color['name']?>">
			<div style="background-color:#<?=$color['hex']?>;display:block;width:25px;height:25px;border:1px solid #000;float:left;margin-right:5px;"></div>
		</div>
	<?PHP ENDFOREACH; ?>
	<div class="clear"></div>
	<form name="threads" id="threads" action="/threads/quote" method="post">
		<input type="hidden" name="product_id" id="product_id" value="<?=$this->registry['product_id']?>" />
		<input type="hidden" name="color" id="color" value="<?=$this->registry['product_id']['colors'][0]['name']?>" />
		<hr />
		<p><strong><span style="font-size:1.2em;">2)</span> Enter size quantities</strong></p>
		<?PHP FOREACH($this->registry['sizes'] as $k => $v) : ?>
			<div id="<?=$k?>div" style="float:left;text-align:center;width:70px;" class="hide">
				<p><strong><?=$v?></strong><br />
					<input type="text" name="<?=$k?>" id="<?=$k?>" value="0" style="width:60px;" /></p>
			</div>
		<?PHP ENDFOREACH;?>
		<div class="clear"></div>
		<p><label for="color_count">Printing Colors</label><br />
			<input type="text" name="color_count" id="color_count" value="1" style="width:60px;" /></p>
		<p><input type="submit" class="freshbutton-blue" value="Continue" /></p>
	</form>
</div>
<script>
	$(document).ready(function() {
		$('#productimg0').show();
		var sizes = <?=json_encode($this->registry['sizes'])?>;
		console.log(sizes);
		var start = '<?=$this->registry['product']['colors'][0]['smallest']?>';
		var end   = '<?=$this->registry['product']['colors'][0]['largest']?>';
		var show  = false;
		for (var key in sizes) {
		 	if (sizes.hasOwnProperty(key)) {
		 		console.log(start + " " + key);
				if(key == start) show = true;
				if(show) $('#' + key + 'div').show();
				if(key == end) show = false;
			}
		}
	});

	$('.asdf').on('click', function(){
		$('.prodimg').hide();
		$('#productimg' + $(this).attr('rel')).show();
		$('#color').val($(this).attr('title'));
		$('#colordisplay').text($(this).attr('title'));
	});
</script>