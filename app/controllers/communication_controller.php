<?php
/**
 * PACKAGE: cpap wholesale
 * 
 * This uses $this->registry['member'] which is set when the application
 * loads.
 */
use Mailgun\Mailgun;
use Faceboook\Faceboook;

class CommunicationController Extends ControllerBase {

    function __construct ($params = array()) {
        $this->layout = 'application_boot';
        $this->controller = 'communication';
        $this->no_cache();
        $this->authenticate = true;
    }

    function index ($params) {
        $this->registry['res'] = ActiveRecordBase::query("SELECT id, message_attributes, created_at FROM member_sent_communications WHERE member_id = " . $this->registry['member']->id . " ORDER BY created_at DESC LIMIT 25");
        $this->registry['activity_count'] = ActiveRecordBase::query("SELECT count(id) as count FROM activities WHERE member_id = " . $this->registry['member']->id);
        $this->registry['registration_count'] = ActiveRecordBase::query("SELECT count(id) as count FROM registrations WHERE member_id = " . $this->registry['member']->id);
        $this->registry['activity_count'] = $this->registry['activity_count'][0]['count'];
        $this->registry['registration_count'] = $this->registry['registration_count'][0]['count'];
        
        $this->registry['js_files'][] = "/js/plugins/dataTables/jquery.dataTables.min.js";
        $this->registry['js_files'][] = "/js/plugins/dataTables/dataTables.bootstrap.js";
        $this->registry['js_data'] = json_encode(array('member_id' => $this->registry['member']->id));
    }    
    
    function compose () {
        //$activities = Activity::find_all_by(array('member_id' => $this->registry['member']->id, 'archived' => 'n'));
        $activities = ActiveRecordBase::query("SELECT id, name, cost_definition FROM activities WHERE member_id = {$this->registry['member']->id} AND archived = 'n'");
        foreach($activities as $activity){
            $costs = json_decode($activity['cost_definition']);
            //print_r($activities);
            $this->registry['list_selection'][$activity['name']]['id'] = $activity['id'];
            foreach($costs->registration->division as $k => $d)
                $this->registry['list_selection'][$activity['name']]['divisions'][$k] = $d;
        }

        //$this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
        $this->registry['js_files'][] = "/js/plugins/fileupload/bootstrap-fileupload.js";
        $this->registry['js_files'][] = "/js/wysiwyg/wysihtml5-0.3.0.min.js";
        $this->registry['js_files'][] = "/js/wysiwyg/bootstrap3-wysihtml5.js";
        $this->registry['js_files'][] = "/js/wysiwyg/prettify.js";
    }
    
    function compose_calendar_email ($activity_id) {
        //$activities = Activity::find_all_by(array('member_id' => $this->registry['member']->id, 'archived' => 'n'));
       // $activities = ActiveRecordBase::query("SELECT id, name, cost_definition FROM activities WHERE member_id = {$this->registry['member']->id} AND archived = 'n'");
        $res = ActiveRecordBase::query("SELECT id, name, cost_definition FROM activities WHERE member_id = {$this->registry['member']->id} AND archived = 'n' AND id = {$activity_id}");
        $activity = $res[0];
        $calendar_dates = ActivityCalendarDate::find_all_by(array('activity_id' => $activity_id));
        
        if(empty($activity) || empty($calendar_dates)) {
            $this->redirect('communication');
            exit;
        }
        
        $this->registry['subject'] = 'Calendar of Events for ' . $activity['name'];
        $this->registry['message'] = "This email contains a calendar attachment that can be imported into your calendar software.<br />";
        $this->registry['message'] .= ActivityCalendarDate::generate_ics_html_for($activity_id);
        $this->registry['ics']     = 'rh' . $this->registry['member']->id . $activity['id'] . '.ics';
        $costs = json_decode($activity['cost_definition']);
        //print_r($costs);
        $this->registry['list_selection'][$activity['name']]['id'] = $activity['id'];
        foreach($costs->registration->division as $k => $d)
            $this->registry['list_selection'][$activity['name']]['divisions'][$k] = $d;

        $this->registry['js_files'][] = "/js/wysiwyg/wysihtml5-0.3.0.min.js";
        $this->registry['js_files'][] = "/js/wysiwyg/bootstrap3-wysihtml5.js";
        $this->registry['js_files'][] = "/js/wysiwyg/prettify.js";
   }
    
    function confirm() {
        if(empty($_POST)) {
            $this->redirect('communication');
            exit;
        }
        if(empty($_POST['message']) || empty($_POST['subject']) || (empty($_POST['lists']) && !in_array('email_it', $_POST['action']))) {
            Application::set_flash('The message must have a subject, message text and recipient.', 'error');
            $this->redirect('communication');
            exit;
        }
        // process attachment uploads..
        if(!empty($_FILES)) $this->do_attachment_logic();
        
        $this->registry['keys'] = $_POST['keys'];
        $flash_messages = array();
        // message confirmed and ready to send
        if($_POST['submit'] == 'Send Message') {
            // email 
            if(in_array('email_it', $_POST['actions'])) {//print_r($_POST);exit;
                // First, instantiate the SDK with your API credentials and define your domain. 
                //$mg = new Mailgun(MAILGUN_API_KEY, 'bin.mailgun.net', 'ad070f20', $ssl = false); //http://bin.mailgun.net/ad070f20
                $mg = new Mailgun(MAILGUN_API_KEY);
                $domain = MAILGUN_DOMAIN;

                // handle the attachments
                $attachment_arr = array();
                if(!empty($_POST['attachments'])) {
                    $arr = array();
                    foreach($_POST['attachments'] as $pa) $arr[] = SITE_ROOT . 'images/member_attachments/' . $pa;
                    $attachment_arr = array('attachment' => $arr);
                }
                if(!empty($_POST['ics'])) $attachment_arr = array('attachment' => array(SITE_ROOT . SITE_VIEWS . 'cache/ics/' . $_POST['ics']));
                //print_r($attachment_arr);exit;
                // remember the list names
                $list_names = array();
                //print_r($_POST);exit;
                // add the unsubscribe link in the email message..
                $_POST['message'] .= "\r\n" . '<br><hr>
<p style="font-size:10px;">Activity Management by RegistrationHandler.com, click to <a href="%unsubscribe_url%">unsubscribe</a> from this list</p>';
                foreach($_POST['lists'] as $v) {
                    try {
                        if(!in_array($v, $_POST['skip'])) {
                            $list_names[] = $_POST['keys'][$v];
                            $msg_array =  array('from'    => $_POST['from_name'] . '<' . $_POST['from_email'] . '>', 
                                                'to'      => $v . '@registrationhandler.com', //'rhtest@registrationhandler.com', // $v . '@registrationhandler.com', 
                                                'subject' => $_POST['subject'], 
                                                'text'    => strip_tags($_POST['message']),
                                                'html'    => $_POST['message']);    
                            //print_r($msg_array);exit;
                            $result = $mg->sendMessage($domain, $msg_array, $attachment_arr);

                        }
                    } catch (Exception $e) {
                        LogAction::log('Exception thrown in CommunicationController::confirm(): ' . $e->getMessage(), 'CommunicationController');        
                        Application::set_flash('An error has occured sending email messages.', 'error');
                        $this->redirect('communication');
                    }
                }
                $flash_messages[] = 'Your email messages are being delivered.';
                $sent = MemberSentCommunication::create(array('member_id' => $this->registry['member']->id, 
                                                              'message_attributes' => json_encode(array('subject' => $_POST['subject'], 
                                                                                                        'message' => $_POST['message'], 
                                                                                                        'mailing_lists' => $list_names,
                                                                                                        'actions' => $_POST['actions'],
                                                                                                        'attachments' => $_POST['attachments_name']))
                                                              ));
                $sent->save();
                LogForDashboard::log($this->registry['member']->id, 0, 'mail', 'Mail Sent', 'An email has been sent to your registration list.');
                //print_r($sent);exit;                                                                                        
            }
            
            // post to facebook
            if(in_array('facebook_it', $_POST['actions'])) {
                try{
                    $facebookSDK = new Facebook(array(
                      'appId'  => FB_APP_ID,
                      'secret' => FB_APP_SECRET,
                      'cookie' => true
                    ));
                                    
                    $postData = array(
                        'message'     => strip_tags(str_replace(array('<br>', '<br />', '</p>'), array("\r\n", "\r\n", "\r\n\r\n"), $_POST['message'])),
                        'access_token' => $this->registry['member']->facebook_app_token,
                        //'picture'     => 'https://www.registrationhandler.com/images/member_attachments/' . $_POST['attachments'][0], 
                        //'link'        => 'URL',
                        //'name'        => 'allo',
                        //'caption'     => 'here we go',
                        //'description' => 'Description',
                        //'actions'     => array('name' => 'Action name', 'link' => 'Action link')
                    );
                    $post_id = $facebookSDK->api("/{$this->registry['member']->facebook_uid}/feed", 'POST', $postData);
                    //print_r($post_id);exit;
                    if(!empty($post_id)) $flash_messages[] = 'Your message has been posted to facebook.';
                } catch (Exception $e) {
                    LogAction::log('Exception thrown in CommunicationController::confirm(): post to facebook. ' . $e->getMessage(), 'CommunicationController');        
                    Application::set_flash('An error has occured posting to facebook.', 'error');
                }
            }
            if(!empty($flash_messages)) Application::set_flash(implode('<br />', $flash_messages));
            $this->redirect('communication');
        }
        
    }
    
    function manage() {
        $activities = ActiveRecordBase::query("SELECT id, name, cost_definition FROM activities WHERE member_id = {$this->registry['member']->id} AND archived = 'n'");
        foreach($activities as $activity){
            $costs = json_decode($activity['cost_definition']);
            //print_r($costs);
            $this->registry['list_selection'][$activity['name']]['id'] = $activity['id'];
            foreach($costs->registration->division as $k => $d)
                $this->registry['list_selection'][$activity['name']]['divisions'][$k] = $d;
        }
        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
    }
    
    function add_to_list() {
        if(!empty($_POST)) {
            $emails = array_map('trim', explode("\n", $_POST['emails']));
            $email_count = count($emails);
            if($email_count <= 50) {
                foreach($emails as $email) {
                    foreach($_POST['lists'] as $list) {
                        SiteEmail::register_user_for_list(array('address' => $email), $list);
                    }
                }
                if($email_count == 1) Application::set_flash("'{$emails[0]}' has been added to your selected mailing list(s).");
                else if($email_count <= 5) Application::set_flash("'" . implode("', '", $emails) . "' have been added to your selected mailing list(s).");
                else Application::set_flash("The email addresses have been added to your selected mailing list(s).");
            } else {
                Application::set_flash("You can only add up to 50 email addresses at a time to prevent abuse.", 'error');           
            }
        }
        $this->redirect('communication');
    }
    
    function remove_from_list() {
        if(!empty($_POST)) {
            $emails = array_map('trim', explode("\n", $_POST['emails']));
            foreach($emails as $email) {
                foreach($_POST['lists'] as $list) {
                    SiteEmail::remove_user_from_list(array('address' => $email), $list);
                }
                Application::set_flash("'{$email}' has been removed from your selected mailing list(s).");          
            }
        }
        $this->redirect('communication');
    }
    
    function fbconnected() {
         
        //Get the FB UID of the currently logged in user
        $facebookSDK = new Facebook(array(
          'appId'  => FB_APP_ID,
          'secret' => FB_APP_SECRET,
          'cookie' => true
        ));
        
        $user = $facebookSDK->getUser();
        //$access_token = $facebookSDK->getAccessToken();
        //echo $access_token;
        //check permissions list
        $permissions_list = $facebookSDK->api(
           '/me/permissions',
           'GET',
           array(
              'access_token' => $access_token
           )
        );  
        
        //print_r($permissions_list);
        
        $postData = array(
            'message'     => "practice\r\nwe talking bout practice",
            'access_token' => $this->registry['facebook_app_token'],
            //'picture'     => 'Address of image', 
            //'link'        => 'URL',
            'name'        => 'URL title',
            'caption'     => 'Caption (under the URL title)',
            'description' => 'Description',
            //'actions'     => array('name' => 'Action name', 'link' => 'Action link')
        );
        $post_id = $facebookSDK->api("/{$this->registry['member']->facbook_uid}/feed", 'POST', $postData);
        
    }
    
    function fbconnect($params) {
        if(!empty($params)) {
            $params = explode('&', substr($params, 1));
            foreach($params as $p) {
                $temp = explode('=', $p);
                $response[$temp[0]] = urldecode($temp[1]);
            }
            if(!empty($response['error'])) {
                Application::set_flash("Connecting to FaceBook generated an error: {$response['error']} -> {$response['error_description']}", 'error');
                $this->redirect('communication');
                exit;
            }
            
        }
        
        $facebookSDK = new Facebook(array(
          'appId'  => FB_APP_ID,
          'secret' => FB_APP_SECRET,
          'cookie' => true
        ));

        $config = array(
            'redirect_uri' => 'http://www.registrationhandler.com/communication/fbconnect/',
            'scope' => 'publish_stream,read_stream,manage_pages,create_event'
        );
        

        $user = $facebookSDK->getUser();
         
        //if the user has already allowed the application, you'll be able to get his/her FB UID
        if($user) {//print_r($user);echo $fb->getApplicationAccessToken();
           //do stuff when already logged in
            //$fb = new SimpleFacebook($facebookSDK, $config);
            try {
                // Proceed knowing you have a logged in user who's authenticated.
                $user_profile = $facebookSDK->api('/me');
                $this->registry['member']->facebook_app_token = $facebookSDK->getApplicationAccessToken();
                $this->registry['member']->facebook_uid       = $user_profile['id'];
                $this->registry['member']->save();
                Application::set_flash("You have successfully connected your account to facebook and can post messages directly to your page.");
                $this->redirect('communication');
              } catch (FacebookApiException $e) {
                LogAction::log('Exception thrown in CommunicationController::fbconnect(): ' . $e->getMessage(), 'CommunicationController');        
                Application::set_flash("Connecting to FaceBook generated an error: " . $e->getMessage(), 'error');
                $this->redirect('communication');
                $user = null;
              }         
        } else {
           //if not, let's redirect to the ALLOW page so we can get access
           //Create a login URL using the Facebook library's getLoginUrl() method
           $login_url_params = array(
              'scope' => 'publish_stream,read_stream,create_event,manage_pages',
              'fbconnect' =>  1,
              'redirect_uri' => 'https://www.registrationhandler.com/communication/fbconnect/'
           );
           $login_url = $facebookSDK->getLoginUrl($login_url_params);
            
           //redirect to the login URL on facebook
           header("Location: {$login_url}");
           exit;
        }


    }

    private function do_attachment_logic() {
        if(!empty($_FILES)) {//print_r($_FILES);exit;
            $max = count($_FILES['attachments']['name']);
            for($i = 0; $i < $max; $i ++) {
                if($_FILES['attachments']['error'][$i] == 0) {
                    $file_size = filesize($_FILES['attachments']['tmp_name'][$i]);
                    $name = $_FILES['attachments']['name'][$i];
                    $tmp_name = $_FILES['attachments']['tmp_name'][$i];
                    $type = $_FILES['attachments']['type'][$i];
                    // is there an image
                    if($file_size > 1) {
                        if ($file_size <= 2621440) {
                            $allowed   = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif', 'image/tiff', 'application/pdf', 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'application/msword', 'application/excel', 'application/vnd.ms-excel', 'audio/wave', 'audoio/x-wave', 'text/tab-separated-values', 'text/plain', 'application/plain', 'video/mpeg', 'audio/mp3', 'video/quicktime');
                    
                            if (in_array($type, $allowed)) {
                                
                                $extension = substr($name, strpos($name, '.') + 1);
                                $file_name = $this->registry['member']->id . basename($tmp_name) . '.' . $extension;
                                $destination = SITE_ROOT . 'images/member_attachments/' . $file_name;
                        
                    
                                if (move_uploaded_file($tmp_name, $destination)){
                                    chmod($destination, 0775);
                                    $this->registry['attachments'][] = array('extension' => $extension, 'original_name' => basename($name), 'file' => $file_name);
                                } else {
                                    Application::set_flash("There was an error uploading the file '" . basename($name) . "'.", 'error');
                                }
                    
                            } else {
                                Application::set_flash('You are not permitted to send this kind of file. Please use an image, pdf, quicktime video, mp3, mp4 or macro-free office document.', 'error');
                            }
                
                        } else {
                            Application::set_flash('Please choose a file 2.5MB or smaller in size.', 'error');
                        }
                    }
                }
            }
        }
    
    }


    function _ajax_communications_datatable($params) {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables for datatable plugin
         */
         if(empty($_POST)) die;

        // DB table to use
        $table = 'member_sent_communications';
         
        // Table's primary key
        $primaryKey = 'id';
         
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes
        $columns = array(
            array( 'db' => 'id', 'dt' => 0 ),
            array( 'db' => 'message_attributes', 'dt' => 1 ),
            array(
                'db'        => 'created_at',
                'dt'        => 2,
                'formatter' => function( $d, $row ) {
                    return date( 'M j, Y', strtotime($d));
                }
            ),
            array( 'special' => "member_id = " . intval($_POST['member_id']))
        );
         
        // SQL server connection information
        $sql_details = array(
            'user' => DB_USER,
            'pass' => DB_PASSWORD,
            'db'   => DB_NAME,
            'host' => DB_HOST
        );
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
         
        //require( 'ssp.class.php' );
         
        echo json_encode(
            SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
        );
    }


}