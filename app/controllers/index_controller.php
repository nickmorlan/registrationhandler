<?php
/**
 * PACKAGE: brightguy
 * 
 * This controller contains our index functionality
 *
 * @copyright 2010 blackcreek solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class IndexController Extends ControllerBase {

    function __construct ($params = array()) {
        $this->no_cache();
        //$this->authenticate = array('add_to_wishlist');
        $this->layout = 'application_front';
    }
    

    function do_page() {
        if(substr($this->controller_method, 0, 6) != '_ajax_') {
            require(SITE_ROOT . SITE_VIEWS . 'application/' . $this->layout . '.tpl');
        }
    }  

    /**
     * Controller Methods
     */
    function index ($params) {
        if(!empty($params) && is_numeric($params)) {
            session_write_close();
            session_destroy();
            session_start();
            session_regenerate_id();
            
            $_SESSION['last_registration'] = intval($params);
            $registration = Registration::find($params);
            $member = Member::find($registration->member_id);
            $name = (!empty($member->name)) ? Activity::decode_url_safe($member->name) : Activity::decode_url_safe($member->contact_name);
            $this->redirect('register/activities/' . $member->id . '/' . $name );
            exit;
        }

        $this->layout = 'application_index_boot';
        $this->registry['js_files'][] = "/js/plugins/tweetable/tweetable.jquery.min.js";
        $this->registry['js_files'][] = "/js/plugins/carouFredSel/jquery.carouFredSel-6.2.1-packed.js";
    }

    function email_signup() {
      if(!empty($_POST)) {
        if (filter_var($_POST['newsletter_email'], FILTER_VALIDATE_EMAIL)) {
          Application::set_flash('Your email has been added to our notification list.');
          SiteEmail::register_user_for_list(array('address' => $_POST['newsletter_email']), 'notifications');
        } else {
          Application::set_flash($_POST['newsletter_email'] . ' is not a valid email address.', 'error');
        }
      }
      $this->redirect('');
    }
    
    function privacy() {
      $this->registry['sub_title'] = 'Privacy Policy';            
    }

    function terms() {
      $this->registry['sub_title'] = 'Terms of Service';            
    }
    
    function pricing() {
        $this->registry['sub_title'] = 'Pricing';      
        $this->registry['nav_marker'] = 'pricing';      
    }
    
    function benefits() {}
    
    function features() {}

    function contact() {
        $this->registry['sub_title'] = 'Contact Us';
        if(!empty($_POST)) {
          if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) !== false) {
            $settings = array('from' => "{$_POST['name']} <noreply@registrationhandler.com>", 
                              'to'         => 'nick@r5kmedia.com', 
                              'subject'    => 'Registration Handler: ' . $_POST['subject'], 
                              'h:Reply-To' => $_POST['email'],
                              'text'       => $_POST['message']);
            SiteEmail::send_email($settings);
            Application::set_flash('Your message has been sent.');
            $this->redirect('contact');
          }
        }
         $this->registry['nav_marker'] = 'contact';      
   }
    
    function online_youth_football_registrations() {}

    function sitemap () {
        $this->registry['manufacturers'] = ActiveRecordBase::query("SELECT * FROM manufacturers");
        $this->registry['products'] = ActiveRecordBase::query("SELECT products.sku, products.product_name, products.manufacturer, products.description_short, products.category, products.image_alt_text " . 
                                                              "FROM products " .
                                                              "WHERE primaries = '1' " .
                                                              "AND category != 'Bulb' AND category != 'Accessory' AND category != 'Battery' " .
                                                              "ORDER BY manufacturer, product_name");
    }
    
    function teste() {
        $activity = new Activity();
        $activity->generate_default_form_definition_json();
        $activity->form_definition_json = json_encode($activity->generate_default_form_definition_json());
        echo $activity->form_definition_json;
    }
    
    

    function activities($params) {
        $this->view = 'active_registrations';
        $member = Member::find($params[0]);
        if($member->name != urldecode($params[1]) && $member->contact_name != urldecode($params[1])) {
            header('HTTP/1.0 404 Not Found');
            $this->redirect('');
            exit;
        }
        //print_r($member);
        $query = "SELECT activities.* "
               . "FROM activities "
               . "WHERE activities.registration_start_date <= '" . date('Y-m-d') . "' " 
               . "AND activities.registration_end_date >= '" . date('Y-m-d') . "' " 
               . "AND activities.member_id = " . $member->id . " "
               . "ORDER BY activities.registration_start_date";
        $this->registry['activities'] = ActiveRecordBase::fetch_as_object($query, 'activity');
        $query = "SELECT activities.* "
               . "FROM activities "
               . "WHERE activities.registration_start_date <= '" . date('Y-m-d', strtotime('+ 7 days')) . "' " 
               . "AND activities.registration_start_date > '" . date('Y-m-d') . "' " 
               . "AND activities.member_id = " . $member->id . " "
               . "ORDER BY activities.registration_start_date";
        $this->registry['upcoming_activities'] = ActiveRecordBase::fetch_as_object($query, 'activity');
        $query = "SELECT activities.* "
               . "FROM activities "
               . "WHERE activities.registration_end_date >= '" . date('Y-m-d', strtotime('- 7 days')) . "' " 
               . "AND activities.registration_end_date < '" . date('Y-m-d') . "' " 
               . "AND activities.member_id = " . $member->id . " "
               . "ORDER BY activities.registration_start_date";
        $this->registry['closed_activities'] = ActiveRecordBase::fetch_as_object($query, 'activity');
        $this->registry['owner'] = $member;
    }


    function temp_gbyf_contact_email() {

        //print_r($_POST);
        if(!empty($_POST)) {


                //mail('nick@r5kmedia.com', 'gbyf contact form', $message);
                $settings = array('name' => $_POST['Name'], 'email' => $_POST['Email'], 'message' => $_POST['Email'] . "\r\n\r\n" . $_POST['Message']);
                SiteEmail::temp_gbyf($settings);
                
                //if($mail->Send()) {  // send e-mail
                //$status =  'Your message has been sent.';
                //echo $status; die();
                
                
        } 

        die;
    }


}