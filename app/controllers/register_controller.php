<?php
/**
 * PACKAGE: BrightGuy
 * 
 * This controller contains our admin functionality
 *
 * @copyright 2009 r5k
 * @author Nick Morlan<nick@hddg.net>
 */

class RegisterController Extends ControllerBase {

    function __construct() {
        $this->layout = 'application_front';
        $this->controller = 'register';
        $this->no_cache();
    }

    /* so no templates are returned with _ajax methods */
    function do_page() {
        if(substr($this->controller_method, 0, 6) != '_ajax_') {
            require(SITE_ROOT . SITE_VIEWS . 'application/' . $this->layout . '.tpl');
        }
    }


    function index($params) {
        $activity = Activity::find_by(array('id' => $params[0], 'name' => urldecode(str_replace('-', ' ',$params[1]))));
        if(empty($activity)) {
            $this->redirect('');
            exit;
        }
        //$_SESSION['testmode'] = false;
        $registration = new Registration();
        $member       = Member::find($activity->member_id);
        
        // for displaying the cancel button
        $registrations = Registration::find_all_by(array('session_id' => session_id(), 'ip_address' => Helpers::get_real_ip(), 'transaction_ref' => ''));
        $this->registry['current_registration_count'] = count($registrations);
        //echo strtotime($activity->registration_end_date) . ' ' . strtotime(date('Y-m-d'));
        // run a bunch of double checks to be sure there is no one
        // trying to curl data and charge a card...
        if($params[2] == 'test' || $_SESSION['testmode'] === true) {
            // let a test regirstation run thru
            $_SESSION['testmode'] = true;
        } else {
            if(empty($activity) || empty($member)) {
                $this->redirect('');
                exit;
            }
            if(strtotime($activity->registration_start_date) > strtotime(date('Y-m-d'))) {
                $this->registry['closed_error'] = "{$activity->name} opens for registration " . date('M j, Y', strtotime($activity->registration_start_date)) . ".";
            }
            if(strtotime($activity->registration_end_date) < strtotime(date('Y-m-d'))) {
                $this->registry['closed_error'] = "Registration for {$activity->name} has passed.";
            }
            if($member->active == 'n') {
                $this->registry['closed_error'] = "Regitration for {$activity->name} has been closed.";
            }
            if($activity->active == 'n') {
                $this->registry['closed_error'] = "Regitration for {$activity->name} has been closed.";
            }
        }
        
        if(!empty($_POST)) {
            if(!empty($_POST['birth_date'])) $_POST['birth_date'] = date('Y-m-d', strtotime($_POST['birth_date']));
            if(!empty($_POST['birth_date_mm'])) $_POST['birth_date'] = $_POST['birth_date_yyyy'] . '-' . $_POST['birth_date_mm'] . '-' . $_POST['birth_date_dd'];
            $info = $_POST;
            $info['city'] = ucwords($_POST['city']);
            $info['member_created'] = 'n';
            $info['r5k_payment_hold'] = 'n';
            $info['activity_id']  = $activity->id;
            $info['member_id']    = $activity->member_id;
            $info['session_id']   = session_id();
            $info['ip_address']   = Helpers::get_real_ip();
            $info['browser_info'] = $_SERVER['HTTP_USER_AGENT'];
            if(!empty($_POST['custom_fields'])) $info['custom_fields'] = json_encode($_POST['custom_fields']);
            if(!empty($_POST['phone']))         $info['phone'] = Registration::format_to_us_phone_number($_POST['phone']);
            if(!empty($_POST['parent1_phone'])) $info['parent1_phone'] = Registration::format_to_us_phone_number($_POST['parent1_phone']);
            if(!empty($_POST['parent2_phone'])) $info['parent2_phone'] = Registration::format_to_us_phone_number($_POST['parent2_phone']);
            //print_r($info);exit;
            $registration = Registration::create($info);
            if($registration->save()) {
                $_SESSION['current_registration'] = $_SESSION['last_registration'] = $registration->id;
                
                // check to create new user and save info
                if($_POST['save'] == 'y') {
                    $info['email']            = $_POST['email'];
                    $info['password']         = $_POST['password'];
                    $info['password_comfirm'] = $_POST['password_comfirm'];
                    $info['secure_key']       = session_id();
                    $info['ip_address']       = Application::get_real_ip();
                    $user = User::create($info);
                    $user->update_definition_from_registration($registration);
                    
                    if($user->save()) $_SESSION['user'] = $user->id;
                    else Application::set_flash("Sorry but an error occured while saving a profile for your email.", 'error');
                }

                // update saved info with current information
                //echo $_POST['user_select'];exit;
                if($_POST['user_select'] >= 0) {
                    if($user = User::find($_SESSION['user'])) {
                        $_SESSION['user_selected'][] = $_POST['user_select'];
                        $user->update_definition_from_registration($registration, $_POST['user_select']);
                        $user->save();
                    }
                } else {
                    // add another saved defintion..
                    if($this->registry['current_registration_count'] > 0) {
                        if($user = User::find($_SESSION['user'])) {
                            $user->add_definition_from_registration($registration);
                            $user->save();
                        }
                    }
                }

                if($_POST['submit'] == 'Register Another Person') {
                    Application::set_flash("Your information has been saved, please register the next person.");
                    $this->redirect ('register/' . $activity->url_link());              
                } else {
                    // remove 'cart' registrations  from other activityes just in case..
                    ActiveRecordBase::query('DELETE FROM registrations WHERE session_id = \'' . session_id() . '\' AND transaction_ref = \'\' AND activity_id != \'' . $activity->id . '\' AND ip_address = \'' . Helpers::get_real_ip() . '\'');
                    Application::set_flash("Please confirm your information.", 'success');
                    $this->redirect ('register/confirm');
                }
            }           
        } else {
            if(empty($_SESSION['last_registration'])) {
                $registration->state = $activity->state;
            } else {
                // there was a previously finished registtration so use that for a starting value
                $old = Registration::find($_SESSION['last_registration']);
                $registration->email                   = $old->email;
                $registration->phone                   = $old->phone;
                $registration->address_1               = $old->address_1;
                $registration->address_2               = $old->address_2;
                $registration->city                    = $old->city;
                $registration->state                   = $old->state;
                $registration->zip                     = $old->zip;
                $registration->country                 = $old->country;
                $registration->parent1_name            = $old->parent1_name;
                $registration->parent1_phone           = $old->parent1_phone;
                $registration->parent1_email           = $old->parent1_email;
                $registration->parent2_name            = $old->parent2_name;
                $registration->parent2_phone           = $old->parent2_phone;
                $registration->parent2_email           = $old->parent2_email;
                $registration->hospital_name           = $old->hospital_name;
                $registration->emergency_contact       = $old->emergency_contact;
                $registration->emergency_contact_phone = $old->emergency_contact_phone;
                $registration->physician_name          = $old->physician_name;
                $registration->physician_phone         = $old->physician_phone;
                $registration->dentist_name            = $old->dentist_name;
                $registration->dentist_phone           = $old->dentist_phone;
            }
        }
        
        $this->registry['activity'] = $activity;
        $this->registry['registration'] = $registration;
        $this->registry['ics'] = 'rh' . $activity->member_id . $activity->id . '.ics';
        $this->registry['ics_file'] = SITE_ROOT . SITE_VIEWS . 'cache/ics/' . $this->registry['ics'];

        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
        $this->registry['js_files'][] = "/js/plugins/moment.min.js";
        $this->registry['js_files'][] = "/js/plugins/bootstrap-datetimepicker.min.js";
        $this->registry['js_files'][] = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBmj4N4ObWZ5RnLJ8tSSIX_wY-CY_26Q-U&sensor=false";
        $this->registry['js_data'] = json_encode(array('activity' => array('address1' => $activity->address1,
                                                                           'city' => $activity->city,
                                                                           'state' => $activity->state,
                                                                           'zip' => $activity->zip
                                                                           )));
    }
    
    function activities($params) {
        if(is_array($params)) {
            $query = "SELECT members.* "
                   . "FROM members "
                   . "WHERE members.id = " . intval($params[0]) . " "
                   . "AND (members.name = '" . Activity::decode_url_safe($params[1]) . "' OR members.contact_name = '" . Activity::decode_url_safe($params[1]) . "') "
                   . "AND members.active = 'y'";
            $res = ActiveRecordBase::query($query);
            $this->registry['themember'] = $res[0];
            $query = "SELECT activities.* "
                   . "FROM activities "
                   . "WHERE activities.member_id = " . intval($params[0]) . " "
                   . "AND activities.active = 'y' "
                   . "AND activities.registration_start_date <= '" . date('Y-m-d') . "' "
                   . "AND activities.registration_end_date >= '" . date('Y-m-d') . "' "
                   . "ORDER BY activities.registration_start_date, activities.id";
            $this->registry['activities'] = ActiveRecordBase::fetch_as_object($query, 'activity');          
            if(empty($res)) {
                $this->redirect('');
                exit;
            }
        } else if(!empty($params)) {
            
        }
        
    }
    
    
    function confirm($params) {
        if(empty($params)) $query_params = array('session_id' => session_id(), 'ip_address' => Helpers::get_real_ip(), 'transaction_ref' => '');
        else $query_params = $query_params = array('session_id' => $params, 'transaction_ref' => '');
        $registrations = Registration::find_all_by($query_params);
        if(empty($registrations)) {
            $this->redirect('');
            exit;
        }
        //print_r($registration);
        $this->registry['activity'] = Activity::find($registrations[0]->activity_id);
        $costs_def = json_decode($this->registry['activity']->cost_definition, true);//print_r($costs_def);exit;
        $first = true;
        foreach($registrations as $r) {
            $key = array_search($r->division, $costs_def['registration']['division']);
            $cost = ($first) ? $costs_def['registration']['cost'][$key] : $costs_def['registration']['cost'][$key] - $this->registry['activity']->multiple_registration_discount;
            $fee  = ($first) ? 1.5 : .75;
            $r->cost = $cost;
            $this->registry['lineitems'][$r->division]['cost'] += $cost;
            $this->registry['lineitems'][$r->division]['number'] ++;
            $this->registry['fee'] += $fee;
            $this->registry['total'] += $cost;
            $first = false;
        }
        
        // test for a discounted registration for this activity based on the last registration
        //$this->registry['activity']->check_for_registration_discount();
        
        $member = Member::find($this->registry['activity']->member_id);
        if(empty($member) || $member->active == 'n') {
            $this->redirect('');
            exit;
        }
        
        //$_SESSION['testmode'] = false;
        $this->registry['registrations'] = $registrations;
        $this->registry['stripe_api_key'] = ($_SESSION['testmode'] === true) ? STRIPE_PUBLISHABLE_KEY_TEST_MODE : STRIPE_PUBLISHABLE_KEY;
        if(!empty($params)) $this->registry['confirm_session'] = $params;

        // build variables used in rh object for javascript
        $img = (file_exists(SITE_ROOT . "/images/activities/{$this->registry['activity']->id}.png")) ? "/images/activities/{$this->registry['activity']->id}.png" : '';
    
        if(count($this->registry['registrations']) == 1 )  {
            if(!empty($this->registry['registrations'][0]->email))
                $email =  $this->registry['registrations'][0]->email; 
            else if(!empty($this->registry['registrations'][0]->parent1_email))
                $email =  $this->registry['registrations'][0]->parent1_email; 
        }

        //$this->registry['js_files'][] = "https://js.stripe.com/v2/";
        $this->registry['js_files'][] = "https://checkout.stripe.com/checkout.js";
        $this->registry['js_data'] = json_encode(array('activity_id' => $this->registry['activity']->id,
                                                     'activity_name' => $this->registry['activity']->name,
                                                    'checkout_email' => $email,
                                                    'checkout_image' => $img,
                                                   'accept_donation' => $this->registry['activity']->accept_donation,
                                                    'stripe_api_key' => $this->registry['stripe_api_key'],
                                                             'total' => $this->registry['total'],
                                                               'fee' => $this->registry['fee']
                                                                           ));
    }
    
    
    function edit($params) {//exit;
        $activity = Activity::find_by(array('id' => $params[1], 'name' => Activity::decode_url_safe($params[2])));
        $registration = Registration::find_by(array('id' => $params[0], 'session_id' => session_id(), 'ip_address' => Helpers::get_real_ip()));
        if(empty($registration) || empty($activity)) {
            $this->redirect('');
            exit;
        }
    
        $registration->populate_custom_fields();
        if(!empty($_POST)) {
            if(!empty($_POST['birth_date'])) $_POST['birth_date'] = date('Y-m-d', strtotime($_POST['birth_date']));
            if(!empty($_POST['birth_date_mm'])) $_POST['birth_date'] = $_POST['birth_date_yyyy'] . '-' . $_POST['birth_date_mm'] . '-' . $_POST['birth_date_dd'];
            $info = $_POST;
            if(!empty($_POST['custom_fields'])) $info['custom_fields'] = json_encode($_POST['custom_fields']);
            if(!empty($_POST['phone']))         $info['phone'] = Registration::format_to_us_phone_number($_POST['phone']);
            if(!empty($_POST['parent1_phone'])) $info['parent1_phone'] = Registration::format_to_us_phone_number($_POST['parent1_phone']);
            if(!empty($_POST['parent2_phone'])) $info['parent2_phone'] = Registration::format_to_us_phone_number($_POST['parent2_phone']);
            //print_r($info);exit;

            $registration->edit($info);
            if($registration->save()) {
                Application::set_flash("Your registration info has been updated.");
                $this->redirect ('register/confirm');
                exit;
            } 
        } 
        $registration->birth_date = date('m/d/Y', strtotime($registration->birth_date));
        $this->registry['registration'] = $registration;
        $this->registry['activity']     = $activity;
        
        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
        $this->registry['js_files'][] = "/js/plugins/moment.min.js";
        $this->registry['js_files'][] = "/js/plugins/bootstrap-datetimepicker.min.js";
        $this->registry['js_files'][] = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBmj4N4ObWZ5RnLJ8tSSIX_wY-CY_26Q-U&sensor=false";
        $this->registry['js_data'] = json_encode(array('activity' => array('address1' => $activity->address1,
                                                                           'city' => $activity->city,
                                                                           'state' => $activity->state,
                                                                           'zip' => $activity->zip
                                                                           ),
                                                        'division' => $registration->division));
    }

    function destroy_registration($params) {
        $activity = Activity::find_by(array('id' => $params[1], 'name' => Activity::decode_url_safe($params[2])));
        $registration = Registration::find_by(array('id' => $params[0], 'session_id' => session_id(), 'ip_address' => Helpers::get_real_ip(), 'transaction_ref' => ''));
        if(empty($registration) || empty($activity)) {
            $this->redirect('');
            exit;
        }
        //print_r($registration);exit;
        Registration::destroy($registration->id);
        Application::set_flash("Registration for " . $registration->first_name . ' ' . $registration->last_name . ' has been removed.');
        $this->redirect('register/confirm');
    }
    
    /* pushed a cc charge through
    function test5589 () {
        $registration = Registration::find_all_by(array('id' => 1733));
        $activity      = Activity::find($registrations[0]->activity_id);
        $_POST['stripeToken'] = 'tok_4QAVHTorAbFct6';
        $_POST['stripeEmail'] = 'rmlynch12@sbcglobal.net';
        $_POST['fee'] = 0;
        $_POST['cost'] = 115;
        //print_r($registration);
        //print_r($_POST);
        //exit;

        $this->paid_via_stripe($activity, $registrations);
    }
    */


    function pay($method = '') {
        if(empty($_POST)) {
            $this->redirect('');
            exit;
        }
        //print_r($_POST);//exit;
        $session_id    = (!empty($_POST['confirm_session'])) ? $_POST['confirm_session'] : session_id();
        $registrations = Registration::find_all_by(array('session_id' => $session_id, 'ip_address' => Helpers::get_real_ip(), 'transaction_ref' => ''));
        $activity      = Activity::find($registrations[0]->activity_id);
        $member        = Member::find($activity->member_id);
        
        // test for a discounted registration for this activity based on the last registration
        // $activity->check_for_registration_discount();

        // run a bunch of double checks to be sure there is no one
        // trying to curl data and charge a card...
        // skips checks in check mode
        if($_SESSION['testmode'] !== true) {
            if(empty($registrations) || empty($activity) || empty($member)) {
                $this->redirect('');
                exit;
            }
            if($member->active == 'n') {
                Application::set_flash("Regitration for {$activity->name} has been closed.", 'error');
                $this->redirect('register/' . $activity->id . '/' . $activity->name);
                exit;
            }
            if($activity->active == 'n') {
                Application::set_flash("Regitration for {$activity->name} has been closed.", 'error');
                $this->redirect('register/' . $activity->id . '/' . $activity->name);
                exit;
            }
            if($activity->registration_start_date > date('Y-m-d')) {
                Application::set_flash("{$activity->name} opens for registration " . date('M j, Y', strtotime($activity->registration_start_date)) . ".", 'error');
                $this->redirect('register/' . $activity->id . '/' . $activity->name);
                exit;
            }
            if($activity->registration_end_date < date('Y-m-d')) {
                Application::set_flash("Registration for {$activity->name} has passed.", 'error');
                $this->redirect('register/' . $activity->id . '/' . $activity->name);
                exit;
            }
            if(empty($registrations)) {
                LogAction::log('RegisterController:pay() no registrations', 'SiteError');
                Application::set_flash("An error has occured.", 'error');
                $this->redirect('register/' . $activity->id . '/' . $activity->name);
                exit;
            }
        }
        //if($_SESSION['testmode'] == true) {echo $session_id;print_r($regstrations);print_r($activity);exit;}
        // check if there is a limit on the registration
        /*
        $cost_def = json_decode($activity->cost_definition);        
        $division = (array) $cost_def->registration->division;
        $limit    = (array) $cost_def->registration->limit;     
        $key = array_search($_POST['division'], $division);
        if($limit[$key] > 0) {  
            $res = ActiveRecordBase::query("SELECT count(id) as max_regs FROM registrations WHERE activity_id = {$activity->id}");
            if($limit[$key] >= $res[0]['max_regs']) {
                Application::set_flash("Registration has closed, the maximum number of registrations had been reached.", 'error');
                $this->redirect('register/' . $activity->id . '/' . $activity->name);
                exit;
            }
        }*/
    
        // if this registration has been recorded as charged..
        if(!empty($registration->transaction_ref)) {
            $this->redirect("register/complete");
            exit;
        }

        // do the appropriate payment method..
        if($_POST['payment'] == 'cc')         $this->pay_via_authorize_net($activity, $registrations);
        elseif($_POST['payment'] == 'paypal') $this->pay_via_paypal($activity, $registrations);
        elseif($method == 'stripe')           $this->paid_via_stripe($activity, $registrations);
    }   


    function complete($transaction_ref) {
        $registrations = Registration::find_all_by(array('session_id' => session_id(), 'ip_address' => Helpers::get_real_ip(), 'transaction_ref' => $transaction_ref));
        $activity      = Activity::find($registrations[0]->activity_id);
        $_SESSION['current_registration'] = '';
        $_SESSION['last_registration'] = '';
        $_SESSION['testmode'] = '';
        
        if(empty($registrations) || empty($activity)) {
            Application::set_flash('An error has occured.', 'error');
            //$registrations = Registration::find_all_by(array('id' => 1245));
            //$activity      = Activity::find($registrations[0]->activity_id);
            $this->redirect('');
            exit;
        }
        
        $res = ActiveRecordBase::query('SELECT id, name, contact_name FROM members WHERE id = ' . $activity->member_id);
        $this->registry['member'] = $res[0];
        if(empty($this->registry['member']['name'])) $this->registry['member']['name'] = $this->registry['member']['contact_name'];     
        $this->registry['registrations'] = $registrations;
        $this->registry['activity']      = $activity;
        $this->registry['ics'] = 'rh' . $activity->member_id . $activity->id . '.ics';
        $this->registry['ics_file'] = SITE_ROOT . SITE_VIEWS . 'cache/ics/' . $this->registry['ics'];

    }
    
    function invite($id) {
        $registration = Registration::find($id);
        if(empty($registration) || empty($_POST)) {
            $this->redirect('');
            exit;
        }
        
        $activity = Activity::find($registration->activity_id);
        $emails = array_map('trim', explode("\n", $_POST['emails']));
        foreach($emails as $email) {
            SiteEmail::send_activity_invite_for($registration, $activity, $email, $_POST['message']);
        }
        Application::set_flash("Your invites have been sent!");
        $this->redirect('register/complete/' . $registration->transaction_ref);
    }
    
    function download_ics($activity_id) {
        //function download_as_pdf($html, $destination_filename='file.pdf', $preview = true) {
        //echo $html;exit;
        $activity = Activity::find($activity_id);
        if(empty($activity)) {
            $this->redirect('');
            exit;
        }
        
        
        //echo mb_detect_encoding($html);exit;
        $ics_file = SITE_ROOT . SITE_VIEWS . 'cache/ics/rh' . $activity->member_id . $activity->id . '.ics';
        $fsize = filesize($ics_file);
        $path_parts = pathinfo($ics_file);
        $ext = strtolower($path_parts["extension"]);
       
        // Determine Content Type
        switch ($ext) {
            case "ics": $ctype = "text/calendar"; break;
            case "pdf": $ctype = "application/pdf"; break;
            case "exe": $ctype = "application/octet-stream"; break;
            case "zip": $ctype = "application/zip"; break;
            case "doc": $ctype = "application/msword"; break;
            case "xls": $ctype = "application/vnd.ms-excel"; break;
            case "ppt": $ctype = "application/vnd.ms-powerpoint"; break;
            case "gif": $ctype = "image/gif"; break;
            case "png": $ctype = "image/png"; break;
            case "jpeg": $ctype="image/jpg"; break;
            case "jpg": $ctype="image/jpg"; break;
            default: $ctype = "application/force-download";
            }
    
        header("Pragma: public"); // required
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Cache-Control: private",false); // required for certain browsers
        header("Content-Type: text/Calendar");
        header("Content-Disposition: attachment; filename=\"".$activity->name.".ics\";" );
        header("Content-Transfer-Encoding: binary");
        header("Content-Length: ".$fsize);
        ob_clean();
        flush();
        readfile($ics_file);    
        exit;
    }
    
    function paypal_confirmation() {
        $registration = Registration::find_by(array('session_id' => session_id(), 'ip_address' => Helpers::get_real_ip()));
        $activity     = Activity::find($registration->activity_id);

        if(empty($registration) || empty($activity)) {
            Application::set_flash('An error has occured.', 'error');
            $this->redirect('');
            exit;
        }
    
        $r = new PayPal();
        // Final authorization of PayPal transaction. We have to do this before saving the order.
        $final = $r->doPayment();
        //print_r($final);
        $checkout_details = $r->getCheckoutDetails($final['TOKEN']);
        //print_r($checkout_details);
        list($amount, $currency, $invoice, $session_id) = explode('|', $checkout_details['CUSTOM']);

        // update registration record..
        $registration->transaction_ref = $final['TRANSACTIONID'];
        if(!empty($registration->comments)) $registration->comments .= PHP_EOL;
        $registration->comments .= 'via Paypal: ' . $checkout_details['PAYMENTREQUEST_0_NOTETEXT'];
        $registration->amount_paid     = $activity->cost;

        if ($final['ACK'] == 'Success') {
            $existing_order = Registration::find_by(array('transaction_ref'=> $registration->transaction_ref));
            if (empty($existing_order)) { // Make sure there's no existing order with this PayPal token.
                $registration->save();
                Application::set_flash("Thank you for registering.<br /> <em>Please print this page for your records.</em>","success");
                $this->redirect("register/complete");
            } else {
                Application::set_flash("Oops! This registration was already placed and paid for.","error");
                $this->redirect("register/confirm");
            }
        } elseif ($final['ACK'] == 'Failure') {     
            Application::set_flash("Transaction Failed..<br />{$final['L_LONGMESSAGE0']}","error");
            $this->redirect("register/confirm");
        }
        
    } // end paypal_confirmation

    function _ajax_user_login() {
        $user = User::find_by(array('email' => $_POST['email']));
        $pw = User::encrypt_password($_POST['password'], $user->salt);
        if ($pw == $user->password) {
            $_SESSION['user'] = $user->id;
            echo '{"status":"success","user_id":' . $user->id . ',"info":' . $user->definitions . '}';
        } else {
            echo '{"status":"error"}';
        }

    }
    
    function _ajax_check_user_email() {
        $res = ActiveRecordBase::query("SELECT email FROM users WHERE email = " . ActiveRecordBase::quote($_POST['email']));
        if(!empty($res)) {
            echo '{"status":"success"}';
        } else {
            echo '{"status":"error"}';
        }
    }

    function _ajax_get_user_choices() {
        if ($user = User::find($_SESSION['user'])) {
            echo '{"status":"success","user_id":' . $user->id . ',"info":' . $user->definitions . '}';
        } else {
            echo '{"status":"error"}';
        }
    }

    private function paid_via_stripe($activity, $registrations) {
        //print_r($_POST);exit;
        if(!empty($_POST['stripeToken'])) {
            // get member payment info
            $pay = MemberPaymentSetting::find_by(array('member_id' => $activity->member_id));
            $pk  = (!empty($pay->stripe_access_token) && $pay->stripe_status == 'active') ? $pay->stripe_access_token : '';
            $stripe_config = array(
              "secret_key"      => ($_SESSION['testmode'] === true) ? STRIPE_SECRET_KEY_TEST_MODE : STRIPE_SECRET_KEY,
              "publishable_key" => $pk
            );
            Stripe::setApiKey($stripe_config['secret_key']);
            
            $token = $_POST['stripeToken'];
            $email = $_POST['stripeEmail'];
            
            // build the charge description
            foreach($registrations as $r) {
                if(!empty($registration->email)) $values[$r->division]['desc'][] = $registration->email;
                else if(!empty($registration->parent1_email)) $values[$r->division]['desc'][] = $registration->parent1_email;
                else $values[$r->division]['desc'][] = $registration->first_name . ' ' . $registration->last_name;
                $values[$r->division]['number'] ++;
            }
            foreach($values as $k => $v) {
                $desc .= " ({$v['number']}) {$k} - " . implode(', ', $v['desc']);
            }
            $charge_params = array('currency'    => 'usd',
                                   'amount'      => ($_POST['cost'] * 100) + ($_POST['fee'] * 100),
                                   'card'        => $token,
                                   'description' => $activity->name . ':' . $desc);
            //print_r($charge_params);exit;
            try {
                if(!empty($stripe_config['publishable_key'])) {
                    $charge_params['application_fee'] = $_POST['fee'] * 100;
                    $charge = Stripe_Charge::create($charge_params, $stripe_config['publishable_key']);
                } else {
                    $charge = Stripe_Charge::create($charge_params);
                }
            } catch (Stripe_CardError $e) {
              // The card has been declined
                LogAction::log('Exception thrown RegisterController:paid_via_stripe() Card Declined: ' . $e->json_body['error']['type'] . ' : ' . $e->json_body['error']['message'], 'stripe');
                Application::set_flash('The card has been declined with the following error...<br />' . $e->json_body['error']['message'], 'error');
                $this->redirect('register/confirm');
                exit;
            } catch (Stripe_InvalidRequestError $e) {
                LogAction::log('Exception thrown RegisterController:paid_via_stripe() Card Declined: ' . $e->json_body['error']['type'] . ' : ' . $e->json_body['error']['message'], 'stripe');
                Application::set_flash('The card has been declined with the following error...<br />' . $e->json_body['error']['message'], 'error');
                $this->redirect('register/confirm');
                exit;

            }
            
            $costs = json_decode($activity->cost_definition, true);
            $this->registry['activity'] = Activity::find($registrations[0]->activity_id);
            $costs_def = json_decode($this->registry['activity']->cost_definition, true);
            $first = true;
            date_default_timezone_set('America/New_York');
            $timezone_date = date('Y-m-d');
            foreach($registrations as $r) {
                $key = array_search($r->division, $costs_def['registration']['division']);
                $cost = ($first) ? $costs_def['registration']['cost'][$key] : $costs_def['registration']['cost'][$key] - $this->registry['activity']->multiple_registration_discount;
                $fee  = ($first) ? 1.5 : .75;
                $first = false;
                $r->registration_amount = $cost;
                $r->r5k_fee             = $fee;
                $r->date_paid           = $timezone_date;
                $r->amount_paid         = $cost + $fee;
                $r->donation_amount     = $_POST['donation_amount'];
                $r->transaction_ref     = $charge->id;
                $r->r5k_payment_hold    = 'n';
                // if a memeber is integrated with stripe...
                if($pk !='') {
                    $r->r5k_pay_via       = 'stripe';
                    $r->r5k_pay_reference = $charge->id;
                }
                if($_SESSION['testmode'] === true) {
                    $r->r5k_payment_hold = 'y';
                    $r->notes = 'Test Registration';
                }
                $r->save();
                LogForDashboard::log_registration_for_activity($r, $this->registry['activity']);
                
                // register them to the proper mailing lists
                if(!empty($r->email))
                    SiteEmail::register_user_for_list(array('address' => $r->email), 'a' . $this->registry['activity']->id);
                    if(!empty($key)) SiteEmail::register_user_for_list(array('address' => $r->email), 'a' . $this->registry['activity']->id . 'd' . $key);
                if(!empty($r->parent1_email) && $r->parent1_email != $r->email) {
                    // register parent to the proper mailing lists
                    SiteEmail::register_user_for_list(array('address' => $r->parent1_email), 'a' . $this->registry['activity']->id);
                    if(!empty($key)) SiteEmail::register_user_for_list(array('address' => $r->parent1_email), 'a' . $this->registry['activity']->id . 'd' . $key);  
                }
                if(!empty($r->parent2_email) && $r->parent2_email != $r->email) {
                    // register parent to the proper mailing lists
                    SiteEmail::register_user_for_list(array('address' => $r->parent2_email), 'a' . $this->registry['activity']->id);
                    if(!empty($key)) SiteEmail::register_user_for_list(array('address' => $r->parent2_email), 'a' . $this->registry['activity']->id . 'd' . $key);  
                }
                //if(!empty($r->errors)){print_r($r->errors);exit;}
                $_POST['donation_amount'] = 0; // only save amount with one registration
            }
            //print_r($_POST);exit;
            SiteEmail::send_registration_confirmation_email($registrations, $activity, ($charge_params['amount'] / 100), $email);
            Application::set_flash("Thank you for registering.<br /> <em>Please print this page for your records.</em>","success");
            $this->redirect ('register/complete/' . $charge->id);
            exit;
        } else {
            Application::set_flash("This transaction was denied..", 'error');
            $this->redirect ('register/confirm');
            exit;
        }
    }

    private function pay_via_authorize_net($activity, $registration) {
        // check with authorize.net 
        require_once SITE_ROOT . 'app/library/AuthorizeNet/AuthorizeNet.php'; 
        $transaction = new AuthorizeNetAIM;
        $transaction->setSandbox(AUTHORIZENET_SANDBOX);
        $transaction->setFields(
            array(
            'test_request' => true,
            'amount' => $activity->cost, 
            'card_num' => $_POST['account_num'], 
            'exp_date' => $_POST['exp_month'] . '/' . $_POST['exp_year'],
            'first_name' => $_POST['name'],
            //'last_name' => $_POST['x_last_name'],
            'address' => $_POST['billing_address1'] . chr(10) . $_POST['billing_address2'],
            //'city' => $_POST['bill_to_city'],
            //'state' => $_POST['bill_to_state'],
            //'country' => $_POST['bill_to_country'],
            'zip' => $_POST['billing_zip'],
            //'email' => $_POST['email'],
            //'email_customer' => FALSE
            //'card_code' => $_POST['x_card_code'],
            )
        );
        $response = $transaction->authorizeAndCapture();
        //print_r($response);exit;
        if ($response->approved) {
            $registration->date_paid       = date('Y-m-d');
            $registration->amount_paid     = $activity->cost;
            $registration->transaction_ref = $response->authorization_code;
            $registration->save();
            Application::set_flash("Thank you for registering.<br /> <em>Please print this page for your records.</em>","success");
            $this->redirect ('register/complete');
        } else {
            // transaction declined..
            //Logger::log('Authorize.net Refusal: response_reason_code=' . $response->response_reason_code.'&response_code='.$response->response_code.'&response_reason_text=' .$response->response_reason_text);
            Application::set_flash("This transaction was denied for the following reasons..<br/>" . $response->response_reason_text, 'error');
            $this->redirect ('register/confirm');
            exit;
        }
        
    
    }
    
    private function pay_via_paypal($activity, $registration) {
        $r = new PayPal();
        $amount = $activity->cost;
        $desc = $activity->name . ' : ' . $registration->first_name . ' ' . $registration->last_name;
        //$amount = '.03';
        $ret = ($r->doExpressCheckout($amount, $desc, session_id()));
        
        // doExpressCheckout will send the user to paypal and they will return to the url
        // defined in the paypal class
        
        // If we reach this point, an error has occurred. The auxiliary information is in the $ret array.
        echo 'An error has occurred:<br /><br />';
        ob_start();
        var_dump($ret);
        $text = ob_get_clean();
        //Logger::log('checkout_controller: paypal.. ' . $text);
        print_r($ret);
    
    }
    
    private function reset_session() {
        setcookie('registrations', '', time() - (36000), '/','youshouldsign.com'); 
        unset($_COOKIE['key']);
        $_COOKIE = array();
        $_SESSION = array();
        session_destroy();

    }
    /*
    function test() {
        $activity = Activity::find(35);
        $registrations = Registration::find_all_by(array('transaction_ref' => ' tok_35gMLB1DWUc1Ve'));
        //print_r($registrations);exit;
        SiteEmail::send_registration_confirmation_email($registrations, $activity, '84.75');
        
    }
    */

}