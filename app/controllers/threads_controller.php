<?php
/**
 * PACKAGE: cpap wholesale
 * 
 * This uses $this->registry['member'] which is set when the application
 * loads.
 */

class ThreadsController Extends ControllerBase {

	function __construct ($params = array()) {
		$this->no_cache();
		$this->authenticate = true;
		Shirtsio::setApiKey(SHIRTSIO_API_KEY);
		$this->registry['js_files'][] = '<script type="text/javascript" src="/js/slimbox2.js"></script>';
		$this->registry['css_files'][] = '<link rel="stylesheet"  href="/css/slimbox2.css"  type="text/css" />';

}

	function index ($params) {
		$resp = Products::list_categories();
		$this->registry['categories'] = $resp;
		print_r($resp);
		
	}    
	
	/**
	 * Bring back category listing from shirtsio service with base quote
	 */
	function category ($params) {
		try {
			$resp = Products::list_products($params);
			$this->registry['categories'] = $resp;
		} catch (Shirtsio_InvalidRequestError $e) {
		  	LogAction::log('Exception thrown ThreadsController:category() ' . $e->http_body['error'], 'shirtsio');
			$this->redirect('threads');
		}
		
		foreach($resp as $r) {
			$attrs = array('garment[0][product_id]' => $r['product_id'],
			               'garment[0][color]'=> 'White',
			               'garment[0][sizes][med]'=> 25,
                           'print[front][color_count]' => 1);
			try {
				// grab base quote
				$this->registry['quotes'][$r['product_id']] = Quote::get_quote($attrs);
			}
			catch (Shirtsio_ApiError $e) {
		  		LogAction::log('Exception thrown ThreadsController:category() grabbing base quotes' . $e->http_body['error'], 'shirtsio');
			}
		}

	}

	/**
	 * Bring back product detail
	 */
	function product ($params) {
		try {
			$resp = Products::get_product($params);
			$this->registry['product'] = $resp;
			$this->registry['product_id'] = $params;
			print_r($resp);
		} catch (Exception $e) {
		  	LogAction::log('Exception thrown ThreadsController:product() ' . $e->http_body['error'], 'shirtsio');
			$this->redirect('threads');
		}

		$this->registry['sizes'] = array('xxsml' => '2XSmall',
										 'xsml' => 'XSmall',
										 'sml' => 'Small',
										 'med' => 'Medium',
										 'lrg' => 'Large',
										 'xlg' => 'XLarge',
										 'xxl' => '2XLarge',
										 'xxxl' => '3XLarge',
										 'xxxxl' => '4XLarge',
										 'xxxxxl' => '5XLarge',
										 'xxxxxxl' => '6XLarge');
		
	}

	/**
	 * Set up quote with user supplied values
	 */
	function quote() {
		
		if(!empty($_POST)) {
			print_r($_POST);
			$attrs = array('garment[0][product_id]' => $_POST['product_id'],
			               'garment[0][color]' => $_POST['color'],
			               'garment[0][sizes][med]' => $_POST['med'],
			               'garment[0][sizes][lrg]' => $_POST['lrg'],
                           'print[front][color_count]' => $_POST['color_count']);
			print_r($attrs);
			try {
				$quote_resp = Quote::get_quote($attrs);
			} catch (Exception $e) {
				print_r($e);
			}
			print_r($quote_resp);
		}		
	}

	function test(){
		$activities = Activity::find_all();
		foreach($activities as $activity) SiteEmail::register_mailing_lists_for($activity);
	}
}