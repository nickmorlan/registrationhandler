<?php
/**
 * PACKAGE: Garfield Survey
 *
 * Adminstration
 *
 * @copyright 2012 r5k Media llc
 * @author Nick Morlan<nick@r5kmedia.com>
 */
class IndexController Extends ControllerBase {

	function __construct() {
		$this->no_cache();
		$this->layout = 'admin';
	}

	/**
	 * Replaces the parent method to grab the correct view..
	 * added for this controller for special handling
     */  	 
	function render ($class, $method) {
		$view = (empty($this->view)) ? $method : $this->view;
		$this->controller_method = $method;
		$view_folder = "_r5kadmin/" . strtolower(Inflector::singularize(str_replace('Controller', '', $class))) . '/';
		if($class !== false) {
			$view_file = SITE_ROOT . SITE_VIEWS . $view_folder . $view  . '.tpl';
			if(file_exists($view_file)) {
				require(SITE_ROOT . SITE_VIEWS . $view_folder . $view  . '.tpl');
			} else {
				require(SITE_ROOT . SITE_VIEWS . 'error/404.tpl');
			}
		}
	}

	/**
	 * Replaces the parent method to grab the correct partials..
	 * added for this controller for special handling
	 */
	function render_partial($controller, $partial) {
		include(SITE_ROOT . SITE_VIEWS . '_r5kadmin/' . $controller . '/_' . $partial . '.tpl');
	}

	function index ($params) {
	}
	
	function login () {
		if(!empty($_POST)) {
			//quick password check...
			if(($_POST['theuser'] == 'nick' && ($_POST['thepw'] == 'nick')) ) {
				setcookie('auth', 'Y', time()+(60*360), "/", '.registrationhandler.com');
				Application::set_flash("You are now logged in.", 'success');
				$this->redirect('_r5kadmin/index');
			} else {
				Application::set_flash("Invalid Login", 'error');
			}
		}		
	}

	function logout () {
		setcookie('auth', 'Y', time()-3600,"/");
		Application::set_flash("You have been logged out.", 'success');
		$this->redirect('_r5kadmin/index/login');
	}
	
	
	function test_payment($param) {
		$query = "UPDATE registrations SET r5k_pay_via = 'Value1', r5k_pay_reference = 'xxxxccccc' " .
		         "WHERE member_id = 5 " .
		         "AND date_paid < '" . date('Y-m-d') . "' " .
		         "AND transaction_ref != '' " .
		         "AND registration_amount > 0 " .
		         "AND r5k_pay_reference = '' " .
		         "AND r5k_payment_hold = 'n' ";
		echo $query;
	
	}
	
	
	

}