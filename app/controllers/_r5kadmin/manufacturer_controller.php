<?php
/**
 * PACKAGE: BrightGuy
 * 
 * This controller contains our admin functionality
 *
 * @copyright 2009 r5k
 * @author Nick Morlan<nick@hddg.net>
 */
class RegistrationController Extends ControllerBase {

	function __construct() {
		$this->no_cache();
		$this->layout = 'admin' . $_SESSION['level'];
		$this->methods['index'] = array('3', '4');
		$this->methods['create'] = array('3', '4');
		$this->methods['edit'] = array('3', '4');
		$this->methods['destroy'] = array('3', '4');
		$this->methods['process_image'] = array('3', '4');
		$this->methods['delete_registration_image'] = array('3', '4');
	}

	function index ($params) {
		$start = 1;
		$limit = 50;
		$order_by = 'id';
		$asdc = 'ASC';
		if(!is_array($params)) {
			$param[] = $params;
		} else {
			$param = $params;
		}
		foreach($param as $p) {
			if(strpos($p, ':')) {
				$a = explode(':', $p);
				switch($a[0]) {
					case 'start':
						$start = $a[1];
						break;
					case 'limit':
						$limit = $a[1];
						break;
					case 'order_by':
						$order_by = $a[1];
						break;
					case 'asdc':
						$asdc = $a[1];
						break;
				}
			}
		}
		$registrations = Registration::find_all_with_pagination($start, $limit, $order_by, $asdc);
		$this->registry['paginator'] = array_pop($registrations);
		$this->registry['registrations'] = $registrations;
	}
	/**
	 * Replaces the parent method to grab the correct view..
	 * added for this controller for special handling
     */  	 
	function render ($class, $method) {
		$view = (empty($this->view)) ? $method : $this->view;
		$this->controller_method = $method;
		$view_folder = "admin/" . strtolower(Inflector::singularize(str_replace('Controller', '', $class))) . '/';
		if($class !== false) {
			$view_file = SITE_ROOT . SITE_VIEWS . $view_folder . $view  . '.tpl';
			if(file_exists($view_file)) {
				require(SITE_ROOT . SITE_VIEWS . $view_folder . $view  . '.tpl');
			} else {
				require(SITE_ROOT . SITE_VIEWS . 'error/404.tpl');
			}
		}
	}

	/**
	 * Replaces the parent method to grab the correct partials..
	 * added for this controller for special handling
	 */
	function render_partial($controller, $partial) {
		include(SITE_ROOT . SITE_VIEWS . 'admin/' . $controller . '/_' . $partial . '.tpl');
	}


	function imports($params) {
		$products    = Product::find_all();
		$this->registry['product_count']      = count($products);
	}

	function destroy($id) {
		if(Registration::destroy($id)) {
			Application::set_flash ("The registration has been deleted.");
		} else {
			Application::set_flash ("An error has occured.", 'error');
		}
		$this->redirect ('admin/registration');
	}
	
	function create() {

		if(!empty($_POST)) {
			$registration = Registration::create($_POST);
			$registration->created_at = date('Y-m-d');
			if($registration->save()) {
				if($_FILES['image']['tmp_name'][0] != '') {
					$destination = SITE_ROOT . "images/registrations/";
					AdminHelper::resize_image_by_max($_FILES['image']['tmp_name'], $destination . str_replace(' ', '_', $registration ->name). '.jpg', 300);
				}
				Application::set_flash("The Registration has been created.");
				$this->redirect ('admin/registration');
			} 
		} else {
			$registration = new Registration();
		}
		$this->registry['registration'] = $registration;
//
//		if(!empty($_POST)) {
//			$registration = Registration::create($_POST);
//			if($registration->save()) {
//				Application::set_flash("The Registration has been created.");
//				$this->redirect ('admin/registration');
//			} 
//		} else {
//			$registration = new Registration();
//			$res = ActiveRecordBase::query("SHOW TABLE STATUS WHERE name = 'registrations'");
//		}
//			$this->registry['registration'] = $registration;
	}

	function edit($id) {

		if($registration = Registration::find($id)) {
			if(!empty($_POST)) {
				if ($_POST['action']!="delete_image") {
					$registration->edit($_POST);
					if($_FILES['image']['tmp_name'][0] != '') {
						$destination = SITE_ROOT . "images/registrations/";
						AdminHelper::resize_image_by_max($_FILES['image']['tmp_name'], $destination . str_replace(' ', '_', $registration ->name). '.jpg', 300);
					}
					if($registration->save()) {
						Application::set_flash("The Registration has been saved.");
						$this->redirect ('admin/registration');
					}
				} else {
					$this->delete_registration_image($id);
				}
			}
			$this->registry['registration'] = $registration;
		} else {
			$this->redirect ('admin/registration');
		}

//		if($registration = Registration::find($id)) {
//			if(!empty($_POST)) {
//				if($_POST['action'] != 'image') {
//					$registration->edit($_POST);
//					if($registration->save()) {
//						Application::set_flash("The Registration has been saved.");
//						$this->redirect ('admin/registration/index');
//					}
//				} else {
//					$this->process_images($_POST['name']);
//				}
//			} 
//			$this->registry['registration'] = $registration;
//		} else {
//			$this->redirect ('admin/registration/index');
//		}
	}

	function search () {
		if(!empty($_POST)) {
			$searchterm = ($_POST['compare'] == 'LIKE') ? "%" . $_POST['searchterm'] . "%" : $_POST['searchterm'];
			$products = Product::fetch_as_object("SELECT * FROM products WHERE {$_POST['field']} {$_POST['compare']} '{$searchterm}'", 'Product');
			$this->registry['products'] = $products;
		}
		$this->view = 'index';
	}

//	private function process_images ($mfr_name) {					
//		if (($_SESSION['level'] != '') && (($this->authentications['process_images']) || ($this->authentications['all']))) {
//			$destination = SITE_ROOT . "images/registrations/";
//			if($_FILES['image']['tmp_name'][0] != '') {
//				AdminHelper::resize_image_by_max($_FILES['image']['tmp_name'], $destination . $mfr_name. '.jpg', 300);
//
//			}
//			$this->layout = 'admin' . $_SESSION['level'];
//		} else {
//			Application::set_flash(SITE_ADMIN_UNAUTHORIZED_USER_MESSAGE, 'error');
//			$this->redirect('admin/index/login');
//		}
//	}

	function delete_registration_image($id) {
		if ($registration = Registration::find($id)) {
			$file_name = str_replace(' ', '_', $registration->name) . '.jpg';
			$destination = SITE_ROOT . "images/registrations/";
			unlink($destination . $file_name);
			$this->redirect ('admin/registration/edit/' . $id);
		}
	}
}