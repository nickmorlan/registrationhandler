<?php
/**
 * PACKAGE: BrightGuy
 * 
 * This controller contains our admin functionality
 *
 * @copyright 2009 r5k
 * @author Nick Morlan<nick@hddg.net>
 */
class ReportController Extends ControllerBase {

	function __construct() {
		$this->no_cache();
		$this->layout = 'admin' . $_SESSION['level'];
		$this->methods['index'] = array('1', '3', '4');
		$this->methods['sales_by_date'] = array('1', '3', '4');
		$this->methods['sales_by_state'] = array('1', '3', '4');
		$this->methods['products_by_state'] = array('1', '3', '4');
		$this->methods['products_by_unit'] = array('1', '3', '4');
		$this->methods['view_order'] = array('1', '3', '4');
	}

	/**
	 * Replaces the parent method to grab the correct view..
	 * added for this controller for special handling
     */  	 
	function render ($class, $method) {
		$view = (empty($this->view)) ? $method : $this->view;
		$this->controller_method = $method;
		$view_folder = "admin/" . strtolower(Inflector::singularize(str_replace('Controller', '', $class))) . '/';
		if($class !== false) {
			$view_file = SITE_ROOT . SITE_VIEWS . $view_folder . $view  . '.tpl';
			if(file_exists($view_file)) {
				require(SITE_ROOT . SITE_VIEWS . $view_folder . $view  . '.tpl');
			} else {
				require(SITE_ROOT . SITE_VIEWS . 'error/404.tpl');
			}
		}
	}

	/**
	 * Replaces the parent method to grab the correct partials..
	 * added for this controller for special handling
	 */
	function render_partial($controller, $partial) {
		include(SITE_ROOT . SITE_VIEWS . 'admin/' . $controller . '/_' . $partial . '.tpl');
	}

	function index () {
	}

	function sales_by_date () {

		$query = "SELECT text_value FROM controls WHERE name = 'site_name' ORDER BY text_value ASC";
		$site_names = Control::fetch_as_object($query);
		$site_select_menu = '<select name="site_name" id="site_name">
		<option value="">All sites</option>';
		foreach ($site_names as $site_name) {
			$site_select_menu .= "
		<option value=\"{$site_name->text_value}\">{$site_name->text_value}</option>";			
		}
		$site_select_menu.="
	</select>";
		$this->registry['site_select_menu'] = $site_select_menu;

		if(!empty($_POST)) {
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];

			if (($start_date =='') && ($end_date == '')) { // If both dates are blank, redirect.
				Application::set_flash('Please enter at least one date or select a range from the menu.');
				$this->redirect('admin/report/sales_by_date');
			} else {
	
				// If one date is blank, set it equal to the other so that we don't get a date range.
				if ($end_date == '') {
					$end_date = $start_date;
				}
				if ($start_date == '') {
					$start_date = $end_date;
				}
	
				// Make sure the start date is earlier than the end date.
				if ($end_date < $start_date) {
					$hold = $start_date;
					$start_date = $end_date;
					$end_date = $hold;
				}

				// If the two dates are the same, we'll search by start date only.
				if ($start_date == $end_date) {
					$range = False;
				} else {
					$range = True;
				}
	
				$start_date = date('Y-m-d', strtotime($start_date));
				$this->registry['start_date'] = $start_date;

				$site_name = $_POST['site_name'];
				$this->registry['site_name'] = $site_name;

				if ($range) {
					$this->registry['range'] = date('F j, Y', strtotime($start_date)) . ', to ' . date('F j, Y', strtotime($end_date));
					$end_date = date('Y-m-d', strtotime($end_date));
					$this->registry['end_date'] = $end_date;

					if ($site_name == '') {

						$query = "SELECT * FROM orders WHERE created_at >= '{$start_date}' AND created_at <= '{$end_date}' ORDER BY created_at DESC, id DESC";
					} else {
						$query = "SELECT * FROM orders WHERE created_at >= '{$start_date}' AND created_at <= '{$end_date}' AND site_name = '{$site_name}' ORDER BY created_at DESC, id DESC";
					}

				} else {

					if ($site_name == '') {

						$query = "SELECT * FROM orders WHERE created_at = '{$start_date}' ORDER BY id DESC";
					} else {
						$query = "SELECT * FROM orders WHERE created_at = '{$start_date}' AND site_name = '{$site_name}' ORDER BY id DESC";
					}

					$this->registry['range'] = date('F j, Y', strtotime($start_date));
					$this->registry['end_date'] = '';
				}

				$total_subtotals = 0.00;
				$total_shipping = 0.00;
				$total_tax = 0.00;
				$sales = 0.00;

				$total_costs = 0.00;
				$order_cost_totals = array();

				$orders = Order::query($query);
				if (count($orders) == 0) {
					$this->registry['no_results_message'] = "No orders were found for {$this->registry['range']}.";
				} else {
					$this->registry['no_results_message'] = '';
				}
				$this->registry['order_totals'] = array();
				foreach ($orders as $order) {
					$sales += $order['sales_tax'] + $order['ship_cost'] - $order['order_discount'];
					$order_subtotal = 0.00;
					$order_cost_total = 0.00;
					$lineitems = LineItem::find_all_by(array('order_id' => $order['id']));
					foreach ($lineitems as $lineitem) {
						$sales += $lineitem->price * $lineitem->quantity;
						$order_subtotal += $lineitem->price * $lineitem->quantity;
						if ($product = Product::find_by(array('sku'=>$lineitem->sku))) {
							$total_costs += $product->cost * $lineitem->quantity;
							$order_cost_total += $product->cost * $lineitem->quantity;
						} else {
							Application::set_flash('Lookup error for product ' . $lineitem->sku . '.');
						}
					}
					$order_total = $order_subtotal + $order['sales_tax'] + $order['ship_cost'] - $order['order_discount'];
					$this->registry['order_subtotals'][$order['id']] = $order_subtotal;
					$this->registry['order_totals'][$order['id']] = $order_total;
					$this->registry['order_cost_totals'][$order['id']] = $order_cost_total;

					$total_subtotals += $order_subtotal;
					$total_shipping += $order['ship_cost'];
					$total_tax += $order['sales_tax'];
				}
				$this->registry['orders'] = $orders;
				$this->registry['subtotals'] = $total_subtotals;
				$this->registry['shipping'] = $total_shipping;
				$this->registry['tax'] = $total_tax;
				$this->registry['sales'] = $sales;
				$this->registry['total_costs'] = $total_costs;

			}

		} else {
			$this->registry['range'] = '';
		}
	}

	function excel_sales_by_date() {
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];

		$site_name = $_POST['site_name'];

		$start_date = date('Y-m-d', strtotime($start_date)); // Dates should already be in this format, but just in case...
		if (!empty($end_date)) {
			$end_date = date('Y-m-d', strtotime($end_date)); // Again, just in case.
			$range = date('F j, Y', strtotime($start_date)) . ', to ' . date('F j, Y', strtotime($end_date));
			$range_for_filename = $start_date . '_' . $end_date;
			$query = "SELECT * FROM orders WHERE created_at >= '{$start_date}' AND created_at <= '{$end_date}'";
			if (!empty($site_name)) {
				$query .= " AND site_name = '{$site_name}'";
			}
			$query .=" ORDER BY created_at DESC, id DESC";
		} else {
			$range = date('F j, Y', strtotime($start_date));
			$range_for_filename = $start_date;
			$query = "SELECT * FROM orders WHERE created_at = '{$start_date}'";
			if (!empty($site_name)) {
				$query .= " AND site_name = '{$site_name}'";
			}
			$query .= " ORDER BY created_at DESC, id DESC";
		}

		$total_subtotals = 0.00;
		$total_shipping = 0.00;
		$total_tax = 0.00;
		$sales = 0.00;

		$total_costs = 0.00;
		$order_cost_totals = array();

		$orders = Order::query($query);
		$order_totals = array();
		foreach ($orders as $order) {
			$order_subtotal = 0;
			$lineitems = LineItem::find_all_by(array('order_id' => $order['id']));
			foreach ($lineitems as $lineitem) {
				$order_subtotal += $lineitem->price * $lineitem->quantity;
				$order_subtotal += $lineitem->price * $lineitem->quantity;

				if ($product = Product::find_by(array('sku'=>$lineitem->sku))) {
					$total_costs += $product->cost * $lineitem->quantity;
					$order_cost_total += $product->cost * $lineitem->quantity;
				} else {
					Application::set_flash('Lookup error for product ' . $lineitem->sku . '.');
				}

			}
			$order_subtotals[$order['id']] = $order_subtotal;

			$order_subtotals[$order['id']] = $order_subtotal;
			$order_totals[$order['id']] = $order_total;
			$order_cost_totals[$order['id']] = $order_cost_total;

		}

		/** PHPExcel */
		require_once SITE_ROOT . 'app/library/PHPExcel/PHPExcel.php';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		if (!empty($site_name)) {
			$site_title = $site_name;
		} else {
			$site_title = "All_Sites";
		}

		// Set properties
		$objPHPExcel->getProperties()->setCreator("Scott Ryan")
					     ->setLastModifiedBy("Scott Ryan")
					     ->setTitle("Sales Report By Date: " . $range . '-' . $site_title)
					     ->setSubject("Sales Report By Date: " . $range . '-' . $site_title)
					     ->setDescription("Sales Report By Date: " . $range . '-' . $site_title)
					     ->setKeywords("Sales Report By Date")
					     ->setCategory("Sales Report By Date");

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set headers in first row.
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'Order Number');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Date');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Subtotal');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Shipping');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Il Sales Tax');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Total');
		$objPHPExcel->getActiveSheet()->setCellValue('G1', 'Cost');
		$objPHPExcel->getActiveSheet()->setCellValue('H1', 'Net Profit');
		$objPHPExcel->getActiveSheet()->setCellValue('I1', 'Margin');
		
		// Bold them.
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('G1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('H1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('I1')->getFont()->setBold(true);

		// Set column dimensions.
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setWidth(20);

		$row      = 2;
		$rowstart = $row;

		foreach($orders as $o) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $o['id']);
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, date('F j, Y', strtotime($o['date'])));
			$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $order_subtotals[$o['id']]);
			$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $o['ship_cost']);
			$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, $o['sales_tax']);
			$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, '=SUM(C'.$row.':E'.$row.')');

			if (abs($order_cost_totals[$o['id']]) * abs($order_subtotals[$o['id']]) > 0) {

				$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, $order_cost_totals[$o['id']]);
				$objPHPExcel->getActiveSheet()->setCellValue('H' . $row, $order_subtotals[$o['id']] - $order_cost_totals[$o['id']]);
				$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, round(($order_subtotals[$o['id']] - $order_cost_totals[$o['id']])/$order_subtotals[$o['id']], 2));

				$objPHPExcel->getActiveSheet()->getStyle('G' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
				$objPHPExcel->getActiveSheet()->getStyle('H' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
				$objPHPExcel->getActiveSheet()->getStyle('I' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

			} else {
				$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, '(N/A)');
				$objPHPExcel->getActiveSheet()->setCellValue('H' . $row, '(N/A)');
				$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, '(N/A)');

			}

			$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
			$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
			$objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

			$row++;
		}

		$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'TOTALS');
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '=SUM(C'.$rowstart.':C'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '=SUM(D'.$rowstart.':D'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=SUM(E'.$rowstart.':E'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, '=SUM(F'.$rowstart.':F'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('G' . $row, '=SUM(G'.$rowstart.':G'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('H' . $row, '=(C'.$row.'-G'.$row.')');

		if (abs($total_subtotals) * abs ($total_costs) > 0) {

			$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, '=((C'.$row.'-G'.$row.')/(C'.$row.'))');
	
			$objPHPExcel->getActiveSheet()->getStyle('I' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

		} else {

			$objPHPExcel->getActiveSheet()->setCellValue('I' . $row, '(N/A)');

		}

		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('G' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('H' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('G' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('H' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('I' . $row)->getFont()->setBold(true);

		$style['style'] = PHPExcel_Style_Border::BORDER_THIN;
		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('G' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('H' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('I' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$objPHPExcel->getActiveSheet()->setTitle('Sales By Date');

		// Redirect output to client's web browser
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="sales_report_'.$range_for_filename.'_'.$site_title.'.xls"');
		header('Cache-Control: max-age=0');

		// Save Excel file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		die();
	}

	function sales_by_state () {

		$query = "SELECT text_value FROM controls WHERE name = 'site_name' ORDER BY text_value ASC";
		$site_names = Control::fetch_as_object($query);
		$site_select_menu = '<select name="site_name" id="site_name">
		<option value="">All sites</option>';
		foreach ($site_names as $site_name) {
			$site_select_menu .= "
		<option value=\"{$site_name->text_value}\">{$site_name->text_value}</option>";			
		}
		$site_select_menu.="
	</select>";
		$this->registry['site_select_menu'] = $site_select_menu;

		if(!empty($_POST)) {
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];

			if (($start_date =='') && ($end_date == '')) { // If both dates are blank, redirect.
				Application::set_flash('Please enter at least one date or select a range from the menu.');
				$this->redirect('admin/report/sales_by_state');
			} else {
	
				// If one date is blank, set it equal to the other so that we don't get a date range.
				if ($end_date == '') {
					$end_date = $start_date;
				}
				if ($start_date == '') {
					$start_date = $end_date;
				}
	
				// Make sure the start date is earlier than the end date.

				$start_date = date('Y-m-d', strtotime($start_date));
				$end_date = date('Y-m-d', strtotime($end_date));

				if ($end_date < $start_date) {
					$hold = $start_date;
					$start_date = $end_date;
					$end_date = $hold;
				}
	
				// If the two dates are the same, we'll search by start date only.
				if ($start_date == $end_date) {
					$range = False;
				} else {
					$range = True;
				}
	
				$this->registry['start_date'] = $start_date;

				$site_name = $_POST['site_name'];
				$this->registry['site_name'] = $site_name;

				$query = "SELECT lineitems.*, orders.ship_to_state "
					. "FROM lineitems "
					. "JOIN orders ON orders.id = lineitems.order_id "
					. "WHERE ";
				if (!empty($site_name)) {
					$query .= " orders.site_name = '{$site_name}' AND ";
				}
				
				if ($range) {
					$this->registry['range'] = date('F j, Y', strtotime($start_date)) . ', to ' . date('F j, Y', strtotime($end_date));
					$this->registry['end_date'] = $end_date;
					$query .= "orders.created_at >= '{$start_date}' AND orders.created_at <= '{$end_date}'";
				} else {
					$this->registry['range'] = date('F j, Y', strtotime($start_date));
					$this->registry['end_date'] = '';
					$query .= "orders.created_at = '{$start_date}'";
				}

				$query .= " ORDER BY lineitems.sku ASC";

				$lineitems = LineItem::query($query);
				if (count($lineitems) == 0) {
					$this->registry['no_results_message'] = "No orders were found for {$this->registry['range']}.";
				} else {
					$this->registry['no_results_message'] = '';
				}
				$total_sales = 0.00;
				$sku_totals = array();
				$sku_state_totals = array();
				$state_totals = array();

				$total_costs = 0.00;
				$sku_state_cost_totals = array();
				$sku_cost_totals = array();
				$state_cost_totals = array();

				$this_state = '';
				$states = array();
				foreach ($lineitems as $lineitem) {
					$sku_totals[$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
					$total_sales += $lineitem['price'] * $lineitem['quantity'];

					$sku_state_totals[$lineitem['ship_to_state']][$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
					$state_totals[$lineitem['ship_to_state']] += $lineitem['price'] * $lineitem['quantity'];
					if (!in_array($lineitem['ship_to_state'], $states)) {
						$states[] = $lineitem['ship_to_state'];
					}

					if ($product = Product::find_by(array('sku'=>$lineitem['sku']))) {
						$total_costs += $product->cost * $lineitem['quantity'];
						$state_cost_totals[$lineitem['ship_to_state']] += $product->cost * $lineitem['quantity'];
						$sku_cost_totals[$lineitem['sku']] += $product->cost * $lineitem['quantity'];
						$sku_state_cost_totals[$lineitem['ship_to_state']][$lineitem['sku']] += $product->cost * $lineitem['quantity'];
					} else {
						Application::set_flash('Lookup error for product ' . $lineitem['sku'] . '.');
					}

				}
				sort($states);
				$this->registry['states'] = $states;
				$this->registry['state_totals'] = $state_totals;
				$this->registry['total_sales'] = $total_sales;
				$this->registry['sku_totals'] = $sku_totals;
				$this->registry['sku_state_totals'] = $sku_state_totals;
				$this->registry['sku_state_cost_totals'] = $sku_state_cost_totals;
				$this->registry['state_cost_totals'] = $state_cost_totals;
				$this->registry['sku_cost_totals'] = $sku_cost_totals;
				$this->registry['total_costs'] = $total_costs;
			}
		} else {
			$this->registry['range'] = '';
		}
	}

	function excel_sales_by_state() {
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		$site_name = $_POST['site_name'];

		$start_date = date('Y-m-d', strtotime($start_date)); // Dates should already be in this format, but just in case...

		$query = "SELECT lineitems.*, orders.ship_to_state "
			. "FROM lineitems "
			. "JOIN orders ON orders.id = lineitems.order_id "
			. "WHERE ";
		if (!empty($site_name)) {
			$query .= " orders.site_name = '{$site_name}' AND ";
		}
		
		if (!empty($end_date)) {
			$end_date = date('Y-m-d', strtotime($end_date)); // Again, just in case.
			$range = date('F j, Y', strtotime($start_date)) . ', to ' . date('F j, Y', strtotime($end_date));
			$range_for_filename = $start_date . '_' . $end_date;
			$query .= "orders.created_at >= '{$start_date}' AND orders.created_at <= '{$end_date}'";
		} else {
			$range = date('F j, Y', strtotime($start_date));
			$range_for_filename = $start_date;
			$query .= "orders.created_at = '{$start_date}'";
		}

		$query .= " ORDER BY lineitems.sku ASC";

		$lineitems = LineItem::query($query);
		$total_sales = 0.00;
		$sku_totals = array();
		$sku_state_totals = array();
		$state_totals = array();

		$total_costs = 0.00;
		$sku_state_cost_totals = array();
		$sku_cost_totals = array();
		$state_cost_totals = array();

		$this_state = '';
		$states = array();
		foreach ($lineitems as $lineitem) {
			$sku_totals[$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
			$total_sales += $lineitem['price'] * $lineitem['quantity'];

			$sku_state_totals[$lineitem['ship_to_state']][$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
			$state_totals[$lineitem['ship_to_state']] += $lineitem['price'] * $lineitem['quantity'];
			if (!in_array($lineitem['ship_to_state'], $states)) {
				$states[] = $lineitem['ship_to_state'];
			}

			if ($product = Product::find_by(array('sku'=>$lineitem['sku']))) {
				$total_costs += $product->cost * $lineitem['quantity'];
				$state_cost_totals[$lineitem['ship_to_state']] += $product->cost * $lineitem['quantity'];
				$sku_cost_totals[$lineitem['sku']] += $product->cost * $lineitem['quantity'];
				$sku_state_cost_totals[$lineitem['ship_to_state']][$lineitem['sku']] += $product->cost * $lineitem['quantity'];
			} else {
				Application::set_flash('Lookup error for product ' . $lineitem['sku'] . '.');
			}

		}
		sort($states);

		/** PHPExcel */
		require_once SITE_ROOT . 'app/library/PHPExcel/PHPExcel.php';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		if (!empty($site_name)) {
			$site_title = $site_name;
		} else {
			$site_title = "All_Sites";
		}

		// Set properties
		$objPHPExcel->getProperties()->setCreator("Scott Ryan")
					     ->setLastModifiedBy("Scott Ryan")
					     ->setTitle("Sales Report By State: " . $range . '_' . $site_title)
					     ->setSubject("Sales Report By State: " . $range . '_' . $site_title)
					     ->setDescription("Sales Report By State: " . $range . '_' . $site_title)
					     ->setKeywords("Sales Report By State")
					     ->setCategory("Sales Report By State");

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);		
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);	

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$old_state = "";
		$row = 1;
		$state_rowstart = 1;
		$state_total_rows = '';
		$state_cost_total_rows = '';
		$state_net_profit_total_rows = '';
		foreach($states as $s) {
			if ($old_state != $s) {
				if ($old_state != '') {
					$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'TOTAL FOR ' . $old_state);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '=SUM(B'.$state_rowstart.':B'.($row -1).')');
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '=SUM(C'.$state_rowstart.':C'.($row -1).')');
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '=SUM(D'.$state_rowstart.':D'.($row -1).')');

					$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

					$state_total_rows .= 'B' . $row . '+';
					$state_cost_total_rows .= 'C' . $row . '+';
					$state_net_profit_total_rows .= 'D' . $row . '+';
					$row++;
					$row++;
				}

				$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Sales for ' . $s);
				$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
				$row++;

				$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'SKU');
				$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'Sales');
				$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, 'Cost');
				$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, 'Net Profit');
				$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
				$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, 'Margin');
				$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);

				$row++;
				$state_rowstart = $row;
			}

			foreach (array_keys($sku_state_totals[$s]) as $sku) {
				$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $sku);
				$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $sku_state_totals[$s][$sku]);

				if (abs($sku_state_cost_totals[$s][$sku]) * abs($sku_state_totals[$s][$sku]) > 0) {

					$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $sku_state_cost_totals[$s][$sku]);
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '=(B'.$row.'-C'.$row.')');
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=(D'.$row.'/B'.$row.')');

					$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

				} else {

					$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '(N/A)');
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '(N/A)');
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '(N/A)');

				}

				$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

				$row++;
			}

			$old_state = $s;
		}

		$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'TOTAL FOR ' . $old_state);
		$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '=SUM(B'.$state_rowstart.':B'.($row -1).')');

		if (abs($sku_state_cost_totals[$old_state][$sku]) * abs($sku_state_totals[$old_state][$sku]) > 0) {

			$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '=SUM(C'.$state_rowstart.':C'.($row -1).')');
			$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '=SUM(D'.$state_rowstart.':D'.($row -1).')');
			$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=(D'.$row.'/B'.$row.')');

			$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
			$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
			$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

		} else {

			$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '(N/A)');
			$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '(N/A)');
			$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '(N/A)');

		}

		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

		$state_total_rows .= 'B' . $row;
		$state_cost_total_rows .= 'C' . $row;
		$state_net_profit_total_rows .= 'D' . $row;

		$row++;
		$row++;

		$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'TOTAL SALES');
		$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '='.$state_total_rows);
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '='.$state_cost_total_rows);
		$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '='.$state_net_profit_total_rows);
		$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=(D'.$row.'/B'.$row.')');

		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

		$style['style'] = PHPExcel_Style_Border::BORDER_THIN;
		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$row++;
		$row++;

		$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'SKU');
		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'Sales');
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, 'Total Cost');
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, 'Net Profit');
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, 'Margin');
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);

		$row++;

		foreach (array_keys($sku_totals) as $sku) {

			$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $sku);
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, number_format($sku_totals[$sku], 2));

			if ($sku_cost_totals[$sku] > 0) {

				$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, number_format($sku_cost_totals[$sku], 2));
				$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, number_format($sku_totals[$sku] -  $sku_cost_totals[$sku], 2));
				$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, round(($sku_totals[$sku] -  $sku_cost_totals[$sku])/$sku_totals[$sku], 2));

			} else {

				$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '(N/A)');
				$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '(N/A)');
				$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '(N/A)');

			}
		}

		$objPHPExcel->getActiveSheet()->setTitle('Sales By State');

		// Redirect output to client's web browser
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="sales_report_by_state_'.$range_for_filename.'_'.$site_title.'.xls"');
		header('Cache-Control: max-age=0');

		// Save Excel file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		die();
	}

	function products_by_state () {

		$query = "SELECT text_value FROM controls WHERE name = 'site_name' ORDER BY text_value ASC";
		$site_names = Control::fetch_as_object($query);
		$site_select_menu = '<select name="site_name" id="site_name">
		<option value="">All sites</option>';
		foreach ($site_names as $site_name) {
			$site_select_menu .= "
		<option value=\"{$site_name->text_value}\">{$site_name->text_value}</option>";			
		}
		$site_select_menu.="
	</select>";
		$this->registry['site_select_menu'] = $site_select_menu;

		if(!empty($_POST)) {
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];

			$start_date = date('Y-m-d', strtotime($start_date));
			$end_date = date('Y-m-d', strtotime($end_date));

			if (($start_date =='') && ($end_date == '')) { // If both dates are blank, redirect.
				Application::set_flash('Please enter at least one date or select a range from the menu.');
				$this->redirect('admin/report/products_by_state');
			} else {
	
				// If one date is blank, set it equal to the other so that we don't get a date range.
				if ($end_date == '') {
					$end_date = $start_date;
				}
				if ($start_date == '') {
					$start_date = $end_date;
				}
	
				// Make sure the start date is earlier than the end date.
				if ($end_date < $start_date) {
					$hold = $start_date;
					$start_date = $end_date;
					$end_date = $hold;
				}
	
				// If the two dates are the same, we'll search by start date only.
				if ($start_date == $end_date) {
					$range = False;
				} else {
					$range = True;
				}

				$site_name = $_POST['site_name'];
				$this->registry['site_name'] = $site_name;

				$this->registry['start_date'] = $start_date;

				$query = "SELECT lineitems.*, orders.ship_to_state "
					. "FROM lineitems "
					. "JOIN orders ON orders.id = lineitems.order_id "
					. "WHERE ";
				if (!empty($site_name)) {
					$query .= " orders.site_name = '{$site_name}' AND ";
				}
				
				if ($range) {
					$this->registry['range'] = date('F j, Y', strtotime($start_date)) . ', to ' . date('F j, Y', strtotime($end_date));
					$this->registry['end_date'] = $end_date;
					$query .= "orders.created_at >= '{$start_date}' AND orders.created_at <= '{$end_date}'";
				} else {
					$this->registry['range'] = date('F j, Y', strtotime($start_date));
					$this->registry['end_date'] = '';
					$query .= "orders.created_at = '{$start_date}'";
				}

				$query .= " ORDER BY lineitems.sku ASC";

				$lineitems = LineItem::query($query);
				if (count($lineitems) == 0) {
					$this->registry['no_results_message'] = "No orders were found for {$this->registry['range']}.";
				} else {
					$this->registry['no_results_message'] = '';
				}
				$total_sales = 0.00;
				$sku_totals = array();
				$sku_state_totals = array();
				$state_totals = array();

				$total_costs = 0.00;
				$sku_cost_totals = array();
				$sku_state_cost_totals = array();
				$state_cost_totals = array();

				$this_state = '';
				$states = array();
				$skus = array();
				foreach ($lineitems as $lineitem) {
					$sku_totals[$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
					$total_sales += $lineitem['price'] * $lineitem['quantity'];

					if ($product = Product::find_by(array('sku'=>$lineitem['sku']))) {
						$total_costs += $product->cost * $lineitem['quantity'];
						$sku_cost_totals[$lineitem['sku']] += $product->cost * $lineitem['quantity'];
					} else {
						Application::set_flash('Lookup error for product ' . $lineitem['sku'] . '.');
					}

					if (!in_array($lineitem['sku'], $skus)) {
						$skus[] = $lineitem['sku'];
					}

					$sku_state_totals[$lineitem['ship_to_state']][$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
					$state_totals[$lineitem['ship_to_state']] += $lineitem['price'] * $lineitem['quantity'];

					if (!empty($product)) {
						$sku_state_cost_totals[$lineitem['ship_to_state']][$lineitem['sku']] += $product->cost * $lineitem['quantity'];
						$state_cost_totals[$lineitem['ship_to_state']] += $product->cost * $lineitem['quantity'];
					}

					if (!in_array($lineitem['ship_to_state'], $states)) {
						$states[] = $lineitem['ship_to_state'];
					}

				}
				sort($states);
				sort($skus);
				$this->registry['skus'] = $skus;
				$this->registry['states'] = $states;
				$this->registry['state_totals'] = $state_totals;
				$this->registry['total_sales'] = $total_sales;
				$this->registry['sku_totals'] = $sku_totals;
				$this->registry['sku_state_totals'] = $sku_state_totals;

				$this->registry['total_costs'] = $total_costs;
				$this->registry['sku_cost_totals'] = $sku_cost_totals;
				$this->registry['sku_state_cost_totals'] = $sku_state_cost_totals;
				$this->registry['state_cost_totals'] = $state_cost_totals;
			}
		} else {
			$this->registry['range'] = '';
		}
	}

	function excel_products_by_state() {
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		$site_name = $_POST['site_name'];

		$start_date = date('Y-m-d', strtotime($start_date)); // Dates should already be in this format, but just in case...
		$query = "SELECT lineitems.*, orders.ship_to_state "
			. "FROM lineitems "
			. "JOIN orders ON orders.id = lineitems.order_id "
			. "WHERE ";
		if (!empty($site_name)) {
			$query .= " orders.site_name = '{$site_name}' AND ";
		}
		
		if (!empty($end_date)) {
			$end_date = date('Y-m-d', strtotime($end_date)); // Again, just in case.
			$range = date('F j, Y', strtotime($start_date)) . ', to ' . date('F j, Y', strtotime($end_date));
			$range_for_filename = $start_date . '_' . $end_date;
			$query .= "orders.created_at >= '{$start_date}' AND orders.created_at <= '{$end_date}'";
		} else {
			$range = date('F j, Y', strtotime($start_date));
			$range_for_filename = $start_date;
			$query .= "orders.created_at = '{$start_date}'";
		}

		$query .= " ORDER BY lineitems.sku ASC";

		$lineitems = LineItem::query($query);
		$total_sales = 0.00;
		$sku_totals = array();
		$sku_state_totals = array();
		$state_totals = array();

		$total_costs = 0.00;
		$sku_cost_totals = array();
		$sku_state_cost_totals = array();
		$state_cost_totals = array();

		$this_state = '';
		$states = array();
		$skus = array();
		foreach ($lineitems as $lineitem) {
			$sku_totals[$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];

			if ($product = Product::find_by(array('sku'=>$lineitem['sku']))) {
				$total_costs += $product->cost * $lineitem['quantity'];
				$sku_cost_totals[$lineitem['sku']] += $product->cost * $lineitem['quantity'];
			}

			if (!in_array($lineitem['sku'], $skus)) {
				$skus[] = $lineitem['sku'];
			}
			$total_sales += $lineitem['price'] * $lineitem['quantity'];

			$sku_state_totals[$lineitem['ship_to_state']][$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
			$state_totals[$lineitem['ship_to_state']] += $lineitem['price'] * $lineitem['quantity'];

			if (!empty($product)) {
				$sku_state_cost_totals[$lineitem['ship_to_state']][$lineitem['sku']] += $product->cost * $lineitem['quantity'];
				$state_cost_totals[$lineitem['ship_to_state']] += $product->cost * $lineitem['quantity'];
			}

			if (!in_array($lineitem['ship_to_state'], $states)) {
				$states[] = $lineitem['ship_to_state'];
			}

		}
		sort($states);
		sort($skus);

		/** PHPExcel */
		require_once SITE_ROOT . 'app/library/PHPExcel/PHPExcel.php';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		if (!empty($site_name)) {
			$site_title = $site_name;
		} else {
			$site_title = "All_Sites";
		}

		// Set properties
		$objPHPExcel->getProperties()->setCreator("Scott Ryan")
					     ->setLastModifiedBy("Scott Ryan")
					     ->setTitle("Product Sales Report By State: " . $range . '_' . $site_title)
					     ->setSubject("Product Sales Report By State: " . $range . '_' . $site_title)
					     ->setDescription("Product Sales Report By State: " . $range . '_' . $site_title)
					     ->setKeywords("Product Sales Report By State")
					     ->setCategory("Product Sales Report By State");

		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);	
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);


		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		$old_sku = "";
		$row = 1;
		$sku_rowstart = 1;
		$sku_total_rows = '';
		foreach($skus as $sku) {
			if ($old_sku != $sku) {
				if ($old_sku != '') {
					$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'TOTAL FOR ' . $old_sku);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '=SUM(B'.$sku_rowstart.':B'.($row -1).')');
					$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '=SUM(C'.$sku_rowstart.':C'.($row -1).')');
					$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '=SUM(D'.$sku_rowstart.':D'.($row -1).')');
					$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=(SUM(D'.$sku_rowstart.':D'.($row -1).'))/(SUM(B' .$sku_rowstart.':B'.($row-1).'))');

					$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);
					$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

					$sku_total_rows .= 'B' . $row . '+';
					$sku_total_cost_rows .= 'C' . $row . '+';

					$row++;
					$row++;
				}

				$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'Sales for ' . $sku);
				$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
				$row++;

				$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'State');
				$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, 'Sales');
				$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, 'Unit costs');
				$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, 'Net profit');
				$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);

				$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, 'Margin');
				$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);

				$row++;
				$sku_rowstart = $row;
			}

			foreach ($states as $state) {
				if (!empty($sku_state_totals[$state][$sku])) {
					$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $state);
					$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $sku_state_totals[$state][$sku]);
					$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

					if ($sku_state_cost_totals[$state][$sku] > 0) {

						$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $sku_state_cost_totals[$state][$sku]);
						$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $sku_state_totals[$state][$sku] - $sku_state_cost_totals[$state][$sku]);
						$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
						$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, round(($sku_state_totals[$state][$sku] - $sku_state_cost_totals[$state][$sku])/$sku_state_totals[$state][$sku], 2));
						$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

					} else {

						$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '(N/A)');
						$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '(N/A)');
						$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '(N/A)');

					}

					$row++;
				}
			}

			$old_sku = $sku;
		}

		$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'TOTAL FOR ' . $old_sku);
		$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '=SUM(B'.$sku_rowstart.':B'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '=SUM(C'.$sku_rowstart.':C'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '=SUM(D'.$sku_rowstart.':D'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=(SUM(D'.$sku_rowstart.':D'.($row -1).'))/(SUM(B' .$sku_rowstart.':B'.($row-1).'))');


		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

		$sku_total_rows .= 'B' . $row;
		$sku_total_cost_rows .= 'C' . $row;

		$row++;
		$row++;

		$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'TOTAL SALES');
		$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, '='.$sku_total_rows);
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '='.$sku_total_cost_rows);
		$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '=('. $sku_total_rows . ') - (' . $sku_total_cost_rows . ')');
		$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=(('. $sku_total_rows . ') - (' . $sku_total_cost_rows . '))/('.$sku_total_rows.')');

		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);

		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

		$style['style'] = PHPExcel_Style_Border::BORDER_THIN;
		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$objPHPExcel->getActiveSheet()->setTitle('Product Sales By State');

		// Redirect output to client's web browser
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="product_sales_report_by_state_'.$range_for_filename.'_'.$site_title.'.xls"');
		header('Cache-Control: max-age=0');

		// Save Excel file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		die();
	}

	function products_by_unit () {

		$query = "SELECT text_value FROM controls WHERE name = 'site_name' ORDER BY text_value ASC";
		$site_names = Control::fetch_as_object($query);
		$site_select_menu = '<select name="site_name" id="site_name">
		<option value="">All sites</option>';
		foreach ($site_names as $site_name) {
			$site_select_menu .= "
		<option value=\"{$site_name->text_value}\">{$site_name->text_value}</option>";			
		}
		$site_select_menu.="
	</select>";
		$this->registry['site_select_menu'] = $site_select_menu;

		if(!empty($_POST)) {
			$start_date = $_POST['start_date'];
			$end_date = $_POST['end_date'];

			if (($start_date =='') && ($end_date == '')) { // If both dates are blank, redirect.
				Application::set_flash('Please enter at least one date or select a range from the menu.');
				$this->redirect('admin/report/products_by_unit');
			} else {
	
				// If one date is blank, set it equal to the other so that we don't get a date range.
				if ($end_date == '') {
					$end_date = $start_date;
				}
				if ($start_date == '') {
					$start_date = $end_date;
				}
	
				// Make sure the start date is earlier than the end date.
				if ($end_date < $start_date) {
					$hold = $start_date;
					$start_date = $end_date;
					$end_date = $hold;
				}
	
				// If the two dates are the same, we'll search by start date only.
				if ($start_date == $end_date) {
					$range = False;
				} else {
					$range = True;
				}

				$start_date = date('Y-m-d', strtotime($start_date));
				$this->registry['start_date'] = $start_date;

				$site_name = $_POST['site_name'];
				$this->registry['site_name'] = $site_name;

				if ($range) {
					$this->registry['range'] = date('F j, Y', strtotime($start_date)) . ', to ' . date('F j, Y', strtotime($end_date));
					$end_date = date('Y-m-d', strtotime($end_date));
					$this->registry['end_date'] = $end_date;
					$query = "SELECT * FROM lineitems WHERE created_at >= '{$start_date}' AND created_at <= '{$end_date}'";
					if (!empty($site_name)) {
						$query .= " AND site_name = '{$site_name}'";
					}
					$query .= " ORDER BY sku ASC";
				} else {
					$this->registry['range'] = date('F j, Y', strtotime($start_date));
					$this->registry['end_date'] = '';
					$query = "SELECT * FROM lineitems WHERE created_at = '{$start_date}'";
					if (!empty($site_name)) {
						$query .= " AND site_name = '{$site_name}'";
					}
					$query .= " ORDER BY sku ASC";
				}

				$lineitems = LineItem::query($query);
				if (count($lineitems) == 0) {
					$this->registry['no_results_message'] = "No orders were found for {$this->registry['range']}.";
				} else {
					$this->registry['no_results_message'] = '';
				}
				$total_sales = 0.00;
				$sku_sales_totals = array();
				$sku_unit_totals = array();

				$total_costs = 0.00;
				$sku_cost_totals = array();

				$skus = array();

				$site_name = $_POST['site_name'];

				foreach ($lineitems as $lineitem) {
					$total_sales += $lineitem['price'] * $lineitem['quantity'];
					$sku_sales_totals[$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
					$sku_unit_totals[$lineitem['sku']] += $lineitem['quantity'];

					if ($product = Product::find_by(array('sku'=>$lineitem['sku']))) {
						$total_costs += $product->cost * $lineitem['quantity'];
						$sku_cost_totals[$lineitem['sku']] += $product->cost * $lineitem['quantity'];
					} else {
						Application::set_flash('Lookup error for product ' . $lineitem['sku'] . '.');
					}

					if (!in_array($lineitem['sku'], $skus)) {
						$skus[] = $lineitem['sku'];
					}

				}
				sort($skus);
				$this->registry['skus'] = $skus;
				$this->registry['total_sales'] = $total_sales;
				$this->registry['sku_sales_totals'] = $sku_sales_totals;
				$this->registry['sku_unit_totals'] = $sku_unit_totals;

				$this->registry['total_costs'] = $total_costs;
				$this->registry['sku_cost_totals'] = $sku_cost_totals;
			}
		} else {
			$this->registry['range'] = '';
		}
	}

	function excel_products_by_unit() {
		$start_date = $_POST['start_date'];
		$end_date = $_POST['end_date'];
		$site_name = $_POST['site_name'];

		$start_date = date('Y-m-d', strtotime($start_date)); // Dates should already be in this format, but just in case...
		if (!empty($end_date)) {
			$end_date = date('Y-m-d', strtotime($end_date)); // Again, just in case.
			$range = date('F j, Y', strtotime($start_date)) . ', to ' . date('F j, Y', strtotime($end_date));
			$range_for_filename = $start_date . '_' . $end_date;
			$query = "SELECT * FROM lineitems WHERE created_at >= '{$start_date}' AND created_at <= '{$end_date}'";
			if (!empty($site_name)) {
				$query .= " AND site_name = '{$site_name}'";
			}
			$query .=" ORDER BY sku ASC";
		} else {
			$range = date('F j, Y', strtotime($start_date));
			$range_for_filename = $start_date;
			$query = "SELECT * FROM lineitems WHERE created_at = '{$start_date}'";
			if (!empty($site_name)) {
				$query .= " AND site_name = '{$site_name}'";
			}
			$query .=" ORDER BY sku ASC";
		}

		$total_subtotals = 0.00;
		$total_shipping = 0.00;
		$total_tax = 0.00;
		$sales = 0.00;
		$lineitems = LineItem::query($query);
		$sku_sales_totals = array();
		$sku_unit_totals = array();

		$total_costs = 0.00;
		$sku_cost_totals = array();

		$skus = array();
		foreach ($lineitems as $lineitem) {
			$sku_sales_totals[$lineitem['sku']] += $lineitem['price'] * $lineitem['quantity'];
			$sku_unit_totals[$lineitem['sku']] += $lineitem['quantity'];

			if ($product = Product::find_by(array('sku'=>$lineitem['sku']))) {
				$total_costs += $product->cost * $lineitem['quantity'];
				$sku_cost_totals[$lineitem['sku']] += $product->cost * $lineitem['quantity'];
			} else {
				Application::set_flash('Lookup error for product ' . $lineitem['sku'] . '.');
			}

			if (!in_array($lineitem['sku'], $skus)) {
				$skus[] = $lineitem['sku'];
			}

		}
		sort($skus);

		if (!empty($site_name)) {
			$site_title = $site_name;
		} else {
			$site_title = "All_Sites";
		}

		/** PHPExcel */
		require_once SITE_ROOT . 'app/library/PHPExcel/PHPExcel.php';

		// Create new PHPExcel object
		$objPHPExcel = new PHPExcel();

		// Set properties
		$objPHPExcel->getProperties()->setCreator("Scott Ryan")
					     ->setLastModifiedBy("Scott Ryan")
					     ->setTitle("Product Sales Report By Unit: " . $range . '_' . $site_title)
					     ->setSubject("Product Sales Report By Date: " . $range . '_' . $site_title)
					     ->setDescription("Product Sales Report By Unit: " . $range . '_' . $site_title)
					     ->setKeywords("Product Sales Report By Unit")
					     ->setCategory("Product Sales Report By Unit");

		// Set active sheet index to the first sheet, so Excel opens this as the first sheet
		$objPHPExcel->setActiveSheetIndex(0);

		// Set headers in first row.
		$objPHPExcel->getActiveSheet()->setCellValue('A1', 'SKU');
		$objPHPExcel->getActiveSheet()->setCellValue('B1', 'Units');
		$objPHPExcel->getActiveSheet()->setCellValue('C1', 'Sales');
		$objPHPExcel->getActiveSheet()->setCellValue('D1', 'Unit costs');
		$objPHPExcel->getActiveSheet()->setCellValue('E1', 'Net profit');
		$objPHPExcel->getActiveSheet()->setCellValue('F1', 'Margin');
		
		// Bold them.
		$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E1')->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('F1')->getFont()->setBold(true);

		// Set column dimensions.
		$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setWidth(20);
		$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setWidth(20);

		$row      = 2;
		$rowstart = $row;

		foreach($skus as $sku) {
			$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, $sku);
			$objPHPExcel->getActiveSheet()->setCellValue('B' . $row, $sku_unit_totals[$sku]);
			$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER);

			$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, $sku_sales_totals[$sku]);
			$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);

			if ($sku_cost_totals[$sku] > 0) {

				$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, $sku_cost_totals[$sku]);
				$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=(C' . $row . ' - D' . $row . ')');
				$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, '=(E' . $row . '/C' . $row . ')');

				$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
				$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
				$objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);

			} else {

				$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '(N/A)');
				$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '(N/A)');
				$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, '(N/A)');

			}

			$row++;
		}

		$objPHPExcel->getActiveSheet()->setCellValue('A' . $row, 'TOTALS');
		$objPHPExcel->getActiveSheet()->setCellValue('C' . $row, '=SUM(C'.$rowstart.':C'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('D' . $row, '=SUM(D'.$rowstart.':D'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('E' . $row, '=SUM(E'.$rowstart.':E'.($row -1).')');
		$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, '=(C'.$row.'-D'.$row.')/C'.$row);
//		$objPHPExcel->getActiveSheet()->setCellValue('F' . $row, '=(SUM(E'.$rowstart.':E'.($row -1).')/SUM(C'.$rowstart.':C'.($row-1).'))');

		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE);
		$objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_00);


		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getFont()->setBold(true);
		$objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getFont()->setBold(true);

		$style['style'] = PHPExcel_Style_Border::BORDER_THIN;
		$objPHPExcel->getActiveSheet()->getStyle('A' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );
		$objPHPExcel->getActiveSheet()->getStyle('B' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$objPHPExcel->getActiveSheet()->getStyle('C' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$objPHPExcel->getActiveSheet()->getStyle('D' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$objPHPExcel->getActiveSheet()->getStyle('E' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$objPHPExcel->getActiveSheet()->getStyle('F' . $row)->getBorders()->applyFromArray(
				 array(
					 'top'      => $style,
				 )
		 );

		$objPHPExcel->getActiveSheet()->setTitle('Product Sales By Unit');

		// Redirect output to client's web browser
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="product_sales_report_by_unit_'.$range_for_filename.'_'.$site_title.'.xls"');
		header('Cache-Control: max-age=0');

		// Save Excel file
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
		$objWriter->save('php://output');
		die();
	}

	function view_order($id) {
		$order = Order::find($id);

		$lineitems = ActiveRecordBase::query("SELECT lineitems.*, products.product_name, products.sku, products.primaries "
		                                     . "FROM lineitems "
		                                     . "JOIN products ON products.sku = lineitems.sku "
		                                     . "WHERE lineitems.order_id = {$id}");
		$subtotal = 0.00;
		foreach ($lineitems as $item) {
			$subtotal += floatval($item['price']) * $item['quantity'];
		}
		$order->subtotal = $subtotal;
		$order->order_total = '$' . number_format($order->subtotal + $order->sales_tax + $order->ship_cost - $order->order_discount, 2);
		$this->registry['order']       = $order;
		$this->registry['lineitems']   = $lineitems;
	}

	function search () {
		if(!empty($_POST)) {
			$searchterm = ($_POST['compare'] == 'LIKE') ? "%" . $_POST['searchterm'] . "%" : $_POST['searchterm'];
			$categories = Category::fetch_as_object("SELECT * FROM categories WHERE {$_POST['field']} {$_POST['compare']} '{$searchterm}' ORDER BY created_at DESC", 'Category');
			$this->registry['categories'] = $categories;
		}
		$this->view = 'index';
	}
}