<?php
/**
 * PACKAGE: BrightGuy
 * 
 * This controller contains our admin functionality
 *
 * @copyright 2009 r5k
 * @author Nick Morlan<nick@hddg.net>
 */
class MemberController Extends ControllerBase {

	function __construct() {
		$this->no_cache();
		$this->layout = 'admin' . $_SESSION['level'];
		$this->methods['index'] = array('3', '4');
		$this->methods['edit'] = array('3', '4');
		$this->methods['destroy'] = array('3', '4');
	}

	/**
	 * Replaces the parent method to grab the correct view..
	 * added for this controller for special handling
     */  	 
	function render ($class, $method) {
		$view = (empty($this->view)) ? $method : $this->view;
		$this->controller_method = $method;
		$view_folder = "admin/" . strtolower(Inflector::singularize(str_replace('Controller', '', $class))) . '/';
		if($class !== false) {
			$view_file = SITE_ROOT . SITE_VIEWS . $view_folder . $view  . '.tpl';
			if(file_exists($view_file)) {
				require(SITE_ROOT . SITE_VIEWS . $view_folder . $view  . '.tpl');
			} else {
				require(SITE_ROOT . SITE_VIEWS . 'error/404.tpl');
			}
		}
	}

	/**
	 * Replaces the parent method to grab the correct partials..
	 * added for this controller for special handling
	 */
	function render_partial($controller, $partial) {
		include(SITE_ROOT . SITE_VIEWS . 'admin/' . $controller . '/_' . $partial . '.tpl');
	}

	function index ($params) {
		$start = 1;
		$limit = 100;
		//$order_by = 'bill_to_last_name, bill_to_first_name, bill_to_mi, customer_id';
		$order_by = 'bill_to_last_name';
		$asdc = 'ASC';
		// break out our parameters and check for what we need
		if(!is_array($params)) {
			$param[] = $params;
		} else {
			$param = $params;
		}
		foreach($param as $p) {
			if(strpos($p, ':') !== false) {
				$a = explode(':', $p);
				switch($a[0]) {
					case 'start':
						$start = $a[1];
						break;
					case 'limit':
						$limit = $a[1];
						break;
					case 'order_by':
						$order_by = $a[1];
						break;
					case 'asdc':
						$asdc = $a[1];
						break;
				}
			}
		}
		$members = Member::find_all_with_pagination($start, $limit, $order_by, $asdc);
		$this->registry['paginator'] = array_pop($members);
		$this->registry['members'] = $members;
	}
	
	function destroy($id) {
		if(Member::destroy($id)) {
			Application::set_flash ("The member has been deleted.", 'error');
		} else {
			Application::set_flash ("An error has occured.", 'error');
		}
		$this->redirect ('admin/member');
	}

	function edit($id) {
		if($member = Member::find($id)) {
			if(!empty($_POST)) {
				$member->edit($_POST);
				if($member->save()) {
					Application::set_flash("The member has been saved.");
					$this->redirect ('admin/member');
				}
			} 
			$this->registry['member'] = $member;
		} else {
			$this->redirect ('admin/member');
		}
	}

	function search () {
		if(!empty($_POST)) {
			$searchterm = ($_POST['compare'] == 'LIKE') ? "%" . $_POST['searchterm'] . "%" : $_POST['searchterm'];
			$members = Member::fetch_as_object("SELECT * FROM members WHERE {$_POST['field']} {$_POST['compare']} '{$searchterm}' ORDER BY {$_POST['field']} LIMIT 100", 'member');
			$this->registry['members'] = $members;
		}
		$this->view = 'index';
	}

}