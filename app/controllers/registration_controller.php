<?php
/**
 * PACKAGE: BrightGuy
 * 
 * This controller contains our admin functionality
 *
 * @copyright 2009 r5k
 * @author Nick Morlan<nick@hddg.net>
 */
class RegistrationController Extends ControllerBase {

	function __construct() {
		$this->no_cache();
	}

	/* so no templates are returned with _ajax methods */
	function do_page() {
		if(substr($this->controller_method, 0, 6) != '_ajax_') {
			require(SITE_ROOT . SITE_VIEWS . 'application/' . $this->layout . '.tpl');
		}
	}

	function index ($params) {

	}



	function create($param) {
		if(!$activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $param))) {
			$this->redirect('member');
			exit;
		}
		
		if(!empty($_POST)) {
			$registration = Registration::create($_POST);
			$registration->activity_id = $activity->id;
			$registration->member_id = $this->registry['member']->id;
			if($registration->save()) {
				Application::set_flash("The Registration has been created.");
				$this->redirect ('admin/registration');
			} 
		} else {
			$registration = new Registration();
		}
		$this->registry['registration'] = $registration;
		$this->registry['activity']     = $activity;
	}

	function edit($params) {
		$id = $params[0];
		$activity_id = $params[1];
		$activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $activity_id));
		if($registration = Registration::find_by(array('id' => $id, 'member_id' => $this->registry['member']->id, 'activity_id' => $activity->id))) {
			$this->registry['registration'] = $registration;
			$this->registry['activity']     = $activity;
		} else {
			$this->redirect ('member');
		}
		
		$this->registry['activity_id'] = $activity_id;
	}


	
	function import_registrations() {
		$file = SITE_ROOT . 'regs.txt';
		$fh   = fopen($file, 'r');
		//stream_filter_append($fh, 'convert.iconv.UTF-8/ASCII//TRANSLIT');
		$edit_count   = 0;
		$create_count = 0;
		
		while (($data = fgetcsv($fh, 0, chr(9))) !== false) {
			//echo '<pre>';print_r($data);echo '</pre>';
			// set up associative array for edit/create
			$params = array('member_id'   => '1',
			            'country'         => 'US',
						'division'        => $data[0],
						'first_name'      => $data[1],
						'last_name'       => $data[2],
						'grade_level'     => $data[3],
						'school'          => $data[4],
						'birth_date'      => $data[5],
						'address_1'       => $data[6],
						'zip'             => $data[7],
						'home_phone'      => $data[8],
						'parent1_email'   => $data[9],
						'parent2_email'   => $data[10],
						'parent1_name'    => $data[11],
						'parent1_phone'   => $data[12],
						'parent2_name'    => $data[13],
						'parent2_phone'   => $data[14],
						'price'           => $data[15],
						'date_paid'       => $data[16],
						'transaction_ref' => $data[17],
						'amount_paid'     => $data[18],
						//'status'          => $data[19],
						//'primaries'       => $data[20],
						//'keyword'         => $data[21],
						//'charge_time'     => $data[22],
						//'bg_rate'         => $data[23],
						//'safety_rating'   => $data[24],
						//'safety_warning'  => $data[25],
						'allergies'       => $data[28],
						'comments'        => $data[29]);
			

			//if(empty($record)) {
				$record = Registration::create($params);
				$record->created_at = date('Y-m-d');
				$create_count ++;
			//} else {
			//	$record->edit($params);
			//	$edit_count ++;
			//}
			if(!$record->save()) {echo '<pre>';print_r($record);echo '</pre>'; }
		}
	}

	/**  
	 * update the record based on passed values...
	 * post values are 'value' => submitted input value and 'id' => {field_name}_{id}
	 * can grab the notice by the id number at the end of $_POST['id'] and the corrosponding
	 * field by the first portion of $_POST['id'] and set it to $_POST['value']
	 */
	function _ajax_update_registration($activity_id) {
		if(!empty($_POST)) {
			$id = substr(strrchr($_POST['id'], '_'), 1);
			$field = substr($_POST['id'], 0, -(strlen($id) + 1));
			if($registration = Registration::find_by(array('id' => $id, 'member_id' => $this->registry['member']->id, 'activity_id' => $activity_id))) {
				$original_value = $registration->$field;
				// sql format for a date field
				if(strrpos($field, '_date') !== false) $_POST['value'] = date('Y-m-d', strtotime($_POST['value']));
				$registration->$field = trim($_POST['value']);
				if($registration->save()) {
					// in case it is a date field format the display
					if(strrpos($field, '_date') !== false) $registration->$field = date('M j, Y', strtotime($registration->$field));
					echo nl2br($registration->$field);	
				} else {print_r($registration->errors);
					echo $original_value;
				}
			}
		}
	}

}