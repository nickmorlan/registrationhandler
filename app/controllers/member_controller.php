<?php
/**
 * PACKAGE: cpap wholesale
 * 
 * This uses $this->registry['member'] which is set when the application
 * loads.
 */
class MemberController Extends ControllerBase {


    function __construct ($params = array()) {
        $this->layout = 'application_boot';
        $this->controller = 'member';
        $this->no_cache();
        $this->authenticate = array('index', 'destroy', 'edit', 'new_password', 'edit_activity', 'edit_activity_form', 
                                    'destroy_activity', 'payments', 'new_password', 'activity', 'create_activity', 'registrations',
                                    'bailed_registrations', 'destroy_registrations', 'search_registrations', 'create_registration',
                                    'edit_registration', 'stripe_connect', 'verify_bank_account', '_ajax_update_registration', '_ajax_registration_datatable');
    }

    function index ($params) {
        if(!empty($params)) {
            header("HTTP/1.1 404 Not Found");
            exit;
        }
        $query = "SELECT activities.*, "
               . "(SELECT count(registrations.id) FROM registrations WHERE registrations.activity_id = activities.id AND (amount_paid > 0 OR transaction_ref != '' OR member_created = 'y') ) as registered "
               . "FROM activities "
               . "WHERE activities.member_id = " . $this->registry['member']->id . " "
               . "ORDER BY activities.registration_start_date DESC, activities.id ASC";
        $this->registry['activities'] = Activity::fetch_as_object($query);
        
        $this->registry['logs'] = LogForDashboard::get_latest_for_member($this->registry['member']->id);
        $this->registry['payment_info'] = MemberPaymentSetting::find_by(array('member_id' => $this->registry['member']->id));
        //if(empty($this->registry['member']->name)) $this->registry['member']->name = $this->registry['member']->contact_name;
        $this->setup_dashboard_report();
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.js";
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.time.js";
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.tooltip.min.js";

    }


    function edit () {

        if(!empty($_POST)) {
            //if($_POST['country'] != 'US') $_POST['state'] = $_POST['foreign_state'];
            $this->registry['member']->edit($_POST);
            if($this->registry['member']->save()) {
                $file_size = filesize($_FILES['image']['tmp_name']);
                if($file_size > 1) {
                    if ($file_size <= 1048576) {
                        
                        $file_name = basename($_FILES['image']['name']);
                        $file_name = str_replace(' ', '_', $file_name);
                
                        $allowed   = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif');
                        $size = getimagesize($_FILES['image']['tmp_name']);
                        $file_size = filesize($_FILES['image']['tmp_name']);
                        
                        if (in_array($size['mime'], $allowed)) {
            
                            $destination = SITE_ROOT . 'images/members/' . $this->registry['member']->id . '.png';
                            //$thumbs_destination = SITE_ROOT . 'images/activities/thumbs/' . $id . '/';
                    
                
                            if (move_uploaded_file($_FILES['image']['tmp_name'], $destination)) {
                                AdminHelper::resize_image_by_width($destination, $destination, 940);
                                AdminHelper::resize_image_by_width($destination, SITE_ROOT . 'images/members/' . $this->registry['member']->id . '-220.png', 220);
                            } else {
                                Application::set_flash("There was an error uploading the file " . basename($_FILES['image']['name']) . ".", 'error');
                            }
                
                        } else {
                            Application::set_flash('Please choose an image file.', 'error');
                        }
            
                    } else {
                        Application::set_flash('Please choose a file 1MB or smaller in size.', 'error');
                    }
                } // end image processing
                Application::set_flash("Your member information has been saved.");
                $this->redirect ('member');
            }
        }

        $payment = MemberPaymentSetting::find_by(array('member_id' => $this->registry['member']->id));
        if(!empty($payment->tax_id)) $payment->tax_id = 'xxxx' . substr($payment->simple_decrypt($this->registry['member']->salt, $payment->tax_id), -4);
        if(!empty($payment->account_number)) $payment->account_number = 'xxxx' . substr($payment->simple_decrypt($this->registry['member']->salt, $payment->account_number), -4);
        if(!empty($payment->routing_number)) $payment->routing_number = 'xxxx' . substr($payment->simple_decrypt($this->registry['member']->salt, $payment->routing_number), -4);
        $this->registry['payment'] = $payment;

        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
        $this->registry['js_files'][] = "/js/plugins/fileupload/bootstrap-fileupload.js";
        $this->registry['js_files'][] = "https://js.stripe.com/v2/";
        $this->registry['js_data'] = json_encode(array('member_id' => $this->registry['member']->id,
                                                       'payment' => $this->registry['payment'],
                                                       'stripe_publishable_key' => STRIPE_PUBLISHABLE_KEY));


    }

    function controls() {
        $this->registry['controls'] = MemberControl::find_by(array('member_id' => $this->registry['member']->id));
    }

    function payments() {
        if(!empty($_POST)) {
            if($payment = MemberPaymentSetting::find_by(array('member_id' => $this->registry['member']->id))) {
                $payment->edit($_POST);
                $payment->country = 'US';
                if(!empty($_POST['tax_id'])) $payment->tax_id = $payment->simple_encrypt($this->registry['member']->salt, $_POST['tax_id']);
                if($payment->pay_via == 'check') {
                    $payment->cc_fee = 50;
                    $payment->account_number = '';
                    $payment->routing_number = '';
                }elseif($pament->pay_via == 'stripe') {
                    $payment->cc_fee = 29;
                    $payment->account_number = '';
                    $payment->routing_number = '';
                } elseif($pament->pay_via == 'direct_deposit') $payment->cc_fee = 35;
                $payment->save();
            } else {
                $payment = MemberPaymentSetting::create($_POST);            
            }
            $payment->member_id = $this->registry['member']->id;
            $payment->country = 'US';
            if($payment->save()) {
                if($n = MemberNotification::find_by(array('member_id' => $this->registry['member']->id,
                                                       'title' => 'Confirm Payment Info',
                                                       'type' => 'alert')) ) {
                    ///print_r($n);exit;
                    $n->marked_as_read = 'y';
                    $n->save();
                }
                Application::set_flash("Your payment information has been saved.");
            }
        } 
        //print_r($payment);exit;
        $this->redirect ('member/edit');            

        /*  -- reporting
        $cc = $payment->cc_fee / 1000;
        $res = ActiveRecordBase::query("SELECT SUM(amount_paid) as total_collected, SUM(ROUND((amount_paid * {$cc}) + .3, 2)) as fees, ( SUM(amount_paid) - SUM(ROUND((amount_paid * {$cc}) + .3, 2)) ) as balance FROM registrations WHERE member_id = {$this->registry['member']->id} AND r5k_pay_reference = '' AND member_created = 'n' AND amount_paid > 0");
        $regs = ActiveRecordBase::query("SELECT amount_paid, ROUND(((amount_paid * {$cc}) + .3), 2) as fees, ROUND((amount_paid - ((amount_paid * {$cc}) + .3)), 2) as balance, created_at FROM registrations WHERE member_id = {$this->registry['member']->id} AND r5k_pay_reference = '' AND member_created = 'n' AND amount_paid > 0");
        $next_payment_date   = (date('j') > 15) ? date('Y-m-01', strtotime('next month')) : date('Y-m-15');
        $payment_cutoff_date = (date('j') > 15) ? date('Y-m-15') : date('Y-m-01');
        foreach($regs as $reg) {
            if($reg['created_at'] >= $payment_cutoff_date) 
                $processing += $reg['balance'];
            else 
                $next_payment += $reg['balance'];
        }
        $this->registry['processing']          = number_format($processing, 2);
        $this->registry['next_payment']        = number_format($next_payment, 2);
        $this->registry['next_payment_date']   = date('M j Y', strtotime($next_payment_date));
        $this->registry['payment_cutoff_date'] = $payment_cutoff_date;
        $this->registry['regs'] = $regs;
        $this->registry['balance'] = number_format($res[0]['balance'], 2);
        */

    }

    function register ($params) {
        $this->layout = 'application_signin';
        $this->registry['sub_title'] = 'Sign Up';
        $member = new Member();
        if(!empty($_POST['go'])) {
            $lead = EmailCapture::create($_POST);
            $lead->save();
            $member->email = $_POST['email'];
            $member->contact_name = $_POST['name'];
            $member->name = $_POST['organization'];
            // copy over invalid email error 
            if(!empty($lead->errors)) $member->errors = $lead->errors;
        }
        if(!empty($_POST) && empty($_POST['go'])){
            if(!empty($this->registry['member'])) Member::logout(); // just in case
            
            //if($_POST['country'] != 'US') $_POST['state'] = $_POST['foreign_state'];
            $member = Member::create($_POST);
            $member->country = 'US';
            $member->browser_info = $_SERVER['HTTP_USER_AGENT'];
            $member->ip_address = Application::get_real_ip();
            $member->active = 'y';
            if($member->save()) {
                // log in the member to set the cookie
                $member = Member::login(array('email' => $member->email, 'password' => $_POST['password']));
    
                // log the new member
                LogAction::log('New Account Signup.' . print_r($member, true), 'rh', $member->id, true);

                // Send a confirmation email.
                SiteEmail::send_new_member_email($member);
                SiteEmail::send_admin_new_member_notification($member);
                // Add default information to the payment settings
                $name = (!empty($member->name)) ? $member->name : $member->contact_name;
                $setting = MemberPaymentSetting::create(array('member_id' => $member->id,
                                                              'name'      => $name,
                                                              'cc_fee'    => 50,
                                                              'pay_via'   => 'check',
                                                              'address_1' => $member->address_1,
                                                              'address_2' => $member->address_2,
                                                              'city'      => $member->city,
                                                              'state'     => $member->state,
                                                              'zip'       => $member->zip,
                                                              'country'   => $member->country));
                $setting->save();

                $member->notify_alert('Confirm Payment Info', 'You need to save the payment information in your account settings to verify your payment method.', '/member/edit');
                LogForDashboard::log($member->id, 0, 'home', 'Welcome!', 'You set up your Registration Handler account.');
                Application::set_flash("Thank you for registering, {$member->contact_name}.");
                $this->redirect ('member');
            }
        }
        $this->registry['member'] = $member;

        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
    }


    function test_mail(){
            $member = $this->registry['member'];
                $to  = 'nick@r5kmedia.com';//$member->email;
                $subject = 'Your Registration Handler Account';
                $headers = 'From: no-reply@registrationhandler.com <RegistrationHandler>' . "\r\n" .
                       'Reply-To: no-reply@registrationhandler.com' . "\r\n" .
                       'X-Mailer: PHP/' . phpversion() . "\r\n";
                $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                ob_start();
                    include(SITE_ROOT . SITE_VIEWS . 'email_templates/new_member_registration.tpl');
                $message = ob_get_contents();
                ob_end_clean();
                mail($to, $subject, $message, $headers); 
    }
    function login ($params) {
        $this->layout = 'application_signin';
        $this->registry['sub_title'] = 'Sign In';
        if(!empty($_POST)){
            $member = new Member();
            if($member = Member::login($_POST)) {
                Application::set_flash("Welcome, {$member->contact_name}.", 'success');
                $this->redirect ('member');
            } else {
                Application::set_flash("Your login information was invalid.", 'error');
            }
        }
        $this->registry['member'] = $member;
    }



    function logout () {
        Member::logout();
        $this->registry['member'] = -1;
        Application::set_flash('You have been logged out.');
        $this->redirect ('');
    }

    function forgot_password () {
        $this->layout = 'application_signin';
        if(!empty($_POST)) {
            $member = Member::find_by(array('email' => $_POST['email']));
            if(!empty($member)) {
                $member->secure_key = md5(uniqid(mt_rand(), true));
                $member->save();

                $res = Control::find_by(array('name' => 'password retrieval email'));
                $link = SITE_URL . "/member/reset_password/{$member->secure_key}/$member->id";
                if (count($res) > 0) {
                    $email_text = $link . "\r\n\r\n" . $res->text_value;
                } else {
                    $email_text = $link;
                }
                SiteEmail::send_email(array('from'    => 'RegistrationHandler <noreply@registrationhandler.com>', 
                                        'to'      => $member->email, 
                                        'subject' => 'Reset Password Link', 
                                        'text'    => $email_text,
                                        'html'    => $email_text));
                Application::set_flash('Please check your email, ' . ucwords($member->contact_name) . '.');
            } else {
                Application::set_flash("Sorry, but there is no record of {$_POST['email']}.", 'error');
            }
        }
    }
    
    function reset_password ($params) {
        $this->layout = 'application_signin';
        $member = Member::find_by(array('secure_key' => $params[0], 'id' => $params[1]));
        $member->password = '';
        $this->registry['key']    = $params[0];
        $this->registry['id']     = $params[1];
        if(!empty($_POST)) {
            $member->password = $_POST['password'];
            $member->validate_password_confirmation('password', 'confirm_password');
            $member->requires_length_of(5, array('password'));
            if(empty($member->errors)) {
                $member->salt = md5(uniqid(mt_rand(), true));
                $member->password = $member->encrypt_password($member->password, $member->salt);
                $member->save();
                Member::query("UPDATE members SET secure_key = '" . session_id() . "', ip_address = '" . Helpers::get_real_ip() . "' WHERE id = {$member->id}");
                setcookie('key', session_id(), time() + (2592000), '/', COOKIE_ROOT); // 30 day cookie

                Application::set_flash("Your password has been updated.");
                $this->redirect ('member');
            } else {
                Application::set_flash(implode('<br />', $member->errors), 'error');
                $this->redirect ('member/reset_password/' . $params[0] . '/' . $params[1]);
               
            }
        }
        $this->registry['member'] = $member;
        $this->registry['breadcrumb'] = '<a href="/member">' . $this->registry['member']->bill_to_first_name . ' ' . $this->registry['member']->bill_to_last_name . '</a> > Reset password';
    }

    function new_password ($params) {
        if(!empty($_POST)) {
            if($this->registry['member']->encrypt_password($_POST['current_passord'], $this->registry['member']->salt) != $this->registry['member']->password) {
                Application::set_flash("The current password you entered was incorrect.", 'error');
                $this->redirect ('member/edit');
                exit;
            }
            $this->registry['member']->password = $_POST['password'];
            $this->registry['member']->validate_password_confirmation('password', 'confirm_password');
            $this->registry['member']->requires_length_of(5, array('password'));
            if(empty($this->registry['member']->errors)) {
                $this->registry['member']->salt = md5(uniqid(mt_rand(), true));
                $this->registry['member']->password = $this->registry['member']->encrypt_password($this->registry['member']->password, $this->registry['member']->salt);
                $this->registry['member']->save();
                Application::set_flash("Your password has been updated.");
                $this->registry['breadcrumb'] = $this->registry['member']->bill_to_first_name . ' ' . $this->registry['member']->bill_to_last_name;
                $this->redirect ('member/edit');
            }
        }
        $this->registry['breadcrumb'] = '<a href="/member">' . $this->registry['member']->bill_to_first_name . ' ' . $this->registry['member']->bill_to_last_name . '</a> > New password';
    }

    function activity() {
        $this->registry['activities'] = Activity::find_all_by(array('member_id' => $this->registry['member']->id));
    }

    function destroy_activity($param) {
        $activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $param));
        if(empty($activity) || empty($_POST['go'])) {
            $this->redirect('member');
            exit;
        }
        
        SiteEmail::remove_mailing_list_for($activity);
        Activity::destroy($param);
        ActiveRecordBase::query("DELETE FROM registrations WHERE activity_id = " . intval($param) );
        Application::set_flash("'{$activity->name}' and it associated registrations have been permanently removed.");
        LogForDashboard::log($this->registry['member']->id, 0, 'remove', 'Activity Deleted', "{$activity->name} has been deleted.");
        $this->redirect('member');
    }
    function activity_detail($param) {
        $this->registry['activity'] = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $param));
        if(empty($this->registry['activity'])) {
            $this->redirect('member');
            exit;
        }
    
        $res = ActiveRecordBase::query("SELECT count(id) as complete, sum(registration_amount) as registration_amount, sum(donation_amount) as donation_amount FROM registrations WHERE member_id = {$this->registry['member']->id} AND activity_id = {$this->registry['activity']->id} AND (transaction_ref != '' OR member_created = 'y')");
        $this->registry['completed_registrations'] = $res[0]['complete'];
        $this->registry['registration_amount']     = $res[0]['registration_amount'];
        $this->registry['donation_amount']         = $res[0]['donation_amount'];

        $this->setup_dashboard_report();
        $this->registry['logs'] = LogForDashboard::get_latest_for_activity($this->registry['activity']->id);
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.js";
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.time.js";
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.tooltip.min.js";
    }
    
    function edit_activity($param) {
        $activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $param));
        if(empty($activity)) {
            $this->redirect('member');
            exit;
        }
        
        // formatting for display
        if(!empty($activity->registration_start_date) && $activity->registration_start_date != '0000-00-00') $activity->registration_start_date = date('m/d/Y', strtotime($activity->registration_start_date));
        if(!empty($activity->registration_end_date) && $activity->registration_end_date != '0000-00-00') $activity->registration_end_date   = date('m/d/Y', strtotime($activity->registration_end_date));
        if(!empty($activity->activity_date) && $activity->activity_date != '0000-00-00') $activity->activity_date = date('m/d/Y', strtotime($activity->activity_date));
        if(!empty($activity->activity_time)) $activity->activity_time = Helpers::format_mysql_time_for_display($activity->activity_time);

        // handle cost definition
        if(!empty($_POST)) {
            $i = 1;
            if($_POST['multiple_costs'] == 'y') {
                while($_POST['division' . $i]) {
                    $div[$i] = $_POST['division' . $i];
                    $cst[$i] = number_format($_POST['cost' . $i], 2);
                    $lmt[$i] = intval($_POST['limit' . $i]);
                    $i++;
                }           
            } else {
                $div[$i] = $_POST['name'];
                $cst[$i] = number_format($_POST['cost'], 2);
                $lmt[$i] = intval($_POST['limit']);         
            }
            $cost = array('division' => $div, 'cost' => $cst, 'limit' => $lmt);
            unset($_POST['division'], $_POST['cost'], $_POST['limit']);
            //echo json_encode(array('registration' => $cost));
            $_POST['registration_start_date'] = date('Y-m-d', strtotime($_POST['registration_start_date']));
            $_POST['registration_end_date']   = date('Y-m-d', strtotime($_POST['registration_end_date']));
            if(!empty($_POST['activity_date'])) $_POST['activity_date'] = date('Y-m-d', strtotime($_POST['activity_date']));
            if(!empty($_POST['activity_time'])) $_POST['activity_time'] = Helpers::make_mysql_time($_POST['activity_time']);
            if(!empty($_POST['consent'])) $_POST['consent'] = strip_tags($_POST['consent']);
            $_POST['website'] = str_replace(array('http://', 'http:/','https://', 'https:/'), '', strtolower($_POST['website']));
            if(empty($_POST['accept_donation'])) $activity->accept_donation = 'n';
            if($_POST['consent_form'] != 'y') $_POST['consent'] = '';
            $activity->edit($_POST);
            $activity->cost_definition = json_encode(array('registration' => $cost));
            //print_r($_POST);print_r($activity);exit;
            if($activity->save()) {
                LogForDashboard::log($activity->member_id, $activity->id, 'activity', 'Activity Updated', $activity->name . ' has been updated.');
                $this->do_activity_image_upload($activity->id);
                Application::set_flash($activity->name . ' has been updated.');
                $this->redirect('member/activity_detail/' . $activity->id);
                //$this->redirect('member/edit_activity/' . $activity->id);
            }
        }
        $this->registry['activity'] = $activity;

        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
        $this->registry['js_files'][] = "/js/plugins/fileupload/bootstrap-fileupload.js";
        $this->registry['js_files'][] = "/js/wysiwyg/wysihtml5-0.3.0.min.js";
        $this->registry['js_files'][] = "/js/wysiwyg/bootstrap3-wysihtml5.js";
        $this->registry['js_files'][] = "/js/wysiwyg/prettify.js";
        $this->registry['js_files'][] = "/js/plugins/moment.min.js";
        $this->registry['js_files'][] = "/js/plugins/bootstrap-datetimepicker.min.js";
        $this->registry['js_data'] = json_encode($activity);
    }

    function delete_activity_image ($id) {
        if($activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $id))) {
            unlink(SITE_ROOT . 'images/activities/' . $id . '.png');
            Application::set_flash('The image has been deleted.');
            $this->redirect('member/edit_activity/' . $id);     
        } else {
            Application::set_flash('Quit playin..', 'error');
            $this->redirect('member');              
        }
    }

    function edit_activity_form($param) {
        //Activity::generate_form_definition_json();exit;
        $activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $param));
        //$activity->form_definition_json = json_encode($activity->generate_default_form_definition_json());
        //$activity->save();
        //exit;
        $this->registry['activity'] = $activity;
        // get the current form definition
        $def = json_decode($activity->form_definition_json, true);
        $keys = array_keys($def);
        for($i = 0; $i < count($keys); $i ++)
            if(empty($def[$keys[$i]]['fields'])) unset($keys[$i]);
        $options = '';
        foreach($keys as $key) $options .= "<option value=\"{$key}\">" . ucwords($key) . "</option>";
        $this->registry['new_field_section'] .= '<select id="new_field_section" class="form-control" style="width:200px;" name="new_field_section">' . $options . '</select>';
        if(!empty($_POST)) {
            // adding in a new field??
            if(!empty($_POST['new_field_title'])) {
                // print_r($_POST);
                $values = explode(chr(10), $_POST['new_field_values']);
                foreach($values as $val) {
                    $v = trim($val);
                    if(!empty($v)) $value_list[$v] = $v;
                }
                $name = preg_replace("/[^a-zA-Z0-9 ]+/", "", $_POST['new_field_title']);
                foreach($_POST['new_field_validation'] as $v) $validation[$v] = 'true';
                $special = (in_array('date', $_POST['new_field_validation'])) ? 'date' : '';
                $field['type']  = $_POST['new_field_type'];
                $field['name']  = strtolower(str_replace(' ', '_', $name));
                $field['label']  = $_POST['new_field_title'];
                $field['default']  = '';
                $field['validation'] = (!empty($validation)) ? $validation : '';
                $field['special'] = (!empty($special)) ? $special : '';
                $field['value_list'] = (!empty($value_list)) ? $value_list : '';
                $field['notes']  = strip_tags($_POST['new_field_notes']);
                $field['custom']      = 'true';
                $field['removeable']  = 'true';

                $def[$_POST['new_field_section']]['fields'][$name] = $field;
                $activity->form_definition_json = json_encode($def);
                $activity->save();
                LogForDashboard::log($activity->member_id, $activity->id, 'activity', 'Activity Form Updated', 'The signup form for ' . $activity->name . ' has been updated.');
                Application::set_flash("'{$_POST['new_field_title']}' has been added to the {$_POST['new_field_section']} section.");
                $this->redirect('member/edit_activity_form/' . $activity->id);
                exit;       
            }
            
            // pull fields marked for removal into a remove variable and remove it from the post
            if(!empty($_POST['remove'])) {
                $remove = $_POST['remove'];
                unset($_POST['remove']);
            }
            // dont want to create a submit definition 
            unset($_POST['submit']); 
            
            // use this to preserve the order of the keys
            $new_def = array('main' => '', 'parent' => '', 'school' => '', 'emergency' => '', 'medical' => '', 'comments' => ''); // used to edit the definition controlling the order of elements
            $field_count = 0;
            
            // loop through the post variables and assign the values to the corrosponding definition 
            foreach($_POST as $key => $value) {
                foreach($value as $k => $v ) {
                    // section descriptions are not form fields so handle them here
                    if($k == 'description'){
                        $def[$key][$k] = $v;
                        $new_def[$key][$k] = strip_tags($def[$key][$k]);
                    } else {
                        // form section fields
                        // form->section->fields->element->values(->values array)
                        // $key  section [array key]
                        // $v    feilds array
                        // $a    field name (element) [array key]
                        // $b    field element value array
                        // $c    field element value names [array key]
                        // $d    field element value elements [ can be an array ]
                        // $e    field element value elements array key[key only if $d is array]
                        // $f    field element value elements array values
                        foreach($v as $a => $b) {      // for each fields array element
                            foreach($b as $c => $d) {    // for each input element 
                                if($c != 'validation' && $c != 'value_list'){
                                    $def[$key]['fields'][$a][$c] = $d;
                                } elseif($c == 'validation') {
                                    // if we are defining a validation or value list dive into the element array
                                    foreach($d as $e => $f) 
                                        $def[$key]['fields'][$a][$c][$e] = $f;
                                } elseif($c == 'value_list') {
                                    $values = explode(chr(10), $d);
                                    foreach($values as $val) $def[$key]['fields'][$a][$c][trim($val)] = trim($val);
                                    //print_r($def[$key]['fields'][$a][$c]);
                                }
                            } //foreach($b as $c => $d)
                            $new_def[$key][$k][$a] = $def[$key][$k][$a];
                        } //foreach($v as $a => $b)
                    } //if($k == 'description') else 
                } //foreach($value as $k => $v )
            } //foreach($_POST as $key => $value)
            //exit;
            //print_r($new_def);exit;
            // remove the posted removed elememnts
            foreach($remove as $key => $v) {
                foreach($v as $w) {
                    if($w == 'all') $new_def[$key] = '';
                    else if(!empty($w)) unset($new_def[$key]['fields'][$w]);
                }
            }           
            $activity->form_definition_json = json_encode($new_def);
            $activity->save();
            //print_r($def);
            LogForDashboard::log($activity->member_id, $activity->id, 'activity', 'Activity Form Updated', 'The signup form for ' . $activity->name . ' has been updated.');
            Application::set_flash("'The form definition has been saved.");
            $this->redirect('member/activity_detail/' . $activity->id);
            
        }
    }
    
    
    function create_activity() {
        if(!empty($_POST)) {//print_r($_POST);exit;
            if(!empty($_POST['activity_time'])) $_POST['activity_time'] = Helpers::make_mysql_time($_POST['activity_time']);
            if(!empty($_POST['activity_date'])) $_POST['activity_date'] = date('Y-m-d', strtotime($_POST['activity_date']));
            if(!empty($_POST['consent'])) $_POST['consent'] = strip_tags($_POST['consent']);
            $activity = Activity::create($_POST);
            $activity->member_id = $this->registry['member']->id;
            $activity->registration_start_date = date('Y-m-d', strtotime($_POST['registration_start_date']));
            $activity->registration_end_date   = date('Y-m-d', strtotime($_POST['registration_end_date']));
            $activity->website = str_replace(array('http://', 'http:/','https://', 'https:/'), '', strtolower($_POST['website']));
            $activity->archived = 'n';
            
            // get the default form definition...
            $def = $activity->generate_default_form_definition_json();
            
            /*$parts_check = array('parent', 'birthdate', 'shirt', 'parent_volunteer', 'parent_coach', 'school', 'medical',
                                 'primary_doctor', 'primary_dentist', 'medical_comments', 'emergency', 'preferred_hospital', 'donation');
            */
            
            // remove pieces from the form definition based the questionaire...
            if(in_array('parent', $_POST['info'])){
                unset($def['main']['fields']['email']);
                unset($def['main']['fields']['phone']);
                if(!in_array('parent_volunteer', $_POST['info'])) {
                    unset($def['parent']['fields']['parent1_volunteer']);
                    unset($def['parent']['fields']['parent2_volunteer']);
                }
                if(!in_array('parent_coach', $_POST['info'])) {
                    unset($def['parent']['fields']['parent1_coach']);
                    unset($def['parent']['fields']['parent1_experience']);
                    unset($def['parent']['fields']['parent2_coach']);
                    unset($def['parent']['fields']['parent2_experience']);
                }
            } else {
                unset($def['parent']);
            }
            if(!in_array('birth_date', $_POST['info'])) unset($def['main']['fields']['birth_date']);
            if(!in_array('shirt', $_POST['info'])) unset($def['main']['fields']['shirt_size']);
            if(!in_array('school', $_POST['info'])) unset($def['school']);
            if(!in_array('medical', $_POST['info'])) unset($def['medical']);
            if(!in_array('primary_doctor', $_POST['info'])) {
                unset($def['medical']['fields']['physician_name']);
                unset($def['medical']['fields']['physician_phone']);
            }
            if(!in_array('primary_dentist', $_POST['info'])) {
                unset($def['medical']['fields']['dentist_name']);
                unset($def['medical']['fields']['dentist_phone']);
            }
            if(!in_array('preferred_hospital', $_POST['info'])) unset($def['medical']['fields']['preferred_hospital']);
            if(!in_array('medical_comments', $_POST['info'])) unset($def['medical']['fields']['medical_comments']);
            if(!in_array('emergency', $_POST['info'])) unset($def['emergency']);
            if(!in_array('comments', $_POST['info'])) unset($def['comments']);
            
            if(in_array('donation', $_POST['info'])) $activity->accept_donation = 'y';
            else $activity->accept_donation = 'n';
                
            
            //print_r($_POST);print_r($activity);exit;
            
            $activity->form_definition_json = json_encode($def);

            // handle cost definition
            $i = 1;
            if($_POST['multiple_costs'] == 'y') {
                while($_POST['division' . $i]) {
                    $div[$i] = $_POST['division' . $i];
                    $cst[$i] = number_format($_POST['cost' . $i], 2);
                    $lmt[$i] = intval($_POST['limit' . $i]);
                    $i++;
                }           
            } else {
                $div[$i] = $_POST['name'];
                $cst[$i] = number_format($_POST['cost'], 2);
                $lmt[$i] = intval($_POST['limit']);         
            }
            $cost = array('division' => $div, 'cost' => $cst, 'limit' => $lmt);
            unset($_POST['division'], $_POST['cost'], $_POST['limit']);
            if($_POST['consent_form'] != 'y') $_POST['consent'] = '';

            $activity->cost_definition = json_encode(array('registration' => $cost));

            if($activity->save()) {
                LogForDashboard::log($activity->member_id, $activity->id, 'activity', 'New Activity Created', $activity->name . ' has been created.');
                $this->do_activity_image_upload($activity->id);
                SiteEmail::register_mailing_lists_for($activity);
                Application::set_flash($activity->name . ' has been created.');
                $this->redirect('member');
            }
        } else {
            $activity = new Activity();
            $activity->member_id = $this->registry['member']->id;
            $activity->confirmation_text = '<p>Thank you for registering.</p>';
            $activity->description = '<p>Give some details.</p>';
        }
        $this->registry['activity'] = $activity;

        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
        $this->registry['js_files'][] = "/js/plugins/fileupload/bootstrap-fileupload.js";
        $this->registry['js_files'][] = "/js/wysiwyg/wysihtml5-0.3.0.min.js";
        $this->registry['js_files'][] = "/js/wysiwyg/bootstrap3-wysihtml5.js";
        $this->registry['js_files'][] = "/js/wysiwyg/prettify.js";
        $this->registry['js_files'][] = "/js/plugins/moment.min.js";
        $this->registry['js_files'][] = "/js/plugins/bootstrap-datetimepicker.min.js";
        $this->registry['js_data'] = json_encode($activity);
    }

    function manage_activity_calendar($id) {
        $this->registry['activity'] = Activity::find($id);
        if(empty($this->registry['activity']) || $this->registry['activity']->member_id != $this->registry['member']->id){
            $this->redirect('member');
            exit;
        }
        $this->registry['calendar_dates'] = ActivityCalendarDate::find_all_by(array('activity_id' => $this->registry['activity']->id), array('ORDER BY' => 'calendar_date asc'));
        //print_r($_POST);
        if(!empty($_POST)) {
            switch($_POST['action']) {
                case 'add':
                    // format date
                    $_POST['calendar_date'] = date('y-m-d', strtotime($_POST['calendar_date']));
                    $calendar_date = ActivityCalendarDate::create($_POST);
                    $calendar_date->activity_id = $this->registry['activity']->id;
                    $calendar_date->save();
                    LogForDashboard::log($this->registry['activity']->member_id, $this->registry['activity']->id, 'calendar', 'Calendar Date Created', $_POST['title']. ' has been added for ' . $this->registry['activity']->name);
                    Application::set_flash("The avtivity calendar date has been saved.");
                    break;
                case 'edit':
                    // format date
                    $_POST['calendar_date'] = date('y-m-d', strtotime($_POST['calendar_date']));
                    $calendar_date = ActivityCalendarDate::find($_POST['id']);
                    $calendar_date->edit($_POST);
                    $calendar_date->sequence_no ++; 
                    $calendar_date->save();
                    Application::set_flash("The avtivity calendar date has been updated.");
                    break;
                case 'delete':
                    $calendar_date = ActivityCalendarDate::find($_POST['id']);
                    ActivityCalendarDate::destroy($_POST['activity_calendar_date_id']);
                    LogForDashboard::log($this->registry['activity']->member_id, $this->registry['activity']->id, 'calendar', 'Calendar Date Removed', $calendar_date->title . ' has been removed from ' . $this->registry['activity']->name);
                    Application::set_flash("The activity calander date has been removed.");
                    break;
            }
            
            $cal = ActivityCalendarDate::generate_ics_for($this->registry['activity']->id);
            $cal_name = 'rh' . $this->registry['member']->id . $this->registry['activity']->id;
            //echo $cal_name.' '.$cal;exit;
            SimpleCache::write_calendar_file($cal_name, $cal);
            $this->redirect('member/manage_activity_calendar/' . $this->registry['activity']->id);
        }
        
        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
        $this->registry['js_files'][] = "/js/plugins/moment.min.js";
        $this->registry['js_files'][] = "/js/plugins/bootstrap-datetimepicker.min.js";
    }
    

    function destroy_activity_calendar($id) {
        $date = ActivityCalendarDate::find($id);
        $activity = Activity::find($date->activity_id);
        $_POST['action'] = 'delete';
        $_POST['activity_calendar_date_id'] = $id;
        $this->manage_activity_calendar($activity->id);
    }

    function registrations ($params) {
        $id = intval($params);
        $this->registry['activity'] = $activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $id));
        if(empty($activity)) {
            $this->redirect('member');
            exit;
        }

        
        // setup stats
        $res = ActiveRecordBase::query("SELECT count(id) as complete FROM registrations WHERE member_id = {$this->registry['member']->id} AND activity_id = {$activity->id} AND (transaction_ref != '' OR member_created = 'y')");
        $this->registry['completed_registrations'] = $res[0]['complete'];
        $res = ActiveRecordBase::query("SELECT count(id) as bailed FROM registrations WHERE member_id = {$this->registry['member']->id} AND activity_id = {$activity->id} AND (transaction_ref = '' AND member_created != 'y')");
        $this->registry['bailed_registrations'] = $res[0]['bailed'];

        $this->registry['logs'] = LogForDashboard::get_latest_for_activity($this->registry['activity']->id);
        $this->registry['js_files'][] = "/js/plugins/dataTables/jquery.dataTables.min.js";
        $this->registry['js_files'][] = "/js/plugins/dataTables/dataTables.bootstrap.js";
        // send json data to registry data object
        $this->registry['js_data'] = json_encode(array('activity_id' => $activity->id));
    }

    function bailed_registrations($id) {
        $this->registry['activity'] = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $id));
        $query = "SELECT * FROM registrations WHERE member_id = {$this->registry['member']->id} AND activity_id = {$this->registry['activity']->id} AND (transaction_ref = '' AND  member_created = 'n')";
        $this->registry['registrations'] = ActiveRecordBase::fetch_as_object($query, 'registration');
        
        if(empty($this->registry['activity'])) {
            $this->redirect('member');
            exit;
        }else if(empty($this->registry['registrations'])) {
            $this->redirect('member/registrations/' . $this->registry['activity']->id);
            exit;
        }

        $this->registry['logs'] = LogForDashboard::get_latest_for_activity($this->registry['activity']->id);
        $this->registry['js_files'][] = "/js/plugins/dataTables/jquery.dataTables.min.js";
        $this->registry['js_files'][] = "/js/plugins/dataTables/dataTables.bootstrap.js";
        // send json data to registry data object
        $this->registry['js_data'] = json_encode(array('activity_id' => $this->registry['activity']->id));
    }
    
    function destroy_registration($params) {
        $registration = Registration::find_by(array('id' => $params[0], 'activity_id' => $params[1], 'member_id' => $this->registry['member']->id));
        if(empty($registration)) {
            $this->redirect('member');
            exit;
        }
        if(Registration::destroy($registration->id)) {
            $activity = Activity::find($registration->activity_id);
            $costs_def = json_decode($activity->cost_definition, true);//print_r($costs_def);exit;
            $key = array_search($registration->division, $costs_def['registration']['division']);
            // register them to the proper mailing lists
            $user = array('address' => $registration->email);
            SiteEmail::remove_user_from_list($user, 'a' . $activity->id);
            SiteEmail::remove_user_from_list($user, 'a' . $activity->id . 'd' . $key);
            LogForDashboard::log($this->registry['member']->id, $activity->id, 'remove', 'Registrtaion Deleted', "The registration for '{$registration->first_name} {$registration->last_name}' has been deleted.");
            Application::set_flash ("The registration for '{$registration->first_name} {$registration->last_name}' has been deleted.");
        } else {
            Application::set_flash ("An error has occured.", 'error');
        }
        $this->redirect ('member/registrations/' . $registration->activity_id);
    }

    function search_registrations ($activity_id) {
        if(!empty($_POST)) {
            $searchterm = ($_POST['compare'] == 'LIKE') ? "%" . $_POST['searchterm'] . "%" : $_POST['searchterm'];
            $registrations = Registration::fetch_as_object("SELECT * FROM registrations WHERE {$_POST['field']} {$_POST['compare']} '{$searchterm}' AND activity_id = {$activity_id} AND member_id = {$this->registry['member']->id}");
            $activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $activity_id));
            $this->registry['registrations'] = $registrations;
            $this->registry['activity'] = $activity;
            $this->view = 'registrations';
        } else {
            $this->redirect('member/registrations/' . $activity_id);
        }
    }

    function create_registration($param) {
        if(!$activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $param))) {
            $this->redirect('member');
            exit;
        }
        
        if(!empty($_POST)) {
            $info = $_POST;
            $info['registration_amount']   = $info['amount_paid'];
            $info['member_created']   = 'y';
            $info['r5k_payment_hold'] = 'n';
            $info['activity_id']  = $activity->id;
            $info['member_id']    = $activity->member_id;
            $info['session_id']   = session_id();
            $info['ip_address']   = Helpers::get_real_ip();
            $info['browser_info'] = $_SERVER['HTTP_USER_AGENT'];
            if(!empty($_POST['custom_fields'])) $info['custom_fields'] = json_encode($_POST['custom_fields']);
            if(!empty($_POST['phone']))         $info['phone'] = Registration::format_to_us_phone_number($_POST['phone']);
            if(!empty($_POST['parent1_phone'])) $info['parent1_phone'] = Registration::format_to_us_phone_number($_POST['parent1_phone']);
            if(!empty($_POST['parent2_phone'])) $info['parent2_phone'] = Registration::format_to_us_phone_number($_POST['parent2_phone']);
            if(!empty($_POST['birth_date_mm'])) $info['birth_date'] = $_POST['birth_date_yyyy'] . '-' . $_POST['birth_date_mm'] . '-' . $_POST['birth_date_dd'];
            $info['payment_date'] = (!empty($_POST['payment_date'])) ? date('Y-m-d', strtotime($_POST['payment_date'])) : '';
            $registration = Registration::create($info);
            if($registration->save()) {
                $costs_def = json_decode($activity->cost_definition, true);//print_r($costs_def);exit;
                $key = array_search($registration->division, $costs_def['registration']['division']);
                // register them to the proper mailing lists
                $emails = array('email', 'parent1_email', 'parent2_email');
                foreach($emails as $email) {
                    $user = array('address' => $registration->$email,
                                  'name' => $registration->first_name . ' ' . $registration->last_name,
                                  'description' => 'Organizer Added');
                    SiteEmail::register_user_for_list($user, 'a' . $activity->id);
                    if(!empty($key)) SiteEmail::register_user_for_list($user, 'a' . $activity->id . 'd' . $key);
                }
                LogForDashboard::log_registration_for_activity($registration, $activity, true);
                Application::set_flash("The Registration has been created.");
                $this->redirect ('member/registrations/' . $registration->activity_id);
            } 
        } else {
            $registration = new Registration();
        }
        $this->registry['registration'] = $registration;
        $this->registry['activity']     = $activity;
        $this->registry['js_files'][] = '/js/plugins/parsley/parsley.min.js';
        $this->registry['js_files'][] = "/js/plugins/moment.min.js";
        $this->registry['js_files'][] = "/js/plugins/bootstrap-datetimepicker.min.js";
    }

    function edit_registration($params) {
        $id = $params[0];
        $activity_id = $params[1];
        $activity = Activity::find_by(array('member_id' => $this->registry['member']->id, 'id' => $activity_id));

        if($registration = Registration::find_by(array('id' => $id, 'member_id' => $this->registry['member']->id, 'activity_id' => $activity->id))) {
            if(!empty($_POST)) {
                if(!empty($_POST['custom_fields'])) $_POST['custom_fields'] = json_encode($_POST['custom_fields']);
                if(!empty($_POST['phone']))         $_POST['phone'] = Registration::format_to_us_phone_number($_POST['phone']);
                if(!empty($_POST['parent1_phone'])) $_POST['parent1_phone'] = Registration::format_to_us_phone_number($_POST['parent1_phone']);
                if(!empty($_POST['parent2_phone'])) $_POST['parent2_phone'] = Registration::format_to_us_phone_number($_POST['parent2_phone']);
                if(!empty($_POST['date_paid']))     $_POST['date_paid'] = date('Y-m-d', strtotime($_POST['date_paid']));
                if(!empty($_POST['birth_date']))    $_POST['birth_date'] = date('Y-m-d', strtotime($_POST['birth_date']));
                if(!empty($_POST['birth_date_mm'])) $_POST['birth_date'] = $_POST['birth_date_yyyy'] . '-' . $_POST['birth_date_mm'] . '-' . $_POST['birth_date_dd'];
                $registration->date_paid = date('Y-m-d', strtotime($_POST['date_paid']));
                $registration->edit($_POST);
                $registration->save();
                Application::set_flash('Information for ' . $registration->first_name . ' ' .$registration->last_name . ' has been updated.');
                $this->redirect('member/edit_registration/' . $registration->id . '/' . $registration->activity_id);
                exit;
            }
            
            if($registration->date_paid == '0000-00-00') $registration->date_paid = '';
            else $registration->date_paid = date('m/d/Y', strtotime($registration->date_paid));
            //if(!empty($registration->birth_date)) $registration->birth_date = date('m/d/Y', strtotime($registration->birth_date));
            $registration->populate_custom_fields();
            $def = json_decode($activity->form_definition_json);
            foreach($def as $key => $section) 
                foreach($section->fields as $element) {
                    $field = $element->name;
                    $form[$key][$field]['value'] = $registration->$field;
                    $form[$key][$field]['type']  = $element->type;
                }
                
            $this->registry['form_elements'] = $form;           
            $this->registry['registration']  = $registration;
            $this->registry['activity']      = $activity;


        } else {
            $this->redirect ('member');
        }
        

        $this->registry['js_files'][] = "/js/plugins/moment.min.js";
        $this->registry['js_files'][] = "/js/plugins/bootstrap-datetimepicker.min.js";
        $this->registry['js_data'] = json_encode($registration);
    }


    function stripe_connect($params) {
        if(!empty($params)) {
            $params = substr($params, 1);
            parse_str($params, $p);//print_r($p);exit;
            if(!empty($p['error'])) {
                Application::set_flash(ucwords(str_replace('_', ' ', $p['error'])) . '<br />' . $p['error_description'] . '<br /><br />Please try to connect RegistrationHandler to use your Stripe account again.', 'error');
                LogAction::log('New Stripe Account Failure. ' . $this->registry['error'], 'stripe', $this->registry['member']->id, true);
                $this->redirect('member/stripe_connect');
            }   elseif(!empty($p['response_type'])) {
                print_r($p);
            } else {
                $post = array('client_secret' => STRIPE_SECRET_KEY,
                              'code'          => $p['code'],
                              'scope'         => 'read_write',
                              'grant_type'    => 'authorization_code');
                $defaults = array(
                    CURLOPT_POST => 1,
                    CURLOPT_HEADER => 0,
                    CURLOPT_URL => 'https://connect.stripe.com/oauth/token',
                    CURLOPT_FRESH_CONNECT => 1,
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_FORBID_REUSE => 1,
                    CURLOPT_TIMEOUT => 4,
                    CURLOPT_POSTFIELDS => http_build_query($post)
                );
                $ch = curl_init();
                curl_setopt_array($ch, $defaults);
                $result = curl_exec($ch);
                // if the confirmation ping was sucessful save the customer token.
                if(curl_errno($ch) < 1 && !empty($result)) {
                    $payment = MemberPaymentSetting::find_by(array('member_id' => $this->registry['member']->id));
                    if(empty($payment)) $payment = MemberPaymentSetting::create(array('member_id' => $this->registry['member']->id));
                    $res = json_decode($result);
                    // if there was an error
                    if(!empty($res->error)) {
                        //$payment = MemberPaymentSetting::find_by(array('member_id' => $this->registry['member']->id));
                        Application::set_flash($res->error_description . '<br /><br />Please try to connect RegistrationHandler to use your Stripe account again.', 'error');
                        LogAction::log('Stripe Account Failure. Error: ' . $res->error . ' -> ' . $res->error_description, 'stripe', $this->registry['member']->id, true);
                        $this->redirect('member/payments');
                        exit;                   
                    }
                    $payment->stripe_access_token = $res->access_token;
                    $payment->stripe_account = $res->stripe_user_id;
                    $payment->stripe_status = 'active';
                    $payment->stripe_full_return = $result;
                    $payment->pay_via = 'stripe';
                    $payment->cc_fee = 29;
                    $payment->account_number = '';
                    $payment->routing_number = '';
                    $payment->save();
                    Application::set_flash('You have successfully connected your Stripe account to RegistrationHander.');
                    LogAction::log('Stripe Account Connected.', 'stripe', $this->registry['member']->id, true);
                    $this->redirect('member/payments');
                    exit;
                }
            }
        }
    }
    
    function verify_bank_account($member_id) {
        if($member_id > 0 && $member_id != $this->registry['member']->id) {
            header('HTTP/1.0 403 Nope');
            $this->redirect('member');
            exit;
        }
        $setting = MemberPaymentSetting::find_by(array('member_id' => $member_id));
        if(time() > date('U', strtotime($setting->modified_at)) - 10000 && time() < date('U', strtotime($setting->modified_at)) + 10000) {
            Application::set_flash("Your bank account information has been sucessfully added to your account and direct deposit is set up.");
        } else {
            Application::set_flash("An error has occured, please re-submit your banking information.", 'error');
        }
        $this->redirect('member/edit');
    }
    
    function apply_refund($params) {
        if(empty($_POST)) {
            $this->redirect('member');
            exit;
        }
        
        $registration = Registration::find($params[0]);
        $activity = Activity::find($params[1]);
        
        // checks..
        if(empty($registration) || empty($activity)) {
            $this->redirect('member');
            exit;
        }
        if($activity->member_id != $this->registry['member']->id){
            $this->redirect('member');
            exit;
        }
        
        Stripe::setApiKey(STRIPE_SECRET_KEY);
        $ch = Stripe_Charge::retrieve($registration->transaction_ref);
        //print_r($_POST);exit;
        $_POST['refund_amount'] = number_format(str_replace('$', '', $_POST['refund_amount']), 2);
        // check valid amount
        if(intval($_POST['refund_amount']) == 0 ) {
            Application::set_flash('The refund amount must be a valid dollar amount.', 'error'); 
        } elseif($_POST['refund_amount'] > (($registration->registration_amount + $registration->donation_amount) - $registration->refund_amount)) {
            // check that it is not greater than the registratoin and donation for this person
            Application::set_flash('You can only refund up to a total of $' . number_format($registration->registration_amount + $registration->donation_amount, 2) . ' for this registration.', 'error'); 
            Application::set_flash('$' . number_format($registration->refund_amount, 2) . ' has already previously been refunded for this registration.', 'error'); 
        } else {
            try{
                $ch->refund(array('amount' => $_POST['refund_amount'] * 100, 'refund_application_fee' => false));
                Application::set_flash("A refund of $" . $_POST['refund_amount'] . " has been applied."); 
            } 
            catch(Exception $e) {
                LogAction::log('Error in refund. ' . $e->getMessage(), 'stripe', $this->registry['member']->id, true);
                Application::set_flash($e->getMessage(), 'error'); 
                $this->redirect('member/edit_registration/' . $registration->id  . '/' . $activity->id);
                exit;
            }
            $registration->refund_amount += $_POST['refund_amount'];
            $registration->notes .= "\r\nA refuned of $" . $_POST['refund_amount'] . " was applied on " . date(DATE_RSS) . '.';
            $registration->save();
        }
        $this->redirect('member/edit_registration/' . $registration->id  . '/' . $activity->id);
        //print_r($ch);
    }
    
    function stripe_webhook() {
        Stripe::setApiKey(STRIPE_SECRET_KEY);       
        // Retrieve the request's body and parse it as JSON
        $body = @file_get_contents('php://input');
        LogAction::log('Stripe Webhook. ' . $body, 'stripe');
        
        $event_json = json_decode($body);
        // handle certain webhook events
        switch($event_json->type) {
            case 'account.application.deauthorized':
                $pay = MemberPaymentSetting::find_by(array('stripe_account' => $event_json->user_id));
                $pay->stripe_status = 'deauthorized';
                $pay->save();
        }
        header("HTTP/1.0 200 OK");
        exit;
    }
    
    
    /* ajax method to check for a member email already in the database */
    function _ajax_unique_email_validation() {
        if(!empty($_POST)) {
            $res = ActiveRecordBase::query("SELECT email FROM members WHERE email = " . ActiveRecordBase::quote($_POST['email']));
            if(empty($res)) {
                echo 'true';
            } else {
                echo 'false';
            }
        }
    }

    /**  
     * update the record based on passed values...
     * post values are 'value' => submitted input value and 'id' => {field_name}_{id}
     * can grab the notice by the id number at the end of $_POST['id'] and the corrosponding
     * field by the first portion of $_POST['id'] and set it to $_POST['value']
     */
    function _ajax_update_registration($activity_id) {
        if(!empty($_POST)) {
            $id = substr(strrchr($_POST['id'], '_'), 1);
            $field = substr($_POST['id'], 0, -(strlen($id) + 1));
            if($registration = Registration::find_by(array('id' => $id, 'member_id' => $this->registry['member']->id, 'activity_id' => $activity_id))) {
                $original_value = $registration->$field;
                // sql format for a date field
                if(strrpos($field, '_date') !== false) $_POST['value'] = date('Y-m-d', strtotime($_POST['value']));
                $registration->$field = trim($_POST['value']);
                if($registration->save()) {
                    // in case it is a date field format the display
                    if(strrpos($field, '_date') !== false) $registration->$field = date('M j, Y', strtotime($registration->$field));
                    echo nl2br($registration->$field);  
                } else {print_r($registration->errors);
                    echo $original_value;
                }
            }
        }
    }
    
    function _ajax_reset_form_section() {
        //echo print_r($_POST,true);exit;
        $activity = new Activity();
        $form = $activity->generate_default_form_definition_json();
        $section[$_POST['section']] = $form[$_POST['section']];
        $activity->form_definition_json = json_encode($section[$_POST['section']]);
        //echo $activity->form_definition_json;exit;
        //print_r($section[$_POST['section']]);exit;
        echo $activity->display_definition_for_section($_POST['section'], json_decode($activity->form_definition_json));
        //$def = json_decode($activity->form_definition_json);
    }

    function _ajax_save_stripe_bank_token() {
        if(empty($_POST)) {
            header('HTTP/1.0 405 Nope');
            exit;
        }
        Stripe::setApiKey(STRIPE_SECRET_KEY);   
        $token     = $_POST['token'];
        $member_id = $_POST['member_id'];
        $setting = MemberPaymentSetting::find_by(array('member_id' => $member_id));
        $member  = Member::find($member_id);
        //print_r($token);
        if(empty($member) || empty($setting)) {
            LogAction::log('New Stripe Recipient Failure. Empty member or payment setting.: ' . print_r($_POST, true), 'error', $this->registry['member']->id);
            header('HTTP/1.0 403 Nope');
            exit;
        }
        try{
            $desc   = (empty($this->registry['member']->name)) ? "{$this->registry['member']->id}: {$this->registry['member']->contact_name}" : "{$this->registry['member']->id}: {$this->registry['member']->name}";
            $tax_id = $setting->simple_decrypt($this->registry['member']->salt, $setting->tax_id);
            $type   = (substr_count($tax_id, '-') === 1) ? 'corporation' : 'individual';
            $recipient = Stripe_Recipient::create(array('name' => $setting->name,
                                                        'type' => $type,
                                                        'tax_id' => '000000000', //$tax_id,
                                                        'description' => $desc,
                                                        'bank_account' => $token['id'],
                                                        'email' => $member->email));
            LogAction::log('New Stripe Recipient Created. ' . print_r($recipient, true), 'stripe', $this->registry['member']->id, true);
            $setting->stripe_recipient = $recipient->id;
            $setting->pay_via            = 'direct_deposit';
            $setting->bank_account_last4 = $recipient->active_account->last4;
            $setting->bank_name          = $recipient->active_account->bank_name;
            $setting->routing_number     = $setting->simple_encrypt($member->salt, $_POST['routing_number']);
            $setting->account_number     = $setting->simple_encrypt($member->salt, $_POST['account_number']);
            $setting->cc_fee             = 35;
            $setting->save();
            echo json_encode(array('status' => 'success'));
        }catch(Stripe_InvalidRequestError $e) { 
            LogAction::log('New Stripe Recipient Stripe_InvalidRequestError.: ' . print_r($e, true), 'error', $this->registry['member']->id);
            $body = $e->getJsonBody();
            echo json_encode(array('status' => 'error', 'message' => $body['error']['message']));
        }
    }

    function _ajax_registration_datatable($params) {
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * Easy set variables for datatable plugin
         */
         if(empty($_POST)) die;

        // DB table to use
        $table = 'registrations';
         
        // Table's primary key
        $primaryKey = 'id';
         
        // Array of database columns which should be read and sent back to DataTables.
        // The `db` parameter represents the column name in the database, while the `dt`
        // parameter represents the DataTables column identifier. In this case simple
        // indexes

        // check for bailed registratiosn...
        if($_POST['bailed'] == 'true') $and = "(transaction_ref = '' AND  member_created = 'n')";
        else $and = "(amount_paid > 0 OR transaction_ref != '' OR member_created = 'y')";
        $columns = array(
            array( 'db' => 'id', 'dt' => 0 ),
            array( 'db' => 'last_name', 'dt' => 1 ),
            array( 'db' => 'first_name',  'dt' => 2 ),
            array( 'db' => 'email',      'dt' => 3 ),
            array( 'db' => 'address_1',    'dt' => 4 ),
            array(
                'db'        => 'created_at',
                'dt'        => 5,
                'formatter' => function( $d, $row ) {
                    return date( 'M j, Y', strtotime($d));
                }
            ),
            array( 'db' => 'parent1_email', 'dt' => 6 ),
            array( 'special' => "activity_id = " . intval($_POST['activity_id']) . " AND member_id = " . intval($_POST['member_id']) . " AND " . $and)
        );
         
        // SQL server connection information
        $sql_details = array(
            'user' => DB_USER,
            'pass' => DB_PASSWORD,
            'db'   => DB_NAME,
            'host' => DB_HOST
        );
         
         
        /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
         * If you just want to use the basic configuration for DataTables with PHP
         * server-side, there is no need to edit below this line.
         */
         
        //require( 'ssp.class.php' );
         
        echo json_encode(
            SSP::simple( $_POST, $sql_details, $table, $primaryKey, $columns )
        );
    }

    private function do_activity_image_upload($activity_id) {
        if(!empty($_FILES)) {
            $file_size = filesize($_FILES['image']['tmp_name']);
            // is there an image
            if($file_size > 1) {
                if ($file_size <= 1048576) {
        
                    $file_name = basename($_FILES['image']['name']);
                    $file_name = str_replace(' ', '_', $file_name);
            
                    $allowed   = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif');
                    $size = getimagesize($_FILES['image']['tmp_name']);
                    $file_size = filesize($_FILES['image']['tmp_name']);
            
                    if (in_array($size['mime'], $allowed)) {
        
                        $destination = SITE_ROOT . 'images/activities/' . $activity_id . '.png';
                        $thumb = SITE_ROOT . 'images/activities/' . $activity_id . '-125.png';
                        $micro = SITE_ROOT . 'images/activities/' . $activity_id . '-35.png';
                        //$thumbs_destination = SITE_ROOT . 'images/activities/thumbs/' . $id . '/';
                
            
                        if (move_uploaded_file($_FILES['image']['tmp_name'], $destination)) {
                            AdminHelper::resize_image_by_max($destination, $destination, 220);
                            AdminHelper::resize_image_by_max($destination, $thumb, 125);
                            AdminHelper::resize_image_by_max($destination, $micro, 35);
                        } else {
                            Application::set_flash("There was an error uploading the file " . basename($_FILES['image']['name']) . ".", 'error');
                        }
            
                    } else {
                        Application::set_flash('Please choose an image file.', 'error');
                    }
        
                } else {
                    Application::set_flash('Please choose a file 1MB or smaller in size.', 'error');
                }
            }
        }
    
    }

    private function setup_dashboard_report($activity_id = 0) {
        $start_date = date('Y-m-d', strtotime('00:00:00 - 2 weeks UTC'));
        $end_date = date('Y-m-d', strtotime('00:00:00 tomorrow UTC'));
        $type = 'all';
        if($type == 'web') $type_query = "AND member_created = 'n' AND amount_paid > 0 AND registrations.date_paid >= '{$start_date}' AND registrations.date_paid <= '{$end_date}' ";
        else $type_query = "AND ((registrations.date_paid >= '{$start_date}' AND registrations.date_paid <= '{$end_date}') OR (registrations.created_at >= '{$start_date}' AND registrations.created_at <= '{$end_date}')) ";
        if($activity_id > 0) $activity_query = "AND regisrtations.activity_id = {$activity_id} ";
        // graph values
        $query = "SELECT registrations.date_paid, registrations.created_at, registrations.amount_paid, registrations.registration_amount, registrations.donation_amount, registrations.r5k_fee, registrations.division, registrations.activity_id, activities.name, activities.accept_donation "
               . "FROM registrations "
               . "JOIN activities on activities.id = registrations.activity_id "
               . "WHERE registrations.member_id = " . $this->registry['member']->id . " "
               . $type_query
               . $activity_query
               . "AND registrations.r5k_payment_hold = 'n' "
               . "AND activities.archived = 'n' "
               . "ORDER BY registrations.date_paid DESC, activities.name, registrations.division";
        $activities2 = ActiveRecordBase::query($query);
        
        // set up associate array to access values
        foreach($activities2 as $reg) {
            if($reg['date_paid'] == '0000-00-00' || empty($reg['date_paid'])) 
                $reg['date_paid'] = $reg['created_at'];
            $name = ($reg['name'] == $reg['division']) ? $reg['name'] : $reg['name'] . ': ' . $reg['division'];
            $this->registry['summary'][$reg['date_paid']]['registrations'] ++;
            $this->registry['summary'][$reg['date_paid']]['detail']['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])]['name'] = $name;
            $this->registry['summary'][$reg['date_paid']]['detail']['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])]['registrations'] ++;
            $divisions['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])] = $reg['division'];

        }

        //$this->registry['divisions'] = $divisions;
        $date = $start_date;

        while($date <= $end_date) {
            foreach($divisions as $aid => $values) {
                foreach($values as $key => $division) {
                    $line_values[$aid . $key][] = (empty($this->registry['summary'][$date]['detail'][$aid][$key]['registrations'])) ? 0 : $this->registry['summary'][$date]['detail'][$aid][$key]['registrations'];
                    if(empty($line_labels[$aid . $key]))
                        $line_labels[$aid . $key] = $this->registry['summary'][$date]['detail'][$aid][$key]['name'];
                }
            }
            
            $date = date('Y-m-d', strtotime($date . ' 00:00:00 + 1 day UTC'));
        }

        $keycounter = 0;
        foreach($line_values as $key => $lv) {
            for($i = 0; $i <= 14; $i ++) {
                //$datapoints[$keycounter]['values'][] = "[(new Date(+new Date().setDate(new Date().getDate() - 14 - {$i}))).getTime(), {$lv[$i]}]";
                $v = 14 - $i;
                //$datapoints[$keycounter]['values'][] = "[" . strtotime(" -{$v} days UTC") * 1000 . " , {$lv[$i]}]";
                $datapoints[$keycounter]['values'][$i][] = strtotime("00:00:00 -{$v} days UTC") * 1000;
                $datapoints[$keycounter]['values'][$i][] = $lv[$i];
                $datapoints[$keycounter]['label']  = $line_labels[$key];
            }
            $keycounter ++;
        }

        $ouput = array();
        foreach ($datapoints as $key => $dp) {
            $output[$key]['label'] = htmlspecialchars($dp['label'], ENT_QUOTES);
            $output[$key]['data'] = $dp['values'];
        }

        // send json data to registry data object
        $this->registry['js_data'] = json_encode($output);


    }

    function import() {
        $text = file_get_contents(SITE_ROOT . 'reg.txt');
        $lines = explode(PHP_EOL, $text);
        foreach($lines as $line) {
            $fields = explode("\t", $line);
            $reg = Registration::create(array(
                'activity_id' => 48,
                'member_id' => 14,
                'first_name' => $fields[1],
                'last_name' => $fields[2],
                'grade_level' => $fields[3],
                'school' => $fields[4],
                'birth_date' => date('Y-m-d', strtotime($fields[5])),
                'address_1' => $fields[6],
                'zip' => $fields[7],
                'city' => 'Green',
                'state' => 'OH',
                'country' => 'US',
                'phone' => $fields[8],
                'parent1_email' => $fields[9],
                'parent2_email' => $fields[10],
                'parent1_name' => $fields[11],
                'parent1_phone' => $fields[12],
                'parent2_name' => $fields[13],
                'parent2_phone' => $fields[14],
                'member_created' => 'y',
                'registration_amount' => str_replace('$', '', $fields[15]),
                'date_paid' => date('Y-m-d', strtotime($fields[16])),
                'transaction_ref' => $fields[17],
                'amount_paid' => str_replace('$', '', $fields[18]),
                'comments' => trim($fields[21] . ' ' . $fields [22])
                ));
            //$reg->save();
            //print_r($reg); exit;
        }
        echo 'done';exit;
    }


/*
    function test(){
        $dates = ActivityCalendarDate::find_all();
        $ics = "BEGIN:VCALENDAR
METHOD:PUBLISH
VERSION:2.0
PRODID:-//RegistrationHandler.com//NONSGML v1.0//EN
CALSCALE:GREGORIAN
";
        foreach($dates as $d) {
$ics .= "BEGIN:VEVENT
UID:RHCAD-{$d->id}
DTSTAMP:" . date('Ymd\THis\Z', strtotime($d->modified_at)) . "
DTSTART;VALUE=DATE:" . date('Ymd', strtotime($d->calendar_date)) . "
SEQUENCE:{$d->sequence_no}
TRANSP:TRANSPARENT
SUMMARY:{$d->title}
DESCRIPTION:" . str_replace(array("\r\n", "\n"), '\n', $d->notes) . "
END:VEVENT
";
        }
$ics .= "END:VCALENDAR
"; 
//echo date('Ymd', strtotime('2012-01-10 + 1 days'));
    /*
    BEGIN:VALARM
UID:RHCADA-" . $d->id . "
TRIGGER;VALUE=DATE-TIME:" .date('YmdTHisZ', strtotime('0')) . "
ACTION:NONE
END:VALARM

*/
/*
        echo $ics;exit;
        $this->registry['member'] = Member::find(1);
        echo  $this->registry['member']->encrypt_password('bulldogs', $this->registry['member']->salt) .'<br />';
        echo  $this->registry['member']->encrypt_password('bulldog', $this->registry['member']->salt) .'<br />';
        echo  $this->registry['member']->encrypt_password('nick2888', $this->registry['member']->salt) .'<br />';
        echo  $this->registry['member']->password;
    }
*/


}