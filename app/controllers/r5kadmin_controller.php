<?php
/**
 * PACKAGE: brightguy
 * 
 * This controller contains our index functionality
 *
 * @copyright 2010 blackcreek solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class R5kadminController Extends ControllerBase {

    function __construct ($params = array()) {
        $this->no_cache();
        $this->controller = 'r5kadmin';
        $this->authenticate = true;
        $this->layout = 'application_boot';
    }
    

    function do_page() {
        if(substr($this->controller_method, 0, 6) != '_ajax_') {
            require(SITE_ROOT . SITE_VIEWS . 'application/' . $this->layout . '.tpl');
        }
    }  

    /**
     * Controller Methods
     */
    function index ($params) {
         $this->registry['payment_info'] = MemberPaymentSetting::find_by(array('member_id' => $this->registry['member']->id));
        //if(empty($this->registry['member']->name)) $this->registry['member']->name = $this->registry['member']->contact_name;
        $query = "SELECT activities.*, "
               . "(SELECT count(registrations.id) FROM registrations WHERE registrations.activity_id = activities.id AND (amount_paid > 0 OR transaction_ref != '' OR member_created = 'y') AND r5k_payment_hold = 'n') as registered, "
               . "(SELECT sum(registrations.registration_amount + registrations.donation_amount) FROM registrations WHERE registrations.activity_id = activities.id AND (amount_paid > 0 OR transaction_ref != '' OR member_created = 'y') AND r5k_payment_hold = 'n') as amount "
               . "FROM activities "
               . "WHERE activities.member_id = " . $this->registry['member']->id . " "
               . "ORDER BY activities.registration_start_date DESC, activities.id ASC";
        $this->registry['activities'] = Activity::fetch_as_object($query);

        $this->setup_dashboard_index_report();
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.js";
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.time.js";
        $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.tooltip.min.js";
  }

 
    function activity ($params) {
      //$params = 46;
      $this->registry['activity'] = Activity::find_by(array('id' => $params, 'member_id' => $this->registry['member']->id));
      if(empty($this->registry['activity'])) {
        $this->redirect('/report');
        exit;
      }
      $this->registry['payment_info'] = MemberPaymentSetting::find_by(array('member_id' => $this->registry['member']->id));
      $cc_fee = $this->registry['payment_info']->cc_fee / 1000;
      $query = "SELECT sum((registrations.registration_amount + registrations.donation_amount) ) as amt "
               . "FROM registrations "
               . "WHERE r5k_payment_hold = 'n' "
               . "AND activity_id = {$this->registry['activity']->id} ";
      $res = ActiveRecordBase::query($query);
      $this->registry['amount'] = $res[0]['amt'];

      $query = "SELECT sum(registrations.donation_amount) as amt, count(id) as registered "
               . "FROM registrations "
               . "WHERE r5k_payment_hold = 'n' "
               . "AND activity_id = {$this->registry['activity']->id} ";
      $res = ActiveRecordBase::query($query);
      $this->registry['donations'] = $res[0]['amt'];

      $res = ActiveRecordBase::query("SELECT count(id) as complete FROM registrations WHERE member_id = {$this->registry['member']->id} AND activity_id = {$this->registry['activity']->id} AND (amount_paid > 0 OR transaction_ref != '' OR member_created = 'y')");
      $this->registry['completed_registrations'] = $res[0]['complete'];
      $res = ActiveRecordBase::query("SELECT count(id) as bailed FROM registrations WHERE member_id = {$this->registry['member']->id} AND activity_id = {$this->registry['activity']->id} AND (transaction_ref = '' AND member_created != 'y')");
      $this->registry['bailed_registrations'] = $res[0]['bailed'];

      $registrations = Registration::find_all_by(array('activity_id' => $this->registry['activity']->id));
      $this->setup_dashboard_reports($this->registry['activity']->id);


      $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.js";
      $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.tooltip.min.js";
      $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.time.js";
      $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.pie.js";
      $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.resize.js";
      $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.stack.js";
      $this->registry['js_files'][] = "/js/plugins/flot/jquery.flot.orderBars.js";
   }

    function payment ($params) {
      $this->registry['history'] = MemberPayment::find_all_by(array('member_id' => $this->registry['member']->id, array('ORDER BY' => 'date_paid DESC')));


      $this->registry['payment_info'] = MemberPaymentSetting::find_by(array('member_id' => $this->registry['member']->id));
      $cc_fee = $this->registry['payment_info']->cc_fee / 1000;
      $res = ActiveRecordBase::query("SELECT count(id) as count FROM registrations WHERE member_id = " . $this->registry['member']->id . " AND registrations.r5k_payment_hold = 'n' AND registrations.r5k_pay_reference = '' AND member_created = 'n' ");
      $this->registry['registration_count'] = $res[0]['count'];
      
      $query = "SELECT sum((registrations.registration_amount + registrations.donation_amount) * {$cc_fee}) as amt "
               . "FROM registrations "
               . "WHERE r5k_pay_reference = '' "
               . "AND r5k_payment_hold = 'n' "
               . "AND member_created = 'n' "
               . "AND member_id = {$this->registry['member']->id} ";
      $res = ActiveRecordBase::query($query);
      $this->registry['amount_pending'] = $res[0]['amt'];

 
   }

    private function setup_dashboard_reports($activity_id = 0) {
        //$start_date = date('Y-m-d', strtotime('00:00:00 - 2 weeks UTC'));
        //$end_date = date('Y-m-d', strtotime('00:00:00 tomorrow UTC'));
        if($activity_id > 0) $activity_query = "AND registrations.activity_id = {$activity_id} ";
        // graph values
        $query = "SELECT registrations.date_paid, registrations.created_at, registrations.amount_paid, registrations.registration_amount, registrations.donation_amount, registrations.r5k_fee, registrations.division, registrations.activity_id, activities.name, activities.accept_donation "
               . "FROM registrations "
               . "JOIN activities on activities.id = registrations.activity_id "
               . "WHERE registrations.member_id = " . $this->registry['member']->id . " "
               . $activity_query
               . "AND registrations.r5k_payment_hold = 'n' "
               . "ORDER BY registrations.date_paid DESC, activities.name, registrations.division";
        $activities2 = ActiveRecordBase::query($query);
        
        // set up associate array to access values
        $start_date = strtotime('now');
        foreach($activities2 as $reg) {
            if($reg['date_paid'] == '0000-00-00' || empty($reg['date_paid'])) 
                $reg['date_paid'] = $reg['created_at'];
            $name = ($reg['name'] == $reg['division']) ? $reg['name'] : $reg['name'] . ': ' . $reg['division'];
            $this->registry['summary'][$reg['date_paid']]['registrations'] ++;
            $this->registry['summary'][$reg['date_paid']]['detail']['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])]['name'] = $name;
            $this->registry['summary'][$reg['date_paid']]['detail']['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])]['registrations'] ++;
            $divisions['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])] = $reg['division'];
            $this->registry['totals']['aid'.$reg['activity_id']]['name'] = $reg['name'];
            $this->registry['totals']['aid'.$reg['activity_id']]['registrations'] ++;
            $this->registry['totals']['aid'.$reg['activity_id']]['registration_amount'] += $reg['registration_amount'];
            $this->registry['totals']['aid'.$reg['activity_id']]['donation_amount'] += $reg['donation_amount'];
            $this->registry['totals']['aid'.$reg['activity_id']]['division'][$reg['division']]['registrations'] ++;
            $this->registry['totals']['aid'.$reg['activity_id']]['division'][$reg['division']]['registration_amount'] += $reg['registration_amount'];
            $this->registry['totals']['aid'.$reg['activity_id']]['division'][$reg['division']]['donation_amount'] += $reg['donation_amount'];
            if($reg['donation_amount'] > 0) 
              $this->registry['totals']['aid'.$reg['activity_id']]['division'][$reg['division']]['donations'] ++;
            $this->registry['total_registrations'] ++;
            $this->registry['total_registration_amount'] += $reg['registration_amount'];
            $this->registry['total_donation_amount'] += $reg['donation_amount'];

            $start_date = (strtotime($reg['date_paid']) < $start_date) ? strtotime($reg['date_paid']): $start_date;
            $end_date = (strtotime($reg['date_paid']) > $end_date) ? strtotime($reg['date_paid']): $end_date;
        }
        $start_date = date('Y-m-d', $start_date);
        $end_date = date('Y-m-d', $end_date);
        // summary of categories  
        $res = ActiveRecordBase::query("SELECT count(division) as total, division FROM registrations WHERE activity_id = {$activity_id} GROUP BY division");
        foreach($res as $r) {
          $donut_chart[] = array('label' => $r['division'], 'data' => $r['total']);
          //$divisions[] = $r['division'];
        }
        $output['donut']  = $donut_chart;
        //$this->registry['divisions'] = $divisions;
        
        $date = $start_date;

        $keycounter = 0;
        while($date <= $end_date) {

            foreach($divisions as $aid => $values) {
              //  $datapoints[$keycounter]['values'][$i][] = strtotime($date) * 1000;
               // $datapoints[$keycounter]['values'][$i][] += $lv[$i];
               // $datapoints[$keycounter]['label']  = $line_labels[$key];
               foreach($values as $key => $division) {
                    $line_values[$aid . $key][$date] = (empty($this->registry['summary'][$date]['detail'][$aid][$key]['registrations'])) ? 0 : $this->registry['summary'][$date]['detail'][$aid][$key]['registrations'];
                    if(empty($line_labels[$aid . $key]))
                        $line_labels[$aid . $key] = $this->registry['summary'][$date]['detail'][$aid][$key]['name'];
                }
            }
            
            $date = date('Y-m-d', strtotime($date . ' 00:00:00 + 1 day UTC'));
            $keycounter ++;
        }

        //$this->registry['line_values'] = $line_values;
        //$this->registry['line_labels'] = $line_labels;
        $keycounter = 0;
        foreach($line_values as $key => $lv) {
            $total = 0;
            foreach($lv as $date => $value) {
                //$datapoints[$keycounter]['values'][] = "[(new Date(+new Date().setDate(new Date().getDate() - 14 - {$i}))).getTime(), {$lv[$i]}]";
                //$v = 14 - $i;
                //$datapoints[$keycounter]['values'][] = "[" . strtotime(" -{$v} days UTC") * 1000 . " , {$lv[$i]}]";
              $total += $value;
              $datapoints[$keycounter]['values'][] = array(strtotime($date) * 1000, $total);
              $datapoints[$keycounter]['label']  = $line_labels[$key];
            }
            $keycounter ++;
        }

        $ouput = array();
        foreach ($datapoints as $key => $dp) {
            $output['line'][$key]['label'] = htmlspecialchars($dp['label'], ENT_QUOTES);
            $output['line'][$key]['data'] = $dp['values'];
        }

        $output['start_date'] = $start_date;
        $output['end_date'] = $end_date;
        
        // horizontal graph
        $ar = array();
        $i = 1;
        foreach($this->registry['totals'] as $activity) {
          foreach($activity['division'] as $key => $value) {
            $ar[$i]['registration_amount'] = $value['registration_amount'];
            $ar[$i]['donation_amount'] = $value['donation_amount'];
            $ar['data']['label'][$i] = "{$key}";
            $ar['data']['donation'][] = array($value['donation_amount'], $i);
            $ar['data']['registration'][] = array($value['registration_amount'], $i);
            $i ++;
          }
        }
        $output['horizontal']['data'] = array(array('label' => "Registration Amount", 'data' => $ar['data']['registration']));
        if($this->registry['activity']->accept_donation == 'y') $output['horizontal']['data'][] = array('label' => "Donation Amount", 'data' => $ar['data']['donation']);
        $i = 1;
        foreach($ar['data']['label'] as $v) {
          $output['horizontal']['ticks'][] = array($i, $v);
          $i++;
        }

        $output['activity_id'] = $this->registry['activity']->id;
        $output['member_id'] = $this->registry['member']->id;
        $output['form_definition'] = json_decode($this->registry['activity']->form_definition_json);
        // send json data to registry data object
        $this->registry['js_data'] = json_encode($output);


    }

    private function setup_dashboard_index_report($activity_id = 0) {
        $start_date = date('Y-m-d', strtotime('00:00:00 - 2 weeks UTC'));
        $end_date = date('Y-m-d', strtotime('00:00:00 tomorrow UTC'));
        $type = 'all';
        if($type == 'web') $type_query = "AND member_created = 'n' AND amount_paid > 0 AND registrations.date_paid >= '{$start_date}' AND registrations.date_paid <= '{$end_date}' ";
        else $type_query = "AND ((registrations.date_paid >= '{$start_date}' AND registrations.date_paid <= '{$end_date}') OR (registrations.created_at >= '{$start_date}' AND registrations.created_at <= '{$end_date}')) ";
        if($activity_id > 0) $activity_query = "AND regisrtations.activity_id = {$activity_id} ";
        // graph values
        $query = "SELECT registrations.date_paid, registrations.created_at, registrations.amount_paid, registrations.registration_amount, registrations.donation_amount, registrations.r5k_fee, registrations.division, registrations.activity_id, activities.name, activities.accept_donation "
               . "FROM registrations "
               . "JOIN activities on activities.id = registrations.activity_id "
               . "WHERE registrations.member_id = " . $this->registry['member']->id . " "
               . $type_query
               . $activity_query
               . "AND registrations.r5k_payment_hold = 'n' "
               . "AND activities.archived = 'n' "
               . "ORDER BY registrations.date_paid DESC, activities.name, registrations.division";
        $activities2 = ActiveRecordBase::query($query);
        
        // set up associate array to access values
        foreach($activities2 as $reg) {
            if($reg['date_paid'] == '0000-00-00' || empty($reg['date_paid'])) 
                $reg['date_paid'] = $reg['created_at'];
            $name = ($reg['name'] == $reg['division']) ? $reg['name'] : $reg['name'] . ': ' . $reg['division'];
            $this->registry['summary'][$reg['date_paid']]['registrations'] ++;
            $this->registry['summary'][$reg['date_paid']]['detail']['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])]['name'] = $name;
            $this->registry['summary'][$reg['date_paid']]['detail']['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])]['registrations'] ++;
            $divisions['aid'.$reg['activity_id']][str_replace(' ', '_', $reg['division'])] = $reg['division'];

        }

        //$this->registry['divisions'] = $divisions;
        $date = $start_date;

        while($date <= $end_date) {
            foreach($divisions as $aid => $values) {
                foreach($values as $key => $division) {
                    $line_values[$aid . $key][] = (empty($this->registry['summary'][$date]['detail'][$aid][$key]['registrations'])) ? 0 : $this->registry['summary'][$date]['detail'][$aid][$key]['registrations'];
                    if(empty($line_labels[$aid . $key]))
                        $line_labels[$aid . $key] = $this->registry['summary'][$date]['detail'][$aid][$key]['name'];
                }
            }
            
            $date = date('Y-m-d', strtotime($date . ' 00:00:00 + 1 day UTC'));
        }

        $keycounter = 0;
        foreach($line_values as $key => $lv) {
            for($i = 0; $i <= 14; $i ++) {
                //$datapoints[$keycounter]['values'][] = "[(new Date(+new Date().setDate(new Date().getDate() - 14 - {$i}))).getTime(), {$lv[$i]}]";
                $v = 14 - $i;
                //$datapoints[$keycounter]['values'][] = "[" . strtotime(" -{$v} days UTC") * 1000 . " , {$lv[$i]}]";
                $datapoints[$keycounter]['values'][$i][] = strtotime("00:00:00 -{$v} days UTC") * 1000;
                $datapoints[$keycounter]['values'][$i][] = $lv[$i];
                $datapoints[$keycounter]['label']  = $line_labels[$key];
            }
            $keycounter ++;
        }

        $ouput = array();
        foreach ($datapoints as $key => $dp) {
            $output[$key]['label'] = htmlspecialchars($dp['label'], ENT_QUOTES);
            $output[$key]['data'] = $dp['values'];
        }



        // send json data to registry data object
        $this->registry['js_data'] = json_encode($output);


    }

    function _ajax_demo_breakdown() {
      if(!empty($_POST)) {

        $json_search = false;
        switch($_POST['source']) {
          case 'area':
            $query_start = "SELECT CONCAT(city, ', ', state) as value, COUNT(CONCAT(city, ', ', state)) as total ";
            $query_end = "GROUP BY value ORDER BY total DESC";
            break;

          case 'age':
            $query_start = "SELECT CONCAT(city, ', ', state) as value, COUNT(CONCAT(city, ', ', state)) as total ";
            $query_end = "GROUP BY value ORDER BY total DESC";
            break;

          case 'custom':
            $query_start = "SELECT custom_fields ";
            $query_end = '';
            $json_search = true;
            break;

          default:
            $query_start = "SELECT {$_POST['source']} as value, COUNT({$_POST['source']}) as total ";
            $query_end = "GROUP BY value ORDER BY total DESC";
           

        }

        $query = $query_start  .
                 "FROM registrations WHERE activity_id = {$_POST['activity_id']} " .
                 "AND member_id = {$_POST['member_id']} " .
                 "AND r5k_payment_hold = 'n' " .
                 "AND (date_paid > '2000-01-01' OR amount_paid > 0) " .
                 $query_end;
        $res = ActiveRecordBase::query($query);
        $total = ActiveRecordBase::query("SELECT COUNT(id) as total FROM registrations " .
                 "WHERE activity_id = {$_POST['activity_id']} " .
                 "AND member_id = {$_POST['member_id']} " .
                 "AND r5k_payment_hold = 'n' " .
                 "AND (date_paid > '2000-01-01' OR amount_paid > 0) ");
        $total = $total[0]['total'];
        
        if($json_search) {
          // search custom form fields
          $data = array();
          $build = array();
          foreach($res as $r) {
            $section = json_decode($r['custom_fields']);
            foreach($section as $field){
              // multiple selectison possible
              if(is_array($field->$_POST['field'])){
                foreach($field->$_POST['field'] as $v){
                 if(empty($build[$v])) $build[$v] = 1;
                  else $build[$v] ++;
                }
              } else {
                // single valeus
                if(empty($build[$field->$_POST['field']])) $build[$field->$_POST['field']] = 1;
                else $build[$field->$_POST['field']] ++;
              }
            }
          }
          arsort($build);
          foreach($build as $k => $v) $data[] = array('value' => (empty($k))?'Not Available': $k, 'total' => $v, 'percentage' => round($v / $total, 3, PHP_ROUND_HALF_UP) * 100);
        } else {
          // regular database fields
          foreach($res as $r) {
            if($r['value'] == '') $r['value'] = 'Not Available';
            $r['percentage'] = round($r['total'] / $total, 3, PHP_ROUND_HALF_UP) * 100;
            $data[] = $r;
          }
        }
        //print_r($res);
        $result = array('status' => 'ok', 'heading' => $_POST['heading'], 'data' => $data);
        header('Content-Type: application/json');
        echo json_encode($result);
        
      }
    }


}