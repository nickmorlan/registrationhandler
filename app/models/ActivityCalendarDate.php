<?php
/**
 * PACKAGE: r5k Builder
 * 
 * Cart Model
 *
 * @copyright 2009 Hardman Design
 * @author Nick Morlan<nick@hddg.net>
 */
class ActivityCalendarDate extends ActiveRecordBase {


	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_create() {
		$this->requires(array('activity_id', 'title', 'calendar_date'));
	}

	function before_save() {
	}

	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array('activity_id', 'calendar_date', 'title', 'notes', 'sequence_no');
	 	                       
		parent::__construct('activity_calendar_date');
	}
	
  	static function find ($id) {
	    return parent::find('activity_calendar_date', $id);
	}

	static function find_all () {
		return parent::find_all('activity_calendar_date');
	}

	static function find_all_by ($array, $mod = array()) {
		return parent::find_all_by('activity_calendar_date', $array, $mod);
	}

	static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
		return parent::find_all_with_pagination('activity_calendar_date', $start, $limit);
	}

	static function find_by_query_with_pagination($sql, $start, $limit) {
		return parent::find_by_query_with_pagination('activity_calendar_date', $sql, $start, $limit);
	}

	static function destroy ($id) {
		return parent::destroy('activity_calendar_date', $id);
	}

	static function create ($params) {
		return parent::create('activity_calendar_date',$params);
	}
	
	/**
	 * Given an activity id this method will generate an .ics file
	 * that can be read by calendar applications
	 *
	 * @params int  $activity_id    the id of the acticity to build the calendar for
	 * @return text                 text of the ics file
	 **/
	static function generate_ics_for($activity_id){
		$dates = self::find_all_by(array('activity_id' => $activity_id));
		$ics = '';
		if(!empty($dates)) {
			$ics = "BEGIN:VCALENDAR
METHOD:PUBLISH
VERSION:2.0
PRODID:-//RegistrationHandler.com//NONSGML v1.0//EN
CALSCALE:GREGORIAN
";
			foreach($dates as $d) {
$ics .= "BEGIN:VEVENT
UID:RHCAD-{$d->id}
DTSTAMP:" . date('Ymd\THis\Z', strtotime($d->modified_at)) . "
DTSTART;VALUE=DATE:" . date('Ymd', strtotime($d->calendar_date)) . "
SEQUENCE:{$d->sequence_no}
TRANSP:TRANSPARENT
SUMMARY:{$d->title}
DESCRIPTION:" . str_replace(array("\r\n", "\n"), '\n', $d->notes) . "
END:VEVENT
";
			}
$ics .= "END:VCALENDAR
"; 
		}
		return $ics;
	}

	/**
	 * Given an activity id this method will generate html output 
	 *
	 * @params int  $activity_id    the id of the acticity to build the calendar for
	 * @return text                 html of the events
	 **/
	static function generate_ics_html_for($activity_id){
		$activity = Activity::find($activity_id);
		$dates = self::find_all_by(array('activity_id' => $activity_id));
		$html = '';
		if(!empty($dates)) {
			$html = "<b>Important Dates for {$activity->name}</b><br />";
			foreach($dates as $d) {
				$html .= "<b>" . date('D F j Y', strtotime($d->calendar_date)) . "</b><br/>{$d->title}";
				if(!empty($d->notes)) $html .= '<br />' . nl2br($d->notes);
				$html.= "<br />";			}
		}
		return $html;
	}

}