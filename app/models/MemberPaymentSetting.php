<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class MemberPaymentSetting extends ActiveRecordBase {


	private static $instance;

	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_save () {
		switch ($this->pay_via) {
			case 'check':
				$this->requires(array('name', 'address_1', 'city', 'state', 'zip' ));
				break;
			case 'paypal':
				//$this->requires(array('paypal_email'));
				$this->validates_as_email(array('paypal_email'));
				break;
			case 'ach':
				//$this->requires(array('bank_name', 'account_number', 'routing_number'));
				break;
			case 'stripe':
				//$this->requires(array('stripe_access_token'));
				break;
			default;
				$this->requires(array('pay_via'));
		}
	}

/*	function before_create () {
		$this->validates_as_email(array('email'));
		$this->requires_unique_key(array('email'));
		$this->validate_password_confirmation('password', 'confirm_password');
		// save the encrypted pw and the random salt
		if (empty($this->errors)) {
			$this->salt = md5(uniqid(mt_rand(), true));
			$this->password = self::encrypt_password($this->password, $this->salt);
		}
	}
	*/
	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array('member_id', 'activity_id', 'pay_via', 'name', 'tax_id', 'address_1', 'address_2', 'city', 'state',
		                       'zip',  'country', 'paypal_email', 'bank_name', 'account_number', 'bank_account_last4', 'cc_fee',
		                       'routing_number', 'stripe_access_token', 'stripe_recipient', 'stripe_account', 'stripe_full_return', 'stripe_status', 'created_at');              
		parent::__construct('member_payment_setting');
	}

	static function find ($id) {
		return parent::find('member_payment_setting', $id);
	}

	static function find_all ($mod = array()) {
		return parent::find_all('member_payment_setting', $mod);
	}

	static function find_all_by ($array) {
		return parent::find_all_by('member_payment_setting', $array);
	}

	static function find_by ($array) {
		return parent::find_by('member_payment_setting', $array);
	}


	static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
		return parent::find_with_pagination('member_payment_setting', $start, $limit, $order_by, $asdc, $query);
	}


	static function destroy ($id) {
		return parent::destroy('member_payment_setting', $id);
	}

	static function create ($params) {
		return parent::create('member_payment_setting',$params);
	}

	
	private function calculate_stripe_fee($amount) {
		return number_format(($amount * .029) + .3, 2);
	}
	
	function simple_encrypt($salt, $text)
    {
        return trim(base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $salt, $text, MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND))));
    }

    function simple_decrypt($salt, $text)
    {
        return trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $salt, base64_decode($text), MCRYPT_MODE_ECB, mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_ECB), MCRYPT_RAND)));
    }

}
