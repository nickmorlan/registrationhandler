<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class MemberPayment extends ActiveRecordBase {



    /**
     * The following 6 functions are required for all ActiveRecord Objects
     * php 5.3 will be fixing some big issues
     */ 
    function __construct () {
        // setting the columns here saves database calls
        // good to do when structure is set
        $this->columns = array('member_id', 'payment_ref', 'description', 'amount', 'date_paid');
                               
        parent::__construct('member_payment');
    }

    static function find ($id) {
        return parent::find('member_payment', $id);
    }

    static function find_all () {
        return parent::find_all('member_payment');
    }

    static function find_all_by ($array) {
        return parent::find_all_by('member_payment', $array);
    }

    static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
        return parent::find_all_with_pagination('member_payment', $start, $limit, $order_by, $asdc);
    }

    static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
        return parent::find_with_pagination('member_payment', $start, $limit, $order_by, $asdc, $query);
    }

    static function destroy ($id) {
        return parent::destroy('member_payment', $id);
    }

    static function create ($params) {
        $params['browser_info'] = $_SERVER['HTTP_USER_AGENT'];
        $params['ip_address']   = Application::get_real_ip();
        return parent::create('member_payment', $params);
    }
    
    
    private function notify() {
        $to      = SITE_LOG_NOTIFY_EMAIL_ADDRESS;
        $subject = 'RH Activity Log: ' . $this->type;
        $headers = 'From: <Turd Burd>info@registrationhandler.com' . "\r\n" .
               'Reply-To: info@registrationhandler.com' . "\r\n" .
               'Content-type: text/html' . "\r\n" .
               'X-Mailer: PHP/' . phpversion();

        
        $message = '<html><head><style>pre{font-size:12px;}</style><body>';
        $message .= "<p>" . $this->message . "</p>";
        $message .= "<p>Nick!!!</p>";
        $message .= "<p>----------------------------------------------------------</p>";
        $message .= "<pre>" . print_r($this, true) . "</pre>";

        $message .= '</body></html>';
        mail($to, $subject, $message, $headers);
    
    }
}