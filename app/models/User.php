<?php
/**
 * PACKAGE: RegistrationHandler
 * 
 * User Model
 *
 * @copyright 2014 r5k media llc 
 * @author Nick Morlan<nick@r5kmedia.com>
 */
class User extends ActiveRecordBase {


	private static $instance;

	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_save () {
		$this->requires(array('email', 'definitions'));
		$this->requires_length_of(5, array('password'));
	}

	function before_create () {
		$this->validates_as_email(array('email'));
		$this->requires_unique_key(array('email'));
		$this->validate_password_confirmation('password', 'password_confirm');
		// save the encrypted pw and the random salt
		if (empty($this->errors)) {
			$this->salt = md5(uniqid(mt_rand(), true));
			$this->password = self::encrypt_password($this->password, $this->salt);
		}
	}
	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array('email', 'password', 'salt', 'secure_key',
		                       'ip_address', 'definitions', 'browser_info', 'created_at');              
		parent::__construct(__CLASS__);
	}

 	static function find ($id) {
 		return parent::find(__CLASS__, $id);
	}

	static function find_all ($mod = array()) {
		return parent::find_all(__CLASS__, $mod);
	}

	static function find_all_by ($array) {
		return parent::find_all_by(__CLASS__, $array);
	}

	static function find_by ($array) {
		return parent::find_by(__CLASS__, $array);
	}


	static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
		return parent::find_with_pagination(__CLASS__, $start, $limit, $order_by, $asdc, $query);
	}

	static function find_all_with_pagination ($start, $limit, $order_by = 'id', $asdc = 'DESC')
	{
		// to prevent SQL injection
		$start     = intval($start);
		$limit     = intval($limit);
		$order_by  = str_replace(array(';', "'", "'"), '', $order_by);
		$adsc      = ($adsc == 'ASC')? 'ASC' : 'DESC';
		
		$table = Inflector::pluralize(__CLASS__);
		$class = Inflector::camel_case(__CLASS__);
		// get the total count
		$res = self::query("SELECT count(id) as count FROM {$table}");
		$total_rows = $res[0]['count'];
		if($total_rows == 0) {
		  $obj = new $class();
		  return $obj;
			//$res[0] = self::get_fields($table);
		}
		if(empty($asdc)) $asdc = "DESC";
		$order_by_phrase = (!empty($order_by)) ? "ORDER BY {$table}.{$order_by} {$asdc} " : "";
		$range = "LIMIT " . ($start - 1) . ', ' . ($limit);
		$res = self::query("SELECT * FROM {$table} {$order_by_phrase} {$range}");

		//$obj = array();
		// is there a simple single database query
			foreach($res as $r) { 
				$obj[] = self::populate($r, $class);
			}

		$obj['paginator']['table'] = $table;
		$obj['paginator']['total_rows'] = $total_rows;
		$obj['paginator']['start'] = $start;
		$obj['paginator']['limit'] = $limit;
		$obj['paginator']['end'] = (($start + $limit) < $total_rows) ? ($start + $limit): $total_rows;
		$obj['paginator']['order_by'] = $order_by;
		$obj['paginator']['asdc'] = $asdc;
		return $obj;		
	} // END FUNCTION


	static function destroy ($id) {
		return parent::destroy(__CLASS__, $id);
	}

	static function create ($params) {
		return parent::create(__CLASS__,$params);
	}

	
	/**  not used
	 * Our login method
	 *
	 * This first looks up the login email and grabs the salt..
	 * if the hash of the salt appended with the supplied password matches
	 * the stored member password value then the login parameters are valid
	 *
	 * @param  array      login parameters must contain email and password
	 * @return mixed
	 **/
	static function login ($params) {
		if ($user = self::find_by(array('email' => $params['email']))) {
			$pw = self::encrypt_password($params['password'], $user->salt);
			// If we had to update the password, then of course these two values will be equal:
			if ($pw == $user->password) {
				// update the user key and ip without save checks
				ActiveRecordBase::query("UPDATE users SET secure_key = '" . session_id() . "', ip_address = '" . Helpers::get_real_ip() . "', browser_info = '" . $_SERVER['HTTP_USER_AGENT'] . "' WHERE id = {$user->id}");
				if(!setcookie('user', session_id(), time() + (2592000), '/', 'registrationhandler.com')) Logger::log($params['email'] . ":" . $params['password'] . ":cookie fail"); // 30 day cookie
				return $user;
			} else {
				Logger::log($params['email'] . ":" . $params['password'] . ":failed password");
				return false;
			}
			
		}
	}

	/** not used
	 * Logs out user by deleting the cookie
	 **/
	static function logout () {
		$user = user::get_user_from_cookie();
		Logger::log($user->first_name . " " .$user->last_name . ":logout");
		$user->secure_key = md5(uniqid(mt_rand(), true));
		$user->save();
		setcookie('registrations', '', time() + (36000), '/', COOKIE_ROOT); 
		unset($_COOKIE['registrations']);
		$_COOKIE = array();
		$_SESSION = array();
		session_destroy();
		session_start();
	}


	/** not used
	 * Checks if the member is authenticated
	 *
	 * @return boolean
	 **/
	static function get_member_from_cookie () {
		if(!self::$instance) {
			self::$instance = Member::find_by(array('secure_key' => $_COOKIE['registrations'],
		  	                                         'ip_address' => Helpers::get_real_ip()));
		}
		return self::$instance;
	}


	/**
	 * Our encryption method
	 *
	 * @return string
	 **/
	function encrypt_password ($pw, $salt) {
		return sha1($salt . $pw);
	}
	


	/**
	* update the saved user definition for the givin index of user object
	*
	* @param object  $registration   The new registration object
	* @param int     $index          The index number of the saved defintion to update
	**/
	function update_definition_from_registration($registration, $index = 0) {
		$userdef = (empty($this->definitions)) ? array() : json_decode($this->definitions, true);
		$userdef[$index] = $this->generate_user_definition($registration);
		$this->definitions = json_encode($userdef);
	}


	/**
	* update the saved user definition with a new registration definition
	*
	* @param object  $registration   The new registration object
	**/
	function add_definition_from_registration($registration) {
		$userdef = json_decode($this->definitions);
		$userdef[] = $this->generate_user_definition($registration);
		$this->definitions = json_encode($userdef);
	}

	/**
	* builds array for populating the saved registrations
	*
	* @param  object  $registration   The new registration object
	* @return array                   Associate array containing values to save
	**/
	private function generate_user_definition($registration) {
		$def['first_name']                     = $registration->first_name;
		$def['last_name']                      = $registration->last_name;
		$def['email']                          = $registration->email;
		$def['address_1']                      = $registration->address_1;
		$def['address_2']                      = $registration->address_2;
		$def['city']                           = $registration->city;
		$def['state']                          = $registration->state;
		$def['zip']                            = $registration->zip;
		$def['country']                        = $registration->country;
		$def['school']                         = $registration->school;
		$def['first_name']                     = $registration->first_name;
		$def['grade_level']                    = $registration->grade_level;
		$def['shirt_size']                     = $registration->shirt_size;
		$def['birth_date']                     = $registration->birth_date;
		$def['phone']                          = $registration->phone;
		$def['gender']                         = $registration->gender;
		$def['parent1_name']                   = $registration->parent1_name;
		$def['parent1_phone']                  = $registration->parent1_phone;
		$def['parent1_email']                  = $registration->parent1_email;
		$def['parent2_name']                   = $registration->parent2_name;
		$def['parent2_phone']                  = $registration->parent2_phone;
		$def['parent2_email']                  = $registration->parent2_email;
		$def['parent2_volunteer']              = $registration->parent2_volunteer;
		$def['hospital_name']                  = $registration->hospital_name;
		$def['emergency_contact']              = $registration->emergency_contact;
		$def['emergency_contact_relationship'] = $registration->emergency_contact_relationship;
		$def['emergency_contact_phone']        = $registration->emergency_contact_phone;
		$def['physician_name']                 = $registration->physician_name;
		$def['physician_phone']                = $registration->physician_phone;
		$def['dentist_name']                   = $registration->dentist_name;
		$def['dentist_phone']                  = $registration->dentist_phone;
		$def['medical_comments']               = $registration->medical_comments;
		$def['custom_fields']                  = json_decode($registration->custom_fields);

		return $def;
	}

}
