<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class Control extends ActiveRecordBase {

	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_save() {
		$this->requires(array('name'));
	}

	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array( 'name', 'text_value', 'int_value', 'date_value', 'string_value', 'created_at');
		parent::__construct(__CLASS__);
	}

	static function find ($id) {
		return parent::find(__CLASS__, $id);
	}

	static function find_all ($mod = array()) {
		return parent::find_all(__CLASS__, $mod);
	}

	static function find_all_by ($array, $mod = array()) {
		return parent::find_all_by(__CLASS__, $array, $mod);
	}

	static function find_by ($array) {
		return parent::find_by(__CLASS__, $array);
	}

	static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
		return parent::find_all_with_pagination(__CLASS__, $start, $limit, $order_by, $asdc);
	}

	static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
		return parent::find_with_pagination(__CLASS__, $start, $limit, $order_by, $asdc, $query);
	}

	static function fetch_as_object ($sql) {
		return parent::fetch_as_object($sql, __CLASS__);
	}

	static function destroy ($id) {
		return parent::destroy(__CLASS__, $id);
	}

	static function create ($params) {
		return parent::create(__CLASS__,$params);
	}
}