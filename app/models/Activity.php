<?php
/**
 * PACKAGE: RegistrationHandler 
 * 
 * Activity Model
 *
 * @copyright 2013 r5k Media llc 
 * @author Nick Morlan<nick@r5kmedia.com>
 *
 *          'type'       => ['text', 'hidden', 'textarea', 'checkbox', 'select']
 *          'name'       => [name of db field and/or input]
 *          'label'      => [label text for form display]
 *          'sort_order' => [sort order of form element]
 *          'default'    => [default value of input]
 *          'validation' => [array('required' => 'true', 'date' => 'true', 'phoneUS' => 'true', 'zipcode' => 'true', 'min' => '123', 'max' => '1234')]
 *          'value_list' => [array('option' => 'value')],
 *          'special'    => ['address1', 'address2', 'date', 'us_states_dropdown', 'country_dropdown', 'grade_level_dropdown']
 *          'custom'     => [true]
 *          'removeable' => [true]
 */
 
class Activity extends ActiveRecordBase {

    /**
     * Use the before save filter to validate data.
     */ 
    function before_save() {
        $this->requires(array('name', 'member_id', 'description', 'registration_start_date', 'registration_end_date'));
        //$this->validates_as_date(array('registration_start_date', 'registration_end_date'));
    }

    /**
     * The following 6 functions are required for all ActiveRecord Objects
     * php 5.3 will be fixing some big issues
     */ 
    function __construct () {
        // setting the columns here saves database calls
        // good to do when structure is set
        $this->columns = array( 'member_id', 'name', 'cost', 'cost_definition', 'consent', 'activity_type',
                                'description', 'location', 'confirmation_text', 'website', 'registration_start_date', 'activity_date', 'activity_time',
                                'registration_end_date', 'form_definition', 'form_definition_json', 'active', 'archived', 'map_link',
                                'accept_donation', 'multiple_registration_discount', 'address1', 'address2', 'city', 'state', 'zip', 'country');
        parent::__construct(__CLASS__);
    }

    static function find ($id) {
        return parent::find(__CLASS__, $id);
    }

    static function find_all ($mod = array()) {
        return parent::find_all(__CLASS__, $mod);
    }

    static function find_all_by ($array, $mod = array()) {
        return parent::find_all_by(__CLASS__, $array, $mod);
    }

    static function find_by ($array) {
        return parent::find_by(__CLASS__, $array);
    }

    static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
        return parent::find_all_with_pagination(__CLASS__, $start, $limit, $order_by, $asdc);
    }

    static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
        return parent::find_with_pagination(__CLASS__, $start, $limit, $order_by, $asdc, $query);
    }

    static function fetch_as_object ($sql) {
        return parent::fetch_as_object($sql, __CLASS__);
    }

    static function destroy ($id) {
        return parent::destroy(__CLASS__, $id);
    }

    static function create ($params) {
        $params['active'] = 'y';
        return parent::create(__CLASS__,$params);
    }
    
    
    
    function display_tumbnail_image($for = 'site') {
        if(file_exists(SITE_ROOT . 'images/activities/' . $this->id . '-35.png')) {
            $pre = ($for == 'email') ? 'http://' . SITE_URL : '';
            return '<img src="' . $pre . '/images/activities/' . $this->id . '-35.png" style="margin-top:-5px;" />';
        }
    }
    
    function url_link(){
        return $this->id . '/' . urlencode(str_replace(' ', '-', $this->name));
    }
    
    function full_url_link(){
        return 'https://www.registrationhandler.com/register/' . $this->id . '/' . urlencode(str_replace(' ', '-', $this->name));
    }
    
    function decode_url_safe($param){
        return urldecode(str_replace('-', ' ', $param));
    }
    
    // build the needed fields for the xcel export
    function get_export_field_array_from_json() {
        $def = json_decode($this->form_definition_json);
        $fields = array();
        foreach($def as $key => $section) {
            foreach($section->fields as $element) {//var_dump($element);exit;
                $fields[] = ($element->custom === 'true') ? $key . '_' . $element->name : $element->name;
            }
        }
        $fields[] = 'notes';
        $fields[] = 'date_paid';
        $fields[] = 'amount_paid';
        $fields[] = 'transaction_ref';
        $fields[] = 'created_at';
        return $fields;
    }
        
    // may remove...
    function display_pricing_options() {
        $costs = json_decode($this->cost_definition);
        $num = count(get_object_vars($costs->registration->division));
        if($num > 1) {
            for($i = 1; $i <= $num; $i ++){
                if(!empty($_SESSION['last_registration'])) {
                    $test = Registration::find($_SESSION['last_registration']);
                    if($this->member_id == $test->member_id) {
                        if($this->multiple_registration_discount > 0) {
                            $html .= "<p><input type=\"radio\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" /> <label for=\"division{$i}\" style=\"font-weight:normal;\"><span style=\"text-decoration:line-through;font-weight:100;color:#ccc;font-size:.9em;\">$" . number_format($costs->registration->cost->$i, 2) . '</span> <span style="margin-left:5px;font-weight:bold;">$' . number_format($costs->registration->cost->$i - $this->multiple_registration_discount, 2) . '</span>  ' . $costs->registration->division->$i . '</label></p>';                         
                        } else {
                            $html .= "<p><input type=\"radio\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" /> <label for=\"division{$i}\" style=\"font-weight:normal;\"> " . '<span style="margin-left:5px;font-weight:bold;">$' . number_format($costs->registration->cost->$i - $this->multiple_registration_discount, 2) . '</span>  ' . $costs->registration->division->$i . '</label></p>';
                        }
                    } else {
                            $html .= "<p><input type=\"radio\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" /> <label for=\"division{$i}\" style=\"font-weight:normal;\"> " . '<span style="margin-left:5px;font-weight:bold;">$' . number_format($costs->registration->cost->$i - $this->multiple_registration_discount, 2) . '</span>  ' . $costs->registration->division->$i . '</label></p>';
                    }
                } else {
                    $html .= "<p><input type=\"radio\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" /> <label for=\"division{$i}\" style=\"font-weight:normal;\"> " . '<span style="margin-left:5px;font-weight:bold;">$' . number_format($costs->registration->cost->$i - $this->multiple_registration_discount, 2) . '</span>  ' . $costs->registration->division->$i . '</label></p>';
                }
            }       
        } else {
            $i = 1;
            if(!empty($_SESSION['last_registration'])) {
                $test = Registration::find($_SESSION['last_registration']);
                if($this->member_id == $test->member_id) {
                    if($this->multiple_registration_discount > 0) {
                        $html .= "<p><input type=\"radio\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" /> <label for=\"division{$i}\" style=\"font-weight:normal;\"><span style=\"text-decoration:line-through;font-weight:100;color:#ccc;font-size:.9em;\">$" . number_format($costs->registration->cost->$i, 2) . '</span> <span style="margin-left:5px;font-weight:bold;">$' . number_format($costs->registration->cost->$i - $this->multiple_registration_discount, 2) . '</span>  ' . $costs->registration->division->$i . '</label></p>';
                    } else {
                        $html .= "<p align=\"right\"><input type=\"hidden\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" /> <label for=\"division{$i}\" style=\"font-weight:normal;\">$ " . $costs->registration->cost->$i . '</label></p>';
                    } 
                } else {
                    $html .= "<p align=\"right\"><input type=\"hidden\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" /> <label for=\"division{$i}\" style=\"font-weight:normal;\">$ " . $costs->registration->cost->$i . '</label></p>';
                }
            } else {
                $html .= "<p align=\"right\"><input type=\"hidden\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" /> <label for=\"division{$i}\" style=\"font-weight:normal;\">$ " . $costs->registration->cost->$i . '</label></p>';
            }
        
        }
        return $html;
    }


    function display_category_selection_for_member_create_registration_page() {
        $costs = json_decode($this->cost_definition);
        $num = count(get_object_vars($costs->registration->division));
        $html = '<div class="form_group">
        <div class="col-md-offset-3 col-md-8 well">
        <p><strong>Select Registration Category</strong></p>';
        for($i = 1; $i <= $num; $i ++){
                $html .= "<div class=\"checkbox\">
                        <label> <input type=\"radio\" required data-parsley-errors-container=\"#ers\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" rel=\"" . number_format($costs->registration->cost->$i, 2) . "\" /> " . $costs->registration->division->$i . '</label>
                        </div>';
        }   
        $html .= '</div>
            </div><!-- /.form-group --> 
            <div class="col-md-offset-3"><div id="ers" class="clear form-group"></div></div>';
        return $html;
    }

    function display_category_selection() {
        $costs = json_decode($this->cost_definition);
        $num = count(get_object_vars($costs->registration->division));
        $html = '<div class="form_group">
        <div class="col-md-offset-3 col-md-8 well">
        <p><strong>Select Registration Category</strong></p>';
        for($i = 1; $i <= $num; $i ++){
                $html .= "<div class=\"checkbox\">
                        <label> <input type=\"radio\" required data-parsley-errors-container=\"#ers\" name=\"division\" id=\"division{$i}\" value=\"{$costs->registration->division->$i}\" rel=\"" . number_format($costs->registration->cost->$i, 2) . "\" /> <strong>$" .  number_format($costs->registration->cost->$i, 2) . '</strong> - ' . $costs->registration->division->$i . '</label>
                        </div>';
        }   
        $html .= '</div>
            </div><!-- /.form-group --> 
            <div class="col-md-offset-3"><div id="ers" class="clear form-group"></div></div>';
        return $html;
    }

    function display_pricing_for_registration_page() {
        $costs = json_decode($this->cost_definition);
        $num = count(get_object_vars($costs->registration->division));
        if($num > 1) {
            for($i = 1; $i <= $num; $i ++){
                if(!empty($_SESSION['last_registration'])) {
                    $test = Registration::find($_SESSION['last_registration']);
                    if($this->member_id == $test->member_id) {
                        if($this->multiple_registration_discount > 0) {
                            $html .= "<div style=\"border-top:#e4e5e6 1px solid;border-bottom:#e4e5e6 1px solid;padding-top:3px;\"><div class=\"left\"><p>{$costs->registration->division->$i}</p></div><div class=\"right\"><p align=\"right\">$ " .  number_format($costs->registration->cost->$i - $this->multiple_registration_discount, 2) . '</p></div><div class="clear"></div></div>';
                        } else {
                            $html .= "<div style=\"border-top:#e4e5e6 1px solid;border-bottom:#e4e5e6 1px solid;padding-top:3px;\"><div class=\"left\"><p>{$costs->registration->division->$i}</p></div><div class=\"right\"><p align=\"right\">$ " .  number_format($costs->registration->cost->$i - $this->multiple_registration_discount, 2) . '</p></div><div class="clear"></div></div>';
                        }
                    } else {
                        $html .= "<div style=\"border-top:#e4e5e6 1px solid;border-bottom:#e4e5e6 1px solid;padding-top:3px;\"><div class=\"left\"><p>{$costs->registration->division->$i}</p></div><div class=\"right\"><p align=\"right\">$ " . $costs->registration->cost->$i . '</p></div><div class="clear"></div></div>';
                    }
                } else {
                    $html .= "<div style=\"border-top:#e4e5e6 1px solid;border-bottom:#e4e5e6 1px solid;padding-top:3px;\"><div class=\"left\"><p>{$costs->registration->division->$i}</p></div><div class=\"right\"><p align=\"right\">$ " . $costs->registration->cost->$i . '</p></div><div class="clear"></div></div>';
                }
            }       
        } else {
            $i = 1;
            if(!empty($_SESSION['last_registration'])) {
                $test = Registration::find($_SESSION['last_registration']);
                if($this->member_id == $test->member_id) {
                    if($this->multiple_registration_discount > 0) {
                        $html .= "<div style=\"border-top:#e4e5e6 1px solid;border-bottom:#e4e5e6 1px solid;padding-top:3px;\"><div class=\"left\"><p>{$costs->registration->division->$i}</p></div><div class=\"right\"><p align=\"right\">$ " .  number_format($costs->registration->cost->$i - $this->multiple_registration_discount, 2) . '</p></div><div class="clear"></div></div>';
                    } else {
                        $html .= "<div style=\"border-top:#e4e5e6 1px solid;border-bottom:#e4e5e6 1px solid;padding-top:3px;\"><div class=\"left\"><p>{$costs->registration->division->$i}</p></div><div class=\"right\"><p align=\"right\">$ " . $costs->registration->cost->$i . '</p></div><div class="clear"></div></div>';
                    } 
                } else {
                    $html .= "<div style=\"border-top:#e4e5e6 1px solid;border-bottom:#e4e5e6 1px solid;padding-top:3px;\"><div class=\"left\"><p>{$costs->registration->division->$i}</p></div><div class=\"right\"><p align=\"right\">$ " . $costs->registration->cost->$i . '</p></div><div class="clear"></div></div>';
                }
            } else {
                $html .= "<div style=\"border-top:#e4e5e6 1px solid;border-bottom:#e4e5e6 1px solid;padding-top:3px;\"><div class=\"left\"><p>{$costs->registration->division->$i}</p></div><div class=\"right\"><p align=\"right\">$ " . $costs->registration->cost->$i . '</p></div><div class="clear"></div></div>';
            }
        
        }
        return $html;
    }

    function display_checkout_donation() {
        if($this->accept_donation == 'y') {
            $html = '<p align="right"><input type="checkbox" id="activate_donation" /><label for="activate_donation">I would like to make a donation of $ </label> <input type="text" name="donation_amount" id="donation_amount" value="0.00" style="width:40px;padding:4px;font-size:.8em;" disabled="disabled" />
            <span style="color:#900;font-size:.75em" id="donation_error" class="hide"><br /><em>Please enter a valid dollar amount.</em></span></p>';
            $html .= '<hr />';
            $js = "<script>\$('#activate_donation').on('change', function(){
                if(\$('#activate_donation').prop('checked') === true) {
                    \$('#donation_amount').prop('disabled', false).val('0.00').focus().select();
                } else {
                    \$('#donation_amount').prop('disabled', true).val('0.00');
                    \$('#donation_error').hide();
                }
                calculate_total();
            });
            \$('#donation_amount').on('change', function(){calculate_total();});
            </script>
            ";
            return $html.$js;
        }
    }
    

    function generate_js_for_comfirm() {
        $this->cost_definition = json_decode($this->cost_definition);
        $num = count($this->cost_definition->registration->division);
        if($num > 1) {
            for($i = 0; $i < $num; $i ++){
                $html .= "<p><input type=\"radio\" name=\"division\" id=\"division{$i}\" value=\"{$this->cost_definition->registration->division[$i]}\" /> <label for=\"division{$i}\"><span style=\"font-weight:normal;\">$" . number_format($this->cost_definition->registration->cost[$i], 2) . "</span> {$this->cost_definition->registration->division[$i]}</label></p>";
            }       
        }   
        return $html;
    }


    function generate_confirmation_for_registration($registration) {
        $def = json_decode($this->form_definition_json);
        foreach($def as $key => $section) {
            if(!empty($section->fields))
                switch($key) {
                    case 'main':
                        $html .= $this->build_confirmation_main_section($section, $registration);
                        break;
                    case 'parent':
                        $html .= $this->build_confirmation_parent_section($section, $registration);
                        break;
                    case 'school':
                        $html .= $this->build_confirmation_school_section($section, $registration);
                        break;
                    case 'medical':
                        $html .= $this->build_confirmation_medical_section($section, $registration);
                        break;
                    case 'comments':
                        $html .= $this->build_confirmation_comment_section($section, $registration);
                        break;
                    default:
                        $html .= $this->build_confirmation_section($key, $section, $registration);
                }
        }
        return $html;
    }

    // build the main confiramtion section
    private function build_confirmation_main_section($section, $registration) {
        $main = array();
        foreach($section->fields as $element) {
            // work the form elements into an associative array to work with
            $field = $element->name;
            $main[$field] = $registration->$field;
            $labels[$field] = $element->label;
        }
        $custom = (array) json_decode($registration->custom_fields);
        $html = '<h5 class="content-title">Registration Information</h5><div class="col-md-6"><p>';
        $html .=  $main['first_name'] . ' ' . $main['last_name'] . '<br />';
        if(!empty($main['phone'])) $html .= $main['phone'] . '<br />';
        if(!empty($main['email'])) $html .= $main['email'] . '<br />';
        if(!empty($main['address_1'])) $html .= $main['address_1'] . '<br />';
        if(!empty($main['address_2'])) $html .= $main['address_2'] . '<br />';
        if(!empty($main['city'])) $html .= $main['city'] . ' ' . $main['state'] . ', ' . $main['zip'] . '<br />';
        $html .= '</p></div><div class="col-md-6"><p>';
        if(!empty($main['shirt_size'])) $html .= '<strong>Shirt Size</strong> ' . $main['shirt_size'] . '<br />';
        if(!empty($main['birth_date'])) $html .= '<strong>Born</strong> ' . date('M j, Y', strtotime($main['birth_date'])) . '<br />';
        if(!empty($custom['main'])) {
            foreach($custom['main'] as $k => $v){//$html .= print_r($v, true);
                if(is_array($v)) $html .= "<strong>{$labels[$k]}</strong> " . implode(' &amp; ', $v) . '<br />';
                else $html .= "<strong>{$labels[$k]}:</strong> " . nl2br($v) . "<br />";
        }}
        return $html . '</p></div><div class="clear margin_bottom"></div>';
    }

    // build the parent confiramtion section
    private function build_confirmation_parent_section($section, $registration) {
        $main = array();
        foreach($section->fields as $element) {
            // work the form elements into an associative array to work with
            $field = $element->name;
            $main[$field] = $registration->$field;
            $labels[$field] = $element->label;
        }
        $custom = (array) json_decode($registration->custom_fields);
        $html = '<h5 class="content-title">Parent Information</h5><div class="col-md-6"><p>';
        $html .=  $main['parent1_name'] . '<br />';
        if(!empty($main['parent1_email'])) $html .= $main['parent1_email'] . '<br />';
        if(!empty($main['parent1_phone'])) $html .= $main['parent1_phone'] . '<br />';
        if(!empty($main['parent1_volunteer'])) $html .= 'Willing to volunteer.';
        if(!empty($main['parent2_name'])){
            $html .= '</p></div><div class="col-md-6"><p>' . $main['parent2_name'] . '<br />';
            if(!empty($main['parent2_email'])) $html .= $main['parent2_email'] . '<br />';
            if(!empty($main['parent2_phone'])) $html .= $main['parent2_phone'] . '<br />';
            if(!empty($main['parent2_volunteer'])) $html .= 'Willing to volunteer.';
        }
        if(!empty($custom['parent'])) {
            $html .= '</p></div><div class="col-md-12"><p>';
            foreach($custom['parent'] as $k => $v)
                if(is_array($v)) $html .= "<strong>{$labels[$k]}</strong> " . implode(' &amp; ', $v);
                else $html .= "<strong>{$labels[$k]}</strong> {$v}<br />";
        }
        return $html . '</p></div><div class="clear margin_bottom"></div>';
    }

    // build the medical confiramtion section
    private function build_confirmation_medical_section($section, $registration) {
        $main = array();
        foreach($section->fields as $element) {
            // work the form elements into an associative array to work with
            $field = $element->name;
            $main[$field] = $registration->$field;
            $labels[$field] = $element->label;
        }
        $custom = (array) json_decode($registration->custom_fields);
        $html = '<h5 class="content-title">Medical Information</h5><div class="col-md-6">';
        if(!empty($main['physician_name'])) $html .= '<p><strong>Physician Name</strong><br />' . $main['physician_name'] . '</p>';
        if(!empty($main['physician_phone'])) $html .= '<p><strong>Physician Phone</strong><br />' . $main['physician_phone'] . '</p>';
        if(!empty($main['dentist_name'])) $html .= '<p><strong>Dentist Name</strong><br />' . $main['dentist_name'] . '</p>';
        if(!empty($main['dentist_phone'])) $html .= '<p><strong>Dentist Phone</strong><br />' . $main['dentist_phone'] . '</p>';
        $html .= '</p></div><div class="col-md-6">';
        if(!empty($main['medical_history'])) $html .= '<p><strong>Medical History</strong><br />' . nl2br($main['medical_history']) . '</p>';
        if(!empty($main['allergies'])) $html .= '<p><strong>Allergies</strong><br />' . nl2br($main['allergies']) . '</p>';
        if(!empty($custom['medical'])) {
            $html .= '</div><div class="col-md-12"><p>';
            foreach($custom['medical'] as $k => $v)
                if(is_array($v)) $html .= "<strong>{$labels[$k]}</strong> " . implode(' &amp; ', $v);
                else $html .= "<strong>{$labels[$k]}</strong> {$v}<br />";
            $html .= '</p>';
        }
        return $html . '</div><div class="clear margin_bottom"></div>';
    }

    // build the comment confiramtion section
    private function build_confirmation_comment_section($section, $registration) {
        $main = array();
        foreach($section->fields as $element) {
            // work the form elements into an associative array to work with
            $field = $element->name;
            $main[$field] = $registration->$field;
            $labels[$field] = $element->label;
        }
        $custom = (array) json_decode($registration->custom_fields);
        $html = '<h5 class="content-title">Comments</h5><div class="col-md-12">'; 
        if(!empty($registration->comments))     
            $html .= '<p>' . nl2br($registration->comments) . '</p>';
            if(!empty($custom['comment'])) {
                $html .= '<p>';
                foreach($custom['comment'] as $k => $v)
                    if(is_array($v)) $html .= "<strong>{$labels[$k]}</strong> " . implode(' &amp; ', $v);
                    else $html .= "<strong>{$labels[$k]}</strong> {$v}<br />";
                $html .= '</p>';
            }
            return $html . '</div><div class="clear margin_bottom"></div>';
    }

    // build the school confiramtion section
    private function build_confirmation_school_section($section, $registration) {
        $main = array();
        foreach($section->fields as $element) {
            // work the form elements into an associative array to work with
            $field = $element->name;
            $main[$field] = $registration->$field;
            $labels[$field] = $element->label;
        }
        $custom = (array) json_decode($registration->custom_fields);
        $html = '<h5 class="content-title">School Information</h5><div class="col-md-6">';
        if(!empty($main['school'])) $html .=  '<p><strong>School</strong><br />' . $main['school'] . '</p>';
        if(!empty($main['grade_level'])) $html .= '</div><div class="col-md-6"><p><strong>Grade Level</strong><br />' . $main['grade_level'] . '</p>';
        if(!empty($custom['school'])) {
            $html .= '</div><div class="col-md-12"><p>';
            foreach($custom['school'] as $k => $v)
                if(is_array($v)) $html .= "<strong>{$labels[$k]}</strong> " . implode(' &amp; ', $v);
                else $html .= "<strong>{$labels[$k]}</strong> {$v}<br />";
            $html .= '</p>';
        }
        return $html . '</div><div class="clear margin_bottom"></div>';
    }

    // build the generic confiramtion section
    private function build_confirmation_section($key, $section, $registration) {
        $html = '<h5 class="content-title">' . ucwords(str_replace('_', ' ', $key)) . '</h5><div class="col-md-12">';
        foreach($section->fields as $element) {
            // work the form elements into an associative array to work with
            $field = $element->name;
            $html .= '<p><strong>' . $element->label . '</strong><br />' . nl2br($registration->$field) . '</p>';
        }
        return $html . '</div><div class="clear margin_bottom"></div>';
    }

    // generate the validations for each partial, checking the attributes for optional fields 
    // output will end up like... input_name: {required:true, email:true}
    function generate_js_validations_from_json() {
        // read the json definition and loop throught the elements
        $validation = '';
        $def = json_decode($this->form_definition_json);
        foreach($def as $key => $section) {
            foreach($section->fields as $element) {
                // grab the validations if present and build the text string for the validator
                if(!empty($element->validation)) {//var_dump($element->validation);exit;
                    $name = ($element->custom == 'true') ? "'custom_fields[$key][{$element->name}]'" : $element->name;
                    if($element->special == 'date_text_input') {
                        $validation .= $name . '_mm: {min:1, max:12}, ';
                        $validation .= $name . '_dd: {min:1, max:31}, ';
                        $validation .= $name . '_yyyy: {min:1900, max:' . date('Y') . '}, ';
                    } else {
                        $validation .= $name . ': {';
                        foreach($element->validation as $k => $v) 
                            if(!empty($v)) $validation .= $k . ':' . $v . ', ';
                        
                        // trim the trailing comma and close the }
                        $validation = substr($validation, 0, -2) . '}, ';
                    }
                }
            }
        }
        // trim the trailing comma if there is output..
        return (!empty($validation)) ? substr($validation, 0, -2) : '';

    }


    /**
     *
     * Form building methods...
     *
     **/
    
    function build_form_html_from_json_definition($registration = null, $current_count = 0, $member_section = false) {
        if(empty($registration)) $registration = new Registration();
        
        $def = json_decode($this->form_definition_json);
          /*  
          // debug json 

           switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    echo ' - No errors';
                break;
                case JSON_ERROR_DEPTH:
                    echo ' - Maximum stack depth exceeded';
                break;
                case JSON_ERROR_STATE_MISMATCH:
                    echo ' - Underflow or the modes mismatch';
                break;
                case JSON_ERROR_CTRL_CHAR:
                    echo ' - Unexpected control character found';
                break;
                case JSON_ERROR_SYNTAX:
                    echo ' - Syntax error, malformed JSON';
                break;
                case JSON_ERROR_UTF8:
                    echo ' - Malformed UTF-8 characters, possibly incorrectly encoded';
                break;
                default:
                    echo ' - Unknown error';
                break;
            } */
    
        $form_html = '';
        $custom = json_decode($registration->custom_fields);
        //foreach($custom as $key => $section)
            //foreach($section as $field_name => $field_value)
                //$registration->$key . '_' . $field_name = $field_value;

        foreach($def as $key => $section) {
            if($key != 'main' && !empty($section->fields)) $form_html .= '<hr />';
            if(!empty($section->description) && !$member_section) $form_html .= '<div class="alert alert-info"><strong>' . $section->description . '</strong></div>';
            foreach($section->fields as $field => $element) {

                switch($element->type) {
                    case 'text':
                        $form_html .= $this->generate_text_field_for_form($element, $registration, $key);
                        break;
                    case 'hidden':
                        $form_html .= $this->generate_hidden_field_for_form($element, $registration, $key);
                        break;
                    case 'textarea':
                        $form_html .= $this->generate_textarea_for_form($element, $registration, $key);
                        break;
                    case 'select':
                        $form_html .= $this->generate_select_for_form($element, $registration, $key);
                        break;
                    case 'checkbox':
                        $form_html .= $this->generate_checkbox_for_form($element, $registration, $key);
                        break;
                    default:
                        $form_html .= $this->generate_text_field_for_form($element, $registration, $key);
                        break;
                }
            }
            if($key == 'main') {
                if($current_count == 0 && !$member_section && false) {
                    $form_html .= '<div class="grid_4"><div class="info_box_yellow" id="userlogindiv"><p><strong><em>Have a RegistrationHandler account?</em></strong></p>
                    <p>Log in here to complete the form with your previously saved information.</p>
                    <p><label for="loginformemail" style="width:70px;float:left;margin-top:10px;">Email</label>
                        <input type="text" value="" name="loginformemail" id="loginformemail" />
                    </p>
                    <p><label for="loginformpassword" style="width:70px;float:left;margin-top:10px;">Password</label>
                        <input type="password" value="" name="loginformpassword" id="loginformpassword" />
                    </p>
                    <p class="hide error" id="usererrortext" style="margin-left:70px;"></p>
                    <p style="margin-left:70px;"><input type="button" class="freshbutton-green" value="Log In" id="userlogin" /><img src="/images/site/loading.gif" id="userloading" class="hide" /></p>';
                } else {
                    $form_html .= '<div class="grid_4"><div class="info_box_yellow hide" id="userlogindiv">';
                }
                $form_html .= '</div></div>';
            }
        }
        echo $form_html;
    }

    
    // generate text field and any err messages
    private function generate_text_field_for_form($element, $registration, $key) {
        $name = ($element->custom == 'true') ? "custom_fields[$key][{$element->name}]" : $element->name;
        $field = ($element->custom == 'true') ? "{$key}_{$element->name}" : $element->name;
        $class_val = 'register_form';
        $parsley_validation = '';
        foreach ($element->validation as $key => $value) {
            if($key != 'required') $key = 'data-parsley-' . $key;
            $parsley_validation .= ' ' . $key;
        }
        $notes = (!empty($element->notes)) ? '<div class="info_box_yellow" style="width:304px;"><p>' . nl2br($element->notes) . '</div>' : '';
        // build errors ^               <button type="button" class="btn btn-default demo-element ui-tooltip" data-toggle="tooltip" data-placement="right" title="Tooltip on right">

        if(!empty($registration->errors[$field])) { 
            $error     = '<span class="form_error">' .$registration->errors[$field] . '</span><br />';
            $class_val = 'error';
        }
        if(empty($registration->$field)) $registration->$field = $element->default;
        $class = (!empty($class_val)) ? 'class="' . $class_val . '"' : '';

        switch($element->special) {
            case 'address1':
                $form_html = '<div class="form-group"><label class="col-md-3">' . $element->label . '</label>
                    <div class="col-md-8">
                        <input type="text" name="' . $name . '" id="' . $element->name . '" class="form-control" value="' . $registration->$field . '"' . $parsley_validation . ' />';
                break;
            case 'address2':
                $form_html = '<br /><input type="text" name="' . $name . '" id="' . $element->name . '" class="form-control" value="' . $registration->$field . '"' . $parsley_validation . ' />
                    </div> <!-- /.col -->
                </div> <!-- /.form-group -->';  
                break;
            case 'date_text_input':
                if($registration->$field == '0000-00-00'|| empty($registration->$field)) $registration->$field = '';
                else $registration->$field = date('m/d/Y', strtotime($registration->$field));

                $form_html = '<div class="form-group"><label class="col-md-3">' . $element->label . '</label>
                    <div class="col-md-8">
                        <div class="input-group date datetimepicker" id="datetimepicker-' . $name . '">
                            <input type="text" name="' . $name . '" id="' . $element->name . '" value="' . $registration->$field . '"' . $parsley_validation . ' class="form-control" />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div> <!-- /.col -->
                </div> <!-- /.form-group -->';
                break;
            case 'date':
                if($registration->$field == '0000-00-00' || empty($registration->$field)) $registration->$field = '';
                else $registration->$field = date('m/d/Y', strtotime($registration->$field));
                $form_html = '<div class="form-group"><label class="col-md-3">' . $element->label . '</label>
                    <div class="col-md-8">
                        <div class="input-group date datetimepicker" id="datetimepicker-' . $name . '">
                            <input type="text" name="' . $name . '" id="' . $element->name . '" value="' . $registration->$field . '"' . $parsley_validation . ' class="form-control" />
                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span>
                            </span>
                        </div>
                    </div> <!-- /.col -->
                </div> <!-- /.form-group -->';
                break;
            default:
                $type = 'text';
                if (array_key_exists('email', $element->validation)) $type = 'email';
                $form_html = '<div class="form-group"><label class="col-md-3">' . $element->label . '</label>
                    <div class="col-md-8">
                        <input type="' . $type . '" name="' . $name . '" id="' . $element->name . '" class="form-control" value="' . $registration->$field . '"' . $parsley_validation .' />
                    </div> <!-- /.col -->
                </div> <!-- /.form-group -->';
        }
        return $form_html;
    }
    
    // generate hidden field 
    private function generate_hidden_field_for_form($element, $registration, $key) {
        $field = $element->name;
        if(empty($registration->$field)) $registration->$field = $element->default;
        $form_html = "<input type=\"hidden\" name=\"{$element->name}\" id=\"{$element->name}\" value=\"{$registration->$field}\" />";   

        return $form_html;
    }
    
    // generate checkbox field 
    private function generate_checkbox_for_form($element, $registration, $key) {
        $name = ($element->custom == 'true') ? "custom_fields[$key][{$element->name}][]" : $element->name;
        $field = ($element->custom == 'true') ? "{$key}_{$element->name}" : $element->name;
        
        // build errors
        if(!empty($registration->errors[$field])) { 
            $error     = '<span class="form_error">' .$registration->errors[$field] . '</span><br />';
            $class_val = 'error';
        }
        $class = (!empty($class_val)) ? 'class="' . $class_val . '"' : '';
        $notes = (!empty($element->notes)) ? '<div class="info_box_yellow" style="width:304px;"><p>' . nl2br($element->notes) . '</div>' : '';
        $value_list = (array) $element->value_list;//print_r($element);
        $form_html = '<div class="form-group"><div class="col-md-3"></div>
                <div class="col-md-8">';

        // list of options..
        $count = 1;
        if(count($value_list) > 1) {
            
            foreach($value_list as $k => $v) {
                $checked = (in_array($v, $registration->$field)) ?  ' checked' : '';
                $form_html .= '<div class="checkbox">
                      <label><input type="checkbox" name="' . $name . '[]" id="' . $element->name . '_' . $count . '" value="' . $k . '"' . $checked . ' />' . $v . '</label>
                      </div><!-- /.checkbox -->';   
                $count ++;
            }
        } else {
            // only one option
            $checked = (!empty($registration->$field)) ?  ' checked' : '';
                $form_html .= '<div class="checkbox">
                      <label><input type="checkbox" name="' . $name . '" id="' . $element->name . '_' . $count . '" value="yes"' . $checked . ' />' . $element->label . '</label>
                      </div><!-- /.checkbox -->';   
                $count ++;
        }
        return $form_html . '</div> <!-- /.col -->
            </div> <!-- /.form-group -->';
    }
    
    // generate textarea and any err messages
    private function generate_textarea_for_form($element, $registration, $key) {
        $name = ($element->custom == 'true') ? "custom_fields[$key][{$element->name}]" : $element->name;
        $field = ($element->custom == 'true') ? "{$key}_{$element->name}" : $element->name;     // build errors
        $class_val = 'register_form';
        $notes = (!empty($element->notes)) ? '<div class="info_box_yellow" style="width:304px;"><p>' . nl2br($element->notes) . '</div>' : '';
        if(!empty($registration->errors[$field])) { 
            $error     = '<span class="form_error">' . $registration->errors[$field] . '</span><br />';
            $class_val = 'error';
        }
        if(empty($registration->$field)) $registration->$field = $element->default;
        $class = (!empty($class_val)) ? 'class="' . $class_val . '"' : '';
        //$form_html = "<p>{$error}<label for=\"{$name}\">{$element->label}</label><br />{$notes}<textarea name=\"{$name}\" id=\"{$name}\" {$class} />{$registration->$field}</textarea></p>";  
        $form_html = '<div class="form-group"><label class="col-md-3">' . $element->label . '</label>
            <div class="col-md-8">
                <textarea name="' . $name . '" id="' . $name . '" rows="6" class="form-control">' . $registration->$field . '</textarea>
            </div> <!-- /.col -->
        </div> <!-- /.form-group -->';
        return $form_html;
    }
    
    // generate text field and any err messages
    private function generate_select_for_form($element, $registration, $key) {
        $name = ($element->custom == 'true') ? "custom_fields[$key][{$element->name}]" : $element->name;
        $field = ($element->custom == 'true') ? "{$key}_{$element->name}" : $element->name;
        $class_val = 'register_form';
        // build errors
        if(!empty($registration->errors[$field])) { 
            $error     = '<span class="form_error">' . $registration->errors[$field] . '</span><br />';
            $class_val = 'error';
        }
        $form_html = '<div class="form-group"><label class="col-md-3">' . $element->label . '</label>
            <div class="col-md-8">
                <select name="' . $name . '" id="' . $element->name . '" class="form-control ' . $class_val . '">';

        if(empty($registration->$field)) $registration->$field = $element->default;
        // build the default values
        $notes = (!empty($element->notes)) ? '<div class="info_box_yellow" style="width:304px;"><p>' . nl2br($element->notes) . '</div>' : '';
        switch($element->special) {
            case 'us_states_dropdown':
                $value_list = array('' => '(none selected)', 'AL' => 'Alabama' , 'AK' => 'Alaska' , 'AZ' => 'Arizona' , 'AR' => 'Arkansas' , 'CA' => 'California' , 'CO' => 'Colorado' , 'CT' => 'Connecticut' , 'DE' => 'Delaware' , 'DC' => 'District of Columbia' , 'FL' => 'Florida' , 'GA' => 'Georgia' , 'HI' => 'Hawaii' , 'ID' => 'Idaho' , 'IL' => 'Illinois' , 'IN' => 'Indiana' , 'IA' => 'Iowa' , 'KS' => 'Kansas' , 'KY' => 'Kentucky' , 'LA' => 'Louisiana' , 'ME' => 'Maine' , 'MD' => 'Maryland' , 'MA' => 'Massachesetts' , 'MI' => 'Michigan' , 'MN' => 'Minnesota' , 'MS' => 'Mississippi' , 'MO' => 'Missouri' , 'MT' => 'Montana' , 'NE' => 'Nebraska' , 'NV' => 'Nevada' , 'NH' => 'New Hampshire' , 'NJ' => 'New Jersey' , 'NM' => 'New Mexico' , 'NY' => 'New York' , 'NC' => 'North Carolina' , 'ND' => 'North Dakota' , 'OH' => 'Ohio' , 'OK' => 'Oklahoma' , 'OR' => 'Oregon' , 'PA' => 'Pennsylvania' , 'RI' => 'Rhode Island' , 'SC' => 'South Carolina' , 'SD' => 'South Dakota' , 'TN' => 'Tennessee' , 'TX' => 'Texas' , 'UT' => 'Utah' , 'VT' => 'Vermont' , 'VA' => 'Virginia' , 'WA' => 'Washington' , 'WV' => 'West Virginia' , 'WI' => 'Wisconsin' , 'WY' => 'Wyoming'); 
                foreach ($value_list as $option => $v) {
                    $selected = '';
                    if($registration->$field == $option) $selected = ' selected="selected"';
                    $form_html .= "<option value=\"{$option}\"{$selected}>{$v}</option>";
                }
                break;
            case 'grade_level_dropdown':
                $value_list = array('' => '(none selected)', 'Pre-K' => 'Pre-K' , 'Kindergarten' => 'Kindergarten' , '1' => '1' , '2' => '2' , '3' => '3' , '4' => '4' , '5' => '5' , '6' => '6' , '7' => '7' , '8' => '8' , '9' => '9' , '10' => '10' , '11' => '11' , '12' => '12' , 'Associates' => 'Associates' , 'Bachelors' => 'Bachelors' , 'Masters' => 'Masters', 'Post Masters' => 'Post Masters'); 
                foreach ($value_list as $option => $v) {
                    $selected = '';
                    if($registration->$field == $option) $selected = ' selected="selected"';
                    $form_html .= "<option value=\"{$option}\"{$selected}>{$v}</option>";
                }
                break;
            default:
                $value_list = (array) $element->value_list;
                foreach($value_list as $k => $v) {
                    $selected = '';
                    if($v == '') $k = '';
                    if($registration->$field == $k) $selected = ' selected="selected"';
                    $form_html .= '<option value="'.$k.'"'.$selected.'>'.$v.'</option>';
                }
        }
        return $form_html . "</select></div> <!-- /.col -->
                    </div> <!-- /.form-group -->";
    }

    /***********************************************************************************************************
     * END Form building methods
     **/


    function display_cost() {
        $costs = json_decode($this->cost_definition);
        $num = count(get_object_vars($costs->registration->division));
        if($num > 1) {
            for($i = 1; $i <= $num; $i ++){
                if($costs->registration->cost->$i > 0) {
                    if(!empty($_SESSION['last_registration'])) {
                        $test = Registration::find($_SESSION['last_registration']);
                        if($this->member_id == $test->member_id) {
                            if($this->multiple_registration_discount > 0) {
                                $html .=  '<br /><span style="text-decoration:line-through;">$' . $costs->registration->cost->$i . '</span> <span style="margin-left:5px;font-weight:bold;" class="green">$' . ($costs->registration->cost->$i - $this->multiple_registration_discount) . '</span> ' . $costs->registration->division->$i;
                            } else {
                                $html .= '<br /><strong class="green">$' . $costs->registration->cost->$i . '</strong> ' . $costs->registration->division->$i;
                            }
                        }
                    } else {
                        $html .= '<br /><strong class="green">$' . $costs->registration->cost->$i . '</strong> ' . $costs->registration->division->$i;
                    }
                } else {
                    $html . '<br />' . $costs->registration->division->$i;
                }
            }   
            return $html;
        } else {
            $i = 1;
            if($costs->registration->cost->$i == 0) return $costs->registration->division->$i;
            if(!empty($_SESSION['last_registration'])) {
                $i = 1;
                $test = Registration::find($_SESSION['last_registration']);
                if($this->member_id == $test->member_id) 
                    if($this->multiple_registration_discount > 0)
                        return '<span style="text-decoration:line-through;">$' . $costs->registration->cost->$i . '</span> <span style="margin-left:5px;font-weight:bold;" class="green">$' . ($costs->registration->cost->$i - $this->multiple_registration_discount) . '</span>';
            }
            $i = 1;
            return '<strong class="green">$' . $costs->registration->cost->$i . '</strong>';
        }
    }
    
    function display_definition_for_edit_page() {
        $def = json_decode($this->form_definition_json);
        // use an original definition to add missing sections if desired
        $alldef = $this->generate_default_form_definition_json();
        $orig_keys = array_keys($alldef);
        //print_r($orig_keys);
        $keycount = 0;
        foreach($orig_keys as $key) {   //echo $key.' ' .$keycount . '<br>';
            if(array_key_exists($key, $def)) {
                $form_html .= $this->display_definition_for_section($key, $def->$key);          
            } else {
                $form_html .= '<div id="' . $key . '_section" style="margin-bottom:10px;"><div class="right"><input type="button" class="reset_button btn btn-secondary" title="' . $key . '" id="reset_' . $key . '" value="Reset ' . ucwords($key) . '" /></p></div></div><div class="clear"></div>';
            }
        }
        return $form_html;
    }

    function display_definition_for_section($key, $section) {
        $form_html .= '<div id="' . $key . '_section" class="portlet portlet-boxed" style="margin-bottom:10px;padding:2px;"><div>
            <div class="pull-left"><h4 class="content-title">' . ucwords($key) . ' Information Section</h4></div><div class="pull-right">';
        if($key != 'main') $form_html .= '<a href="javascript:;" class="delete_section btn btn-sm btn-primary" title="' . $key . '" id="remove_button_' . $key . '">Remove ' . ucwords($key) . '</a><input type="hidden" name="remove[' . $key . '][]" value="" id="remove_' . $key .'" /><br />';
        $form_html .= '<a href="javascript:;" class="reset_button btn btn-sm btn-secondary pull-right" title="' . $key . '" id="reset_' . $key . '">Reset ' . ucwords($key) . '</a></div>
            <div class="right hide loader"><img src="/images/site/loading.gif" style="margin-top:5px;"></div>
            <div class="clear"></div>
            </div>';
        $count = 0;
        foreach($section->fields as $element) {
            if($element->name == 'country') continue;                   
            if($element->name == 'address_2') {
                $form_html .= '<input type="hidden" name="main[fields][address_2][name]" value="address_2"><input type="hidden" name="main[fields][address_2][type]" value="text">';
                continue;                   
            }
            
            $bg = ($count % 2 == 1) ? 'background:#f0f1f2;' : '';
            $count ++;
            $form_html .= '<div id="div_' . $element->name . '" style="padding:2px;' . $bg . '">
                <div id="child1_' . $element->name . '" style="float:left;width:300px;">
                    <p><label for="' . $key . '[fields][' . $element->name . '][label]">Field Title</label><br />
                        <input style="width:200px;" class="form-control" type="text" name="' . $key . '[fields][' . $element->name . '][label]" value="' . $element->label . '" /></p>';
            if(!empty($element->notes)) $form_html .= '<p><label for="notes' . $key . '[fields][' . $element->name . '][label]">Descripition Displayed</label><br />
                        <textarea name="' . $key . '[fields][' . $element->name . '][notes]" class="form-control"  style="width:225px;height:75px;font-size:10px;padding:3px;">' . $element->notes . '</textarea></p>';

            $form_html .= '</div>       
                <div id="child2_' . $element->name . '" style="float:left;width:200px;">
                    <p><strong>This field is...</strong><br />
                    <input type="hidden" name="' . $key . '[fields][' . $element->name . '][name]" value="' . $element->name . '" />
                    <input type="hidden" name="' . $key . '[fields][' . $element->name . '][type]" value="' . $element->type . '" />';
            if($element->type == 'checkbox') $form_html .= 'a checkbox<br />';
            else if($element->type == 'textarea') $form_html .= 'a large text input<br />';
            else if($element->type == 'select') $form_html .= 'a dropdown<br />';
            foreach($element->validation as $k => $v) {
                if(in_array($k, array('min', 'max'))) {
                    $form_html .= $k . '<input class="datetimpicker" type="text" name="' . $key . '[fields][' . $element->name . '][validation][' . $k . ']" value="' . $element->validation->$k . '" /><br />';
                } else $form_html .= $k . '<input type="hidden" name="' . $key . '[fields][' . $element->name . '][validation][' . $k . ']" value="true" /><br />';
            }
            $form_html .= '</p>
            ';
            if($element->custom == 'true') {
                $form_html .= '<input type="hidden" name="' . $key . '[fields][' . $element->name . '][custom]" value="' . $element->custom . '" />';
                $form_html .= '<input type="hidden" name="' . $key . '[fields][' . $element->name . '][removeable]" value="' . $element->removeable . '" />';
            }
            if (!empty($element->value_list) && ($element->custom == 'true' || $element->type == 'select'))   {
                foreach($element->value_list as $v) $list .= $v. chr(10);
                $form_html .= '
                        <p><strong>With these values...</strong><span style="font-size:11px;">(one per line)</span><br> 
                            <textarea name="' . $key . '[fields][' . $element->name . '][value_list]" class="form-control"  style="width:225px;height:75px;font-size:10px;padding:3px;">' . implode(chr(10), (array) $element->value_list) . '</textarea>';
            }
            $form_html .= '</div>';
            if($element->removeable == 'true') 
                $form_html .= '<div style="float:right;"><a href="javascript:;" class="delete btn btn-sm btn-primary" title="' . $element->name . '" id="remove_button_' . $element->name . '">Remove</a><a href="javascript:;" class="activate hide btn btn-small btn-secondary" title="' . $element->name . '" id="activate_' . $element->name .'">Activate</a><input type="hidden" name="remove[' . $key . '][]" value="" id="remove_' . $element->name .'" /></div>';
            $form_html .= '
                <div class="clear"></div>
            </div>';
            
        }
        if(!empty($section->fields)) {
            $form_html .= '<div style="padding:2px;">
            <p><label>Descripton for ' . ucwords($key) . ' form section</label><br />
                <textarea name="' . $key . '[description]" class="form-control" rows="4">' . $section->description . '</textarea></p>
            </div>';
        }
        return $form_html . '</div>';
    }
        
    function display_registration_page_event_info(){
        $query = "SELECT members.name, members.contact_name "
               . "FROM members "
               . "WHERE id = {$this->member_id}";
        $res = ActiveRecordBase::query($query);
        $html = '<p>';
        if(!empty($res[0]['name'])) $html .= $res[0]['name'];
        else $html .= $res[0]['contact_name'];
        if($this->activity_type == 'Event' || $this->activity_type == 'Race')
            $html .= '<br />' . date('D M j, Y ', strtotime($this->activity_date)) . ' at ' . Helpers::format_mysql_time_for_display($this->activity_time);
        
        $html .= '<br />' . $this->city . ', ' . $this->state . '</p>';
        return $html;   
    }
    
    function display_address(){
        $html = '<p style="padding:5px;"><strong>' . $this->location . '</strong><br />' . $this->address1 . '<br />';
        if(!empty($this->address2)) $html .= $this->address2 . '<br />';
        $html .= $this->city . ', ' . $this->state . ' ' . $this->zip . '</p>';
        return $html;   
    }
    
    function display_member_header() {
        $query = "SELECT members.* "
               . "FROM members "
               . "JOIN activities ON activities.member_id = members.id "
               . "WHERE activities.id = {$this->id}";
        $res = ActiveRecordBase::query($query);
        return Member::display_member_header_for($res[0]);
    }

    function generate_default_form_definition_json() {
        $main['email'] = array(
            'type'       => 'text',
            'name'       => 'email',
            'label'      => 'Email',
            'sort_order' => 3,
            'default'    => '',
            'validation' => array('required' => 'true', 'email' => 'true'),
            'value_list' => '',
            'special'    => '');

        $main['first_name'] = array(
            'type'       => 'text',
            'name'       => 'first_name',
            'label'      => 'First Name',
            'sort_order' => 1,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $main['last_name'] = array(
            'type'       => 'text',
            'name'       => 'last_name',
            'label'      => 'Last Name',
            'sort_order' => 2,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '');
                
        $main['phone'] = array(
            'type'       => 'text',
            'name'       => 'phone',
            'label'      => 'Phone',
            'sort_order' => 4,
            'default'    => '',
            'validation' => array('required' => 'true', 'phoneUS' => 'true'),
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $main['birth_date'] = array(
            'type'       => 'text',
            'name'       => 'birth_date',
            'label'      => 'Birth Date',
            'sort_order' => 5,
            'default'    => '',
            'validation' => array('required' => 'true', 'date' => 'true'),
            'value_list' => '',
            'special'    => 'date_text_input',
            'removeable' => 'true');
        
        $main['address_1'] = array(
            'type'       => 'text',
            'name'       => 'address_1',
            'label'      => 'Address',
            'sort_order' => 6,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => 'address1');
        
        $main['address_2'] = array(
            'type'       => 'text',
            'name'       => 'address_2',
            'label'      => '',
            'sort_order' => 7,
            'default'    => '',
            'validation' => '',
            'value_list' => '',
            'special'    => 'address2');
        
        $main['city'] = array(
            'type'       => 'text',
            'name'       => 'city',
            'label'      => 'City',
            'sort_order' => 8,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $main['state'] = array(
            'type'       => 'select',
            'name'       => 'state',
            'label'      => 'State',
            'sort_order' => 9,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => 'us_states_dropdown');
        
        $main['zip'] = array(
            'type'       => 'text',
            'name'       => 'zip',
            'label'      => 'Zip Code',
            'sort_order' => 10,
            'default'    => '',
            'validation' => array('required' => 'true', 'zipcode' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $main['country'] = array(
            'type'       => 'hidden',
            'name'       => 'country',
            'label'      => 'Country',
            'sort_order' => 11,
            'default'    => 'US',
            'validation' => array('required' => 'true'),
            'special'    => 'country_dropdown');
        
        $main['shirt_size'] = array(
            'type'       => 'select',
            'name'       => 'shirt_size',
            'label'      => 'Shirt Size',
            'sort_order' => 11,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => array('' => '', 'Youth Small' => 'Youth Small', 'Youth Medium' => 'Youth Medium', 'Youth Large' => 'Youth Large', 'Adult Small' => 'Adult Small', 'Adult Medium' => 'Adult Medium', 'Adult Large' => 'Adult Large', 'Adult XL' => 'Adult XL', 'Adult XXL' => 'Adult XXL'),
            'special'    => '');
        
        $parent['parent1_name'] = array(
            'type'       => 'text',
            'name'       => 'parent1_name',
            'label'      => 'Parent Name',
            'sort_order' => 12,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $parent['parent1_email'] = array(
            'type'       => 'text',
            'name'       => 'parent1_email',
            'label'      => 'Parent Email',
            'sort_order' => 13,
            'default'    => '',
            'validation' => array('required' => 'true', 'email' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $parent['parent1_phone'] = array(
            'type'       => 'text',
            'name'       => 'parent1_phone',
            'label'      => 'Parent Phone',
            'sort_order' => 14,
            'default'    => '',
            'validation' => array('required' => 'true', 'phoneUS' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $parent['parent1_volunteer'] = array(
            'type'       => 'checkbox',
            'name'       => 'parent1_volunteer',
            'label'      => 'Would be willing to volunteer.',
            'sort_order' => 15,
            'default'    => 'n',
            'validation' => '',
            'value_list' => 'y',
            'special'    => '',
            'removeable' => 'true');
        
        $parent['parent1_coach'] = array(
            'type'       => 'checkbox',
            'name'       => 'parent1_coach',
            'label'      => 'Would be willing to coach.',
            'sort_order' => 15,
            'default'    => 'n',
            'validation' => '',
            'value_list' => 'y',
            'special'    => '',
            'removeable' => 'true');
        
        $parent['parent1_experience'] = array(
            'type'       => 'select',
            'name'       => 'parent1_experience',
            'label'      => 'Experience',
            'sort_order' => 15,
            'default'    => 'n',
            'validation' => '',
            'value_list' => array('', 'Basic Fundamentals', 'Played High School', 'Played College', 'Previous Coaching Experience'),
            'special'    => '',
            'removeable' => 'true');
        
        $parent['parent2_name'] = array(
            'type'       => 'text',
            'name'       => 'parent2_name',
            'label'      => 'Second Parent Name',
            'sort_order' => 16,
            'default'    => '',
            'validation' => '',
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $parent['parent2_email'] = array(
            'type'       => 'text',
            'name'       => 'parent2_email',
            'label'      => 'Second Parent Email',
            'sort_order' => 17,
            'default'    => '',
            'validation' => array('email' => 'true'),
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $parent['parent2_phone'] = array(
            'type'       => 'text',
            'name'       => 'parent2_phone',
            'label'      => 'Second Parent Phone',
            'sort_order' => 18,
            'default'    => '',
            'validation' => array('phoneUS' => 'true'),
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $parent['parent2_volunteer'] = array(
            'type'       => 'checkbox',
            'name'       => 'parent2_volunteer',
            'label'      => 'Would be willing to volunteer.',
            'sort_order' => 19,
            'default'    => 'n',
            'validation' => '',
            'value_list' => 'y',
            'special'    => '',
            'removeable' => 'true');
        
        $parent['parent2_coach'] = array(
            'type'       => 'checkbox',
            'name'       => 'parent2_coach',
            'label'      => 'Would be willing to coach.',
            'sort_order' => 15,
            'default'    => 'n',
            'validation' => '',
            'value_list' => 'y',
            'special'    => '',
            'removeable' => 'true');
        
        $parent['parent2_experience'] = array(
            'type'       => 'select',
            'name'       => 'parent2_experience',
            'label'      => 'Experience',
            'sort_order' => 15,
            'default'    => 'n',
            'validation' => '',
            'value_list' => array('', 'Basic Fundamentals', 'Played High School', 'Played College', 'Previous Coaching Experience'),
            'special'    => '',
            'removeable' => 'true');
        
        $school['school'] = array(
            'type'       => 'text',
            'name'       => 'school',
            'label'      => 'School',
            'sort_order' => 20,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $school['grade_level'] = array(
            'type'       => 'select',
            'name'       => 'grade_level',
            'label'      => 'Grade Level',
            'sort_order' => 21,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => 'grade_level_dropdown');
        
        $emergency['emergency_contact_relationship'] = array(
            'type'       => 'text',
            'name'       => 'emergency_contact_relationship',
            'label'      => 'Relationship to Participant',
            'sort_order' => 22,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $emergency['emergency_contact'] = array(
            'type'       => 'text',
            'name'       => 'emergency_contact',
            'label'      => 'Emergency Contact',
            'sort_order' => 22,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '');
        
        $emergency['emergency_contact_phone'] = array(
            'type'       => 'text',
            'name'       => 'emergency_contact_phone',
            'label'      => 'Emergency Contact Phone',
            'sort_order' => 23,
            'default'    => '',
            'validation' => array('required' => 'true', 'phoneUS' => 'true'),
            'value_list' => '',
            'special'    => '');
                
        $medical['physician_name'] = array(
            'type'       => 'text',
            'name'       => 'physician_name',
            'label'      => 'Preferred Physician',
            'sort_order' => 25,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $medical['physician_phone'] = array(
            'type'       => 'text',
            'name'       => 'physician_phone',
            'label'      => 'Preferred Physician Phone',
            'sort_order' => 26,
            'default'    => '',
            'validation' => array('required' => 'true', 'phoneUS' => 'true'),
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $medical['dentist_name'] = array(
            'type'       => 'text',
            'name'       => 'dentist_name',
            'label'      => 'Preferred Dentist',
            'sort_order' => 27,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $medical['dentist_phone'] = array(
            'type'       => 'text',
            'name'       => 'dentist_phone',
            'label'      => 'Preferred Dentist Phone',
            'sort_order' => 28,
            'default'    => '',
            'validation' => array('required' => 'true', 'phoneUS' => 'true'),
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $medical['hospital_name'] = array(
            'type'       => 'text',
            'name'       => 'hospital_name',
            'label'      => 'Preferred Hospital',
            'sort_order' => 24,
            'default'    => '',
            'validation' => array('required' => 'true'),
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        $medical['medical_comments'] = array(
            'type'       => 'textarea',
            'name'       => 'medical_history',
            'label'      => 'Medical Comments (history, prescriptions, allergies, etc)',
            'sort_order' => 29,
            'default'    => '',
            'validation' => '',
            'value_list' => '',
            'special'    => '',
            'removeable' => 'true');
        
        
        $comments['comments'] = array(
            'type'       => 'textarea',
            'name'       => 'comments',
            'label'      => 'Comments',
            'sort_order' => 31,
            'default'    => '',
            'validation' => '',
            'value_list' => '',
            'special'    => '');

        $form['main']['fields']      = $main;
        $form['parent']['fields']    = $parent;
        $form['school']['fields']    = $school;
        $form['emergency']['fields'] = $emergency;
        $form['medical']['fields']   = $medical;
        $form['comments']['fields']  = $comments;

        $form['main']['description']      = "Please fill in registration information. This information is never shared with third parties nor will we ever contact you for solicitation.";
        $form['school']['description']    = "Please let us know the school to be attended and grade level for the participant during this activity.";
        $form['parent']['description']    = "Please fill in at least one parents contact information.";
        $form['emergency']['description'] = "Please fill in an emergency contact in case the primary contacts are not available.";
        $form['medical']['description']   = "Please fill in your preferred medical contacts and history if applicable.";

        //print_r($form);
        //echo(json_encode($form));
        //echo 'adsf';
        
        return $form;
        //$this->form_definition_json = json_encode($form);
    
    }


}