<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class LogAction extends ActiveRecordBase {



	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array('member_id', 'type', 'message', 'browser_info', 'ip_address');
	 	                       
		parent::__construct('log_action');
	}

	static function find ($id) {
  		return parent::find('log_action', $id);
	}

	static function find_all () {
		return parent::find_all('log_action');
	}

	static function find_all_by ($array) {
		return parent::find_all_by('log_action', $array);
	}

	static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
		return parent::find_all_with_pagination('log_action', $start, $limit, $order_by, $asdc);
	}

	static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
		return parent::find_with_pagination('log_action', $start, $limit, $order_by, $asdc, $query);
	}

	static function destroy ($id) {
		return parent::destroy('log_action', $id);
	}

	static function create ($params) {
		$params['browser_info'] = $_SERVER['HTTP_USER_AGENT'];
		$params['ip_address']   = Application::get_real_ip();
		return parent::create('log_action', $params);
	}
	
	static function log($msg, $type = 'application', $mid = 0, $notify = false) {
		$log = parent::create('log_action', 
		                      array('browser_info' => $_SERVER['HTTP_USER_AGENT'],
						            'ip_address'   => Application::get_real_ip(),
						            'member_id'    => $mid,
						            'message'      => $msg,
						            'type'         => $type));
		$log->save();
		
		if($notify) $log->notify();
	}
	
	private function notify() {
		$to	     = SITE_LOG_NOTIFY_EMAIL_ADDRESS;
		$subject = 'RH Activity Log: ' . $this->type;
		$headers = 'From: <Turd Burd>info@registrationhandler.com' . "\r\n" .
			   'Reply-To: info@registrationhandler.com' . "\r\n" .
		       'Content-type: text/html' . "\r\n" .
			   'X-Mailer: PHP/' . phpversion();

		
		$message = '<html><head><style>pre{font-size:12px;}</style><body>';
		$message .= "<p>" . $this->message . "</p>";
		$message .= "<p>Nick!!!</p>";
		$message .= "<p>----------------------------------------------------------</p>";
		$message .= "<pre>" . print_r($this, true) . "</pre>";

		$message .= '</body></html>';
		mail($to, $subject, $message, $headers);
	
	}
}