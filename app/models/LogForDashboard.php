<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class LogForDashboard extends ActiveRecordBase {

    /**
     * Use the before save filter to validate data.
     */ 
    function before_save() {
        $this->requires(array('member_id', 'type', 'title', 'description'));
    }

    /**
     * The following 6 functions are required for all ActiveRecord Objects
     * php 5.3 will be fixing some big issues
     */ 
    function __construct () {
        // setting the columns here saves database calls
        // good to do when structure is set
        $this->columns = array( 'member_id', 'activity_id', 'type', 'title', 'description');
        parent::__construct('log_for_dashboard');
    }

    static function find ($id) {
        return parent::find('log_for_dashboard', $id);
    }

    static function find_all ($mod = array()) {
        return parent::find_all('log_for_dashboard', $mod);
    }

    static function find_all_by ($array, $mod = array()) {
        return parent::find_all_by('log_for_dashboard', $array, $mod);
    }

    static function find_by ($array) {
        return parent::find_by('log_for_dashboard', $array);
    }

    static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
        return parent::find_all_with_pagination('log_for_dashboard', $start, $limit, $order_by, $asdc);
    }

    static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
        return parent::find_with_pagination('log_for_dashboard', $start, $limit, $order_by, $asdc, $query);
    }

    static function fetch_as_object ($sql) {
        return parent::fetch_as_object($sql, 'log_for_dashboard');
    }

    static function destroy ($id) {
        return parent::destroy('log_for_dashboard', $id);
    }

    static function create ($params) {
        return parent::create('log_for_dashboard',$params);
    }


    static function log($member_id, $activity_id, $type, $title, $description) {
        $params = array('member_id'   => $member_id,
                        'activity_id' => $activity_id,
                        'type'        => $type,
                        'title'       => $title,
                        'description' => $description);
        $log = self::create($params);
        $log->save();
    }

    static function log_registration_for_activity($registration, $activity, $member_created = false) {
        $description = ($member_created) ? "{$registration->first_name} {$registration->last_name} has been created for {$registration->division}" :
                                         "{$registration->first_name} {$registration->last_name} has registered for {$registration->division}";
        $params = array('member_id'   => $activity->member_id,
                        'activity_id' => $activity->id,
                        'type'        => 'registration',
                        'title'       => 'New Registration',
                        'description' => $description);
        $log = self::create($params);
        $log->save();
    }

    static function get_latest_for_member($member_id) {
        return self::find_all_by(array('member_id' => $member_id), array('ORDER BY' => 'created_at DESC', 'LIMIT' => 5));
    }

    static function get_latest_for_activity($activity_id) {
        return self::find_all_by(array('activity_id' => $activity_id), array('ORDER BY' => 'created_at DESC', 'LIMIT' => 5));
    }

    function display_as_list_item() {
        switch($this->type) {
            case 'information':
                $class="fa-check-square text-secondary";
                break;
            case 'notification':
                $class="fa-check-square text-secondary";
                break;
            case 'registration':
                $class="fa-male text-success";
                break;
            case 'activity':
                $class="fa-trophy text-warning";
                break;
            case 'calendar':
                $class="fa-calendar text-default";
                break;
            case 'mail':
                $class="fa-envelope text-secondary";
                break;
            case 'error':
                $class="fa-exclamation-triangle text-danger";
                break;
            case 'home':
                $class="fa-home text-success";
                break;
            case 'remove':
                $class="fa-remove text-warning";
                break;
            default;
                $class = "fa-info";
        }

        return "<li>
            <i class=\"icon-li fa {$class}\"></i>
            <strong>{$this->title}</strong>
            <br>
            <small class=\"text-muted\">" . date('D M d Y', strtotime($this->created_at)) . "</small><br />
            {$this->description}
          </li>";

    }
}