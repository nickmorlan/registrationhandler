<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class MemberControl extends ActiveRecordBase {

	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_save() {
		$this->requires(array('name'));
	}

	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array( 'member_id', 'form_background_color', 'form_header_color', 'paypal_email',
		                        'salt', 'bank_account_number', 'bank_routing_number', 'payment_method');
		parent::__construct('member_control');
	}

	static function find ($id) {
		return parent::find('member_control', $id);
	}

	static function find_all ($mod = array()) {
		return parent::find_all('member_control', $mod);
	}

	static function find_all_by ($array, $mod = array()) {
		return parent::find_all_by('member_control', $array, $mod);
	}

	static function find_by ($array) {
		return parent::find_by('member_control', $array);
	}

	static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
		return parent::find_all_with_pagination('member_control', $start, $limit, $order_by, $asdc);
	}

	static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
		return parent::find_with_pagination('member_control', $start, $limit, $order_by, $asdc, $query);
	}

	static function fetch_as_object ($sql) {
		return parent::fetch_as_object($sql, 'member_control');
	}

	static function destroy ($id) {
		return parent::destroy('member_control', $id);
	}

	static function create ($params) {
		return parent::create('member_control',$params);
	}
}