<?php
/**
 * PACKAGE: r5k Builder
 *
 * This file contains the SiteEmail helper class that connects with
 * the MailGun API to send site confirmaiton emails and handle
 * the activity mailing lists
 *
 * The mailing lists are identified by the activity id and the index of the cost 
 * definition.  a1@registrationhandler is the master list for the activity (a) with
 * and id of 1 (a1).  a1d3@registrationhandler.com is a list for that activity (a1) whose 
 * members signed up for the division/category (d) with an index of 3 (a1d3)
 *
 * @copyright 2013 r5k Media LLC
 * @author Nick Morlan<nick@r5kmedia.com>
 */


// uses mailgun for sending emails..
use Mailgun\Mailgun;


class SiteEmail {

    /**
     * send a confirmation email after completed registration
     *
     * @param $registrations  the registrations in the order
     * @param $activity       the activity registered for
     * @param $total_amount   total amount charged for the transaction
     * @param $email          email address to send to
     */
    static function send_registration_confirmation_email($registrations, $activity, $total_amount, $email) {
        $member = Member::find($activity->member_id);
                        
        ob_start();
        include SITE_ROOT . SITE_VIEWS . "email_templates/site/register.tpl";
        $html = ob_get_contents();
        ob_end_clean();
        
        $text = strip_tags($html);
        
        self::send_email(array('from'    => 'RegistrationHandler <noreply@registrationhandler.com>', 
                                'to'      => $email, 
                                'subject' => 'Reciept for ' . $activity->name, 
                                'text'    => $text,
                                'html'    => $html));
    }
    
    
    /**
     * send a confirmation email after completed registration
     *
     * @param $registrations  the registrations in the order
     * @param $activity       the activity registered for
     * @param $email          email address to send to
     * @param $message        custom user message
     */
    static function send_activity_invite_for($registration, $activity, $email, $message) {
        $member = Member::find($activity->member_id);
                        
        ob_start();
        include SITE_ROOT . SITE_VIEWS . "email_templates/site/invite_for_activity.tpl";
        $html = ob_get_contents();
        ob_end_clean();
        
        $text = strip_tags($html);
        
        self::send_email(array('from'    => "{$registration->first_name} {$registration->last_name} <{$registration->email}>", 
                                'to'      => $email, 
                                'subject' => 'Join me for the ' . $activity->name, 
                                'text'    => $text,
                                'html'    => $html));
    }
    
    
    /**
     * send anew member email
     *
     * @param $member  the new member
     */
    static function send_new_member_email($member) {
        ob_start();
        include SITE_ROOT . SITE_VIEWS . "email_templates/site/new_member_registration.tpl";
        $html = ob_get_contents();
        ob_end_clean();
        
        $text = strip_tags($html);
        
        self::send_email(array('from'    => 'RegistrationHandler <noreply@registrationhandler.com>', 
                                'to'      => $member->email, 
                                'subject' => 'Your New RegistrationHandler Account.', 
                                'text'    => $text,
                                'html'    => $html));
    }

    /**
     * set up a new mailing list
     *
     * @param $activity  pass the activity to set its list
     */
    static function register_mailing_lists_for($activity) {
        $mg = new Mailgun(MAILGUN_API_KEY);
        $costs = json_decode($activity->cost_definition);
        foreach($costs->registration->division as $k => $d) {
            try {
                $result = $mg->post("lists",
                      array('address'     => 'a' . $activity->id . 'd' . $k . '@registrationhandler.com',
                            'description' => $activity->name . ': ' . $d));
                if($result->http_response_code != 200)
                    LogAction::log('Error in SiteEmail::register_mailing_list_for(): ' . print_r($result, true), 'SiteEmail');        
            } catch (Exception $e) {
                LogAction::log('Exception thrown in SiteEmail::register_mailing_list_for(): ' . $e->getMessage(), 'SiteEmail');        
            }
        }
        // set up master list
        try {
            $result = $mg->post("lists",
                  array('address'     => 'a' . $activity->id . '@registrationhandler.com',
                        'description' => $activity->name . ' Mailing List'));
            if($result->http_response_code != 200)
                LogAction::log('Error in SiteEmail::register_mailing_list_for(): ' . print_r($result, true), 'SiteEmail');        
        } catch (Exception $e) {
            LogAction::log('Exception thrown in SiteEmail::register_mailing_list_for(): ' . $e->getMessage(), 'SiteEmail');        
        }
    }
    
    /**
     * remove a  mailing list
     *
     * @param $id  pass the activity remove its lists
     */
    static function remove_mailing_list_for($activity) {
        $mg = new Mailgun(MAILGUN_API_KEY);
        $costs = json_decode($activity->cost_definition);
        foreach($costs->registration->division as $k => $d) {
            try {
                $result = $mg->delete("lists/a{$activity->id}d{$k}@registrationhandler.com");
                if($result->http_response_code != 200)
                    LogAction::log('Error in SiteEmail::remove_mailing_list_for(): ' . print_r($result, true), 'SiteEmail');        
            } catch (Exception $e) {
                LogAction::log('Exception thrown in SiteEmail::remove_mailing_list_for(): ' . $e->getMessage(), 'SiteEmail');        
            }
        }
        // remove master list
        try {
            $result = $mg->delete("lists/a{$activity->id}d{$k}@registrationhandler.com");
            if($result->http_response_code != 200)
                LogAction::log('Error in SiteEmail::remove_mailing_list_for(): ' . print_r($result, true), 'SiteEmail');        
        } catch (Exception $e) {
            LogAction::log('Exception thrown in SiteEmail::remove_mailing_list_for(): ' . $e->getMessage(), 'SiteEmail');        
        }
    }
    
    
    /**
     * register a user for the mailing list
     *
     * @param $user  assoc array containing email, name, description, subscribed, vars
     * @param $id    the activity list id to subscribe to
     * @param $cid   the category/division id to subscribe to
     */
    static function register_user_for_list($user, $list) {
        //$mg = new Mailgun(MAILGUN_API_KEY, 'bin.mailgun.net', 'ad070f20', $ssl = false); //http://bin.mailgun.net/ad070f20 for debugging
        $mg = new Mailgun(MAILGUN_API_KEY);
        try {
            foreach($user as $k => $v) $params[$k] = $v;
            $params['subscribed'] = true;
            $params['upsert'] = true;
            $result = $mg->post('lists/' . $list . '@registrationhandler.com/members', $params);
            
        } catch (Exception $e) {
            //print_r($e);  
            LogAction::log('Error in SiteEmail::register_user_for_list(): ' . $e->getMessage(), 'SiteEmail');        
            //Application::set_flash('An error has occured adding \'' . $email . '\' to your mailing list.', 'error');          
        }
        //Application::set_flash("'{$email}' has been added to your selected mailing list(s).");            
    
    }
    
    static function remove_user_from_list($user, $list) {
        //$mg = new Mailgun(MAILGUN_API_KEY, 'bin.mailgun.net', 'ad070f20', $ssl = false); //http://bin.mailgun.net/ad070f20 for debugging
        $mg = new Mailgun(MAILGUN_API_KEY);
        try {
            foreach($user as $k => $v) $params[$k] = $v;
            $params['subscribed'] = false;
            $params['upsert'] = true;
            $result = $mg->post('lists/' . $list . '@registrationhandler.com/members', $params);
            
        } catch (Exception $e) {
            LogAction::log('Error in SiteEmail::register_user_for_list(): ' . $e->getMessage(), 'SiteEmail');        
        }
    
    }
    

    static function send_admin_new_member_notification($member) {
        
        $text = print_r($member, true);
        $html = nl2br($text);
        self::send_email(array('from'     => 'RegistrationHandler <noreply@registrationhandler.com>', 
                                'to'      => 'nick@r5kmedia.com', 
                                'subject' => 'New RegistrationHandler Account.', 
                                'text'    => $text,
                                'html'    => $html));
    }

    static function temp_gbyf($settings) {
        self::send_email(array( 'from'        => "{$settings['name']} <noreply@greenbulldogyouthfootball.com>", 
                                'to'         => 'nick@r5kmedia.com', 
                                'subject'    => 'GBYF Contact Form', 
                                'h:Reply-To' => $settings['email'],
                                'text'       => $settings['message']));

    }

    static function send_email($msg_array) {
        # First, instantiate the SDK with your API credentials and define your domain. 
        $mg = new Mailgun(MAILGUN_API_KEY);
        $domain = MAILGUN_DOMAIN;

        # Now, compose and send your message.
        try {
            $result = $mg->sendMessage($domain, $msg_array);
        } catch (Exception $e) {
            LogAction::log('Error in SiteEmail::send_email(): ' . $e->getMessage(), 'SiteEmail');        
        }
    
    }

}