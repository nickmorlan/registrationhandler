<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class MemberNotification extends ActiveRecordBase {



    function before_save() {
        $this->requires(array('member_id', 'type', 'title', 'description'));
    }

    /**
     * The following 6 functions are required for all ActiveRecord Objects
     * php 5.3 will be fixing some big issues
     */ 
    function __construct () {
        // setting the columns here saves database calls
        // good to do when structure is set
        $this->columns = array('member_id', 'type', 'icon', 'title', 'description', 'link', 'marked_as_read');
                               
        parent::__construct('member_notification');
    }

    static function find ($id) {
        return parent::find('member_notification', $id);
    }

    static function find_all () {
        return parent::find_all('member_notification');
    }

    static function find_all_by ($array) {
        return parent::find_all_by('member_notification', $array);
    }

    static function find_by ($array) {
        return parent::find_by('member_notification', $array);
    }

    static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
        return parent::find_all_with_pagination('member_notification', $start, $limit, $order_by, $asdc);
    }

    static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
        return parent::find_with_pagination('member_notification', $start, $limit, $order_by, $asdc, $query);
    }

    static function destroy ($id) {
        return parent::destroy('member_notification', $id);
    }

    static function create ($params) {
        if(empty($params['marked_as_read'])) $params['marked_as_read'] = 'n';
        return parent::create('member_notification', $params);
    }
    
    
    static function notify($member_id, $type, $icon, $title, $description, $link) {
        $notification = self::create(array('member_id' => $member_id,
                                            'type' => $type,
                                            'icon' => $icon,
                                            'title' => $title,
                                            'description' => $description,
                                            'link' => $link ));
        $notification->save();
    }

}