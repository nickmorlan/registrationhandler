<?php
/**
 * PACKAGE: r5k Builder
 * 
 * Cart Model
 *
 * @copyright 2009 Hardman Design
 * @author Nick Morlan<nick@hddg.net>
 */
class AdminUser extends ActiveRecordBase {


	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_create() {
		$this->requires(array('username', 'password'));
	}

	function before_save() {
	}

	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array('username', 'password', 'level', 'name', 'created_at');
	 	                       
		parent::__construct('admin_user');
	}
	
  	static function find ($id) {
	    return parent::find('admin_user', $id);
	}

	static function find_all () {
		return parent::find_all('admin_user');
	}

	static function find_all_by ($array) {
		return parent::find_all_by('admin_user', $array);
	}

	static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
		return parent::find_all_with_pagination('admin_user', $start, $limit);
	}

	static function find_by_query_with_pagination($sql, $start, $limit) {
		return parent::find_by_query_with_pagination('admin_user', $sql, $start, $limit);
	}

	static function destroy ($id) {
		return parent::destroy('admin_user', $id);
	}

	static function create ($params) {
		return parent::create('admin_user',$params);
	}
}