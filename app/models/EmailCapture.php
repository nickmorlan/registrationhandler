<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class EmailCapture extends ActiveRecordBase {


	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_save() {
		$this->validates_as_email(array('email'));
	}

	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array('email', 'source', 'query_string', 'browser_info', 'ip_address', 'conversion');
	 	                       
		parent::__construct('email_capture');
	}

  static function find ($id) {
   return parent::find('email_capture', $id);
	}

	static function find_all () {
		return parent::find_all('email_capture');
	}

	static function find_all_by ($array) {
		return parent::find_all_by('email_capture', $array);
	}

	static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
		return parent::find_all_with_pagination('email_capture', $start, $limit, $order_by, $asdc);
	}

	static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
		return parent::find_with_pagination('email_capture', $start, $limit, $order_by, $asdc, $query);
	}

	static function destroy ($id) {
		return parent::destroy('email_capture', $id);
	}

	static function create ($params) {
		$params['query_string'] = $_SERVER['QUERY_STRING'];
		$params['browser_info'] = $_SERVER['HTTP_USER_AGENT'];
		$params['ip_address']   = Application::get_real_ip();
		$params['conversion']   = 'n';
		return parent::create('email_capture', $params);
	}

}