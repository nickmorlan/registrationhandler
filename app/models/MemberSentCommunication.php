<?php
/**
 * PACKAGE: Registration Handler
 * 
 * MemberSentCommunication Model
 *
 * @copyright 2013 r5k Media llc
 * @author Nick Morlan<nick@r5kmedia.com>
 */
class MemberSentCommunication extends ActiveRecordBase {

	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_save() {
		$this->requires(array('member_id', 'message_attributes'));
	}

	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array('member_id', 'message_attributes');
		parent::__construct('member_sent_communication');
	}

	static function find ($id) {
		return parent::find('member_sent_communication', $id);
	}

	static function find_all ($mod = array()) {
		return parent::find_all('member_sent_communication', $mod);
	}

	static function find_all_by ($array, $mod = array()) {
		return parent::find_all_by('member_sent_communication', $array, $mod);
	}

	static function find_by ($array) {
		return parent::find_by('member_sent_communication', $array);
	}

	static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
		return parent::find_all_with_pagination('member_sent_communication', $start, $limit, $order_by, $asdc);
	}

	static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
		return parent::find_with_pagination('member_sent_communication', $start, $limit, $order_by, $asdc, $query);
	}

	static function fetch_as_object ($sql) {
		return parent::fetch_as_object($sql, 'member_sent_communication');
	}

	static function destroy ($id) {
		return parent::destroy('member_sent_communication', $id);
	}

	static function create ($params) {
		return parent::create('member_sent_communication',$params);
	}
}