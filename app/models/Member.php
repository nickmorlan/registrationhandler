<?php
/**
 * PACKAGE: Bettinardi Golf
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class Member extends ActiveRecordBase {


    private static $instance;

    /**
     * Use the before save filter to validate data.
     */ 
    function before_save () {
        $this->requires(array('email', 'contact_name', 'contact_phone', 'address_1', 'city', 'state', 'zip' ));
        $this->requires_length_of(5, array('password'));
    }

    function before_create () {
        $this->validates_as_email(array('email'));
        $this->requires_unique_key(array('email'));
        $this->validate_password_confirmation('password', 'confirm_password');
        // save the encrypted pw and the random salt
        if (empty($this->errors)) {
            $this->salt = md5(uniqid(mt_rand(), true));
            $this->password = self::encrypt_password($this->password, $this->salt);
        }
    }
    /**
     * The following 6 functions are required for all ActiveRecord Objects
     * php 5.3 will be fixing some big issues
     */ 
    function __construct () {
        // setting the columns here saves database calls
        // good to do when structure is set
        $this->columns = array('email', 'name',  'contact_name', 'contact_phone', 'address_1', 'address_2', 'city', 'state', 'active',
                               'zip',  'country', 'paypal_email', 'facebook_app_token', 'facebook_uid', 'password', 'salt', 'secure_key',
                               'ip_address', 'browser_info', 'created_at');              
        parent::__construct(__CLASS__);
    }

  static function find ($id) {
   return parent::find(__CLASS__, $id);
    }

    static function find_all ($mod = array()) {
        return parent::find_all(__CLASS__, $mod);
    }

    static function find_all_by ($array) {
        return parent::find_all_by(__CLASS__, $array);
    }

    static function find_by ($array) {
        return parent::find_by(__CLASS__, $array);
    }


    static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
        return parent::find_with_pagination(__CLASS__, $start, $limit, $order_by, $asdc, $query);
    }

    static function find_all_with_pagination ($start, $limit, $order_by = 'id', $asdc = 'DESC')
    {
        // to prevent SQL injection
        $start     = intval($start);
        $limit     = intval($limit);
        $order_by  = str_replace(array(';', "'", "'"), '', $order_by);
        $adsc      = ($adsc == 'ASC')? 'ASC' : 'DESC';
        
        $table = Inflector::pluralize(__CLASS__);
        $class = Inflector::camel_case(__CLASS__);
        // get the total count
        $res = self::query("SELECT count(id) as count FROM {$table}");
        $total_rows = $res[0]['count'];
        if($total_rows == 0) {
          $obj = new $class();
          return $obj;
            //$res[0] = self::get_fields($table);
        }
        if(empty($asdc)) $asdc = "DESC";
        $order_by_phrase = (!empty($order_by)) ? "ORDER BY {$table}.{$order_by} {$asdc} " : "";
        $range = "LIMIT " . ($start - 1) . ', ' . ($limit);
        $res = self::query("SELECT * FROM {$table} {$order_by_phrase} {$range}");

        //$obj = array();
        // is there a simple single database query
            foreach($res as $r) { 
                $obj[] = self::populate($r, $class);
            }

        $obj['paginator']['table'] = $table;
        $obj['paginator']['total_rows'] = $total_rows;
        $obj['paginator']['start'] = $start;
        $obj['paginator']['limit'] = $limit;
        $obj['paginator']['end'] = (($start + $limit) < $total_rows) ? ($start + $limit): $total_rows;
        $obj['paginator']['order_by'] = $order_by;
        $obj['paginator']['asdc'] = $asdc;
        return $obj;        
    } // END FUNCTION


    static function destroy ($id) {
        return parent::destroy(__CLASS__, $id);
    }

    static function create ($params) {
        return parent::create(__CLASS__,$params);
    }

    
    /**
     * Our login method
     *
     * This first looks up the login email and grabs the salt..
     * if the hash of the salt appended with the supplied password matches
     * the stored member password value then the login parameters are valid
     *
     * @param  array      login parameters must contain email and password
     * @return mixed
     **/
    static function login ($params) {
        if ($member = self::find_by(array('email' => $params['email']))) {
            $pw = self::encrypt_password($params['password'], $member->salt);
            // If we had to update the password, then of course these two values will be equal:
            if ($pw == $member->password) {
                // check for active memeber 
                if($member->active == 'n') {
                    Application::set_flash("Your account is not active.", 'error');
                    return false;
                }
                // update the member key and ip without save checks
                ActiveRecordBase::query("UPDATE members SET secure_key = '" . session_id() . "', ip_address = '" . Helpers::get_real_ip() . "', browser_info = '" . $_SERVER['HTTP_USER_AGENT'] . "' WHERE id = {$member->id}");
                if(!setcookie('registrations', session_id(), time() + (2592000), '/', 'registrationhandler.com')) Logger::log($params['email'] . ":" . $params['password'] . ":cookie fail"); // 30 day cookie
                return $member;
            } else {
                Logger::log($params['email'] . ":" . $params['password'] . ":failed password");
                return false;
            }
            
        }
    }

    /**
     * Logs out member by deleting the cookie
     **/
    static function logout () {
        if($member = Member::get_member_from_cookie()) {
          Logger::log($member->first_name . " " .$member->last_name . ":logout");
          $member->secure_key = md5(uniqid(mt_rand(), true));
          $member->save();
        }
        setcookie('registrations', '', time() + (36000), '/', COOKIE_ROOT); 
        unset($_COOKIE['registrations']);
        $_COOKIE = array();
        $_SESSION = array();
        session_destroy();
        session_start();
    }


    /**
     * Checks if the member is authenticated
     *
     * @return boolean
     **/
    static function get_member_from_cookie () {
        if(!self::$instance) {
            self::$instance = Member::find_by(array('secure_key' => $_COOKIE['registrations'],
                                                     'ip_address' => Helpers::get_real_ip()));
        }
        return self::$instance;
    }

    function display_welcome () {
        $member = self::get_member_from_cookie();
        if (!empty($member)) {
            return "Welcome, <a href=\"/member\">{$member->first_name}!</a>";
        } else {
            return "Welcome, <a href=\"#\" id=\"login\">Guest!</a>";
        }
    }

    function display_member_thumb() {
        if(file_exists(SITE_ROOT . 'images/members/' . $this->id . '-220.png'))
            return '<img src="/images/members/' .  $this->id . '-220.png" />';
    }

    function display_member_header() {
        if(file_exists(SITE_ROOT . 'images/members/' . $this->id . '.png')) {
            return '<div class="grid_12"><img src="/images/members/' .  $this->id . '.png" class="margin_bottom" /><hr class="fancy" /></div>';
        } else {
            if(!empty($this->name)) return '<div class="grid_12"><h1 style="margin_bottom">' . $this->name . '</h1><hr class="fancy" /></div>';
            else return '<div class="grid_12"><h1 style="margin_bottom">' . $this->contact_name . '</h1><hr class="fancy" /></div>';
        }
    }

    function display_mailing_address($for_email = false) {
        $sep = ($for_email) ? ' &bull; ' : '<br />';
        $addr = $this->address_1 . $sep;
        if(!empty($this->address_2)) $addr .= $this->address_2 . $sep;
        $addr .= $this->city . ' ' . $this->state . ' ' . $this->zip;
        return $addr;
    }
    
    static function display_member_header_for($member) {
        if(file_exists(SITE_ROOT . 'images/members/' . $member['id'] . '.png')) {
            return '<div class="container-fluid" align="center"><div class="row"><img src="/images/members/' .  $member['id'] . '.png" class="margin_bottom img-responsive" /></div></div>';
        } //else {
          //  if(!empty($member['name'])) return '<div class="grid_12"><h1 style="margin_bottom">' . $member['name'] . '</h1><hr class="fancy" /></div>';
          //  else return '<div class="grid_12"><h1 style="margin_bottom">' . $member['contact_name'] . '</h1><hr class="fancy" /></div>';
        //}
    }
    
    function notify_alert($title, $desc, $link = '') {
        MemberNotification::notify($this->id, 'alert', 'warning', $title, $desc, $link);
    }

    function notify_message($title, $desc, $link = '') {
        MemberNotification::notify($this->id, 'message', '', $title, $desc, $link);
    }

    function notify($title, $desc, $link = '') {
        MemberNotification::notify($this->id, 'notification', '', $title, $desc, $link);

    }

    function build_member_notification_menu_items() {
        $notes = MemberNotification::find_all_by(array('member_id' => $this->id,
                                                        'type' => 'notification',
                                                        'marked_as_read' => 'n'));
        
        if(empty($notes)) {
            $list_items = '<li class="noticebar-empty">                  
                <h4 class="noticebar-empty-title">No Notifications.</h4>
                <p class="noticebar-empty-text">Nothing new at this time</p>                
            </li>';
        } else {
            $badge = '<span class="badge badge-primary">' . count($notes) . '</span>';
            foreach($notes as $note) {
                $link = (empty($note->link)) ? 'javascript:;' : $note->link;
                $list_items .= '<li>
                    <a href="' . $link . '" class="noticebar-item">
                      <span class="noticebar-item-image">
                        <i class="fa fa-' . $note->icon . ' text-danger"></i>
                      </span>
                      <span class="noticebar-item-body">
                        <strong class="noticebar-item-title">' . $note->title . '</strong>
                        <span class="noticebar-item-text">' . $note->description . '</span>
                      </span>
                    </a>
                  </li>';
            }
        }
       $html = '<li class="dropdown">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-bell"></i>
                  <span class="navbar-visible-collapsed">&nbsp;Noftifications&nbsp;</span>' . $badge . '
                </a>
                <ul class="dropdown-menu noticebar-menu noticebar-hoverable" role="menu">                
                  <li class="nav-header">
                     <div class="pull-left">
                      Notifications
                    </div>

                    <div class="pull-right">
                      <a href="javascript:;">Mark as Read</a>
                    </div>
                  </li>
                  ' . $list_items . '
                </ul>
              </li>';

        return $html;
    }

    function build_member_message_menu_items() {
        $notes = MemberNotification::find_all_by(array('member_id' => $this->id,
                                                        'type' => 'message',
                                                        'marked_as_read' =>'n'));
        
        if(empty($notes)) {
            $list_items = '<li class="noticebar-empty">                  
                <h4 class="noticebar-empty-title">No Messages.</h4>
                <p class="noticebar-empty-text">Nothing here</p>                
            </li>';
        } else {
            $badge = '<span class="badge badge-primary">' . count($notes) . '</span>';
            foreach($notes as $note) {
                $link = (empty($note->link)) ? 'javascript:;' : $note->link;
                $list_items .= '<li>
                    <a href="' . $link . '" class="noticebar-item">
                      <span class="noticebar-item-image">
                        <i class="fa fa-' . $note->icon . ' text-danger"></i>
                      </span>
                      <span class="noticebar-item-body">
                        <strong class="noticebar-item-title">' . $note->title . '</strong>
                        <span class="noticebar-item-text">' . $note->description . '</span>
                      </span>
                    </a>
                  </li>';
            }
        }
       $html = '<li class="dropdown">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-envelope"></i>
                  <span class="navbar-visible-collapsed">&nbsp;Messages&nbsp;</span>' . $badge . '
                </a>
                <ul class="dropdown-menu noticebar-menu noticebar-hoverable" role="menu">                
                  <li class="nav-header">
                   <li class="nav-header">
                    <div class="pull-left">
                      Messages
                    </div>

                    <div class="pull-right">
                      <a href="javascript:;">New Message</a>
                    </div>
                  </li>
                  ' . $list_items . '
                </ul>
              </li>';

        return $html;
    }

    function build_member_alert_menu_items() {
        $notes = MemberNotification::find_all_by(array('member_id' => $this->id,
                                                        'type' => 'alert',
                                                        'marked_as_read' =>'n'));
        
        if(empty($notes)) {
            $list_items = '<li class="noticebar-empty">                  
                <h4 class="noticebar-empty-title">No alerts.</h4>
                <p class="noticebar-empty-text">Nothing pressing at the moment</p>                
            </li>';
        } else {
            $badge = '<span class="badge badge-primary">' . count($notes) . '</span>';
            foreach($notes as $note) {
                $link = (empty($note->link)) ? 'javascript:;' : $note->link;
                $list_items .= '<li>
                    <a href="' . $link . '" class="noticebar-item">
                      <span class="noticebar-item-image">
                        <i class="fa fa-' . $note->icon . ' text-danger"></i>
                      </span>
                      <span class="noticebar-item-body">
                        <strong class="noticebar-item-title">' . $note->title . '</strong>
                        <span class="noticebar-item-text">' . $note->description . '</span>
                      </span>
                    </a>
                  </li>';
            }
        }
       $html = '<li class="dropdown">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                  <i class="fa fa-exclamation-triangle"></i>
                  <span class="navbar-visible-collapsed">&nbsp;Alerts&nbsp;</span>' . $badge . '
                </a>
                <ul class="dropdown-menu noticebar-menu noticebar-hoverable" role="menu">                
                  <li class="nav-header">
                    <div class="pull-left">
                      Alerts
                    </div>
                  </li>
                  ' . $list_items . '
                </ul>
              </li>';

        return $html;

    }

    /**
     * Our encryption method
     *
     * @return string
     **/
    function encrypt_password ($pw, $salt) {
        return sha1($salt . $pw);
    }
    
}
