<?php
/**
 * PACKAGE: RegistrationHandler
 * 
 * Course Model
 *
 * @copyright 2010 BlackCreek Solutions 
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class Registration extends ActiveRecordBase {

	/**
	 * Use the before save filter to validate data.
	 */ 
	function before_save() {
		$this->requires(array('first_name', 'last_name'));
		$activity = Activity::find($this->activity_id);
		//print_r($this);exit;
		
		$required = array();
		$email    = array();
		$phoneUS  = array();

		$def = json_decode($activity->form_definition_json);
		foreach($def as $key => $section) {
			foreach($section->fields as $element) {
				// grab the validations if present and build the text string for the validator
				if(!empty($element->validation) && $element->custom !== 'true') {//var_dump($element->validation);exit;
					foreach($element->validation as $k => $v) {
						switch($k){
							case 'required':
								$required[] = $element->name;
								break;
							case 'email':
								$email[] = $element->name;
								break;
							case 'phoneUS':
								$phoneUS[] = $element->name;
								break;
						}
					
					}
				}
			}
		}
		//print_r($required);exit;
		if(!empty($required)) $this->requires($required);
		if(!empty($email))    $this->validates_as_email($email);
		if(!empty($phoneUS))  $this->validates_as_phoneUS($phoneUS);


	}

	/**
	 * The following 6 functions are required for all ActiveRecord Objects
	 * php 5.3 will be fixing some big issues
	 */ 
	function __construct () {
		// setting the columns here saves database calls
		// good to do when structure is set
		$this->columns = array( 'member_id', 'activity_id', 'first_name', 'last_name', 'email', 'address_1', 'address_2', 'city', 'state', 'zip', 
		                        'country', 'school', 'grade_level', 'gender', 'division', 'shirt_size', 'birth_date', 'phone', 'parent1_name', 'member_created',
		                        'parent1_phone', 'parent1_email', 'parent1_volunteer', 'parent2_name', 'parent2_phone', 'parent2_email', 'parent2_volunteer', 
		                        'date_paid', 'amount_paid', 'refund_amount', 'transaction_ref', 'hospital_name', 'r5k_pay_via', 'r5k_pay_reference', 'r5k_payment_hold', 'cc_fees',
		                        'emergency_contact', 'emergency_contact_relationship', 'emergency_contact_phone', 'physician_name', 'physician_phone',
		                        'dentist_name', 'dentist_phone', 'medical_comments', 'comments', 'notes', 'registration_amount', 'donation_amount', 'r5k_fee',
		                        'custom_fields', 'session_id', 'ip_address', 'browser_info', 'created_at');
		parent::__construct(__CLASS__);
	}

	static function find ($id) {
		return parent::find(__CLASS__, $id);
	}

	static function find_all ($mod = array()) {
		return parent::find_all(__CLASS__, $mod);
	}

	static function find_all_by ($array, $mod = array()) {
		return parent::find_all_by(__CLASS__, $array, $mod);
	}

	static function find_by ($array) {
		return parent::find_by(__CLASS__, $array);
	}

	static function find_all_with_pagination($start, $limit, $order_by = null, $asdc = null) {
		return parent::find_all_with_pagination(__CLASS__, $start, $limit, $order_by, $asdc, array('session_id'));
	}

	static function find_with_pagination($start, $limit, $order_by = null, $asdc = null, $query = null) {
		return parent::find_with_pagination(__CLASS__, $start, $limit, $order_by, $asdc, $query);
	}

	static function fetch_as_object ($sql) {
		return parent::fetch_as_object($sql, __CLASS__);
	}

	static function destroy ($id) {
		return parent::destroy(__CLASS__, $id);
	}

	static function create ($params) {
		return parent::create(__CLASS__,$params);
	}

	static function build_search_bar($activity_id) {
		// build the fields menu
		$fielddropdown = '<select name="field">';
		$fields = array('last_name', 'first_name',  'city', 'state', 'zip', 'birth_date', 'grade_level', 'school', 'division');
		foreach($fields as $field){
			$fielddropdown .= '<option value="'.$field . '"';
			if($_POST['field'] == $field) $fielddropdown .= ' selected';
			$fielddropdown .= '>'. Inflector::humanize($field).'</option>';
		}
		$fielddropdown .= '</select>';
		$searchterm = (!empty($_POST['searchterm'])) ? $_POST['searchterm'] : '' ;
		$search = '';
		$search .=  '<form name="searchbar" id="searchbar" action="/member/search_registrations/' . $activity_id . '" method="post" style="display:inline;">'
				. $fielddropdown
				. '<select name="compare">' 
				. '<option value="LIKE"';if($_POST['compare']=="LIKE") {$search .= " selected";} $search .='>Contains</option>'
				. '<option value="="';if($_POST['compare']=="=") {$search .= " selected";} $search .='>Is Equal To</option>'
				. '<option value="<"';if($_POST['compare']=="<") {$search .= " selected";} $search .='>Is Less Than</option>'
				. '<option value=">"';if($_POST['compare']==">") {$search .= " selected";} $search .='>Is Greater Than</option>'
				. '</select>'
				. '<input type="text" name="searchterm" value ="'.$searchterm.'">'
				. '<p style="display:inline;"><input type="submit" name="Search" value="Search" style="margin-left:15px;" class="freshbutton-lightblue" /></p>'		
				. '</form>';
				
		echo $search;
		
	}

	static function build_excel_export_link($method = 'to_excel', $params = array() ) {
		//$image = '<img src="/images/site/excel.gif" title="export this list to an excel file" alt="export this list to an excel file" />';
		//$params = implode('/', $params);
		//return '<a href="/export/' . $method . '/' . $params . '">' . $image . '</a>';
		$params = implode('/', $params);
		return '<p><a href="/export/' . $method . '/' . $params . '" class="btn btn-info btn-sm">Export Data</a></p>';
	}

	// build the pagination links for the
	static function build_pagination_links_for_member($arr, $activity_id) {
		$html = "<p>{$arr['start']} - {$arr['end']} of {$arr['total_rows']} ";
#		$arr['order_by'] = str_replace(' ', '', $arr['order_by']);
		$link = "/member/registrations/{$activity_id}/"; 
		if($arr['start'] != 1) {
			$html .= "| <a href=\"" . $link . '/';
			if(!empty($arr['start'])) {
				$new_start = $arr['start'] - $arr['limit'];
				if($new_start < 1) $new_start = 1;
				$html .= "start:" . $new_start . "&";
			}
			if(!empty($arr['limit'])) $html .= "limit:{$arr['limit']}/";
			if(!empty($arr['order_by'])) $html .= "order_by:{$arr['order_by']}/";
			if(!empty($arr['asdc'])) $html .= "asdc:{$arr['asdc']}/";
			$html .= "\">&lt;Prev</a> ";
		}
		if(($arr['start'] + $arr['limit']) < $arr['total_rows']) {
			$html .= "| <a href=\"" . $link . '/';
			$html .= "start:" . ($arr['start'] + $arr['limit']) . "/";
			if(!empty($arr['limit'])) $html .= "limit:{$arr['limit']}/";
			if(!empty($arr['order_by'])) $html .= "order_by:{$arr['order_by']}/";
			if(!empty($arr['asdc'])) $html .= "asdc:{$arr['asdc']}/";
			$html .= "\">Next&gt;</a>";
		}
		$pre = '';
		$onchange = "document.location.href = '" . $link . '/';
		if(!empty($arr['start'])) $onchange .= "start:{$arr['start']}/";
		if(!empty($arr['limit'])) $onchange .= "limit:'+document.getElementById('limit').value+'/";
		if(!empty($arr['order_by'])) $onchange .= "order_by:{$arr['order_by']}/";
		if(!empty($arr['asdc'])) $onchange .= "asdc:{$arr['asdc']}";
		$onchange .= "'";
		$html .= "| Show <select id=\"limit\" onchange=\"{$onchange}\">"
		      . "<option value=\"10\""; 
		      if($arr['limit'] == 10) $html .=  " selected";
		$html .= ">10</option><option value=\"25\"";
		      if($arr['limit'] == 25) $html .=  " selected";
		$html .= ">25</option><option value=\"50\"";
		      if($arr['limit'] == 50) $html .=  " selected";
		$html .= ">50</option><option value=\"100\"";
		      if($arr['limit'] == 100) $html .=  " selected";
		$html .= ">100</option></select>";
		$html .= "</p>";
		echo $html;
	}


	// fromat srting to us phone format
	static function format_to_us_phone_number($val) {
		$val = str_replace(array('(', ')', '-', '_', '', ' '), '', $val);
		if(strlen($val) == 10) $val = '(' . substr($val, 0, 3) . ') ' . substr($val, 3, 3) . '-' . substr($val, 6);
		return $val;
	}
	
	
	function populate_custom_fields() {
		$custom_fields = (array) json_decode($this->custom_fields);
		if(!empty($custom_fields)) $this->custom_fields_values = array();
		foreach($custom_fields as $key => $values) 
			foreach($values as $k => $v) {
				$field_name = $key . '_' . $k;
				$this->$field_name = $v;
				$this->custom_fields_values[$key][$k] = $v;
			}
	}
	
	
	
	function send_registration_complete_email() {
		$activity = Activity::find($this->activity_id);
		// set up all the email addresses
		$rec = array();
		if(!empty($this->email)) $rec[] = $this->email;
		if(!empty($this->parent1_email)) $rec[] = $this->parent1_email;
		if(!empty($this->parent2_email)) $rec[] = $this->parent2_email;
		$to	     = implode(', ', $rec);
		$subject = $activity->name . ' Confirmation';
		$headers = 'From: info@greenbulldogyouthfootball.com' . "\r\n" .
			   'Reply-To: noreply@greenbulldogyouthfootball.com' . "\r\n" .
		       'Content-type: text/html' . "\r\n" .
			   'X-Mailer: PHP/' . phpversion();

		
		$message = '<html><head><style>p{font-size:12px;}</style><body>';
		$message .= "<p style=\"font-size:14px;\"><strong>Your registration has been processed.</strong></p>";

		$message .= '<p><strong>' . $activity->name . '</strong>';
		$message .= '<br /><strong>For: </strong>' . $this->first_name . ' ' . $this->last_name;
		$message .= '<br /><strong>Confirmation #: </strong>' . str_replace('tok_', '', $this->transaction_ref);
		$message .= '<br /><strong>Paid: </strong> $' . $this->amount_paid . ' on ' . date('M j, Y', strtotime($this->date_paid)) . '</p>';
		$message .= '<p style="margin-top:20px;">GBYF Thanks you for your patronage.</p>';
		$message .= '<p>Please add \'info@greenbulldogyoutfootball.com\' to your email address book. Important information and announcements will be coming from that email address once the season approaches.</p>';
		$message .= '<p><a href="http://www.greenbulldogyouthfootball.com">http://www.greenbulldogyouthfootball.com</a></p>';


		$message .= '</body></html>';
		mail($to, $subject, $message, $headers);
	
	}

	function validates_as_phoneUS($a = array()) {
		foreach($a as $key) {
			if(empty($this->$key)) continue; // skip blank values
			//if(!preg_match($pattern, $this->$key)) 
			$value = str_replace(array(' ', '-', '(', ')', 'x', ','), '', $this->$key);
			if(strlen($value < 10) && !is_numeric($value)) {
				$this->errors[$key] = Inflector::humanize($key) . " must be a valid US phone number.";
			}
		}
	}

/**
 * Validates as email
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function validates_as_email($a = array()) {
		foreach($a as $key) {
			if(empty($this->$key)) continue; // skip blank values
			$this->errors[$key] = Inflector::humanize($key) . " must be a valid email address.";
			// First, we check that there's one @ symbol, 
			// and that the lengths are right.
			if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $this->$key)) return false;
			// Split it into sections to make life easier
			$email_array = explode("@", $this->$key);
			$local_array = explode(".", $email_array[0]);
			for ($i = 0; $i < sizeof($local_array); $i++) {
				if(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) return false;
			}
			// Check if domain is IP. If not, 
			// it should be valid domain name
			if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
				$domain_array = explode(".", $email_array[1]);
				if (sizeof($domain_array) < 2) return false; // Not enough parts to domain
				for ($i = 0; $i < sizeof($domain_array); $i++) {
					if(!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) return false;
				}
			}
			unset($this->errors[$key]);
		}
		return true;
	}


}