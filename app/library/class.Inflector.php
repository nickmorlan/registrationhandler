<?php
/**
 * PACKAGE: r5k Builder
 * 
 * Word inflector for databases
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class Inflector {

	/**
	 * Pluralizes table names
	 *
	 * @TODO: Add special cases.. 
	 * @param   string  $name    Word to inflect
	 * @return  string           Plural form
	 */
	static function pluralize($word) {
		if(substr($word, -1) != "y") {
			return strtolower($word) . 's';
		} else {
			return strtolower(substr($word, 0, -1)) . 'ies';
		}
	}

	/**
	 * Singularize table names
	 *
	 * @TODO: Add special cases.. condsider moving to seperate class..
	 * @param   string  $name    Word to inflect
	 * @return  string           Plural form
	 */
	static function singularize($word) {
		if(substr($word, -3) != "ies") {
			if(substr($word, -1) == 's') {
				return substr($word, 0, -1);
			} else {
				return $word;
			}
		} else {
			return substr($word, 0, -3) . 'y';
		}
	}
	
	/**
	 * CamelCase table names for class objects
	 *
	 * Replaced '_' underscores and camel cases for map between table names
	 * and the respective object classes
	 *
	 * @param   string  $name    Word to camelcase
	 * @return  string           Camel form
	 */
	static function camel_case($word) {
		$word = str_replace(" ", '', self::humanize($word));
		return $word;
	}

	/**
	 * Humanize table and field names for class objects
	 *
	 * Replaced '_' underscores with spaces and camel cases for map between table names
	 * and the respective object classes
	 *
	 * @param   string  $name    Word to camelcase
	 * @return  string           Camel form
	 */
	static function humanize($word) {
		$word = ucwords(str_replace("_", " ", $word));
		return $word;
	}


}