<?php
/**
 * PACKAGE: r5k Builder
 *
 * Simple logger cache builds a site cache to cut down on api calls
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
Class SimpleCache {

	/**
	 * Builds 'cached' file for common dynamic parts that don't really need 
	 * to be processed every time
	 */
	static function build_cache_file ($file, $html, $type) {
		$file  = SITE_ROOT . SITE_VIEWS . 'cache/' . $type . '_' . $file . '.tpl';

		// make sure decent output is captured
		if(strlen($html) > 1000) {
			if($handle = fopen($file, 'w')) {
				if(fwrite($handle, $html) === FALSE) Logger::log("unable to write to cache/_amazon.tpl");
			fclose($handle);
			}
		}
	}
	

	static function write_calendar_file ($file, $cal) {
		$file  = SITE_ROOT . SITE_VIEWS . 'cache/ics/' . $file . '.ics';

		// write the calendar file
		if(empty($cal)) {
			if(file_exists($file)) 
				if(!unlink($file)) LogAction::log('ICAL removal failure. SimpleCache::write_calendar_file() ', 'SimpleCache');
		} else {
			if($handle = fopen($file, 'w')) {
				if(fwrite($handle, $cal) === FALSE) LogAction::log('ICAL write failure. SimpleCache::write_calendar_file() ', 'SimpleCache');
			fclose($handle);
			}
		}
		
		
	}

	/* old code for reference
	static function build_buy_dot_com () {
		$file  = SITE_ROOT . SITE_VIEWS . 'cache/_buy.tpl';
		// if the file is more than a 12 hours old rebuild it
		//echo filesize($file);
		if(filemtime($file) < (time() - 43200)) {
			// grab the buy.com link feed
			$url  = 'http://rss.linksynergy.com/promo.rss?promoid=3210&token=d2bfb6e6104a475778a25acc3388c3a476d14822829fd8a0fc584fd0d619b1c9';
			$xml  = simplexml_load_string(file_get_contents($url));
			$html = '';
			foreach($xml->channel->item as $x) {
				$background = (empty($background)) ? 'background:#eee;' : '';
				$html .= '<div style="padding:3px;border:1px solid #966;' . $background .'"><p><a href="' . $x->guid . '">' . $x->title . '</a></p></div>';
			}
			//echo 'adsfasfasdfasdf'.$html;
			if(strlen($html) > 1000) {
				if($handle = fopen($file, 'w')) {
					if(fwrite($handle, $html) === FALSE) Logger::log("unable to write to cache/_buy.tpl");
				fclose($handle);
				}
			}
		}
	}
	
	static function build_walmart () {
		$file  = SITE_ROOT . SITE_VIEWS . 'cache/_walmart.tpl';
		// if the file is more than a 12 hours old rebuild it 43200
		//echo filesize($file);
		if(filemtime($file) < (time() - 43200)) {
			// grab the walmart.com link feed
			$url  = 'http://rss.linksynergy.com/promo.rss?promoid=1002&token=d2bfb6e6104a475778a25acc3388c3a476d14822829fd8a0fc584fd0d619b1c9';
			$xml  = simplexml_load_string(file_get_contents($url));
			$html = '';
			foreach($xml->channel->item as $x) {
				$background = (empty($background)) ? 'background:#eee;' : '';
				$html .= '<div style="padding:3px;border:1px solid #966;' . $background .'"><p><a href="' . $x->guid . '">' . $x->title . '</a></p></div>';
			}
			//echo 'adsfasfasdfasdf'.$html;
			if(strlen($html) > 1000) {
				if($handle = fopen($file, 'w')) {
					if(fwrite($handle, $html) === FALSE) Logger::log("unable to write to cache/_walmart.tpl");
				fclose($handle);
				}
			}
		}
	}
	*/
	
}