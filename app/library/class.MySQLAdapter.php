<?php
/**
 * PACKAGE: r5k Builder
 * 
 * MySQL Database Adaptor
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class MySQLAdapter{

  static function get_dbh() {
    $dbh = mysql_connect(DB_HOST, DB_USER, DB_PASSWORD);
    if (!$dbh)
      throw new Exception("Could not connect to database server: ".mysql_error());
    if (!mysql_select_db(DB_NAME, $dbh))
      throw new Exception("Could not select database: " . mysql_error());
    return $dbh;
    mysql_set_charset('utf8', $dbh); 
    //mysql_query("SET time_zone = '-04:00'");
  }

  static function query($query, $dbh=null) {
    
    if(DEBUG) $_SESSION['sql'][] = $query;
    //echo $query.'<br /><br />';
    $res = mysql_query($query, $dbh);
    if (!$res) {
     echo $query.'<br /><br />';
     throw new Exception("Error: " . mysql_error());
    }
    $rows = array();
    if ($res !== true && mysql_num_rows($res) != 0) {
      while ($row = mysql_fetch_assoc($res))
       $rows[] = $row;
      mysql_free_result($res);
    }
    //catch�(Exception�$e)�{�echo�'Caught�exception:�' .��$e->getMessage() .�"\n";}
    return $rows;
  }

  static function quote($string, $dbh=null, $type=null) {
    return "'" . mysql_real_escape_string($string, $dbh) . "'";
  }

  static function last_insert_id($dbh=null, $resource=null) {
    if (is_null($resource))
      return mysql_insert_id();
    else
      return mysql_insert_id($resource);
  }
}
