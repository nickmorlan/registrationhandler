<?php
/**
 * PACKAGE: r5k Builder
 * 
 * This file contains the Admin HTML helper class that builds common
 * elements as reusable code as well as Administration specific
 * functions
 *
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */

class AdminHelper {

	/**
	 * Builds pagination links
	 *
	 * @param  array  $arr    Paginator array passed from object
	 */
	static function build_admin_pagination_links($arr) {
		$html = "<p>{$arr['start']} - {$arr['end']} of {$arr['total_rows']} ";
#		$arr['order_by'] = str_replace(' ', '', $arr['order_by']);
		if($arr['start'] != 1) {
			$html .= "| <a href=\"" . SITE_ADMIN_FOLDER. Inflector::singularize($arr['table']) . '/';
			if(!empty($arr['start'])) {
				$new_start = $arr['start'] - $arr['limit'];
				if($new_start < 1) $new_start = 1;
				$html .= "start:" . $new_start . "&";
			}
			if(!empty($arr['limit'])) $html .= "limit:{$arr['limit']}/";
			if(!empty($arr['order_by'])) $html .= "order_by:{$arr['order_by']}/";
			if(!empty($arr['asdc'])) $html .= "asdc:{$arr['asdc']}/";
			$html .= "\">&lt;Prev</a> ";
		}
		if(($arr['start'] + $arr['limit']) < $arr['total_rows']) {
			$html .= "| <a href=\"" . SITE_ADMIN_FOLDER . Inflector::singularize($arr['table']) . '/';
			$html .= "start:" . ($arr['start'] + $arr['limit']) . "/";
			if(!empty($arr['limit'])) $html .= "limit:{$arr['limit']}/";
			if(!empty($arr['order_by'])) $html .= "order_by:{$arr['order_by']}/";
			if(!empty($arr['asdc'])) $html .= "asdc:{$arr['asdc']}/";
			$html .= "\">Next&gt;</a>";
		}
		$pre = '';
		$onchange = "document.location.href = '" . $pre . SITE_ADMIN_FOLDER . Inflector::singularize($arr['table']) . '/';
		if(!empty($arr['start'])) $onchange .= "start:{$arr['start']}/";
		if(!empty($arr['limit'])) $onchange .= "limit:'+document.getElementById('limit').value+'/";
		if(!empty($arr['order_by'])) $onchange .= "order_by:{$arr['order_by']}/";
		if(!empty($arr['asdc'])) $onchange .= "asdc:{$arr['asdc']}";
		$onchange .= "'";
		$html .= "| Show <select id=\"limit\" onchange=\"{$onchange}\">"
		      . "<option value=\"10\""; 
		      if($arr['limit'] == 10) $html .=  " selected";
		$html .= ">10</option><option value=\"25\"";
		      if($arr['limit'] == 25) $html .=  " selected";
		$html .= ">25</option><option value=\"50\"";
		      if($arr['limit'] == 50) $html .=  " selected";
		$html .= ">50</option><option value=\"100\"";
		      if($arr['limit'] == 100) $html .=  " selected";
		$html .= ">100</option></select>";
		$html .= "</p>";
		echo $html;
	}

	/**
	 * This method builds the search bar display
   *
	 * @param  string  $table  Table to build search
	 */
	static function build_search_bar($table, $dest = '') {
		if(empty($dest)) $dest = $table . '.php?';
		// build the fields menu
		$fielddropdown = '<select name="field">';
		$fieldselect = ActiveRecordBase::query("SHOW FIELDS FROM {$table}");
		foreach($fieldselect as $fields){
			if($fields['Field'] != 'id'){
				$fielddropdown .= '<option value="'.$fields['Field'] . '"';
				if($_POST['field'] == $fields['Field']) $fielddropdown .= ' selected';
				$fielddropdown .= '>'. Inflector::humanize($fields['Field']).'</option>';
			}
		}
		$fielddropdown .= '</select>';
		$searchterm = (!empty($_POST['searchterm'])) ? $_POST['searchterm'] : '' ;
		$search = '';
		$search .=  '<form name="searchbar" id="searchbar" action="'.$dest.'search" method="post" style="display:inline;">'
				. $fielddropdown
				. '<select name="compare">' 
				. '<option value="LIKE"';if($_POST['compare']=="LIKE") {$search .= " selected";} $search .='>Contains</option>'
				. '<option value="="';if($_POST['compare']=="=") {$search .= " selected";} $search .='>Is Equal To</option>'
				. '<option value="<"';if($_POST['compare']=="<") {$search .= " selected";} $search .='>Is Less Than</option>'
				. '<option value=">"';if($_POST['compare']==">") {$search .= " selected";} $search .='>Is Greater Than</option>'
				. '</select>'
				. '<input type="text" name="searchterm" value ="'.$searchterm.'">'
				. '<input type="submit" name="Search" value="Search" class="freshbutton-lightblue" />'		
				. '</form>';
				
		echo $search;
		
	}

	/**
	 * This method builds the search bar display
	 *
	 * @param  string  $value  Text to check and return
	 * @return string					Html 
	 */
	static function red_or_green ($value) {
		$class = 'green';
		if(in_array($value, array('n', 'no', 'false', 'f', 'N', 'F'))) $class = 'red';
		return "<strong class=\"{$class}\">{$value}</strong>";
	}

	
	/**
	 * This method creates a resized image from an uploaded file
	 *
	 * @param  string  $source      path to temp image
	 * @param  string  $dest        path and name of image to save
	 * @param  string  $new_width   scale to this many pixels wide
	 * @param  string  $new_height  (optional) scale to this many pixels high 
	 */
	static function resize_image_by_width($source, $dest, $new_width, $new_height = 0) {
		$size = getimagesize($source);
		// make sure the file is an image
		
		$allowedMimes = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif');
		if(in_array($size['mime'], $allowedMimes)) {
			$width = $size[0];
			$height = $size[1];
			

			// if the height is empty then scale it proportionally
			if(empty($new_height)) $new_height = ($new_width * $height) / $width;		
			
			if($size['mime'] == 'image/png') {
				$im = imagecreatefrompng($source);
				$new_im = ImageCreatetruecolor($new_width, $new_height);
				//imagecolortransparent($new_im, imagecolorallocatealpha($new_im, 0, 0, 0, 127));
				imagealphablending($new_im, false); // setting alpha blending on
				imagesavealpha($new_im, true); // save alphablending setting (important)
				imagecopyresampled($new_im, $im, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagepng($new_im, $dest, 9);
			} else if($size['mime'] == 'image/gif') {
				$new_im = ImageCreatetruecolor($new_width, $new_height);
				$im = imagecreatefromgif($source);
				imagecopyresampled($new_im, $im, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagegif($new_im, $dest);
			} else {
				$new_im = ImageCreatetruecolor($new_width, $new_height);
				$im = imagecreatefromjpeg($source);
				imagecopyresampled($new_im, $im, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagejpeg($new_im, $dest, 100);
			}
			imagedestroy($new_im);
		}	
	}
	
	/**
	 * This method creates a resized image from an uploaded file
	 *
	 * @param  string  $source      path to temp image
	 * @param  string  $dest        path and name of image to save
	 * @param  string  $new_width   scale to this many pixels wide
	 * @param  string  $new_height  (optional) scale to this many pixels high 
	 */
	static function resize_image_by_max($source, $dest, $max) {
		$size = getimagesize($source);
		// make sure the file is an image
		
		$allowedMimes = array('image/jpg', 'image/jpeg', 'image/png', 'image/gif');
		if(in_array($size['mime'], $allowedMimes)) {
			$width = $size[0];
			$height = $size[1];

			if($width < $height) {
				$new_height = $max;
				$new_width  = ($max * $width) / $height;				
			} else {
				$new_height = ($max * $height) / $width;
				$new_width  = $max;
			}

			if($size['mime'] == 'image/png') {
				$new_im = ImageCreatetruecolor($new_width, $new_height);
				//imagecolortransparent($new_im, imagecolorallocatealpha($new_im, 0, 0, 0, 127));
				imagealphablending($new_im, false); // setting alpha blending on
				imagesavealpha($new_im, true); // save alphablending setting (important)
				$im = imagecreatefrompng($source);
				imagecopyresampled($new_im, $im, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagepng($new_im, $dest, 9);
			} else if($size['mime'] == 'image/gif') {
				$new_im = ImageCreatetruecolor($new_width, $new_height);
				$im = imagecreatefromgif($source);
				imagecopyresampled($new_im, $im, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagepng($new_im, $dest,9);
			} else {
				$new_im = ImageCreatetruecolor($new_width, $new_height);
				$im = imagecreatefromjpeg($source);
				imagecopyresampled($new_im, $im, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
				imagepng($new_im, $dest, 9);
			}
			imagedestroy($new_im);
			//die;
		}	
	}
	
	/**
	 * This processes and saves the images uploaded from a form in the administration section
	 *
	 * @param  int     $id         record id of the object the image corrosponds to
	 * @param  string  $section    used to determine image path
	 */
	static function process_image_uploads ($id, $section, $length = 110, $width = 80) {
		$destination = SITE_ROOT . "images/{$section}/";
		$ext = 'jpg';
		if($_FILES['image']['type'] == 'image/png') $ext = 'png';
		if($_FILES['image']['type'] == 'image/gif') $ext = 'gif';
		self::resize_image_by_width($_FILES['image']['tmp_name'], $destination . $id . '.' . $ext, $length, $width);
	}

	/**
	 * This converts high ascii chars to their html equiv
	 *
	 * @param  string  $text    text to convert
	 * @return string
	 */
	static function convert_high_ascii ($text) {
		$ascii = array(chr(8216),
		               chr(8217),
		               chr(8221),
		               chr(8222),
		               chr(8482),
		               chr(174),
		               chr(169));
		$html  = array('&#8216',
		               '&#8217',
		               '&#8221',
		               '&#8222',
		               '&#8482',
		               '&#174',
		               '&#169',
		               
		               chr(169));
		return str_replace($ascii, $html, $text);
	}
	
	

	static function smart_resize_image( $file, $width = 0, $height = 0, $proportional = false, $output = 'file', $delete_original = true, $use_linux_commands = false ){
		if ( $height <= 0 && $width <= 0 ) {
		  return false;
		}
		
		$info = getimagesize($file);
		$image = '';
		
		$final_width = 0;
		$final_height = 0;
		list($width_old, $height_old) = $info;
		
		if ($proportional) {
		  if ($width == 0) $factor = $height/$height_old;
		  elseif ($height == 0) $factor = $width/$width_old;
		  else $factor = min ( $width / $width_old, $height / $height_old);  
		
		  $final_width = round ($width_old * $factor);
		  $final_height = round ($height_old * $factor);
		
		}
		else {
		  $final_width = ( $width <= 0 ) ? $width_old : $width;
		  $final_height = ( $height <= 0 ) ? $height_old : $height;
		}
		
		switch ( $info[2] ) {
		  case IMAGETYPE_GIF:
			$image = imagecreatefromgif($file);
		  break;
		  case IMAGETYPE_JPEG:
			$image = imagecreatefromjpeg($file);
		  break;
		  case IMAGETYPE_PNG:
			$image = imagecreatefrompng($file);
		  break;
		  default:
			return false;
		}
		
		$image_resized = imagecreatetruecolor( $final_width, $final_height );
		   
		if ( ($info[2] == IMAGETYPE_GIF) || ($info[2] == IMAGETYPE_PNG) ) {
		  $trnprt_indx = imagecolortransparent($image);
		
		  // If we have a specific transparent color
		  if ($trnprt_indx >= 0) {
		
			// Get the original image's transparent color's RGB values
			$trnprt_color    = imagecolorsforindex($image, $trnprt_indx);
		
			// Allocate the same color in the new image resource
			$trnprt_indx    = imagecolorallocate($image_resized, $trnprt_color['red'], $trnprt_color['green'], $trnprt_color['blue']);
		
			// Completely fill the background of the new image with allocated color.
			imagefill($image_resized, 0, 0, $trnprt_indx);
		
			// Set the background color for new image to transparent
			imagecolortransparent($image_resized, $trnprt_indx);
		
		 
		  }
		  // Always make a transparent background color for PNGs that don't have one allocated already
		  elseif ($info[2] == IMAGETYPE_PNG) {
		
			// Turn off transparency blending (temporarily)
			imagealphablending($image_resized, false);
		
			// Create a new transparent color for image
			$color = imagecolorallocatealpha($image_resized, 0, 0, 0, 127);
		
			// Completely fill the background of the new image with allocated color.
			imagefill($image_resized, 0, 0, $color);
		
			// Restore transparency blending
			imagesavealpha($image_resized, true);
		  }
		}
		
		imagecopyresampled($image_resized, $image, 0, 0, 0, 0, $final_width, $final_height, $width_old, $height_old);
		
		if ( $delete_original ) {
		  if ( $use_linux_commands )
			exec('rm '.$file);
		  else
			@unlink($file);
		}
		
		switch ( strtolower($output) ) {
		  case 'browser':
			$mime = image_type_to_mime_type($info[2]);
			header("Content-type: $mime");
			$output = NULL;
		  break;
		  case 'file':
			$output = $file;
		  break;
		  case 'return':
			return $image_resized;
		  break;
		  default:
		  break;
		}
		
		switch ( $info[2] ) {
		  case IMAGETYPE_GIF:
			imagepng($image_resized, $output);
		  break;
		  case IMAGETYPE_JPEG:
			imagepng($image_resized, $output);
		  break;
		  case IMAGETYPE_PNG:
			imagepng($image_resized, $output);
		  break;
		  default:
			return false;
		}
		
		return true;
	}	
}
