<?php
/**
 * PACKAGE: r5k Builder
 *
 * Simple logger class logs messages to file
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
Class Logger {

	/**
	 *
	 *
	 */
	static function log ($message) {
		$log_file = SITE_ROOT . 'debug.log';
		if (is_writable($log_file)) 
			if (!$handle = fopen($log_file, 'a')) {
				fwrite($handle, date('Y-m-d H:i:s') . chr(9) . microtime() . chr(9) . Application::get_real_ip() . chr(9) . $_SERVER['HTTP_REFERER'] . chr(9) . $message . "\r\n");
				fclose($handle);
			}
	}

}