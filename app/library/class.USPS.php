<?php

	 class USPS
	{
		private $user_id = null;
 		private $post_url = "http://production.shippingapis.com/ShippingAPI.dll";
 		//private $post_url = "http://testing.shippingapis.com/ShippingAPITest.dll";

 		function __construct( $uid )
 		{
 			$this->user_id = $uid;
 		}

 		/**************************************************************************************
 			GET RATE

 			ex.

 			$ordersArray = array(
 				"org_zip" => $orgZip,
 				"dest_zip" => $destZip,
 				"pkgs" => $pkg,
 				"service" => array("EXPRESS","PARCEL","PRIORITY")
 			);

 			$rv = $usps->GetRate($ordersArray);

 		**************************************************************************************/

 		function GetRate( $array )
 		{
 			$pkgArray = $array["pkgs"];
 			$destZip = $array["dest_zip"];
 			$orgZip = $array["org_zip"];
 			$service = $array["service"];
//print_r($pkgArray);
 			$returnArray = array();

 			if(is_array($service)) $numServices = count($service);
 			else $numServices = 1;

 			for($i=0; $i<$numServices; $i ++)
 			{
 				if(is_array($service)) $curService = $service[$i];
 					else $curService = $service;

 				// create xml
 				$xml = self::CreateMailXML($pkgArray, $orgZip, $destZip, $curService);
//echo $xml;
 				// send to usps and get xml back
 				$post_response = self::SendToUSPS($xml);
				//echo substr($post_response, 0, 7);
				//return $post_response;
				//if there is an error exit with the error
				if(substr($post_response, 0, 7) == '<Error>') {
					return $post_response;
				}
 				// convert to xml
 				$xml_response = self::ConvertXMLToArray($post_response);
 				$xml_response = self::CreateRateArray($xml_response);
//print_r( $xml_response);
 				$rate = 0;

 				foreach( $xml_response["RATEV3RESPONSE"] as $key => $value )
 				{

					 foreach($xml_response["RATEV3RESPONSE"][$key] as $rk => $rv)
					 {
						 if(is_array($xml_response["RATEV3RESPONSE"][$key][$rk]))
						 {
							 $rate += $xml_response["RATEV3RESPONSE"][$key][$rk]["RATE"];
						 }
					 }
				 }

 				array_push($returnArray, array("service" => $curService, "price" => $rate ));
 			}

 			return $returnArray;
		 }

		/**************************************************************************************
			Track Package

			ex.

			$usps->TrackPackage("12345678908776543221"); // Tracking number

		**************************************************************************************/

		function TrackPackage($trackingnum)
 		{
 			$tracking_num = $trackingnum;
			 $returnArray = array();
 			$detailsArray = array();

 			// create xml
 			$xml = self::CreateTrackingXML($tracking_num);

 			// send to usps
 			$post_response = self::SendToUSPS($xml);

 			// convert xml
 			$xml_response = self::ConvertXMLToArray($post_response);

 			foreach($xml_response as $index)
 			{
 				if($index["tag"] == "TRACKSUMMARY")
 				{
 					$returnArray["summery"] = $index["value"];
				 }

 				if($index["tag"] == "TRACKDETAIL")
 				{
					 array_push($detailsArray, $index["value"]);
 				}
			 }

 			$returnArray["details"] = $detailsArray;

			return $returnArray;

 		}

 		private function CreateTrackingXML($tracking_num)
		 {
 			$xml = 'API=TrackV2&XML=<TrackRequest USERID="'.$this->user_id.'">
 			<TrackID ID="'.$tracking_num.'"></TrackID>
 			</TrackRequest>';

 			return $xml;
 		}

		private function CreateMailXML($pkgArray, $orgZip, $destZip, $service)
		{
			$xml = 'API=RateV3&XML=<RateV3Request USERID="'.$this->user_id.'">';

			$x = 1; // Package number

			foreach($pkgArray as $pkg)
			{
				$weight = $pkg["weight"]*$pkg["amount"];

				if($weight > 70)
				{
					 while($weight > 70)
					 {
						$xml .= '<Package ID="'.$x++.'p">
						<Service>'.$service.'</Service>
						<ZipOrigination>'.$orgZip.'</ZipOrigination>
						<ZipDestination>'.$destZip.'</ZipDestination>
						<Pounds>70</Pounds>
							<Ounces>0</Ounces>
						<Size>regular</Size>
						<Width>15</Width>
						<Length>15</Length>
						<Height>20</Height>
						<Machinable>true</Machinable>
						</Package>';

						$weight -= 70;
					}
					$xml .= '<Package ID="'.$x++.'p">
					<Service>'.$service.'</Service>
					<ZipOrigination>'.$orgZip.'</ZipOrigination>
					<ZipDestination>'.$destZip.'</ZipDestination>
					<Pounds>'.$weight.'</Pounds>
					<Ounces>0</Ounces>
					<Size>regular</Size>
					<Width>15</Width>
					<Length>15</Length>
					<Height>20</Height>
					<Machinable>true</Machinable>
					</Package>';
				} else
				{
					$xml .= '<Package ID="'.$x++.'p">
					<Service>'.$service.'</Service>
					<ZipOrigination>'.$orgZip.'</ZipOrigination>
					<ZipDestination>'.$destZip.'</ZipDestination>
					<Pounds>'.$weight.'</Pounds>
					<Ounces>0</Ounces>
					<Size>regular</Size>
					<Width>15</Width>
					<Length>15</Length>
					<Height>20</Height>
					<Machinable>true</Machinable>
					</Package>';
				}
			}

			$xml .= "</RateV3Request>";

			return $xml;
		}

 		private function CreateRateArray($vals)
 		{
 			foreach ($vals as $xml_elem)
 			{
 				if ($xml_elem['type'] == 'open')
 				{
 					if (array_key_exists('attributes',$xml_elem))
					 {
 						list($level[$xml_elem['level']],$extra) = array_values($xml_elem['attributes']);
 					} else {
 						$level[$xml_elem['level']] = $xml_elem['tag'];
 					}
 				}  

				 if ($xml_elem['type'] == 'complete')
				 {
 					$start_level = 1;
 					$php_stmt = '$params';  

 					while($start_level < $xml_elem['level'])
 					{
 						$php_stmt .= '[$level['.$start_level.']]';
 						$start_level++;
 					}  

					$php_stmt .= '[$xml_elem[\'tag\']] = $xml_elem[\'value\'];'; 

 					eval($php_stmt);
 				}
 			}

 			return $params;
 		}

 		private function SendToUSPS($xml)
 		{
 			$request = curl_init( $this->post_url );

 			curl_setopt( $request, CURLOPT_HEADER, 0 );
 			curl_setopt( $request, CURLOPT_RETURNTRANSFER, 1 );
 			curl_setopt( $request, CURLOPT_POSTFIELDS, $xml );
 			curl_setopt( $request, CURLOPT_SSL_VERIFYPEER, FALSE ); 

 			$post_response = curl_exec( $request ); 

 			curl_close ($request);

 			return $post_response;
 		}

 		private function ConvertXMLToArray($xml)
 		{
 			$xml =  strstr($xml, "<?");

 			$xml_parser = xml_parser_create();
 			xml_parse_into_struct($xml_parser, $xml, $vals, $index);
 			xml_parser_free($xml_parser);  

 			$params = array();
 			$level = array();  

 			return $vals;
 		}
 	}

?>