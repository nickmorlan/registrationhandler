<?php
/**
 * PACKAGE: r5k Builder
 * 
 * Authorize.net code
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class AuthorizeNet {

	
	function __construct () {
	$this->post_url    = "https://secure.authorize.net/gateway/transact.dll";
	$this->post_values = array("x_login"           => "9k4QAx68Z",
											       "x_tran_key"        => "8N824NQ84vg386gj",
	
									      		 "x_version"         => "3.1",
									      		 "x_delim_data"      => "TRUE",
									      		 "x_delim_char"      => "|",
								      			 "x_relay_response"  => "FALSE",
	
							      				 "x_type"            => "AUTH_CAPTURE",
							      				 "x_method"          => "CC",
							      				 "x_card_num"        => '',
							      				 "x_card_code"       => '',
							      				 "x_exp_date"        => '',
	
						      					 "x_amount"          => '',
						      					 "x_tax"             => '0',
						      					 "x_description"     => "Bettinardi Golf",
	
							      				 "x_first_name"      => '',
								      			 "x_last_name"       => '',
								      			 "x_address"         => '',
							      				 "x_state"           => '',
							      				 "x_zip"             => ''
								      			 // Additional fields can be added here as outlined in the AIM integration
								      			 // guide at: http://developer.authorize.net
								      			 );
	
	
	}

	/**
	 * Magic methods..
	 */
	function __get ($key) {
		return (!empty($this->$key)) ? $this->$key : null;
	}

	/**
	 * Magic methods..
	 */
	function __set ($key, $val) {
		$this->$key = $val;
	}
	
	function execute () {
		// This section takes the input fields and converts them to the proper format
		// for an http post.  For example: "x_login=username&x_tran_key=a1B2c3D4"
		$post_string = "";
		foreach( $this->post_values as $key => $value ) $post_string .= "$key=" . urlencode( $value ) . "&"; 
		$post_string = rtrim( $post_string, "& " );
		
		$request = curl_init($this->post_url);                   // initiate curl object
		curl_setopt($request, CURLOPT_HEADER, 0);                // set to 0 to eliminate header info from response
		curl_setopt($request, CURLOPT_RETURNTRANSFER, 1);        // Returns response data instead of TRUE(1)
		curl_setopt($request, CURLOPT_POSTFIELDS, $post_string); // use HTTP POST to send form data
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, FALSE);    // uncomment this line if you get no gateway response.
		$post_response = curl_exec($request);                    // execute curl post and store results in $post_response
		curl_close ($request);                                   // close curl object
	
		// This line takes the response and breaks it into an array using the specified delimiting character
		$response_array = explode($this->post_values["x_delim_char"],$post_response);
		$this->response_code    = $response_array[0];
		$this->reason_code      = $response_array[2];
		$this->response_message = $response_array[3];
		$this->auth_code        = $response_array[4];
		$this->transaction_id   = $response_array[6];
		echo "<pre>";print_r($this);echo "</pre>";
		}
	
}
