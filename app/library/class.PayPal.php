<?php
/**
 * PACKAGE: CPAP Wholesale
*	
 * Class PayPal
 *
 * @copyright 2011 Hardman Design
 * @author Scott Ryan<scott@blackcreeksolutions.com>
 */
class PayPal {

	// These constants are obtained from PayPal and must be changed to those for Bettinardi's PayPal account.

	
	const API_USERNAME  = PAYPAL_API_USERNAME;
	const API_PASSWORD  = PAYPAL_API_PASSWORD;
	const API_SIGNATURE = PAYPAL_API_SIGNATURE;

	private $endpoint;
	private $host;
	private $gate;
	private $PP_RETURN;
	private $PP_CANCEL;

	function __construct($real = false) {
		$this->endpoint = '/nvp';
		//$real = true;
		if ($real) {
			// Actual transaction.
			$this->host = "api-3t.paypal.com";
			$this->gate = 'https://www.paypal.com/cgi-bin/webscr?';
		} else {
			// Sandbox transaction.
			$this->host = "api-3t.sandbox.paypal.com";
			$this->gate = 'https://www.sandbox.paypal.com/cgi-bin/webscr?';
		}

		$this->PP_RETURN = "http://youshouldsign.com/register/paypal_confirmation/";
		$this->PP_CANCEL = "http://youshouldsign.com/register/confirm/";

	}

	/**
	 * @return HTTPRequest
	 */
	private function response($data){
		$r = new HTTPRequest($this->host, $this->endpoint, 'POST', true);
		$result = $r->connect($data);
		//print_r($r);
		//$c = curl_init ($url);
		//curl_setopt ($c, CURLOPT_POST, true);
		//curl_setopt ($c, CURLOPT_POSTFIELDS, $body);
		//curl_setopt ($c, CURLOPT_RETURNTRANSFER, true);
		//$result = curl_exec ($c);		
		if ($result<400) return $r;
		return false;
	}

	private function buildQuery($data = array()){
		$data['USER'] = self::API_USERNAME;
		$data['PWD'] = self::API_PASSWORD;
		$data['SIGNATURE'] = self::API_SIGNATURE;
		$data['VERSION'] = '64.0';
		$query = http_build_query($data);
		return $query;
	}

	/**
	 * Main payment function
	 * 
	 * If OK, the customer is redirected to the PayPal gateway.
	 * If error, the error info is returned.
	 * 
	 * @param float $amount Amount (2 numbers after decimal point)
	 * @param string $desc Item description
	 * @param string $id identifier used to restore previous shopping cart session;
	 *		     probably either session ID or member ID, depending whether
	 *		     customer is a guest or a member; could also be an order ID
	 * @param string $invoice Invoice number (can be omitted)
	 * @param string $currency 3-letter currency code (USD, GBP, CZK, etc.) (USD by default)
	 * 
	 * @return array error info
	 */
	public function doExpressCheckout($amount, $desc, $id='', $currency='USD', $invoice='') {
		$data = array(
		'PAYMENTACTION' =>'Authorization',
		'PAYMENTREQUEST_n_PAYMENTACTION' => 'Authorization',
		'AMT'           => $amount,
		'RETURNURL'     => $this->PP_RETURN,
		'CANCELURL'     => $this->PP_CANCEL,
		'DESC'          => $desc,
		'NOSHIPPING'    => "1",
		'ALLOWNOTE'     => "1",
		'CURRENCYCODE'  => $currency,
		'METHOD'        => 'SetExpressCheckout');
		
		$data['CUSTOM'] = $amount.'|'.$currency.'|'.$invoice .'|' . $id;
		if ($invoice) $data['INVNUM'] = $invoice;
		$query = $this->buildQuery($data);
		//print_r($query);
		$result = $this->response($query);
		if (!$result) return false;
		$response = $result->getContent();
		$return = $this->responseParse($response);
		//print_r($return);exit;
		if ($return['ACK'] == 'Success') {
			header('Location: '.$this->gate.'cmd=_express-checkout&useraction=commit&token='.$return['TOKEN'].'');
			die();
		}
		return($return);
	}

	public function getCheckoutDetails($token){
		$data = array(
		'TOKEN' => $token,
		'METHOD' =>'GetExpressCheckoutDetails');
		$query = $this->buildQuery($data);

		$result = $this->response($query);

		if (!$result) return false;
		$response = $result->getContent();
		$return = $this->responseParse($response);
		return($return);
	}

	public function doPayment(){
		$token = $_GET['token'];
		$payer = $_GET['PayerID'];
		$details = $this->getCheckoutDetails($token);
		if (!$details) return false;
		list($amount, $currency, $invoice, $userinfo) = explode('|', $details['CUSTOM']); // $userinfo ($id) is not used
		$data = array(
		'PAYMENTACTION' => 'Authorization',
		'PAYMENTREQUEST_n_PAYMENTACTION' => 'Authorization',
		'PAYERID' => $payer,
		'TOKEN' =>$token,
		'AMT' => $amount,
		'CURRENCYCODE'=>$currency,
		'METHOD' =>'DoExpressCheckoutPayment');
		$query = $this->buildQuery($data);

		$result = $this->response($query);

		if (!$result) return false;
		$response = $result->getContent();
		$return = $this->responseParse($response);

		/* For example:
		 *
		 * [AMT] => 10.00
		 * [CURRENCYCODE] => USD
		 * [PAYMENTSTATUS] => Completed
		 * [PENDINGREASON] => None
		 * [REASONCODE] => None
		 */

		return($return);
	}

	public function doCapture($id, $amount){
		$data = array(
		'AUTHORIZATIONID' => $id,
		'AMT' => $amount,
		'CURRENCYCODE' => 'USD',
		'COMPLETETYPE' => 'Complete',
		'METHOD' =>'DoCapture');
		$query = $this->buildQuery($data);

		$result = $this->response($query);

		if (!$result) return false;
		$response = $result->getContent();
		$return = $this->responseParse($response);


		return($return);
	}

	public function doVoid($id){
		$data = array(
		'AUTHORIZATIONID' => $id,
		'METHOD' =>'DoVoid');
		$query = $this->buildQuery($data);

		$result = $this->response($query);

		if (!$result) return false;
		$response = $result->getContent();
		$return = $this->responseParse($response);


		return($return);
	}

	private function getScheme() {
		$scheme = 'http';
		if (isset($_SERVER['HTTPS']) and $_SERVER['HTTPS'] == 'on') {
			$scheme .= 's';
		}
		return $scheme;
	}

	private function responseParse($resp){
		$a=explode("&", $resp);
		$out = array();
		foreach ($a as $v){
			$k = strpos($v, '=');
			if ($k) {
				$key = trim(substr($v, 0, $k));
				$value = trim(substr($v, $k+1));
				if (!$key) continue;
				$out[$key] = urldecode($value);
			} else {
				$out[] = $v;
			}
		}
		return $out;
	}
}

?>