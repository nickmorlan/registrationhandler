<?php
/**
 * PACKAGE: r5k Builder
 * 
 * The base controller contains our base functionality for pulling the page pieces 
 * together.
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class ControllerBase {

	var $layout                    = SITE_DEFAULT_APPLICATION_TEMPLATE;     // default application template for the views
	var $view                      = '';                                    // the template to view within the application template
	var $controller                = '';                                    // the template to view within the application template
	var $controller_method         = '';                                    // the calling controller method
	var $registry                  = array();                               // registry array for holding variables or objects
	                                                                        //  pass on to the view
	var $authenticate              = array();                               // array of methods that the user needs to be authenticated for



	function __construct($params = array()) {}
	
	/**
	 * Renders the view file
	 *
	 * @param  string  $class    The classController .. to grab the correct folder
	 * @param  string  $method   The controller method will get the controller view
	 */
	function render($class, $method) {
		//$this->controller_method = $method;
		$view_folder = strtolower((str_replace('Controller', '', $class))) . '/';
		if($class !== false) {
			$view_file = SITE_ROOT . SITE_VIEWS . $view_folder . $this->view  . '.tpl';
			if(file_exists($view_file)) {
				require($view_file);
			} else {
				//Application::set_flash("{$class}->{$method} view not found.", 'error');
				$this->registry['method']    = $this->controller_method;
				$this->registry['view']      = $this->view;
				$this->registry['view_file'] = str_replace(array('/srv/www/hddg/demo.hddg.net/html/app/', '/srv/www/hddg/hddg.net/html/app/'), '', $view_file);
				header("HTTP/1.1 404 Not Found");
				require(SITE_ROOT . SITE_VIEWS . 'error/404.tpl');
			}
		}
	}

	/**
	 * Renders the page
	 *
	 * @param  string  $controller_method    The to call the corrosponding view method
	 */
	function do_page() {
		if(substr($this->controller_method, 0, 6) != '_ajax_') {
			require(SITE_ROOT . SITE_VIEWS . 'application/' . $this->layout . '.tpl');
		}
	}
	
	/**
	 * Renders the partials
	 *
	 * @param  string  $controller    The corrosponding view folder
	 * @param  string  $partial       The corrosponding partial view
	 */
	function render_partial($controller, $view, $var, $var2) {
		include(SITE_ROOT . SITE_VIEWS . $controller . '/_' . $view . '.tpl');
	}
	
	/**
	 * Pulls in the javascript files put into the registry['js_files'] array
	 * and looks for view page specific javascript files to pull in
	 */
	function load_registry_js_files() {
		foreach($this->registry['js_files'] as $file) {
			echo "<script src=\"{$file}\"></script>\r\n";
		}
		if(file_exists(SITE_ROOT . SITE_JS_VIEWS. "{$this->controller}/{$this->controller_method }.js"))
			echo '<script src="/' . SITE_JS_VIEWS . "{$this->controller}/{$this->controller_method }.js\"></script>\r\n";
	}

	/**
	 * Performs a header redirect for our application
	 *
	 * @param  string  $controller    The controller 
	 * @param  string  $method        The controller method 
	 */
	static function redirect ($controller, $method = null, $params = null) {
		@session_write_close();
		$location =  $controller;
		if(!empty($method)) $location .= '/' . $method;
		if(!empty($params)) $location .= '/' . $params;
		header("Location: https://" . SITE_URL . "/{$location}");
	} // END FUNCTION

	/**
	 * Jump to the specified URL
	 *
	 * @param  string  $location    Where to go
	 */
	function jump($location) {
		header("Location: {$location}");
	}
	
	/**
	 * Return url params with key pairs as an assoc array
	 *
	 * @param   arr  $params    Url Params
	 * @return  arr  
	 */
	function params_to_array ($params) {
		foreach($params as $p) {
			if(strpos($p, ':') !== false) {
				$a = explode(':', $p);
				$arr[$a[0]] = $a[1];
			}
		}
		return (!empty($arr))? $arr : null;
	}
	
	/**
	 * Checks if the controller method requires authentication
	 *
	 * @param   string   $method    Controller method to check
	 * @return  boolean  
	 */
	function fails_needed_authentication ($method) {
		if(($this->authenticate === true) || (in_array($method, $this->authenticate)) )
			if(empty($this->registry['member'])) return true;
		return false;
	}

	/**
	 * Checks administrator authentication
	 *
	 * @param   string   $method    Controller method to check
	 * @return  boolean  
	 */
	function admin_authenticate ($method) {
		if(array_key_exists($method, $this->methods))  {
			return (in_array($_SESSION['level'], $this->methods[$method])); 
		} else {
			return true;
		}
	}

	// so caching doesn;t trip up functionality
	function no_cache() {
		header("Cache-Control: no-cache, not-store, must-revalidate"); // HTTP/1.1
		header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past	
	}
}