<?php
/**
 * PACKAGE: r5k Builder
 *
 * This file contains the Custom Fields helper class that builds common
 * elements as reusable code
 *
 * This has many built in form elements that get build according to what ever 
 * model is passed to them.  This enables the form to display errors and error
 * messages if the model fails validation.
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 * updated February 10, 2012 by Eric Ridgley, pulled from class.HTML.php file
 */
class CustomFields {

	static function text_field ($obj, $name, $type = 'text') {
		$html = '';
		$html .= "<label>".str_replace("_", " ", $name)."</label><br/>";
		if(!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<input type=\"{$type}\" name=\"{$name}\" "
		      . "id=\"{$name}\" value=\"{$obj->ccfr[$name]}\"";
		if(!empty($obj->errors[$name])) $html .= ' class="error"';
		$html .= " /><br/>";
		echo $html;
	}

	/**
	 * Builds a form text ara
	 *
	 * @param  obj      $obj    Active Record object
	 * @param  string   $name   Name of the text area.. corrosponds to the db field
	 * @param  string   $rows   Number of rows
	 * @param  string   $cols   Number of columns
	 */
	static function text_area ($obj, $name, $rows = '', $cols = '') {
		$html = '';
		$html .= "<label>".str_replace("_", " ", $name)."</label><br/>";
	if (!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<textarea name=\"{$name}\" "
		      . "id=\"{$name}\"";
		if (!empty($rows)) $html .= " rows=\"{$rows}\"";
		if (!empty($rows)) $html .= " columns=\"{$cols}\"";
		$html .= ">{$obj->ccfr[$name]}</textarea><br />";
		echo $html;
	}

	/**
	 * Builds a form check box
	 *
	 * @param  obj      $obj    Active Record object
	 * @param  string   $name   Name of the check.. corrosponds to the db boolean
	 */
	static function check_box ($obj, $name, $value) {
		$html = '';
		#if (!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<label>".str_replace("_", " ", $name)."</label><br/>";
		foreach($value as $v){
			$html .= "{$v} <input type=\"checkbox\" name=\"".$name."_".trim($v)."\" value=\"".trim($v)."\""
				  . "id=\"{$name}\"";
			$name_value = str_replace(" ", "_", $name."_".trim($v));
			if($obj->ccfr[$name_value] == trim($v) ) $html .= " checked";
			$html .= " /><br/>\n";
		}
		echo $html;
	}

	/**
	 * Builds a form radio box
	 *
	 * @param  obj      $obj     Active Record object
	 * @param  string   $name    Name of the check..
	 * @param  string   $value   value of the check..
	 */
	static function radio_box ($obj, $name, $value) {
		$html = '';
		$html .= "<label>".str_replace("_", " ", $name)."</label><br/>";
		#if (!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		foreach($value as $v){
			$html .= "<input type=\"radio\" name=\"".trim($name)."\" value=\"".trim($v)."\" "
				  . "id=\"{$name}\" ";
			#$name_value = str_replace(" ", "_", $name);
			if ($obj->ccfr[$name] == trim($v)) $html .= "checked ";
			$html .= "/> {$v}<br/>";
			}
		echo $html;
	}

	/**
	 * Builds a form select menu
	 *
	 * @param  obj      $obj     Active Record object
	 * @param  string   $name    Name of the text field.. corrosponds to the db field
	 * @param  array    $values  Option values
	 */
	static function input_select ($obj, $name, $options = array(), $on_change = '', $other ='') {
		$html     = '';
		$selected = '';
		#if (!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<label>".str_replace("_", " ", $name)."</label><br/>";
		$html .= "<select name=\"{$name}\" id=\"{$name}\"";
		if(!empty($on_change)) $html .= " onchange=\"{$on_change}\"";
		if(!empty($other)) $html .= " {$other}\" ";
		$html .= ">";
		foreach ($options as $option => $v) {
			$selected = '';
			if($obj->ccfr[$name] == $option) $selected = ' selected';
			$html .= "<option value=\"{$option}\"{$selected}>{$v}</option>";
		}
		$html .= "</select><br />";
		echo $html;
	}



}