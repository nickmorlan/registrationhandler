<?php
/**
 * PACKAGE: r5k Builder
 *
 * Helper code
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
Class Helpers {
	/**
	 * Returns ip address
	 */
	static function get_real_ip () {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		//check ip from share internet
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		//to check ip is pass from proxy
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}

	/**
	 * Encrypts the credit card number using blowfish encryption
	 *
	 * @param   string  $num  The credit card number passed as a string
	 * @return  string
	 */
	static function encrypt_value ($val) {
			// the following encrypts our credit card info
			$cipher = mcrypt_module_open(MCRYPT_BLOWFISH,'','cbc','');
			mcrypt_generic_init($cipher, CC_ENC_KEY, '87654321');
			$encrypted = mcrypt_generic($cipher, trim($val));
			mcrypt_generic_deinit($cipher);
			return base64_encode($encrypted);
	} // END FUNCTION

	/**
	 * Decrypts the credit card number using blowfish encryption
	 *
	 * @param   string  $num  The encrypted passed as a string
	 * @return  string
	 */
	static function decrypt_value ($val) {
		if(!empty($val)) {
			// the following encrypts our credit card info
			$num = base64_decode($val);
			$cipher = mcrypt_module_open(MCRYPT_BLOWFISH,'','cbc','');
			mcrypt_generic_init($cipher, CC_ENC_KEY, '87654321');
			$decrypted = mdecrypt_generic($cipher, $num);
			mcrypt_generic_deinit($cipher);
			return trim($decrypted);
		}
	} // END FUNCTION

	/**
	 * Cleans the 'series name' for use in html links
	 *
	 * @param   string  $num  THe value to replace spaces
	 * @return  string
	 */
	static function prepare_text_param_for_url ($val) {
		return strtolower(str_replace(array(' '), '_', $val));
	}

	/**
	 * Cleans the 'series name' for use in html links
	 *
	 * @param   string  $num  THe value to replace spaces
	 * @return  string
	 */
	static function prepare_text_param_for_search ($val) {
		return strtolower(str_replace(array('_'), ' ', $val));
	}

	function remove_item_by_value($array, $val = '', $preserve_keys = true) {
		if (empty($array) || !is_array($array)) return false;
		if (!in_array($val, $array)) return $array;
	
		foreach($array as $key => $value) {
			if ($value == $val) unset($array[$key]);
		}
	
		return ($preserve_keys === true) ? $array : array_values($array);
	}

	/**
	 * makes a mysql time formatted string (HH:MM:SS) from a 1:00 pm 
	 * formatted string
	 **/
	static function make_mysql_time($time) {
		$time = trim($time);
		$ampm = substr($time, -2);
		$time = trim(substr($time, 0, -2));
		$hour_minute = explode(':', $time);
		if(strtoupper($ampm) == 'PM' && $hour_minute[0] != 12) $hour_minute[0] += 12;
		if(strtoupper($ampm) == 'AM' && $hour_minute[0] == 12) $hour_minute[0] = 0;
		return str_pad($hour_minute[0], 2, "0", STR_PAD_LEFT) . ':' . str_pad($hour_minute[1], 2, "0", STR_PAD_LEFT) . ':00';
	}

	/**
	 * displays formatted active record errors
	 **/
	static function display_errors($object) {
		if(!empty($object->errors)) {
			$html = '<div class="alert alert-danger text-left"><i class="fa fa-exclamation-triangle"></i> ';
			$html .= implode('<br /><i class="fa fa-exclamation-triangle"></i> ', $object->errors );
			return $html . '</div>';
		}
	}

	/**
	 * makes a mysql time formatted string (HH:MM:SS) from a 1:00 pm 
	 * formatted string
	 **/
	static function format_mysql_time_for_display($time) {
		$ampm = ' AM';
		$hour_minute = explode(':', $time);
		$hour_minute[0] = intval($hour_minute[0]); // kill the leading zero
		if($hour_minute[0] == 0) $hour_minute[0] = 12; // midnight
		if($hour_minute[0] > 12) {
			$hour_minute[0] -= 12;
			$ampm = ' PM';
		}
		return $hour_minute[0] . ':' . $hour_minute[1] . $ampm;
	}

	/**
	 * Shows the image if it exists or show a placeholder
	 *
	 * @param   string  $img  iamge to use
	 * @return  string  $plc 
	 */
	static function show_image_if_exists ($img, $plc = "/images/assets/no_image.gif") {
		if(file_exists(SITE_ROOT . $img)) return $img;
		return $plc;
	}

	static function state_json() {
		return "{'' : '(none selected)', 'AL' : 'Alabama' , 'AK' : 'Alaska' , 'AZ' : 'Arizona' , 'AR' : 'Arkansas' , 'CA' : 'California' , 'CO' : 'Colorado' , 'CT' : 'Connecticut' , 'DE' : 'Delaware' , 'DC' : 'District of Columbia' , 'FL' : 'Florida' , 'GA' : 'Georgia' , 'HI' : 'Hawaii' , 'ID' : 'Idaho' , 'IL' : 'Illinois' , 'IN' : 'Indiana' , 'IA' : 'Iowa' , 'KS' : 'Kansas' , 'KY' : 'Kentucky' , 'LA' : 'Louisiana' , 'ME' : 'Maine' , 'MD' : 'Maryland' , 'MA' : 'Massachesetts' , 'MI' : 'Michigan' , 'MN' : 'Minnesota' , 'MS' : 'Mississippi' , 'MO' : 'Missouri' , 'MT' : 'Montana' , 'NE' : 'Nebraska' , 'NV' : 'Nevada' , 'NH' : 'New Hampshire' , 'NJ' : 'New Jersey' , 'NM' : 'New Mexico' , 'NY' : 'New York' , 'NC' : 'North Carolina' , 'ND' : 'North Dakota' , 'OH' : 'Ohio' , 'OK' : 'Oklahoma' , 'OR' : 'Oregon' , 'PA' : 'Pennsylvania' , 'RI' : 'Rhode Island' , 'SC' : 'South Carolina' , 'SD' : 'South Dakota' , 'TN' : 'Tennessee' , 'TX' : 'Texas' , 'UT' : 'Utah' , 'VT' : 'Vermont' , 'VA' : 'Virginia' , 'WA' : 'Washington' , 'WV' : 'West Virginia' , 'WI' : 'Wisconsin' , 'WY' : 'Wyoming', 'AA' : 'AA - Armed Forces Americas', 'AE' :'AE - Armed Forces Europe', 'AP': 'AP - Armed Forces Pacific'} ";	
	}

	/**
	 * Shows the https version of the image link returned by shirtsio
	 *
	 * @param   string  $link  link to non-https image
	 * @return  string  $link 
	 */
	static function https_shirtsio_image ($link) {
		return str_replace('http://i1.ooshirts.com', 'https://8a52b406ac72b723b416-57220dc4dba4daa2628e7f66fcefe74e.ssl.cf2.rackcdn.com', $link);
	}
}