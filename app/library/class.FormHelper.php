<?php
/**
 * PACKAGE: r5k Builder
 *
 * This file contains the Form helper class that builds common
 * form elements as reusable code
 *
 * This has many built in form elements that get build according to what ever 
 * model is passed to them.  This enables the form to display errors and error
 * messages if the model fails validation.
 *
 * @copyright 2012 r5kMedia LLC
 * @author Nick Morlan<nick@r5kmedia.com>
 */

class FormHelper {

	/**
	 * Builds a form text field
	 *
	 * @param  obj      $obj    Active Record object
	 * @param  string   $name   Name of the text field.. corrosponds to the db field
	 * @param  string   $type   Text or hidden or password
	 * @param  string   $class  Class declarations
	 */
	static function text_field ($obj, $name, $type = 'text', $class = '') {
		$html = '';
		if(!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<input type=\"{$type}\" name=\"{$name}\" "
		      . "id=\"{$name}\" value=\"{$obj->$name}\"";
		if(!empty($obj->errors[$name])) $class .= ' error';
		if(!empty($class)) $html .= ' class="' . trim($class) . '"';
		$html .= " />";
		echo $html;
	}

	/**
	 * Builds a form text ara
	 *
	 * @param  obj      $obj    Active Record object
	 * @param  string   $name   Name of the text area.. corrosponds to the db field
	 * @param  string   $rows   Number of rows
	 * @param  string   $cols   Number of columns
	 */
	static function text_area ($obj, $name, $rows = '', $cols = '') {
		$html = '';
		if (!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<textarea name=\"{$name}\" "
		      . "id=\"{$name}\"";
		if (!empty($rows)) $html .= " rows=\"{$rows}\"";
		if (!empty($rows)) $html .= " columns=\"{$cols}\"";
		$html .= ">{$obj->$name}</textarea>";
		echo $html;
	}

	/**
	 * Builds a form check box
	 *
	 * @param  obj      $obj    Active Record object
	 * @param  string   $name   Name of the check.. corrosponds to the db boolean
	 */
	static function check_box ($obj, $name) {
		$html = '';
		if (!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<input type=\"checkbox\" name=\"{$name}\" "
		      . "id=\"{$name}\"";
		if(!empty($obj->$name)) $html .= " checked";
		$html .= " />";
		echo $html;
	}

	/**
	 * Builds a form radio box
	 *
	 * @param  obj      $obj     Active Record object
	 * @param  string   $name    Name of the check..
	 * @param  string   $value   value of the check..
	 */
	static function radio_box ($obj, $name, $value) {
		$html = '';
		if (!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<input type=\"radio\" name=\"{$name}\" value=\"{$value}\" "
		      . "id=\"{$name}\" ";
		if ($obj->$name == $value) $html .= "checked ";
		$html .= "/> {$value}";
		echo $html;
	}

	/**
	 * Builds a form select menu
	 *
	 * @param  obj      $obj     Active Record object
	 * @param  string   $name    Name of the text field.. corrosponds to the db field
	 * @param  array    $values  Option values
	 */
	static function input_select ($obj, $name, $options = array(), $on_change = '', $other = '') {
		$html     = '';
		$selected = '';
		if (!empty($obj->errors[$name])) $html .= '<span class="form_error">' .$obj->errors[$name] . '</span><br />';
		$html .= "<select name=\"{$name}\" id=\"{$name}\"";
		if(!empty($on_change)) $html .= " onchange=\"{$on_change}\"";
		if(!empty($other)) $html .= " {$other}\" ";
		$html .= ">";
		foreach ($options as $option => $v) {
			$selected = '';
			if($obj->$name == $option) $selected = ' selected="selected"';
			$html .= "<option value=\"{$option}\"{$selected}>{$v}</option>";
		}
		$html .= "</select>";
		echo $html;
	}

	/**
	 * Builds a date select menu -mm-dd-yyy
	 *
	 * @param  obj      $obj    Active Record object
	 * @param  string   $name   Name of the date field.. corrosponds to the db field
	 */
	static function input_date_select ($obj, $name) {
		$month = $name . '_mm';
		$day   = $name . '_dd';
		$year  = $name . '_yyyy';
		$html  = '';
		$year_val  = (empty($obj->$name)) ? date('Y', strtotime('today')) : date('Y', strtotime($obj->$name));
		$month_val = (empty($obj->$name)) ? date('n', strtotime('today')) : date('n', strtotime($obj->$name));
		$day_val   = (empty($obj->$name)) ? date('j', strtotime('today')) : date('j', strtotime($obj->$name));
		$selected_month[$month_val] = ' selected';
		$selected_day[$day_val]     = ' selected';
		$selected_year[$year_val]   = ' selected';
		$month_array  = array('Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec');
		if (!empty($obj->errors[$name])) $html .= '<div class="form_error">' .$obj->errors[$name] . '</div>';
		$html .= "<select name=\"{$name}_mm\" id=\"{$name}_mm\" class=\"dateselect\">";
		for ($i = 1; $i <= 12; $i++) $html .= "<option value=\"{$i}\"" . $selected_month[$i] . ">" . $month_array[$i-1] . "</option>";
		$html .= "</select>";
		$html .= "<select name=\"{$name}_dd\" id=\"{$name}_dd\" class=\"dateselect\">";
		for ($i = 1; $i <= 31; $i++) $html .= "<option value=\"{$i}\"" . $selected_day[$i] . ">{$i}</option>";
		$html .= "</select>";
		$html .= "<select name=\"{$name}_yyyy\" id=\"{$name}_yyyy\" class=\"dateselect\">";
		for ($i = date('Y'); $i >= (date('Y') - 100); $i--) $html .= "<option value=\"{$i}\"" . $selected_year[$i] . ">{$i}</option>";
		$html .= "</select>";
		echo $html;
	}

	/**
	 * Builds a date input menu -mm-dd-yyy
	 *
	 * @param  obj      $obj    Active Record object
	 * @param  string   $name   Name of the date field.. corrosponds to the db field
	 */
	static function input_date_fields($obj, $field) {
		$month = $name . '_mm';
		$day   = $name . '_dd';
		$year  = $name . '_yyyy';
		$html  = '';
		
		if(!empty($obj->$field)) {
			$m_value = date('n', strtotime($obj->$field));
			$d_value = date('j', strtotime($obj->$field));
			$y_value = date('Y', strtotime($obj->$field));
			$m_js = '';
			$d_js = '';
			$y_js = '';
		} else {
			$m_value = 'MM';
			$d_value = 'DD';
			$y_value = 'YYYY';
			$m_js = ' onfocus="if(this.value==\'MM\'){this.value=\'\'}" onblur="if(this.value==\'\'){this.value=\'MM\'}"';
			$d_js = ' onfocus="if(this.value==\'DD\'){this.value=\'\'}" onblur="if(this.value==\'\'){this.value=\'DD\'}"';
			$y_js = ' onfocus="if(this.value==\'YYYY\'){this.value=\'\'}" onblur="if(this.value==\'\'){this.value=\'YYYY\'}"';
		}
		$html .= "<div class=\"left\"><input type=\"text\" style=\"width:30px\" name=\"{$field}_mm\" id=\"{$field}_mm\" value=\"{$m_value}\"{$m_js} /></div><div class=\"left\" style=\"margin-top:10px;\">&nbsp;/&nbsp;</div>";
		$html .= "<div class=\"left\"> <input type=\"text\" style=\"width:30px\" name=\"{$field}_dd\" id=\"{$field}_dd\" value=\"{$d_value}\"{$d_js} /></div><div class=\"left\" style=\"margin-top:10px;\">&nbsp;/&nbsp;</div>";
		$html .= "<div class=\"left\"> <input type=\"text\" style=\"width:60px\" name=\"{$field}_yyyy\" id=\"{$field}_yyyy\" value=\"{$y_value}\"{$y_js} /></div><div class=\"clear\"></div>";
		return $html;
		
	}

	/**
	 * Build a state select menu
	 *
	 * @param  obj      $obj     Active Record object
	 * @param  string   $name    Name of the text field.. corrosponds to the db field
	 */
	function input_state_select ($obj, $name, $other = '') {
		self::input_select($obj, $name, array('' => '(none selected)', 'AL' => 'Alabama' , 'AK' => 'Alaska' , 'AZ' => 'Arizona' , 'AR' => 'Arkansas' , 'CA' => 'California' , 'CO' => 'Colorado' , 'CT' => 'Connecticut' , 'DE' => 'Delaware' , 'DC' => 'District of Columbia' , 'FL' => 'Florida' , 'GA' => 'Georgia' , 'HI' => 'Hawaii' , 'ID' => 'Idaho' , 'IL' => 'Illinois' , 'IN' => 'Indiana' , 'IA' => 'Iowa' , 'KS' => 'Kansas' , 'KY' => 'Kentucky' , 'LA' => 'Louisiana' , 'ME' => 'Maine' , 'MD' => 'Maryland' , 'MA' => 'Massachesetts' , 'MI' => 'Michigan' , 'MN' => 'Minnesota' , 'MS' => 'Mississippi' , 'MO' => 'Missouri' , 'MT' => 'Montana' , 'NE' => 'Nebraska' , 'NV' => 'Nevada' , 'NH' => 'New Hampshire' , 'NJ' => 'New Jersey' , 'NM' => 'New Mexico' , 'NY' => 'New York' , 'NC' => 'North Carolina' , 'ND' => 'North Dakota' , 'OH' => 'Ohio' , 'OK' => 'Oklahoma' , 'OR' => 'Oregon' , 'PA' => 'Pennsylvania' , 'RI' => 'Rhode Island' , 'SC' => 'South Carolina' , 'SD' => 'South Dakota' , 'TN' => 'Tennessee' , 'TX' => 'Texas' , 'UT' => 'Utah' , 'VT' => 'Vermont' , 'VA' => 'Virginia' , 'WA' => 'Washington' , 'WV' => 'West Virginia' , 'WI' => 'Wisconsin' , 'WY' => 'Wyoming', 'AA' => 'AA - Armed Forces Americas', 'AE' =>'AE - Armed Forces Europe', 'AP'=> 'AP - Armed Forces Pacific'), '', $other); 
	}

	/**
	 * Build a country select menu
	 *
	 * @param  obj      $obj     Active Record object
	 * @param  string   $name    Name of the text field.. corrosponds to the db field
	 */
	function input_country_select ($obj, $name) {
		if(empty($obj)) {
			$obj = new member();
			$obj->ship_to_country = 'US';
		}
		self::input_select($obj, $name, array("US" => "United States", "CA" => "Canada", "" => "", "AF" => "Afghanistan", "AL" => "Albania", "DZ" => "Algeria", "AS" => "American Samoa", "AD" => "Andorra", "AO" => "Angola", "AI" => "Anguilla", "AQ" => "Antarctica", "AG" => "Antigua and Barbuda", "AR" => "Argentina", "AM" => "Armenia", "AW" => "Aruba", "AU" => "Australia", "AT" => "Austria", "AZ" => "Azerbaijan", "BS" => "Bahamas", "BH" => "Bahrain", "BD" => "Bangladesh", "BB" => "Barbados", "BY" => "Belarus", "BE" => "Belgium", "BZ" => "Belize", "BJ" => "Benin", "BM" => "Bermuda", "BT" => "Bhutan", "BO" => "Bolivia", "BA" => "Bosnia and Herzegowina", "BW" => "Botswana", "BV" => "Bouvet Island", "BR" => "Brazil", "IO" => "British Indian Ocean Territory", "BN" => "Brunei Darussalam", "BG" => "Bulgaria", "BF" => "Burkina Faso", "BI" => "Burundi", "KH" => "Cambodia", "CM" => "Cameroon", "CV" => "Cape Verde", "KY" => "Cayman Islands", "CF" => "Central African Republic", "TD" => "Chad", "CL" => "Chile", "CN" => "China", "CX" => "Christmas Island", "CC" => "Cocos (Keeling) Islands", "CO" => "Colombia", "KM" => "Comoros", "CG" => "Congo", "CD" => "Congo, the Democratic Republic of the", "CK" => "Cook Islands", "CR" => "Costa Rica", "CI" => "Cote d'Ivoire", "HR" => "Croatia (Hrvatska)", "CU" => "Cuba", "CY" => "Cyprus", "CZ" => "Czech Republic", "DK" => "Denmark", "DJ" => "Djibouti", "DM" => "Dominica", "DO" => "Dominican Republic", "TP" => "East Timor", "EC" => "Ecuador", "EG" => "Egypt", "SV" => "El Salvador", "GQ" => "Equatorial Guinea", "ER" => "Eritrea", "EE" => "Estonia", "ET" => "Ethiopia", "FK" => "Falkland Islands (Malvinas)", "FO" => "Faroe Islands", "FJ" => "Fiji", "FI" => "Finland", "FR" => "France", "FX" => "France, Metropolitan", "GF" => "French Guiana", "PF" => "French Polynesia", "TF" => "French Southern Territories", "GA" => "Gabon", "GM" => "Gambia", "GE" => "Georgia", "DE" => "Germany", "GH" => "Ghana", "GI" => "Gibraltar", "GR" => "Greece", "GL" => "Greenland", "GD" => "Grenada", "GP" => "Guadeloupe", "GU" => "Guam", "GT" => "Guatemala", "GN" => "Guinea", "GW" => "Guinea-Bissau", "GY" => "Guyana", "HT" => "Haiti", "HM" => "Heard and Mc Donald Islands", "VA" => "Holy See (Vatican City State)", "HN" => "Honduras", "HK" => "Hong Kong", "HU" => "Hungary", "IS" => "Iceland", "IN" => "India", "ID" => "Indonesia", "IR" => "Iran (Islamic Republic of)", "IQ" => "Iraq", "IE" => "Ireland", "IL" => "Israel", "IT" => "Italy", "JM" => "Jamaica", "JP" => "Japan", "JO" => "Jordan", "KZ" => "Kazakhstan", "KE" => "Kenya", "KI" => "Kiribati", "KP" => "Korea, Democratic People's Republic of", "KR" => "Korea, Republic of", "KW" => "Kuwait", "KG" => "Kyrgyzstan", "LA" => "Lao People's Democratic Republic", "LV" => "Latvia", "LB" => "Lebanon", "LS" => "Lesotho", "LR" => "Liberia", "LY" => "Libyan Arab Jamahiriya", "LI" => "Liechtenstein", "LT" => "Lithuania", "LU" => "Luxembourg", "MO" => "Macau", "MK" => "Macedonia, The Former Yugoslav Republic of", "MG" => "Madagascar", "MW" => "Malawi", "MY" => "Malaysia", "MV" => "Maldives", "ML" => "Mali", "MT" => "Malta", "MH" => "Marshall Islands", "MQ" => "Martinique", "MR" => "Mauritania", "MU" => "Mauritius", "YT" => "Mayotte", "MX" => "Mexico", "FM" => "Micronesia, Federated States of", "MD" => "Moldova, Republic of", "MC" => "Monaco", "MN" => "Mongolia", "MS" => "Montserrat", "MA" => "Morocco", "MZ" => "Mozambique", "MM" => "Myanmar", "NA" => "Namibia", "NR" => "Nauru", "NP" => "Nepal", "NL" => "Netherlands", "AN" => "Netherlands Antilles", "NC" => "New Caledonia", "NZ" => "New Zealand", "NI" => "Nicaragua", "NE" => "Niger", "NG" => "Nigeria", "NU" => "Niue", "NF" => "Norfolk Island", "MP" => "Northern Mariana Islands", "NO" => "Norway", "OM" => "Oman", "PK" => "Pakistan", "PW" => "Palau", "PA" => "Panama", "PG" => "Papua New Guinea", "PY" => "Paraguay", "PE" => "Peru", "PH" => "Philippines", "PN" => "Pitcairn", "PL" => "Poland", "PT" => "Portugal", "PR" => "Puerto Rico", "QA" => "Qatar", "RE" => "Reunion", "RO" => "Romania", "RU" => "Russian Federation", "RW" => "Rwanda", "KN" => "Saint Kitts and Nevis",  "LC" => "Saint LUCIA", "VC" => "Saint Vincent and the Grenadines", "WS" => "Samoa", "SM" => "San Marino", "ST" => "Sao Tome and Principe",  "SA" => "Saudi Arabia", "SN" => "Senegal", "SC" => "Seychelles", "SL" => "Sierra Leone", "SG" => "Singapore", "SK" => "Slovakia (Slovak Republic)", "SI" => "Slovenia", "SB" => "Solomon Islands", "SO" => "Somalia", "ZA" => "South Africa", "GS" => "South Georgia and the South Sandwich Islands", "ES" => "Spain", "LK" => "Sri Lanka", "SH" => "St. Helena", "PM" => "St. Pierre and Miquelon", "SD" => "Sudan", "SR" => "Suriname", "SJ" => "Svalbard and Jan Mayen Islands", "SZ" => "Swaziland", "SE" => "Sweden", "CH" => "Switzerland", "SY" => "Syrian Arab Republic", "TW" => "Taiwan, Province of China", "TJ" => "Tajikistan", "TZ" => "Tanzania, United Republic of", "TH" => "Thailand", "TG" => "Togo", "TK" => "Tokelau", "TO" => "Tonga", "TT" => "Trinidad and Tobago", "TN" => "Tunisia", "TR" => "Turkey", "TM" => "Turkmenistan", "TC" => "Turks and Caicos Islands", "TV" => "Tuvalu", "UG" => "Uganda", "UA" => "Ukraine", "AE" => "United Arab Emirates", "GB" => "United Kingdom", "UM" => "United States Minor Outlying Islands", "UY" => "Uruguay", "UZ" => "Uzbekistan", "VU" => "Vanuatu", "VE" => "Venezuela", "VN" => "Viet Nam", "VG" => "Virgin Islands (British)", "VI" => "Virgin Islands (U.S.)", "WF" => "Wallis and Futuna Islands", "EH" => "Western Sahara", "YE" => "Yemen", "YU" => "Yugoslavia", "ZM" => "Zambia", "ZW" => "Zimbabwe") );
	}

	/**
	 * Build a title select menu
	 *
	 * @param  obj      $obj     Active Record object
	 * @param  string   $name    Name of the text field.. corrosponds to the db field
	 */
	function input_title_select ($obj, $name, $other = '') {
		self::input_select($obj, $name, array("" => "(none)", "Mr" => "Mr", "Miss" => "Miss", "Mrs" => "Mrs", "Ms" => "Ms"), '', $other); 
	}

	function input_shipping_select ($obj, $name, $other = '') {
		self::input_select($obj, $name, array('USPS Priority Mail' => 'USPS Priority Mail', 'UPS Ground' => 'UPS Ground', 'UPS 3 Day Select' => 'UPS 3 Day Select', 'UPS 2nd Day Air' => 'UPS 2nd Day Air', 'UPS Next Day Air' => 'UPS Next Day Air', 'UPS Next Day Air' => 'UPS Next Day Air', 'UPS Next Day Air' => 'UPS Next Day Air'), '', $other); 
	}

	/**
	 * Builds pagination links
	 *
	 * @param  array  $arr    Paginator array passed from object
	 */
	static function build_pagination_links($arr) {
		$html = "{$arr['start']} - {$arr['end']} of {$arr['total_rows']} ";
		if ($arr['start'] != 1) {
			$html .= "| <a href=\"/{$arr['table']}/";
			if (!empty($arr['start'])) {
				$new_start = $arr['start'] - $arr['limit'];
				if($new_start < 1) $new_start = 1;
				$html .= "start:" . $new_start . "/";
			}
			if (!empty($arr['limit'])) $html .= "limit:{$arr['limit']}/";
			if (!empty($arr['order_by'])) $html .= "order_by:{$arr['order_by']}/";
			if (!empty($arr['asdc'])) $html .= "asdc:{$arr['asdc']}/";
			$html .= "\">&lt;Prev</a> ";
		}
		if(($arr['start'] + $arr['limit']) < $arr['total_rows']) {
			$html .= "| <a href=\"/{$arr['table']}/";
			$html .= "start:" . ($arr['start'] + $arr['limit']) . "/";
			if (!empty($arr['limit'])) $html .= "limit:{$arr['limit']}/";
			if (!empty($arr['order_by'])) $html .= "order_by:{$arr['order_by']}/";
			if (!empty($arr['asdc'])) $html .= "asdc:{$arr['asdc']}/";
			$html .= "\">Next&gt;</a>";
		}
		$pre = '';
		$onchange = "document.location.href = '" . $pre . "/{$arr['table']}/";
		if (!empty($arr['start'])) $onchange .= "start:{$arr['start']}/";
		if (!empty($arr['limit'])) $onchange .= "limit:'+document.getElementById('limit').value+'/";
		if (!empty($arr['order_by'])) $onchange .= "order_by:{$arr['order_by']}/";
		if (!empty($arr['asdc'])) $onchange .= "asdc:{$arr['asdc']}";
		$onchange .= "'";
		$html .= "| Show <select id=\"limit\" onchange=\"{$onchange}\">"
		      . "<option value=\"10\"";
		      if ($arr['limit'] == 10) $html .=  " selected";
		$html .= ">10</option><option value=\"25\"";
		      if ($arr['limit'] == 25) $html .=  " selected";
		$html .= ">25</option><option value=\"50\"";
		      if ($arr['limit'] == 50) $html .=  " selected";
		$html .= ">50</option><option value=\"100\"";
		      if ($arr['limit'] == 100) $html .=  " selected";
		$html .= ">100</option></select>";
		echo $html;
	}

	/**
	 * Builds sort order links
	 *
	 * @param  string  $field     Name of the filed to sort by
	 * @param  string  $display   Text to display for the link - defaults to $field
	 */
	function sort_paginator_by ($reg, $column, $display = null) {
		$order_by    = strtolower($column);
		if (strtolower($reg['order_by']) == $order_by) {
			$asdc = ($reg['asdc'] == 'ASC') ? 'DESC' : 'ASC';
		} else {
			$asdc = 'DESC';
		}
		$display     = (empty($display))? $column : $display;
		$link = "<a href=\"/{$reg['table']}"
		      . "/order_by:{$order_by}"
		      . "/asdc:{$asdc}\">"
		      . $display . "</a>";
		echo $html;

	}


	/**
	 * Builds select menu from an active record object collection
	 *
	 * @param  string  $parent     Name of the active record object
	 * @param  string  $child      Name of the active record child object
	 * @param  bool    $blank      Should there be a blank default value
	 * @param  string  $field      Name of the field to use for the select value
	 * @param  string  $id         Name of the primary key for the select value
	 */
	static function build_select_from_collection ($parent, $child, $blank = false, $field = 'name', $id = 'id') {
		$childClass = Inflector::camel_case($child);
		$html = 'None yet defined.';
		if (method_exists($childClass, 'find_all')) {
			$html = '';
			$o = new $childClass();
			$c = $o->find_all();
			$child = strtolower($child);
			$child_id = $child . '_' . $id;
			if (!empty($parent->errors[$child_id])) $html .= '<div class="form_error">' .$parent->errors[$child_id] . '</div>';
			$html .= "<select id=\"{$child_id}\" name=\"{$child_id}\">";
			if($blank) $html .= '<option>&lt;none&gt;</option>';
			foreach ($c as $obj){
				$html .= '<option value="' . $obj->$id . '"';
				if (is_array($parent)) {
					if($parent[$child_id] == $obj->$id) $html .= " selected";
				} else if (is_object($parent)) {
					if($parent->$child_id == $obj->$id) $html .= " selected";
				} else {
					if($parent == $obj->$id) $html .= " selected";
				}
				$html .= '>' . $obj->$field . '</option>';
			}
			$html .= '</select>';
		}
		echo $html;
	}

	/** 
	 * Alternare tr background colors
	 *
	 * @param string  $color1   first color
	 * @param string  $color2   second color
	 */
	function alternate_tr_colors ($color1, $color2 = '') {
		$this->tr_count ++;
		$color = (($this->tr_count % 2) == 1) ? $color1 : $color2;
		if(!empty($color)) {
			return "<tr style=\"background-color:#{$color};margin:5px;\">";
		} else {
			return '<tr style="margin:5px;">';
		}
	}
}