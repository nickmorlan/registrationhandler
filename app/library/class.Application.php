<?php
/**
 * PACKAGE: r5k Builder
 * 
 * This file controls how the application is routed and controlled
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class Application {
	
	/**
	 * Sets application flash messages
	 *
	 * @param  string  $message    Text to be displayed
	 * @param  string  $type       Type of message for stylesheets
	 */
	static function set_flash ($message, $type = null) {
		switch ($type) {
			case 'status':
				$_SESSION['flash'] .= '<div class="status grid_12 alert alert-info" id="flash"><p>'.$message.'</p></div><br />';
				//$_SESSION['flash'] .= '<script>$(\'#flash.status\').effect("highlight", {color:\'#93ca8d\'}, 3000).delay(5000).slideToggle(\'fast\');</script>';
				break;
			case 'success':
				$_SESSION['flash'] .= '<div class="success grid_12 alert alert-success" id="flash"><p>'.$message.'</p></div><br />';
				//$_SESSION['flash'] .= '<script>$(\'#flash.success\').effect("highlight", {color:\'#93ca8d\'}, 3000).delay(3000).slideToggle(\'fast\');</script>';
				break;
			case 'error':
				$_SESSION['flash'] .= '<div class="error grid_12 alert alert-danger" id="flash"><p>'.$message.'</p></div><br />';
				//$_SESSION['flash'] .= '<script>$(\'#flash.error\').effect("highlight", {color:\'#ff9999\'}, 3000);</script>';
				break;
			case 'warning':
				$_SESSION['flash'] .= '<div class="error grid_12 alert alert-warning" id="flash"><p>'.$message.'</p></div><br />';
				//$_SESSION['flash'] .= '<script>$(\'#flash.warning\').effect("highlight", {color:\'#ff9999\'}, 3000);</script>';
				break;
			default:
				$_SESSION['flash'] .= '<div class="status grid_12 alert alert-info" id="flash"><a class="close" data-dismiss="alert" href="#" aria-hidden="true">&times;</a><i class="fa fa-check-square fa-lg"></i> '.$message.'</div><br />';
				//$_SESSION['flash'] .= '<script>$(\'#flash.status\').effect("highlight", {color:\'#93ca8d\'}, 3000).delay(3000).slideToggle(\'fast\');</script>';
				break;

		}
	}

	/**
	 * Returns the application messages and clears them from memory
	 */
	static function flash () {
		if(!empty($_SESSION['flash'])) {
			echo $_SESSION['flash'];
			$_SESSION['flash'] = '';
		}
	} // END FUNCTION

	/**
	 * Returns ip address
	 */
	static function get_real_ip () {
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
		//check ip from share internet
			$ip=$_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		//to check ip is pass from proxy
			$ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	
	/**
	 * Determines actions for the app to take 
	 *
	 * This fucntion routes the application to the correct
	 * controller files and calls the appropriate methods.
	 * It also will check for needed authentication.
	 * @param  $uri   URI String to parse
	 */
	static function delegate ($uri) {
		//Logger::log('application');
		// break apart the URI
		$start = microtime();
		$pieces = explode('/', $uri);	

		// find the right controller from the parameters
		$controller = array_shift($pieces);
		
		// check if it is a subfolder
		$controller_file  = SITE_ROOT . SITE_CONTROLLERS . strtolower($controller);
		if(file_exists($controller_file)) {
			$controller      = array_shift($pieces);
			if(empty($controller)) $controller = 'index';
			$controller_file = $controller_file . '/' . strtolower($controller);
		}
		
		$controller_file  = $controller_file . '_controller' . SITE_EXT;
		$controller_obj   = Inflector::camel_case($controller) . 'Controller';

		// if the controller file doesn't exists use the default and reset the passed parameters
		if(!file_exists($controller_file)) {
			$controller_obj  = Inflector::camel_case(SITE_DEFAULT_CONTROLLER) . 'Controller';
			$controller_file = SITE_ROOT . SITE_CONTROLLERS . strtolower(SITE_DEFAULT_CONTROLLER) . '_controller' . SITE_EXT;
			$pieces          = explode('/', $uri);
		}
		require($controller_file);
		
		// make a new controller object
		$obj = new $controller_obj($pieces);
	
		if($obj->layout != 'admin') {
			$obj->registry['member'] = Member::get_member_from_cookie();
		}
		
		// default controller method is index
		$controller_method = SITE_DEFAULT_CONTROLLER_METHOD;
		
		// check for a controller method in the passed params
		if (!empty($pieces[0])) {
			if (method_exists($obj, $pieces[0])) {
				$controller_method = array_shift($pieces);
			}
		}		
		
		// the view defaults to the corresponding controller method..
		$obj->view = $controller_method;
		$obj->controller_method = $controller_method;
		
		// check for admin authentication
		if(substr($obj->layout, 0, 5) == 'admin') {
			if(!$obj->admin_authenticate($controller_method)) {
				Application::set_flash("You must be logged in as an administrator to view this section.", 'error');
				$obj->redirect('_r5kadmin/index/login');
				exit;
			}
		}

		// check for needed authentication
		if($obj->fails_needed_authentication($controller_method)) {
			Application::set_flash("You must be logged in to view this section.", 'error');
			ControllerBase::redirect('member', 'login');
			exit;
		}

		// check if there are params to be sent.. send an array or just the value if only 1 parameter
		if(count($pieces) == 1) $pieces = $pieces[0];
		if(method_exists($obj, $controller_method)) $obj->$controller_method($pieces);
		$obj->registry['execution'] = microtime() - $start;
		$obj->do_page();
	}
			
	/**
	 * Open file in utf8 format... for reading in files during import
	 *
	 * @param  string  filename  File to open
	 */
	static function fopen_utf8 ($filename){
    $encoding='';
    $handle = fopen($filename, 'r');
    $bom = fread($handle, 2);
    rewind($handle);
   
    if($bom === chr(0xff).chr(0xfe)  || $bom === chr(0xfe).chr(0xff)){
            // UTF16 Byte Order Mark present
            $encoding = 'UTF-16';
    } else {
        $file_sample = fread($handle, 1000) + 'e'; //read first 1000 bytes
        // + e is a workaround for mb_string bug
        rewind($handle);
   
        $encoding = mb_detect_encoding($line , 'UTF-8, UTF-7, ASCII, EUC-JP,SJIS, eucJP-win, SJIS-win, JIS, ISO-2022-JP');
    }
    if ($encoding){
        stream_filter_append($handle, 'convert.iconv.'.$encoding.'/UTF-8');
    }
    return  ($handle);
   }
   
	/**
	 * This function simply checks for the cookie to be set and shows or hides the element
	 */
   static function social_media_stripe () {
   
   }
   
}