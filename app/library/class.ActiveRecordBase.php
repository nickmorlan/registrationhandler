<?php
/**
 * PACKAGE: r5k Builder
 *
 * This is the base class for our Active Record Database Objects.
 *
 *
 * @copyright 2010 BlackCreek Solutions
 * @author Nick Morlan<nick@blackcreeksolutions.com>
 */
class ActiveRecordBase {

	protected $table;                          // corosponding database table
	protected $primary_key;                    // primary key for the database
	protected $columns       = array();        // holds the db fields for the object
	var       $errors        = array();        // hold error information about our object
	protected $new_record    = false;          // key for update or insert operations
	protected static $dbh;                     // handles the database connection
	
	function __construct($table) {
		$this->table = Inflector::pluralize($table);
		if(empty($this->columns)) {
			$q = "DESCRIBE " . $this->table;
			$res = self::query($q);
			foreach($res as $r){
				$this->columns[$r['Field']] = '';
				$this->$r['Field'] = '';
			}
		} else {
			$temp = $this->columns;
			$this->columns = array();
			foreach($temp as $r){
				$this->columns[$r] = '';
				$this->$r = '';
			}		
		}
		if(empty($this->primary_key)) $this->primary_key = 'id';
	}
	
	/**
	 * Magic methods..
	 */
	function __get($key) {
		if(!empty($this->$key))
			return $this->$key;
	}

	/**
	 * Magic methods..
	 */
	function __set($key, $val) {
		$this->$key = $val;
	}
	
	static function get_fields($table) {
		$q = "DESCRIBE {$table}";
		$res = self::query($q);
		foreach($res as $r){
			$columns[$r['Field']] = $r['Field'];
		}
		return $columns;
	}

	/**
	 * Find returns the database record with the passed id
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $class  The calling class
	 * @param  int     $id     The primary key
	 * @return obj             The database row as an object
	 */
	static function find ($class, $id)
	{
		$table = Inflector::pluralize($class);
		$id = intval($id);
		$res = self::query("SELECT * FROM {$table} WHERE `id` = {$id} LIMIT 1");
		if(!empty($res)) {
			return self::populate($res[0], $class);		
		} else {
			return false;
		}
	} // END FUNCTION

	/**
	 * Finds and returns all database records
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $class  The calling class
	 * @param  array   $mod    Associateive array of query modifiers
	 * @return obj             The database row as an object
	 */
	static function find_all ($class, $mod = array())
	{
		$table = Inflector::pluralize($class);
		$sql = "SELECT * FROM {$table}";
		if (!empty($mod)) 
			foreach($mod as $k => $v) $sql .= " {$k} {$v}";
		
		$res = self::query($sql);
		foreach($res as $r) {
			$obj[] = self::populate($r, $class);
		}
		return $obj;		
	} // END FUNCTION

	/**
	 * Finds and returns all database records by the passed values
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $field  The field to search on
	 * @param  string  $value  The value to search for
	 * @return obj             The database row as an object
	 */
	static function find_all_by ($class, $array, $mod = array())
	{
		$table = Inflector::pluralize($class);
		$sql = "SELECT * FROM {$table} WHERE ";
		foreach($array as $key => $value) $sql .= $key . " = " . self::quote($value) . ' AND ';
		$sql = substr($sql, 0, -5);
		if (!empty($mod)) 
			foreach($mod as $k => $v) $sql .= " {$k} {$v}";
		$res = self::query($sql);
		if(self::num_rows($res) == 1) {
			$obj[] = self::populate($res[0], $class);
		} else {
			foreach($res as $r) {
				$obj[] = self::populate($r, $class);
			}
		}
		if(!empty($obj)) {
			return $obj;
		} else {
			return array();
		}
	} // END FUNCTION

	/**
	 * Finds and returns a single database record by the passed values
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $field  The field to search on
	 * @param  string  $value  The value to search for
	 * @return obj             The database row as an object
	 */
	static function find_by ($class, $array)
	{
		$table = Inflector::pluralize($class);
		$sql = "SELECT * FROM {$table} WHERE ";
		foreach($array as $key => $value) $sql .= $key . " = " . self::quote($value) . ' AND ';
		$sql = substr($sql, 0, -5);
		$res = self::query($sql);
		if(!empty($res)) {
			return self::populate($res[0], $class);
		} else {
			return false;
		}
	} // END FUNCTION

	/**
	 * Find returns the database record with the passed id
	 *
	 * Lots of tricky stuff in here
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $class     The calling class
	 * @param  int     $start     The starting number
	 * @param  int     $limit     The number of returned rows
	 * @param  string  $order_by  Order by field
	 * @param  int     $asdc      ASC/DSC
	 * @return array(obj)      Collection of objects
	 */

	static function find_all_with_pagination ($class, $start, $limit, $order_by = 'id', $asdc = 'DESC', $ignore = array())
	{
		// to prevent SQL injection
		$start     = intval($start);
		$limit     = intval($limit);
		$order_by  = str_replace(array(';', "'", "'"), '', $order_by);
		$adsc      = ($adsc == 'ASC')? 'ASC' : 'DESC';
		
		$table = Inflector::pluralize($class);
		$class = Inflector::camel_case($class);
		// get the total count
		$res = self::query("SELECT * FROM {$table}");
		$total_rows = count($res);
		if($total_rows == 0) {
		  $obj = new $class();
		  return $obj;
			//$res[0] = self::get_fields($table);
		}
		$fields = '';
		$join = '';
		
		// check for fields ending with '_id' to create joins for the object
		// using $c to mark the array to pull out the correct object from the
		// query when we are constructing our results
		$children = array();
		$c = 1;
		foreach($res[0] as $k => $v) {
			$fields .= "{$table}.{$k} as t0_{$k}, ";
			if(substr($k, -3) == '_id' && (!in_array($k, $ignore))) {
				if(!in_array($k, array('parent_user_id', 'child_user_id'))) {
					$children[$c] = substr($k, 0, -3);
					$c++;
				}
			}
		}

		// build fields and joins for the children
		if(!empty($children)) {
			$c = 1;
			foreach($children as $k => $v) {
				$child_table = Inflector::pluralize($v);
				$child_fields = self::get_fields($child_table);
				foreach($child_fields as $cf) $fields .= "{$child_table}.{$cf} as t{$c}_{$cf}, ";
				$join .= "LEFT OUTER JOIN {$child_table} ON ({$child_table}.id = {$table}.{$v}_id "
				       . "AND {$table}.{$v}_id <> '0') ";
				$c++;
			}
		}

		// remove the trailing comma from the query
		$fields = substr($fields, 0, -2) . ' ';
		if(empty($asdc)) $asdc = "DESC";
		$order_by_phrase = (!empty($order_by)) ? "ORDER BY {$table}.{$order_by} {$asdc} " : "";
		$range = "LIMIT " . ($start - 1) . ', ' . ($limit);
		$res = self::query("SELECT {$fields} FROM {$table} {$join} {$order_by_phrase} {$range}");

		//$obj = array();
		// is there a simple single database query
		if(empty($join)) { 
			foreach($res as $r) { 
				$obj[] = self::populate($r, $class);
			}
		// or is there a join for multiple
		} else {
			$child = array();
			foreach($res as $re => $r) { 
				$class = Inflector::camel_case($class);
				$o = new $class();
				foreach($r as $k => $v) {					
					// take out the 't0_' prefix in the results for the main table to populate our object
					if(substr($k, 0, 3) == 't0_') {
						$key = str_replace('t0_', '', $k);
						$o->$key = $v; 
					// otherwise populate a child array with the fields/values
					} else {
						// this gives us a limit of 10 children associations.
						// grabs the number built in the query
						$c = substr($k, 1, 1);
						$child[$c][substr($k, 3)] = $v; 
					}
					// grab all child records..
					foreach($child as $k => $v) {
						$key = $children[$k];
						$o->$key = $v;
					}
				}
				$obj[] = $o;
			}
		}
		//PRINT_R($children);

		$obj['paginator']['table'] = $table;
		$obj['paginator']['total_rows'] = $total_rows;
		$obj['paginator']['start'] = $start;
		$obj['paginator']['limit'] = $limit;
		$obj['paginator']['end'] = (($start + ($limit - 1)) < $total_rows) ? ($start + ($limit - 1)): $total_rows;
		$obj['paginator']['order_by'] = $order_by;
		$obj['paginator']['asdc'] = $asdc;
		return $obj;		
	} // END FUNCTION

	static function find_with_pagination ($class, $start, $limit, $order_by, $asdc, $query)
	{
		// to prevent SQL injection
		$start     = intval($start);
		$limit     = intval($limit);

		$table = Inflector::pluralize($class);
		$class = Inflector::camel_case($class);
		// get the total count
		$res = self::query($query);
		$total_rows = count($res);//echo $total_rows;exit;
		if($total_rows == 0) {
		  return $false;
			//$res[0] = self::get_fields($table);
		}

		// remove the trailing comma from the query
		//$fields = substr($fields, 0, -2) . ' ';
		if(empty($asdc)) $asdc = "DESC";
		$order_by_phrase = (!empty($order_by)) ? "ORDER BY {$table}.{$order_by} {$asdc} " : "";
		$range = "LIMIT " . ($start - 1) . ', ' . ($limit);
		$res = self::query("{$query} {$order_by_phrase} {$range}");

		foreach($res as $r) { 
			$obj[] = self::populate($r, $class);
		}

		$obj['paginator']['table'] = $table;
		$obj['paginator']['total_rows'] = $total_rows;
		$obj['paginator']['start'] = $start;
		$obj['paginator']['limit'] = $limit;
		$obj['paginator']['end'] = (($start + ($limit - 1)) < $total_rows) ? ($start + ($limit - 1)): $total_rows;
		return $obj;		
	} // END FUNCTION

	/**
	 * Creates a new database record
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $class      The calling class
	 * @param  array   $params     The array of fields 
	 */
	static function create ($class, $params)
	{
		$class = Inflector::camel_case($class);
		$obj = new $class();
		foreach($params as $p => $v) {
			if(array_key_exists($p, $obj->columns)) {
				$obj->$p = $v;
			}
		}
		$obj->created_at = date('Y-m-d');
		$obj->new_record = true;
		return $obj;
	} // END FUNCTION

	/**
	 * Edits a database record
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $class      The calling class
	 * @param  array   $params     The array of fields 
	 */
	function edit ($params)
	{
		foreach($params as $p => $v) {
			if(array_key_exists($p, $this->columns)) {
				$this->$p = $v;
			}
		}
	} // END FUNCTION

	/**
	 * Saves a database record
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $class      The calling class
	 * @param  array   $params     The array of fields 
	 */
	function save ()
	{		
		if(is_callable(array($this, 'before_save')))	$this->before_save();
		
		if($this->new_record) 
			if(is_callable(array($this, 'before_create'))) $this->before_create();
		
		
		if(empty($this->errors)) {
			if($this->new_record) {
				foreach($this->columns as $p => $v) {
					if($p != 'updated_at') {
						$column[] = $p;
						$value[] = self::quote($this->$p);
					}
				}
				$columns = implode(', ', $column);
				$values = implode(', ', $value);
				$res = self::query("INSERT INTO {$this->table} ({$columns}) VALUES ({$values})");
				if($res === false) {
					throw new Exception ("Error in saving record.");
				} else {
					$this->id = self::last_insert_id();
					return true;
				}
			} else {
				$updates = '';
				foreach($this->columns as $p => $v) {
						$updates[] =  '`' . $p . '` = ' . self::quote($this->$p);
				}
				$updates = implode(', ', $updates);
				$pk = $this->primary_key;
				$res = self::query("UPDATE {$this->table} SET {$updates} "
												 . "WHERE `{$this->primary_key}` = " . $this->$pk);
			}
			if($res === false) {
				throw new Exception ("Error in saving record.");
			} else {
				return true;
			}
		} else {
			return false;
		}
	} // END FUNCTION

	/**
	 * Deletes the database record
	 *
	 * Need to pass a class to the find from the extended 
	 * model classes due to php's handling of calling classes..
	 * MyExtendedActiveRecordClass::find(1) would return 'ActiveRecord'
	 * when using __CLASS__
	 *
	 * @param  string  $class  The calling class
	 * @param  int     $id     The primary key
	 */
	static function destroy ($class, $id)
	{
		$table = Inflector::pluralize($class);
		$id = intval($id);
		$res = self::query("DELETE FROM {$table} WHERE `id` = {$id} LIMIT 1");
		if($res === false) {
			throw new Exception ("Error in deleting record..");
		} else {
			return true;
		}
	} // END FUNCTION

	/**
	 * Returns an object
	 *
	 * @param  string  $sql      SQL query for object
	 * @return obj             New object corosponding to database fields
	 */
	static function fetch_as_object ($sql, $class)
	{
		$res = self::query($sql);
		$obj = array();
		foreach($res as $r) {
			$obj[] = self::populate($r, $class);
		}
		return $obj;
	} // END FUNCTION

	/**
	 * Populates the object
	 *
	 * @param  array   $r       Associative array containg database fields and values
	 * @param  string  $class  Class name for object
	 * @return obj             New object corosponding to database fields
	 */
	static function populate ($arr, $class)
	{
		$class = Inflector::camel_case($class);
		$obj = new $class();
		$key = '';
		$obj->new_record = false;
		foreach($arr as $r => $val) {
			// take out the 't0_' prefix in the results for the main table to populate our object
			// if it is coming from a table join
			if(stristr($r, 't0_') !== false) {
				$key = str_replace('t0_', '', $r);
				$obj->$key = $val;
			} else {
				$obj->$r = $val;
			}
		}
		return $obj;
	} // END FUNCTION

	/**
	 * Returns mysql formatted date
	 *
	 * @param  string   $value       value to convert to yyyy-mm-dd
	 * @return string                
	 */
	static function mysql_date($value) {
		return self::quote(date('Y-m-d', strtotime($value)));
	}

	/**
	 * This method builds the search bar display
   *
	 * @param  string  $table  Table to build search
	 */
	static function build_search_bar($table, $dest = '') {
		if(empty($dest)) $dest = $table . '.php?';
		// build the fields menu
		$fielddropdown = '<select name="field">';
		$fieldselect = ActiveRecordBase::query("SHOW FIELDS FROM {$table}");
		foreach($fieldselect as $fields){
			if($fields['Field'] != 'id'){
				$fielddropdown .= '<option value="'.$fields['Field'] . '"';
				if($_POST['field'] == $fields['Field']) $fielddropdown .= ' selected';
				$fielddropdown .= '>'. Inflector::humanize($fields['Field']).'</option>';
			}
		}
		$fielddropdown .= '</select>';
		$searchterm = (!empty($_POST['searchterm'])) ? $_POST['searchterm'] : '' ;
		$search = '';
		$search .=  '<form name="searchbar" id="searchbar" action="'.$dest.'search" method="post" style="display:inline;">'
				. $fielddropdown
				. '<select name="compare">' 
				. '<option value="LIKE"';if($_POST['compare']=="LIKE") {$search .= " selected";} $search .='>Contains</option>'
				. '<option value="="';if($_POST['compare']=="=") {$search .= " selected";} $search .='>Is Equal To</option>'
				. '<option value="<"';if($_POST['compare']=="<") {$search .= " selected";} $search .='>Is Less Than</option>'
				. '<option value=">"';if($_POST['compare']==">") {$search .= " selected";} $search .='>Is Greater Than</option>'
				. '</select>'
				. '<input type="text" name="searchterm" value ="'.$searchterm.'">'
				. '<input type="submit" name="Search" value="Search" />'		
				. '</form>';
				
		echo $search;
		
	}

	/**
   *  DB specific stuff
   */
  static function &get_dbh() {
    if (!self::$dbh) {
      self::$dbh = MySQLAdapter::get_dbh();
    }
    return self::$dbh;
  }

  static function query($query) {
    $dbh =& self::get_dbh();
    return MySQLAdapter::query($query, $dbh);
  }
  
  static function quote($string, $type = null) {
    $dbh =& self::get_dbh();
    return MySQLAdapter::quote(trim($string), $dbh);
  }

  static function last_insert_id($resource = null) {
    $dbh =& self::get_dbh();
    return MySQLAdapter::last_insert_id($resource, $dbh);
  }

  static function num_rows($resource = null) {
    return count($resource);
  }

/**
 *  Validation methods
 */

/**
 *  Requires
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function requires($a = array()) {
		foreach($a as $key) {
			if(empty($this->$key)) {
				$this->errors[$key] = Inflector::humanize($key) . " is a required value.";
			}
		}
	}

/**
 *  Requires Certain length
 *
 * @params  int    $l     Length required
 * @params  array  $a     Array of required fields the object needs
 */
	function requires_length_of($l, $a = array()) {
		foreach($a as $key) {
			if(strlen($this->$key) < $l) {
				$this->errors[$key] = Inflector::humanize($key) . " needs to be {$l} characters long.";
			}
		}
	}
	
/**
 *  Requires Over Certain length
 *
 * @params  int    $l     Length required
 * @params  array  $a     Array of required fields the object needs
 */
	function requires_length_of_at_least($l, $a = array()) {
		foreach($a as $key) {
			if(strlen($this->$key) < $l) {
				$this->errors[$key] = Inflector::humanize($key) . " needs to be at least {$l} characters long.";
			}
		}
	}
	
/**
 *  Requires Either
 * 
 * Makes sure that only one value is entered out of a 
 * collection of fields
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function requires_either($a = array()) {
		$flag = false;
		$empty = true;
		foreach($a as $key) {
			if(!empty($this->$key)) {
				if($flag) {
					$fields = Inflector::humanize(implode(" and ", $a));
					foreach($a as $key) {
						$this->errors[$key] = "There must be only one value entered between " . $fields;
					}
					break;
				} else {
					$flag = true;
					$empty = false;
				}
			}
		}
		if($empty) {
			$fields = Inflector::humanize(implode(" or ", $a));
			foreach($a as $key) {
				$this->errors[$key] = "There must be a value entered for either " . $fields;
			}
		}
	}

/**
 *  Requires Unique Key 
 *
 * @params  array  $a     Array of required uniques the object needs
 */
	function requires_unique_key($a = array()) {
		foreach($a as $key) {
			$res = $this->query("SELECT * FROM {$this->table} "
												. "WHERE {$key} = " . self::quote($this->$key) );
			if(!empty($res)) $this->errors[$key] = "Sorry, that {$key} already exists.";
		}
	}

/**
 *  Requires Unique Key Pairs - (for join tables)
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function requires_unique_key_pairs($a = array()) {
		$res = $this->query("SELECT * FROM {$this->table} "
		                  . "WHERE {$a[0]} = " . self::quote($_POST[$a[0]]) . " "
		                  . "AND {$a[1]} = " . self::quote($_POST[$a[1]]));
		if(!empty($res)) {
			$this->errors['Major'] = "That {$a[0]} and {$a[1]} combination already exists.";
		}
	}

/**
 *  Validates as Numeric
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function validates_as_numeric($a = array()) {
		$pattern = "/[0-9,\.]/";
		foreach($a as $key) {
			//if(!preg_match($pattern, $this->$key)) 
			if(!is_numeric($this->$key)) {
				$this->errors[$key] = Inflector::humanize($key) . " must be a numeric value.";
			}
		}
	}

	function validates_as_phoneUS($a = array()) {
		foreach($a as $key) {
			//if(!preg_match($pattern, $this->$key)) 
			$value = str_replace(array(' ', '-', '(', ')', 'x', ','), '', $this->$key);
			if(strlen($value < 10) && !is_numeric($value)) {
				$this->errors[$key] = Inflector::humanize($key) . " must be a valid US phone number.";
			}
		}
	}

/**
 *  Validates as date
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function validates_as_date($a = array()) {
		foreach($a as $key) 
		 //match the format of the date
		  if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $this->$key, $parts)) {
			//check weather the date is valid of not
			if(!checkdate($parts[2],$parts[3],$parts[1]))
				$this->errors[$key] = Inflector::humanize($key) . " must be a valid date.";
		} else {
			$this->errors[$key] = Inflector::humanize($key) . " must be a valid date.";
		}
	}

/**
 * Validate Age
 *
 * @params  int  $a      Required age
 * @params  int  $key    Key, databse field
 */
	function requires_age_of($a, $key) {
		if(mktime(0, 0, 0, date("m"),   date("d"),   date("Y")-$a) <= strtotime($this->$key) )
			$this->errors[$key] = "You must be at least {$a} years old to register.";
	}
	
/**
 * Validates as email
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function validates_as_email($a = array()) {
		foreach($a as $key) {
			$this->errors[$key] = Inflector::humanize($key) . " must be a valid email address.";
			// First, we check that there's one @ symbol, 
			// and that the lengths are right.
			if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $this->$key)) return false;
			// Split it into sections to make life easier
			$email_array = explode("@", $this->$key);
			$local_array = explode(".", $email_array[0]);
			for ($i = 0; $i < sizeof($local_array); $i++) {
				if(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) return false;
			}
			// Check if domain is IP. If not, 
			// it should be valid domain name
			if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
				$domain_array = explode(".", $email_array[1]);
				if (sizeof($domain_array) < 2) return false; // Not enough parts to domain
				for ($i = 0; $i < sizeof($domain_array); $i++) {
					if(!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) return false;
				}
			}
			unset($this->errors[$key]);
		}
		return true;
	}
/**
 * Validates as email
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function validates_as_email_no_object($key) {
		// First, we check that there's one @ symbol, 
		// and that the lengths are right.
		if (!ereg("^[^@]{1,64}@[^@]{1,255}$", $key)) return false;
		// Split it into sections to make life easier
		$email_array = explode("@", $key);
		$local_array = explode(".", $email_array[0]);
		for ($i = 0; $i < sizeof($local_array); $i++) {
			if(!ereg("^(([A-Za-z0-9!#$%&'*+/=?^_`{|}~-][A-Za-z0-9!#$%&'*+/=?^_`{|}~\.-]{0,63})|(\"[^(\\|\")]{0,62}\"))$", $local_array[$i])) return false;
		}
		// Check if domain is IP. If not, 
		// it should be valid domain name
		if (!ereg("^\[?[0-9\.]+\]?$", $email_array[1])) {
			$domain_array = explode(".", $email_array[1]);
			if (sizeof($domain_array) < 2) return false; // Not enough parts to domain
			for ($i = 0; $i < sizeof($domain_array); $i++) {
				if(!ereg("^(([A-Za-z0-9][A-Za-z0-9-]{0,61}[A-Za-z0-9])|([A-Za-z0-9]+))$", $domain_array[$i])) return false;
			}
		}
		return true;
	}
/**
 *  Validate password confirmation
 *
 * @params  string  $p     Password
 * @params  string  $pc    Password Confirmation
 */
	function validate_password_confirmation($p, $pc) {
		if($_POST[$p] != $_POST[$pc]) $this->errors[$pc] = "Passwords do not match.";
	}


	function validate_cc_number ($cc, $cc_type) {
		include SITE_ROOT . SITE_LIBRARY . '_phpcreditcard.php';
		if (!checkCreditCard ($_POST[$cc], $_POST[$cc_type], $ccerror, $ccerrortext)) {
			$this->errors[$cc] = $ccerrortext;
  	}
	}
	
/**
 *  Check Unique Key Pairs 
 * 
 * If there is a double index on the table set the save() to update instead
 * of insert
 *
 * @params  array  $a     Array of required fields the object needs
 */
	function check_key_pairs($a = array()) {
		$res = $this->query("SELECT * FROM {$this->table} "
		                  . "WHERE {$a[0]} = " . self::quote($this->$a[0]) . " "
		                  . "AND {$a[1]} = " . self::quote($this->$a[1]));
		if(!empty($res)) {
			$this->new_record = false;
		}
	}


	/**
	 * Returns list of errors in error array
	 *
	 * @return string                
	 */
	function display_errors() {
		if(!empty($this->errors)) {
			foreach($this->errors as $err) {
				$html .= "<li>$err</li>";
			}
			return '<p class="error"><strong>Please correct the following error(s) and resubmit.</strong></p><ul id="error_list">' . $html . '</ul>';
		}
	}


}

