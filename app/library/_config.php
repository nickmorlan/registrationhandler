<?php
/**
 * PACKAGE: Registrations
 *
 * Configuration File
 *
 * @copyright 2012  r5kMedia llc
 * @author Nick Morlan<nick@r5kmedia.com>
 */

ini_set('display_errors', 0);
ini_set('session.cookie_domain', 'www.registrationhandler.com');
//error_reporting(E_ERROR | E_PARSE);
error_reporting(E_ALL | E_STRICT);
date_default_timezone_set('UTC');

if ($_SERVER['REMOTE_ADDR'] == "24.165.166.192") define('DEBUG', false);
else define('DEBUG', false); 

define('SITE_DEFAULT_CONTROLLER', 'index');
define('SITE_DEFAULT_CONTROLLER_METHOD', 'index');
define('SITE_DEFAULT_APPLICATION_TEMPLATE', 'application');

define('DB_HOST', 'localhost');
define('DB_NAME', 'registrations');
define('DB_USER', 'registrations');
define('DB_PASSWORD', 'xxxx');

define('SITE_ROOT', '/var/www/vhosts/registrationhandler.com/www/');
define('SITE_URL', 'www.registrationhandler.com');
define('COOKIE_ROOT', 'registrationhandler.com');
define('COMPOSER_ROOT', '/var/www/vhosts/registrationhandler.com/composer/');
define('SITE_NL', chr(13));
define('SITE_EXT', '.php');
define('SITE_LOGS', 'logs/');
define('SITE_CONTROLLERS', 'app/controllers/');
define('SITE_MODELS', 'app/models/');
define('SITE_VIEWS', 'app/views/');
define('SITE_JS_VIEWS', 'js/views/');
define('SITE_LIBRARY', 'app/library/');
define('SITE_ADMIN_INBOX', SITE_ROOT .'InBox/');
define('SITE_ADMIN_IMAGE_INBOX', SITE_ROOT . 'InBox_Images/');
define('SITE_ADMIN_TRASH', SITE_ROOT .'Trash/');
define('SITE_ADMIN_FOLDER', '');
define('SITE_ADMIN_ORDERS', SITE_ROOT . 'orders/');
define('OHIO_SALES_TAX', 0.0625);
define('SITE_EMAIL_ADDRESS', 'nick@r5kmedia.com');
define('SITE_LOG_NOTIFY_EMAIL_ADDRESS', 'nick@r5kmedia.com');

define('SITE_META_TITLE', 'RegistrationHander');

// payment processors 
define("AUTHORIZENET_API_LOGIN_ID", "xxxx");
define("AUTHORIZENET_TRANSACTION_KEY", "xxxx"); 
define("AUTHORIZENET_SANDBOX", true); 
define("PAYPAL_API_USERNAME", "xxxx");
define("PAYPAL_API_PASSWORD", "xxxx");
define("PAYPAL_API_SIGNATURE", "xxxx");
define("STRIPE_PUBLISHABLE_KEY_TEST_MODE", "xxxx");
define("STRIPE_SECRET_KEY_TEST_MODE", "xxxx");
define("STRIPE_TEST_CLIENT_ID", "xxxx");

define("STRIPE_PUBLISHABLE_KEY", "xxxx");
define("STRIPE_SECRET_KEY", "xxxx");
define("STRIPE_LIVE_CLIENT_ID", "xxxx");

// mail gun
define("MAILGUN_API_KEY", 'xxxx');
define("MAILGUN_PUBLIC_KEY", 'xxxx');
define("MAILGUN_DOMAIN", 'registrationhandler.com');

// facebook api
define("FB_APP_ID", "xxxx");
define("FB_APP_SECRET", "xxxx"); 

// shirtsio api
define("SHIRTSIO_API_KEY", "xxxx"); 

session_name('registrations');
session_start();
if(!isset($_SESSION['flash'])) $_SESSION['flash'] = '';
$_SESSION['sql'] = array();
setcookie("cookies", "yes");
		
		
require_once COMPOSER_ROOT . 'vendor/autoload.php';

/*function __autoload($class){
	if(file_exists(SITE_ROOT . SITE_LIBRARY . 'class.' . $class . SITE_EXT)) {
		require(SITE_ROOT . SITE_LIBRARY . 'class.' . $class . SITE_EXT);
	} elseif(file_exists(SITE_ROOT . SITE_MODELS . $class . SITE_EXT)) {
		require(SITE_ROOT . SITE_MODELS . $class . '.php');
	} elseif(file_exists(SITE_ROOT . SITE_LIBRARY . 'stripe/' . $class . '.php')) {
		require(SITE_ROOT . SITE_LIBRARY . 'stripe/' . $class . '.php');
	} else {
		echo("{$class} is not defined.");
	}
}*/

