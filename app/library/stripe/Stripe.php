<?php

// Tested on PHP 5.2, 5.3

// This snippet (and some of the curl code) due to the Facebook SDK.
if (!function_exists('curl_init')) {
  throw new Exception('Stripe needs the CURL PHP extension.');
}
if (!function_exists('json_decode')) {
  throw new Exception('Stripe needs the JSON PHP extension.');
}
if (!function_exists('mb_detect_encoding')) {
  throw new Exception('Stripe needs the Multibyte String PHP extension.');
}

// Stripe singleton
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe.php');

// Utilities
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Util.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Util/Set.php');

// Errors
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Error.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/ApiError.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/ApiConnectionError.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/AuthenticationError.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/CardError.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/InvalidRequestError.php');

// Plumbing
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Object.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/ApiRequestor.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/ApiResource.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/SingletonApiResource.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/AttachedObject.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/List.php');

// Stripe API Resources
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Account.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Card.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Balance.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/BalanceTransaction.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Charge.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Customer.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Invoice.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/InvoiceItem.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Plan.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Token.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Coupon.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Event.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Transfer.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/Recipient.php');
require(SITE_ROOT . SITE_LIBRARY . 'stripe/Stripe/ApplicationFee.php');
